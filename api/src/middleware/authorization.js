import createError from 'http-errors';

import Auth from '../config/fusionauth.js';

export default function authorize(...permittedRoles) {
  return async (req, res, next) => {
    try {
      const authGetUserWithJWT = await Auth.retrieveUserUsingJWT(
        req.cookies._atk,
      );
      const roles = authGetUserWithJWT.response.user.registrations
        .map(reg => reg.roles)
        .flat();

      const hasPermittedRole = permittedRoles.find(pRole =>
        roles.includes(pRole),
      );

      if (req.cookies._rtk && hasPermittedRole) {
        next();
      } else {
        res.status(403).json({ message: 'Forbidden' });
      }
    } catch (err) {
      return next(createError(err.statusCode, err, { level: 'ERROR' }));
    }
  };
}
