import createError from 'http-errors';

import Auth from '../config/fusionauth.js';
import vars from '../config/vars.js';

/*
  Check tokens against FusionAuth for protected routes because
  if user was deleted from FusionAuth, they should not be able
  to access resources anymore
*/

export default async function refreshJWT(req, res, next) {
  if (req.cookies._rtk) {
    try {
      const faReq = await Auth.exchangeRefreshTokenForJWT({
        refreshToken: req.cookies._rtk,
      });

      const accessToken = faReq.response.token;

      req.cookies._atk = accessToken;

      const metaCookie = JSON.parse(req.cookies._meta);

      const cookieOptions = {
        httpOnly: true,
        secure: vars.server.env !== 'development',
      };

      res
        .cookie('_rtk', req.cookies._rtk, {
          ...cookieOptions,
          ...(metaCookie.remember && { maxAge: 1000 * 60 * 43200 }),
        })
        .cookie('_atk', accessToken, {
          ...cookieOptions,
          ...(metaCookie.remember && { maxAge: 1000 * 60 * 30 }),
        });

      return next();
    } catch (err) {
      if (Object.keys(err).length > 1) {
        if (
          err.statusCode === 400 &&
          Object.keys(err.exception.fieldErrors).includes('refreshToken')
        ) {
          if (
            err.exception.fieldErrors.refreshToken[0].code ===
            '[invalid]refreshToken'
          ) {
            return res.status(200).json({ isAuthenticated: false });
          }
        }
        return next(createError(err.statusCode, err, { level: 'ERROR' }));
      }
      return next(
        createError(err.statusCode, 'No error message', { level: 'ERROR' }),
      );
    }
  } else {
    next();
  }
}
