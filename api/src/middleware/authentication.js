export default function authenticate(req, res, next) {
  if (!req.cookies._rtk) {
    return res.status(401).end();
  }

  next();
}
