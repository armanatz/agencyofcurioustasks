import createError from 'http-errors';
import formidable from 'formidable';

import vars from '../config/vars.js';
import doSpaces from '../helpers/doSpaces.js';

import Season from '../queries/Season.js';
import Episode from '../queries/Episode.js';

const handleSingleImg = async (
  file,
  { maxFileSize, folderPath, mimeTypesAllowed = [] },
) => {
  return new Promise((resolve, reject) => {
    if (file.size > maxFileSize) {
      return reject(
        createError(
          400,
          `The image: ${file.name} is larger than ${
            maxFileSize / (1024 * 1024)
          }MB`,
          { level: 'INFO' },
        ),
      );
    }

    if (!mimeTypesAllowed.includes(file.type)) {
      return reject(
        createError(400, 'File type not allowed.', { level: 'INFO' }),
      );
    }

    const fileName = `${Date.now().toString()}${
      file.path.substring(file.path.lastIndexOf('/') + 1).split('_')[1]
    }`;

    const includeEnv = vars.server.env !== 'production';
    const destPath = `${includeEnv ? vars.server.env : ''}${
      includeEnv ? folderPath : folderPath.substr(1)
    }/${fileName}`;

    doSpaces
      .upload(file, destPath)
      .then(result => {
        resolve({
          size: file.size,
          originalName: file.name,
          type: file.type,
          location: !/^https?:\/\//i.test(result.Location)
            ? `https://${result.Location}`
            : result.Location,
          key: result.Key,
        });
      })
      .catch(err => {
        reject(err);
      });
  });
};

const handleMultipleImgs = async (
  files,
  { maxFileSize, folderPath, mimeTypesAllowed = [] },
) => {
  files.forEach(async file => {
    if (file.size > maxFileSize) {
      return createError(
        400,
        `The image: ${file.name} is larger than ${
          maxFileSize / (1024 * 1024)
        }MB`,
        { level: 'ERROR' },
      );
    }

    if (!mimeTypesAllowed.includes(file.type)) {
      return createError(400, 'File type not allowed.', { level: 'ERROR' });
    }
  });

  const uploads = files.map(file => {
    const fileName = `${Date.now().toString()}${
      file.path.substring(file.path.lastIndexOf('/') + 1).split('_')[1]
    }`;

    const includeEnv = vars.server.env !== 'production';
    const destPath = `${includeEnv ? vars.server.env : ''}${
      includeEnv ? folderPath : folderPath.substr(1)
    }/${fileName}`;

    return doSpaces.upload(file, destPath);
  });

  try {
    const s3Results = await Promise.all(uploads);
    return files.map((file, i) => {
      return {
        size: file.size,
        originalName: file.name,
        type: file.type,
        location: !/^https?:\/\//i.test(s3Results[i].Location)
          ? `https://${s3Results[i].Location}`
          : s3Results[i].Location,
        key: s3Results[i].Key,
      };
    });
  } catch (err) {
    return createError(500, err, { level: 'ERROR' });
  }
};

export default function uploadHandler(resource, multiples = false) {
  return (req, res, next) => {
    const form = formidable({
      multiples,
      keepExtensions: true,
    });

    form.parse(req, async (err, fields, files) => {
      try {
        if (err) {
          return next(createError(500, err, { level: 'ERROR' }));
        }

        if (resource === 'season') {
          if (fields.seasonNumber) {
            const doesSeasonExist = await Season.findByNumber(
              fields.seasonNumber,
            );

            if (doesSeasonExist.length > 0) {
              return next(
                createError(
                  409,
                  `Season number ${doesSeasonExist[0].seasonNumber} already exists.`,
                  { level: 'INFO' },
                ),
              );
            }
          }

          if (Object.keys(files).length > 0) {
            const opts = {
              maxFileSize: 1024 * 1024 * 10,
              folderPath: '/products/seasons',
              mimeTypesAllowed: ['image/jpeg', 'image/jpg', 'image/png'],
            };

            const results = await handleSingleImg(files.coverImage, opts);

            if (!results.size) {
              return next(results);
            }

            req.file = results;
          }

          req.body = fields;
          return next();
        }

        if (resource === 'episode') {
          if (fields.episodeNumber && fields.seasonId) {
            const doesEpisodeExist = await Episode.findByNumber(
              fields.episodeNumber,
              fields.seasonId,
            );

            if (doesEpisodeExist) {
              return next(
                createError(
                  409,
                  `Episode number ${doesEpisodeExist.episodeNumber} in season ${doesEpisodeExist.seasonNumber} already exists.`,
                  { level: 'INFO' },
                ),
              );
            }
          }

          if (Object.keys(files).length > 0) {
            const opts = {
              maxFileSize: 1024 * 1024 * 10,
              folderPath: '/products/episodes',
              mimeTypesAllowed: ['image/jpeg', 'image/jpg', 'image/png'],
            };

            const imagesToUpload = {};

            if (files.images) {
              if (Array.isArray(files.images)) {
                const otherImagesArr = [];

                files.images.forEach(el => {
                  const fileNameSplit = el.name.split('_');

                  if (fileNameSplit[0] === 'coverImg') {
                    imagesToUpload.coverImage = el;
                  } else {
                    otherImagesArr.push(el);
                  }
                });

                imagesToUpload.otherImages = otherImagesArr;
              } else {
                const fileNameSplit = files.images.name.split('_');
                if (fileNameSplit[0] === 'coverImg') {
                  imagesToUpload.coverImage = files.images;
                } else {
                  imagesToUpload.otherImages = files.images;
                }
              }
            } else {
              imagesToUpload.coverImage = files.coverImage;
              imagesToUpload.otherImages = files.otherImages;
            }

            let coverImage = undefined;

            if (imagesToUpload.coverImage) {
              coverImage = await handleSingleImg(
                imagesToUpload.coverImage,
                opts,
              );

              if (!coverImage.size) {
                return next(coverImage);
              }
            }

            let otherImages = undefined;

            if (imagesToUpload.otherImages) {
              if (Array.isArray(imagesToUpload.otherImages)) {
                otherImages = await handleMultipleImgs(
                  imagesToUpload.otherImages,
                  opts,
                );

                if (!Array.isArray(otherImages)) {
                  return next(otherImages);
                }
              } else {
                otherImages = await handleSingleImg(
                  imagesToUpload.otherImages,
                  opts,
                );

                if (!otherImages.size) {
                  return next(otherImages);
                }

                otherImages = [otherImages];
              }
            }

            req.files = {
              ...(coverImage && { coverImage }),
              ...(otherImages && { otherImages }),
            };
          }

          req.body = fields;
          return next();
        }
      } catch (err) {
        return next(err);
      }
    });
  };
}
