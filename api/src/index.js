import express from 'express';
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import dayjs from 'dayjs';
import knex from 'knex';

import vars from './config/vars.js';
import knexConfig from './config/knexfile.js';

import addressRoutes from './routes/Address.js';
import authRoutes from './routes/Auth.js';
import episodeRoutes from './routes/Episode.js';
import familyRoutes from './routes/Family.js';
import gameRoutes from './routes/Game.js';
import giftCardRoutes from './routes/GiftCard.js';
import orderRoutes from './routes/Order.js';
import partnerRoutes from './routes/Partner.js';
// import paymentRoutes from './routes/Payment.js';
import productRoutes from './routes/Product.js';
import seasonRoutes from './routes/Season.js';
import settingRoutes from './routes/Setting.js';
import stripeRoutes from './routes/Stripe.js';
import userRoutes from './routes/User.js';

const db = knex({
  ...knexConfig,
  migrations: {
    tableName: 'knex_migrations',
    directory: './src/migrations',
  },
});

const { apiPathPrefix, apiVersion, port, logLevel } = vars.server;
const apiPath = `${apiPathPrefix}/${apiVersion}`;

const logLevelsEnabled = [];

switch (logLevel) {
  case 'debug':
    logLevelsEnabled.push('INFO', 'WARNING', 'ERROR', 'FATAL');
    break;
  case 'warning':
    logLevelsEnabled.push('WARNING', 'ERROR', 'FATAL');
    break;
  case 'error':
    logLevelsEnabled.push('ERROR', 'FATAL');
    break;
  default:
    logLevelsEnabled.push('FATAL');
    break;
}

// Set up Express
const app = express()
  .use(express.urlencoded({ extended: false }))
  .use(cookieParser())
  .set('json spaces', 2);

app.use((req, res, next) => {
  if (req.originalUrl === '/api/v1/stripe/webhook') {
    next();
  } else {
    express.json()(req, res, next);
  }
});

// For running behind a proxy
app.set('trust proxy', true);

app.use(helmet());

// Route namespaces
app.use(`${apiPath}/addresses`, addressRoutes);
app.use(`${apiPath}/auth`, authRoutes);
app.use(`${apiPath}/episodes`, episodeRoutes);
app.use(`${apiPath}/families`, familyRoutes);
app.use(`${apiPath}/games`, gameRoutes);
app.use(`${apiPath}/gift_cards`, giftCardRoutes);
app.use(`${apiPath}/orders`, orderRoutes);
app.use(`${apiPath}/partners`, partnerRoutes);
// app.use(`${apiPath}/payments`, paymentRoutes);
app.use(`${apiPath}/products`, productRoutes);
app.use(`${apiPath}/seasons`, seasonRoutes);
app.use(`${apiPath}/settings`, settingRoutes);
app.use(`${apiPath}/stripe`, stripeRoutes);
app.use(`${apiPath}/users`, userRoutes);

// Error handling
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.log(err);
  if (logLevelsEnabled.includes(err.level)) {
    console.log(
      `[${dayjs().format('YYYY-MM-DD HH:mm:ss A ZZ')}]: ${err.level} - "${
        req.method
      } ${req.url}" | ${err.message}`,
    );
  }

  if (err.statusCode === 500) {
    return res
      .status(500)
      .json({ message: 'Something went wrong with the server.' });
  }
  return res.status(err.statusCode).json({ message: err.message });
});

app.use((_, res) => {
  const error = new Error('Not Found');

  return res.status(404).json({
    message: error.message,
  });
});

process.on('SIGINT', () => {
  console.log('SIGINT signal received. Cleaning up DB connection...');
  db.destroy();
  process.exit();
});

process.on('SIGTERM', () => {
  console.log('SIGTERM signal received. Cleaning up DB connection...');
  db.destroy();
  process.exit();
});

// Init server
app.set('port', port);
app.listen(port, () =>
  console.info(`Server has (re)started and is running on port ${port}`),
);
