import express from 'express';

import controller from '../controllers/User.js';
import authenticate from '../middleware/authentication.js';
import refreshJWT from '../middleware/refreshJWT.js';
import authorize from '../middleware/authorization.js';

const router = express.Router();

// Unprotected routes
router.post('/', controller.createUser);
router.post('/contact_request', controller.contactRequest);
router.post('/mailchimp', controller.addUserToMailchimp);
router.post('/verify/email', controller.verifyUsersEmail);
router.post('/verify/email/resend', controller.resendVerificationEmail);

// Protected routes
router.use(authenticate);
router.use(refreshJWT);
router.get('/', authorize('Superuser', 'Admin'), controller.getAllUsers);
router.get('/current', controller.getCurrentUser);
router.get('/admins', authorize('Superuser'), controller.getAllAdmins);
router.get(
  '/admins/roles',
  authorize('Superuser'),
  controller.getAllAdminRoles,
);
router.get(
  '/wallet',
  authorize('Adult'),
  controller.retrieveCurrentUsersWalletBalance,
);
router.get('/:userId', authorize('Adult'), controller.getUser);
router.get(
  '/referrals/count/:code',
  authorize('Adult'),
  controller.getReferralCount,
);
router.get(
  '/referrals/code/current',
  authorize('Adult'),
  controller.getCurrentUsersReferralCode,
);
router.get(
  '/referrals/code/:userId',
  authorize('Adult'),
  controller.getUsersReferralCode,
);

router.post('/admins', authorize('Superuser'), controller.addOrUpdateAdmin);

router.patch('/current', authorize('Adult'), controller.updateCurrentUser);
router.patch('/:userId', authorize('Adult'), controller.updateUser);
router.patch(
  '/admins/:email',
  authorize('Superuser'),
  controller.addOrUpdateAdmin,
);

router.delete('/:userId', authorize('Adult'), controller.deleteUser);
router.delete('/admins/:id', authorize('Superuser'), controller.deleteAdmin);

export default router;
