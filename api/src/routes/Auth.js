import express from 'express';

import controller from '../controllers/Auth.js';
import authenticate from '../middleware/authentication.js';
import refreshJWT from '../middleware/refreshJWT.js';

const router = express.Router();

// Unprotected routes
router.post('/login', controller.loginUser);
router.post('/password/forgot', controller.passwordForgot);
router.post('/password/reset', controller.passwordReset);

router.use(refreshJWT);
router.get('/check', controller.getAuthStatus);

// Protected routes
router.use(authenticate);
router.get('/logout', controller.logoutUser);

export default router;
