import express from 'express';

import controller from '../controllers/GiftCard.js';
import authenticate from '../middleware/authentication.js';
import refreshJWT from '../middleware/refreshJWT.js';
import authorize from '../middleware/authorization.js';

const router = express.Router();

// Unprotected routes

// Protected routes
router.use(authenticate);
router.use(refreshJWT);

router.get(
  '/',
  authorize('Superuser', 'Admin', 'Support'),
  controller.retrieveAllGiftCards,
);

router.post(
  '/',
  authorize('Superuser', 'Admin', 'Support'),
  controller.createGiftCard,
);
router.post('/redeem', authorize('Adult'), controller.redeemGiftCard);

router.delete(
  '/:id',
  authorize('Superuser', 'Admin', 'Support'),
  controller.deleteGiftCard,
);

export default router;
