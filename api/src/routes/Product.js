import express from 'express';

import controller from '../controllers/Product.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';

const router = express.Router();

// Unprotected routes
router.get('/activation_codes', controller.getAllActivationCodes);
router.get(
  '/activation_codes/episode/:episodeId',
  controller.getEpisodeActivationCode,
);
router.get('/owned/:userId', controller.getOwnedProducts);

// Protected routes
router.use(authenticate);
router.use(refreshJWT);

router.post(
  '/activation_codes',
  authorize('Superuser', 'Admin', 'Editor'),
  controller.createActivationCode,
);
router.post('/assign', authorize('Adult'), controller.assignProduct);

router.delete(
  '/activation_codes/:id',
  authorize('Superuser', 'Admin', 'Editor'),
  controller.deleteActivationCode,
);

export default router;
