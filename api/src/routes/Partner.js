import express from 'express';

import controller from '../controllers/Partner.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';

const router = express.Router();

// Unprotected routes

// Protected routes
router.use(authenticate);
router.use(refreshJWT);
router.get(
  '/',
  authorize('Superuser', 'Admin', 'Marketer'),
  controller.getPartners,
);
router.get(
  '/:id',
  authorize('Superuser', 'Admin', 'Marketer'),
  controller.getPartnerById,
);
router.post(
  '/',
  authorize('Superuser', 'Admin', 'Marketer'),
  controller.createPartner,
);
router.patch(
  '/:id',
  authorize('Superuser', 'Admin', 'Marketer'),
  controller.editPartner,
);
router.delete(
  '/:id',
  authorize('Superuser', 'Admin', 'Marketer'),
  controller.deletePartner,
);

export default router;
