import express from 'express';

import controller from '../controllers/Season.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';
import uploadHandler from '../middleware/uploadHandler.js';

const router = express.Router();

// Unprotected routes
router.get('/', controller.getSeasons);
router.get('/id/:id', controller.getSeasonById);
router.get('/number/:number', controller.getSeasonByNumber);

// Protected routes
router.use(authenticate);
router.use(refreshJWT);

router.post(
  '/',
  authorize('Superuser', 'Admin', 'Editor'),
  uploadHandler('season'),
  controller.createSeason,
);

router.patch(
  '/:id',
  authorize('Superuser', 'Admin', 'Editor'),
  uploadHandler('season'),
  controller.editSeason,
);

router.delete(
  '/:id',
  authorize('Superuser', 'Admin', 'Editor'),
  controller.deleteSeason,
);

export default router;
