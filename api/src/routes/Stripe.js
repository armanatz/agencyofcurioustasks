import express from 'express';

import controller from '../controllers/Stripe.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';

const router = express.Router();

// Unprotected routes
router.get('/price/:id', controller.retrieveProductPrice);

router.post(
  '/webhook',
  express.raw({ type: 'application/json' }),
  controller.webhookListerner,
);

// Protected routes
router.use(authenticate);
router.use(refreshJWT);
router.get(
  '/customer/current',
  authorize('Adult'),
  controller.getStripeCustomer,
);
router.get('/products', authorize('Adult'), controller.getStripeProducts);
router.get(
  '/customer/payment_methods',
  authorize('Adult'),
  controller.retrieveStripeCustomerPaymentMethods,
);
router.get(
  '/subscriptions',
  authorize('Adult'),
  controller.retrieveAllSubscriptions,
);
router.get(
  '/subscriptions/:id',
  authorize('Adult'),
  controller.retrieveStripeSubscription,
);

router.post('/customer', authorize('Adult'), controller.createStripeCustomer);
router.post(
  '/payment_intent',
  authorize('Adult'),
  controller.createStripePaymentIntent,
);
router.post(
  '/setup_intent',
  authorize('Adult'),
  controller.createStripeSetupIntent,
);
router.post(
  '/subscription',
  authorize('Adult'),
  controller.createStripeSubscription,
);
router.post(
  '/discount/check',
  authorize('Adult'),
  controller.checkDiscountCode,
);

router.delete(
  '/customer/payment_methods/:id',
  authorize('Adult'),
  controller.deleteCustomerPaymentMethod,
);
router.delete(
  '/subscriptions/:id',
  authorize('Adult'),
  controller.cancelSubscription,
);

export default router;
