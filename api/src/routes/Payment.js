import express from 'express';

// import controller from '../controllers/Payment.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';

const router = express.Router();

// Unprotected routes

// Protected routes
// router.use(authenticate);
// router.use(refreshJWT);

export default router;
