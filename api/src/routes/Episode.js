import express from 'express';

import controller from '../controllers/Episode.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';
import uploadHandler from '../middleware/uploadHandler.js';

const router = express.Router();

// Unprotected routes
router.get('/', controller.getEpisodes);
router.get('/id/:id', controller.getEpisodeById);
router.get('/season_id/:seasonId', controller.getEpisodesBySeasonId);
router.get(
  '/season_number/:seasonNumber',
  controller.getEpisodesBySeasonNumber,
);

// Protected routes
router.use(authenticate);
router.use(refreshJWT);

router.post(
  '/',
  authorize('Superuser', 'Admin', 'Editor'),
  uploadHandler('episode', true),
  controller.createEpisode,
);

router.patch(
  '/:id',
  authorize('Superuser', 'Admin', 'Editor'),
  uploadHandler('episode', true),
  controller.editEpisode,
);

router.delete(
  '/:id',
  authorize('Superuser', 'Admin', 'Editor'),
  controller.deleteEpisode,
);

export default router;
