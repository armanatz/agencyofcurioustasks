import express from 'express';

import controller from '../controllers/Order.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';

const router = express.Router();

// Unprotected routes

// Protected routes
router.use(authenticate);
router.use(refreshJWT);
router.get(
  '/',
  authorize('Superuser', 'Admin', 'Support', 'Fulfillment'),
  controller.getOrders,
);
router.get(
  '/status_codes',
  authorize('Superuser', 'Admin', 'Support', 'Fulfillment'),
  controller.getStatusCodes,
);
router.get(
  '/export',
  authorize('Superuser', 'Admin', 'Support', 'Fulfillment'),
  controller.exportOrders,
);

router.post('/', authorize('Adult'), controller.createOrder);

router.patch(
  '/:id',
  authorize('Superuser', 'Admin', 'Support', 'Fulfillment'),
  controller.updateOrderItem,
);

export default router;
