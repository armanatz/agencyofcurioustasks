import express from 'express';

import controller from '../controllers/Setting.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';

const router = express.Router();

// Unprotected routes

// Protected routes
router.use(authenticate);
router.use(refreshJWT);

router.get('/', authorize('Adult'), controller.retrieveSettings);
router.get('/name/:name', authorize('Adult'), controller.retrieveSetting);
router.get('/id/:id', authorize('Adult'), controller.retrieveSetting);

router.post('/', authorize('Superuser', 'Admin'), controller.createSetting);

router.patch('/', authorize('Superuser', 'Admin'), controller.updateSetting);

router.delete(
  '/:id',
  authorize('Superuser', 'Admin'),
  controller.deleteSetting,
);

export default router;
