import express from 'express';

import controller from '../controllers/Address.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';

const router = express.Router();

// Unprotected routes

// Protected routes
router.use(authenticate);
router.use(refreshJWT);
router.get('/', authorize('Adult'), controller.getCurrentUsersAddress);
router.get(
  '/:userId',
  authorize('Superuser', 'Admin'),
  controller.getUsersAddress,
);
router.post('/:userId', authorize('Adult'), controller.createAddress);
router.patch('/:userId', authorize('Adult'), controller.updateAddress);

export default router;
