import express from 'express';

import controller from '../controllers/Game.js';
import authenticate from '../middleware/authentication.js';
import authorize from '../middleware/authorization.js';
import refreshJWT from '../middleware/refreshJWT.js';

const router = express.Router();

// Unprotected routes

// Protected routes
router.use(authenticate);
router.use(refreshJWT);

router.get(
  '/activated/:userId',
  authorize('Child'),
  controller.getAllEpisodesActivated,
);
router.get(
  '/activated/:userId/:episodeId',
  authorize('Child'),
  controller.getEpisodeActivated,
);
router.get(
  '/load/:userId/:episodeId',
  authorize('Child'),
  controller.loadEpisodeProgress,
);

router.post(
  '/activate/:episodeId',
  authorize('Child'),
  controller.activateEpisode,
);
router.post(
  '/save/:episodeId',
  authorize('Child'),
  controller.saveEpisodeProgress,
);
router.post(
  '/reset/:username',
  authorize('Superuser', 'Admin', 'Support'),
  controller.resetUsersProgress,
);

export default router;
