import knex from 'knex';
import * as referralCodes from 'referral-codes';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const create = async userId => {
  try {
    let referralCode = referralCodes.generate({
      length: 7,
      charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
    });

    let result = await db
      .table('referralCodes')
      .where({ code: referralCode[0] });

    while (result.length > 0) {
      referralCode = referralCodes.generate({
        length: 7,
        charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      });

      result = await db.table('referralCodes').where({ code: referralCode[0] });
    }

    return db.table('referralCodes').insert({ userId, code: referralCode[0] });
  } catch (err) {
    throw new Error(err);
  }
};

const capture = (userId, code) => {
  return db.table('referrals').insert({ userId, codeUsed: code });
};

const check = userId => {
  return db.table('referrals').where({ userId });
};

const update = (id, placedOrder) => {
  return db
    .table('referrals')
    .update({ placedOrder, updatedAt: db.fn.now(3) })
    .where({ id });
};

const findByUUID = userId => {
  return db.table('referralCodes').where({ userId });
};

const findByCode = code => {
  return db.table('referralCodes').where({ code });
};

const count = async code => {
  const result = await db
    .table('referrals')
    .count('codeUsed')
    .where({ codeUsed: code, placedOrder: true });
  return result[0].count;
};

const queries = {
  create,
  capture,
  check,
  update,
  findByUUID,
  findByCode,
  count,
};

export default queries;
