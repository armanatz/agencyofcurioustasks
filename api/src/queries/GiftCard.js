import knex from 'knex';
import * as referralCodes from 'referral-codes';

import knexConfig from '../config/knexfile.js';
import Auth from '../config/fusionauth.js';

const db = knex(knexConfig);

const create = async value => {
  let giftCardCode = referralCodes.generate({
    length: 16,
    charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
  });

  let result = await db.table('giftCards').where({ code: giftCardCode[0] });

  while (result.length > 0) {
    giftCardCode = referralCodes.generate({
      length: 16,
      charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
    });

    result = await db.table('giftCards').where({ code: giftCardCode[0] });
  }

  return db.table('giftCards').insert({ value, code: giftCardCode[0] });
};

const update = (id, data) => {
  return db
    .table('giftCards')
    .where({ id })
    .update({ ...data, updatedAt: db.fn.now(3) });
};

const del = async id => {
  const giftCard = await db.select().table('giftCards').where({ id });

  if (!giftCard[0].redeemed) {
    return db.table('giftCards').where({ id }).del();
  }

  return db
    .table('giftCards')
    .update({ hidden: true, updatedAt: db.fn.now(3) })
    .where({ id });
};

const all = async redemptionStatus => {
  const query = await db
    .select()
    .from('giftCards')
    .where({
      hidden: false,
      ...(redemptionStatus && { redeemed: redemptionStatus }),
    });
  const result = await Promise.all(
    query.map(async card => {
      if (card.redeemedBy !== null) {
        const authReq = await Auth.retrieveUser(card.redeemedBy);
        return {
          ...card,
          redeemedBy: authReq.response.user,
        };
      }
      return { ...card };
    }),
  );

  return result;
};

const findByCode = code => {
  return db
    .select('id', 'code', 'value', 'redeemed', 'hidden')
    .from('giftCards')
    .where({ code });
};

const queries = {
  create,
  update,
  del,
  all,
  findByCode,
};

export default queries;
