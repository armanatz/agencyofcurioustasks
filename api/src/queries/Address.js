import knex from 'knex';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const create = (id, data) => {
  return db.table('addresses').insert({ userId: id, ...data });
};

const update = (id, data) => {
  return db
    .table('addresses')
    .where({ userId: id })
    .returning(['city', 'country', 'id', 'line1', 'line2', 'postCode', 'state'])
    .update({
      ...data,
      updatedAt: db.fn.now(3),
    });
};

const findByUUID = (id, full = true) => {
  if (full) {
    return db.select().table('addresses').where({ userId: id });
  }
  return db
    .select(['city', 'country', 'id', 'line1', 'line2', 'postCode', 'state'])
    .table('addresses')
    .where({ userId: id });
};

const queries = {
  create,
  update,
  findByUUID,
};

export default queries;
