import knex from 'knex';
import dayjs from 'dayjs';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const all = async (filters = {}) => {
  let partners = await db
    .select(
      'id',
      'name',
      'email',
      'category',
      'promoId',
      'useActivationCode',
      'blockRegistration',
      'enabled',
    )
    .table('partners')
    .where(function () {
      this.where({
        ...(filters.email && { email: filters.email }),
      }).andWhereRaw(
        'LOWER(category) LIKE ?',
        `%${filters.category ? filters.category.toLowerCase() : ''}%`,
      );
    });

  if (filters.name) {
    partners = await db
      .select(
        'id',
        'name',
        'email',
        'category',
        'promoId',
        'useActivationCode',
        'blockRegistration',
        'enabled',
      )
      .table('partners')
      .where(function () {
        this.where({
          ...(filters.email && { email: filters.email }),
        })
          .andWhereRaw('LOWER(name) LIKE ?', `%${filters.name.toLowerCase()}%`)
          .andWhereRaw(
            'LOWER(category) LIKE ?',
            `%${filters.category.toLowerCase()}%`,
          );
      });
  }

  const result = await Promise.all(
    partners.map(async partner => {
      if (partner.promoId !== null) {
        const count = await db
          .table('orders')
          .count('promoId')
          .where({ promoId: partner.promoId })
          .whereBetween(
            'createdAt',
            filters.dateRange
              ? filters.dateRange.split(',')
              : [
                  dayjs().startOf('month').format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
                  dayjs().endOf('month').format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
                ],
          );

        return {
          ...partner,
          registrationsCount: count[0].count,
        };
      }

      const activationCode = await db
        .select('code')
        .table('episodeActivationCodes')
        .where({ partnerId: partner.id });

      let count = [{ count: 0 }];

      if (activationCode.length > 0) {
        count = await db
          .table('episodesActivated')
          .count('codeUsed')
          .where({ codeUsed: activationCode[0].code })
          .whereBetween(
            'createdAt',
            filters.dateRange
              ? filters.dateRange.split(',')
              : [
                  dayjs().startOf('month').format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
                  dayjs().endOf('month').format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
                ],
          );
      }

      return {
        ...partner,
        registrationsCount: count[0].count,
      };
    }),
  );

  return result;
};

const findById = id => {
  return db
    .select(
      'id',
      'name',
      'email',
      'category',
      'promoId',
      'useActivationCode',
      'blockRegistration',
      'enabled',
    )
    .table('partners')
    .where({ id });
};

const findByEmail = email => {
  return db
    .select(
      'id',
      'name',
      'email',
      'category',
      'promoId',
      'useActivationCode',
      'blockRegistration',
      'enabled',
    )
    .table('partners')
    .where({ email });
};

const create = data => {
  return db
    .table('partners')
    .insert(data)
    .returning([
      'id',
      'name',
      'email',
      'category',
      'promoId',
      'useActivationCode',
      'blockRegistration',
      'enabled',
    ]);
};

const update = (id, data) => {
  return db
    .table('partners')
    .where({ id })
    .update({ ...data, updatedAt: db.fn.now(3) })
    .returning([
      'id',
      'name',
      'email',
      'category',
      'promoId',
      'useActivationCode',
      'blockRegistration',
      'enabled',
    ]);
};

const del = id => {
  return db.table('partners').where({ id }).del();
};

const queries = {
  all,
  findById,
  findByEmail,
  create,
  update,
  del,
};

export default queries;
