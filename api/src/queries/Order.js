import knex from 'knex';
import dayjs from 'dayjs';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const create = async ({
  parentId,
  childId,
  episodeIds,
  orderCode,
  promoUsed = false,
  promoId = null,
}) => {
  const order = await db
    .table('orders')
    .insert({
      parentId,
      childId,
      orderCode,
      promoUsed,
      promoId,
    })
    .returning(['id', 'orderCode', 'createdAt']);

  const orderProcessingDays = await db
    .select('value')
    .table('globalSettings')
    .where({ name: 'orderProcessingDays' });

  const calcMaxShipByDate = (duration, units, date = null) => {
    if (date === null) {
      return dayjs().add(duration, units).endOf('day');
    }
    return dayjs(date).add(duration, units).endOf('day');
  };

  const itemsToInsert = [];

  episodeIds.forEach((id, i) => {
    let maxShipByDate;

    let statusCode = 0;

    switch (i) {
      case 0:
        maxShipByDate = calcMaxShipByDate(
          orderProcessingDays[0].value,
          'days',
          null,
        );
        statusCode = 1;
        break;
      case 1:
        maxShipByDate = calcMaxShipByDate(1, 'months', dayjs());
        break;
      default:
        maxShipByDate = calcMaxShipByDate(
          1,
          'months',
          dayjs(itemsToInsert[i - 1].maxShipByDate, 'YYYY-MM-DD'),
        );
        break;
    }

    if (maxShipByDate.day() === 0) {
      maxShipByDate = maxShipByDate.add(1, 'day').endOf('day');
    } else if (maxShipByDate.day() === 6) {
      maxShipByDate = maxShipByDate.add(2, 'day').endOf('day');
    }

    itemsToInsert.push({
      orderId: order[0].id,
      episodeId: id,
      maxShipByDate: maxShipByDate.toISOString(),
      statusCode,
    });
  });

  let orderItems = await db
    .table('orderItems')
    .insert(itemsToInsert)
    .returning(['id', 'orderId', 'episodeId']);

  orderItems = await Promise.all(
    orderItems.map(async item => {
      const episode = await db
        .select('id', 'title')
        .table('episodes')
        .where({ id: item.episodeId });
      return {
        id: item.id,
        episodeId: episode[0].id,
        episodeTitle: episode[0].title,
      };
    }),
  );

  return {
    id: order[0].id,
    code: order[0].orderCode,
    date: order[0].createdAt,
    items: orderItems,
  };
};

const all = async filters => {
  return await db
    .select(
      'oi.id',
      'oi.episodeId',
      'e.title AS episodeTitle',
      'o.parentId',
      'o.childId',
      'o.orderCode',
      'oi.notes',
      'oi.statusCode',
      'sc.name AS statusName',
      'oi.maxShipByDate',
      'oi.courier',
      'oi.trackingUrl',
      'oi.trackingCode',
      'oi.lastUpdateBy',
      'oi.updatedAt AS lastUpdateDate',
      'o.createdAt AS orderDate',
      db.raw(
        `ARRAY_AGG(
          JSON_BUILD_OBJECT(
            'line1', a.line1,
            'line2', a.line2,
            'city', a.city,
            'country', a.country,
            'state', a.state,
            'postCode', a."postCode"
          )
        ) AS address`,
      ),
    )
    .from('orderItems AS oi')
    .join('statusCodes AS sc', 'oi.statusCode', 'sc.code')
    .join('orders AS o', 'oi.orderId', 'o.id')
    .join('episodes AS e', 'oi.episodeId', 'e.id')
    .join('addresses AS a', 'o.parentId', 'a.userId')
    .groupBy(
      'oi.id',
      'e.title',
      'o.parentId',
      'o.childId',
      'o.orderCode',
      'sc.name',
      'o.createdAt',
    )
    .where({
      ...(filters.orderId && { 'o.orderCode': filters.orderId }),
      ...(filters.parentId && { 'o.parentId': filters.parentId }),
      ...(filters.childId && { 'o.childId': filters.childId }),
      ...(filters.status && { 'sc.code': filters.status }),
    })
    .whereBetween(
      'oi.maxShipByDate',
      filters.maxShipByDate
        ? filters.maxShipByDate.split(',')
        : [
            dayjs().subtract(4, 'days').format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
            dayjs().add(2, 'weeks').format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
          ],
    )
    .andWhereBetween(
      'o.createdAt',
      filters.orderDate
        ? [
            dayjs(filters.orderDate.split(',')[0])
              .startOf('day')
              .format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
            dayjs(filters.orderDate.split(',')[1])
              .endOf('day')
              .format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
          ]
        : [
            dayjs()
              .subtract(2, 'weeks')
              .startOf('day')
              .format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
            dayjs().endOf('day').format('YYYY-MM-DD HH:mm:ss.SSSZZ'),
          ],
    )
    .orderBy('oi.maxShipByDate');
};

const findByOrderId = id => {
  return db.select().table('orders').where({ id });
};

const findByParentId = parentId => {
  return db.select().table('orders').where({ parentId });
};

const findByOrderCode = orderCode => {
  return db.select().table('orders').where({ orderCode });
};

const findItemById = id => {
  return db.select().table('orderItems').where({ id });
};

const findAllItemsByOrderId = id => {
  return db.select().table('orderItems').where({ orderId: id });
};

const statusCodes = () => {
  return db.select().table('statusCodes');
};

const updateItem = (id, data) => {
  return db
    .table('orderItems')
    .update({ ...data, updatedAt: db.fn.now(3) })
    .where({ id })
    .returning([
      'id',
      'orderId',
      'episodeId',
      'notes',
      'statusCode',
      'maxShipByDate',
      'courier',
      'trackingUrl',
      'trackingCode',
      'shippingEmailSent',
      'deliveredEmailSent',
      'lastUpdateBy',
      'createdAt',
      'updatedAt',
    ]);
};

const queries = {
  create,
  all,
  findByOrderId,
  findByParentId,
  findByOrderCode,
  findItemById,
  findAllItemsByOrderId,
  statusCodes,
  updateItem,
};

export default queries;
