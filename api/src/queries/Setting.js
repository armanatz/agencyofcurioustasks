import knex from 'knex';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const create = data => {
  return db
    .table('globalSettings')
    .insert(data)
    .returning(['id', 'name', 'value']);
};

const update = async data => {
  const promises = await Promise.all(
    data.map(async el => {
      return await db
        .table('globalSettings')
        .where({ name: el.name })
        .update({ value: el.value, updatedAt: db.fn.now(3) })
        .returning(['id', 'name', 'value']);
    }),
  );

  return promises.flat(1);
};

const all = () => {
  return db.select('id', 'name', 'value').table('globalSettings');
};

const findById = id => {
  return db.select('id', 'name', 'value').table('globalSettings').where({ id });
};

const findByName = name => {
  return db
    .select('id', 'name', 'value')
    .table('globalSettings')
    .where({ name });
};

const del = id => {
  return db.select().table('globalSettings').where({ id }).del();
};

const queries = {
  create,
  update,
  all,
  findById,
  findByName,
  del,
};

export default queries;
