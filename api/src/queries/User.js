import knex from 'knex';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const create = data => {
  return db.table('users').insert(data);
};

const createAdmin = data => {
  return db.table('admins').insert(data);
};

const updateAdmin = (email, data) => {
  return db
    .table('admins')
    .update({ ...data, updatedAt: db.fn.now(3) })
    .where({ email });
};

const del = id => {
  return db.table('users').where('id', id).del();
};

const deleteAdmin = id => {
  return db.table('admins').where({ id }).del();
};

const getAll = filters => {
  if (filters) {
    return db.select().table('users').where(filters);
  }
  return db.select().table('users');
};

const findById = id => {
  return db.select().table('users').where({ id });
};

const findByLoginId = loginId => {
  return db.select().table('users').where({ loginId });
};

const findAdmins = () => {
  return db.select().table('admins');
};

const queries = {
  create,
  createAdmin,
  updateAdmin,
  del,
  deleteAdmin,
  getAll,
  findById,
  findByLoginId,
  findAdmins,
};

export default queries;
