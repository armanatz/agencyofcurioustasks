import knex from 'knex';

import knexConfig from '../config/knexfile.js';
import doSpaces from '../helpers/doSpaces.js';

const db = knex(knexConfig);

const create = async data => {
  const dataToInsert = { ...data };

  delete dataToInsert.images;

  const episode = await db
    .table('episodes')
    .insert(dataToInsert)
    .returning([
      'id',
      'seasonId',
      'stripeProductId',
      'title',
      'description',
      'episodeNumber',
      'boxContents',
      'onlineGames',
      'active',
      'comingSoon',
    ]);

  const activationCodes = await db
    .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
    .table('episodeActivationCodes')
    .where({ episodeId: episode[0].id });

  const coverImageToInsert = {
    episodeId: episode[0].id,
    fileName: data.images.coverImage.key.substring(
      data.images.coverImage.key.lastIndexOf('/') + 1,
    ),
    imageUrl: data.images.coverImage.location,
    coverPhoto: true,
  };

  const otherImagesToInsert = data.images.otherImages.map(image => ({
    episodeId: episode[0].id,
    fileName: image.key.substring(image.key.lastIndexOf('/') + 1),
    imageUrl: image.location,
  }));

  const images = await db
    .table('episodeImages')
    .insert([coverImageToInsert, ...otherImagesToInsert])
    .returning(['id', 'fileName', 'imageUrl', 'coverPhoto']);

  return {
    ...episode[0],
    images,
    activationCodes,
  };
};

const update = async (id, data) => {
  const dataToUpdate = { ...data };

  if (data.images) {
    delete dataToUpdate.images;

    let imagesToInsert = [];

    if (data.images.coverImage) {
      imagesToInsert.push({
        episodeId: id,
        fileName: data.images.coverImage.key.substring(
          data.images.coverImage.key.lastIndexOf('/') + 1,
        ),
        imageUrl: data.images.coverImage.location,
        coverPhoto: true,
      });
    }

    if (data.images.otherImages) {
      data.images.otherImages.forEach(image =>
        imagesToInsert.push({
          episodeId: id,
          fileName: image.key.substring(image.key.lastIndexOf('/') + 1),
          imageUrl: image.location,
        }),
      );
    }

    await db.table('episodeImages').insert(imagesToInsert);
  }

  if (data.filesToDelete) {
    delete dataToUpdate.filesToDelete;

    const fileNames = [];

    if (Array.isArray(data.filesToDelete)) {
      data.filesToDelete.forEach(file => {
        fileNames.push(file.fileName);
      });
    } else {
      fileNames.push(data.filesToDelete.fileName);
    }

    await doSpaces.deleteFiles(fileNames, '/products/episodes');
    await db
      .table('episodeImages')
      .where({ episodeId: id })
      .whereIn('fileName', fileNames)
      .del();
  }

  const episode = await db
    .table('episodes')
    .where({ id })
    .update({ ...dataToUpdate, updatedAt: db.fn.now(3) })
    .returning([
      'id',
      'seasonId',
      'title',
      'description',
      'episodeNumber',
      'boxContents',
      'onlineGames',
      'active',
      'comingSoon',
    ]);

  const images = await db
    .select('id', 'fileName', 'imageUrl', 'coverPhoto')
    .table('episodeImages')
    .where({ episodeId: id });

  const activationCodes = await db
    .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
    .table('episodeActivationCodes')
    .where({ episodeId: id });

  return {
    ...episode[0],
    images,
    activationCodes,
  };
};

const del = async id => {
  return db.table('episodes').where({ id }).del();
};

const all = async () => {
  const seasons = await db.select().table('seasons');

  const result = await Promise.all(
    seasons
      .sort((a, b) => (a.seasonNumber > b.seasonNumber ? 1 : -1))
      .map(async season => {
        const episodes = await db
          .select(
            'id',
            'seasonId',
            'stripeProductId',
            'title',
            'description',
            'episodeNumber',
            'boxContents',
            'onlineGames',
            'active',
            'comingSoon',
          )
          .table('episodes')
          .where({ seasonId: season.id });

        const arr = await Promise.all(
          episodes
            .sort((a, b) => (a.episodeNumber > b.episodeNumber ? 1 : -1))
            .map(async episode => {
              return {
                ...episode,
                seasonNumber: season.seasonNumber,
                images: await db
                  .select('id', 'fileName', 'imageUrl', 'coverPhoto')
                  .table('episodeImages')
                  .where({ episodeId: episode.id }),
                activationCodes: await db
                  .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
                  .table('episodeActivationCodes')
                  .where({ episodeId: episode.id }),
              };
            }),
        );

        return arr;
      }),
  );

  return result.flat(1);
};

const findBySeasonNumber = async seasonNumber => {
  const episodes = await db
    .table('episodes')
    .join('seasons', 'episodes.seasonId', 'seasons.id')
    .join('episodeImages', 'episodes.id', 'episodeImages.episodeId')
    .select(
      'episodes.id',
      'episodes.seasonId',
      'episodes.stripeProductId',
      'seasons.id AS seasonId',
      'seasons.seasonNumber AS seasonNumber',
      'episodes.title',
      'episodes.description',
      'episodes.episodeNumber',
      'episodes.boxContents',
      'episodes.onlineGames',
      'episodes.active',
      'episodes.comingSoon',
      db.raw(
        `ARRAY_AGG(
        JSON_BUILD_OBJECT(
          'id', "episodeImages".id,
          'fileName', "episodeImages"."fileName",
          'imageUrl', "episodeImages"."imageUrl",
          'coverPhoto', "episodeImages"."coverPhoto"
        )
      ) AS images`,
      ),
    )
    .where('seasonNumber', seasonNumber)
    .groupBy('episodes.id', 'seasons.id')
    .orderBy('episodes.episodeNumber');

  const results = await Promise.all(
    episodes.map(async episode => {
      return {
        ...episode,
        activationCodes: await db
          .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
          .table('episodeActivationCodes')
          .where({ episodeId: episode.id }),
      };
    }),
  );

  return results;
};

const findBySeasonId = async seasonId => {
  const episodes = await db
    .table('episodes')
    .join('seasons', 'episodes.seasonId', 'seasons.id')
    .join('episodeImages', 'episodes.id', 'episodeImages.episodeId')
    .select(
      'episodes.id',
      'episodes.seasonId',
      'episodes.stripeProductId',
      'seasons.id AS seasonId',
      'seasons.seasonNumber AS seasonNumber',
      'episodes.title',
      'episodes.description',
      'episodes.episodeNumber',
      'episodes.boxContents',
      'episodes.onlineGames',
      'episodes.active',
      'episodes.comingSoon',
      db.raw(
        `ARRAY_AGG(
        JSON_BUILD_OBJECT(
          'id', "episodeImages".id,
          'fileName', "episodeImages"."fileName",
          'imageUrl', "episodeImages"."imageUrl",
          'coverPhoto', "episodeImages"."coverPhoto"
        )
      ) AS images`,
      ),
    )
    .where('seasonId', seasonId)
    .groupBy('episodes.id', 'seasons.id')
    .orderBy('episodes.episodeNumber');

  const results = await Promise.all(
    episodes.map(async episode => {
      return {
        ...episode,
        activationCodes: await db
          .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
          .table('episodeActivationCodes')
          .where({ episodeId: episode.id }),
      };
    }),
  );

  return results;
};

const findById = async id => {
  const episode = await db
    .table('episodes')
    .join('seasons', 'episodes.seasonId', 'seasons.id')
    .join('episodeImages', 'episodes.id', 'episodeImages.episodeId')
    .select(
      'episodes.id',
      'episodes.seasonId',
      'episodes.stripeProductId',
      'seasons.id AS seasonId',
      'seasons.seasonNumber AS seasonNumber',
      'episodes.title',
      'episodes.description',
      'episodes.episodeNumber',
      'episodes.boxContents',
      'episodes.onlineGames',
      'episodes.active',
      'episodes.comingSoon',
      db.raw(
        `ARRAY_AGG(
          JSON_BUILD_OBJECT(
            'id', "episodeImages".id,
            'fileName', "episodeImages"."fileName",
            'imageUrl', "episodeImages"."imageUrl",
            'coverPhoto', "episodeImages"."coverPhoto"
          )
        ) AS images`,
      ),
    )
    .groupBy('episodes.id', 'seasons.id')
    .where('episodeId', id);

  const result = {
    ...episode[0],
    activationCodes: await db
      .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
      .table('episodeActivationCodes')
      .where({ episodeId: episode[0].id }),
  };

  return result;
};

const findByStripeId = async id => {
  const episode = await db
    .table('episodes')
    .join('seasons', 'episodes.seasonId', 'seasons.id')
    .join('episodeImages', 'episodes.id', 'episodeImages.episodeId')
    .select(
      'episodes.id',
      'episodes.seasonId',
      'episodes.stripeProductId',
      'seasons.id AS seasonId',
      'seasons.seasonNumber AS seasonNumber',
      'episodes.title',
      'episodes.description',
      'episodes.episodeNumber',
      'episodes.boxContents',
      'episodes.onlineGames',
      'episodes.active',
      'episodes.comingSoon',
      db.raw(
        `ARRAY_AGG(
          JSON_BUILD_OBJECT(
            'id', "episodeImages".id,
            'fileName', "episodeImages"."fileName",
            'imageUrl', "episodeImages"."imageUrl",
            'coverPhoto', "episodeImages"."coverPhoto"
          )
        ) AS images`,
      ),
    )
    .groupBy('episodes.id', 'seasons.id')
    .where('stripeProductId', id);

  const result = {
    ...episode[0],
    activationCodes: await db
      .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
      .table('episodeActivationCodes')
      .where({ episodeId: episode[0].id }),
  };

  return result;
};

const findByNumber = async (episodeNumber, seasonId) => {
  const episode = await db
    .table('episodes')
    .join('seasons', 'episodes.seasonId', 'seasons.id')
    .join('episodeImages', 'episodes.id', 'episodeImages.episodeId')
    .select(
      'episodes.id',
      'episodes.seasonId',
      'episodes.stripeProductId',
      'seasons.id AS seasonId',
      'seasons.seasonNumber AS seasonNumber',
      'episodes.title',
      'episodes.description',
      'episodes.episodeNumber',
      'episodes.boxContents',
      'episodes.onlineGames',
      'episodes.active',
      'episodes.comingSoon',
      db.raw(
        `ARRAY_AGG(
          JSON_BUILD_OBJECT(
            'id', "episodeImages".id,
            'fileName', "episodeImages"."fileName",
            'imageUrl', "episodeImages"."imageUrl",
            'coverPhoto', "episodeImages"."coverPhoto"
          )
        ) AS images`,
      ),
    )
    .groupBy('episodes.id', 'seasons.id')
    .where('episodes.episodeNumber', episodeNumber)
    .andWhere('seasonId', seasonId);

  const result = {
    ...episode[0],
    activationCodes: await db
      .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
      .table('episodeActivationCodes')
      .where({ episodeId: episode[0].id }),
  };

  return result;
};

const queries = {
  create,
  update,
  del,
  all,
  findBySeasonNumber,
  findBySeasonId,
  findByStripeId,
  findById,
  findByNumber,
};

export default queries;
