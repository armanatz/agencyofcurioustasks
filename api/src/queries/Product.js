import knex from 'knex';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const assignToChild = async (data, userId = null) => {
  if (!Array.isArray(data)) {
    let alreadyOwned = await db
      .select()
      .table('episodesOwned')
      .where({ userId: data.userId });

    if (userId !== null) {
      alreadyOwned = await db.select().table('episodesOwned').where({ userId });
    }

    const doNotAssign = alreadyOwned.some(item => item.episodeId === data.id);

    if (!doNotAssign) {
      return db
        .table('episodesOwned')
        .insert(data)
        .returning(['id', 'userId', 'episodeId']);
    }
  }

  const alreadyOwned = await db
    .select()
    .table('episodesOwned')
    .where({ userId });
  const episodeIdsOwned = alreadyOwned.map(el => el.episodeId);
  const dataToAddFiltered = data.filter(
    el => !episodeIdsOwned.includes(el.episodeId),
  );

  return db
    .table('episodesOwned')
    .insert(dataToAddFiltered)
    .returning(['id', 'userId', 'episodeId']);
};

const findByUUID = async userId => {
  const products = await db
    .select('id', 'userId', 'episodeId', 'codeUsed')
    .table('episodesOwned')
    .where({ userId });

  const result = await Promise.all(
    products.map(async product => {
      const episode = await db
        .select(
          'id',
          'seasonId',
          'title',
          'description',
          'episodeNumber',
          'boxContents',
          'onlineGames',
          'active',
          'comingSoon',
        )
        .table('episodes')
        .where({ id: product.episodeId });

      return episode;
    }),
  );

  return result.flat(1);
};

const queries = {
  assignToChild,
  findByUUID,
};

export default queries;
