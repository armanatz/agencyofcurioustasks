import knex from 'knex';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const activate = async (codeUsed, episodeId, userId) => {
  await db.table('episodesActivated').insert({ codeUsed, episodeId, userId });

  return db.table('userGameData').insert({ episodeId, userId, progress: 0 });
};

const isActivated = (userId, episodeId = null) => {
  if (episodeId !== null) {
    return db.table('episodesActivated').where({ userId, episodeId });
  }

  return db.table('episodesActivated').where({ userId });
};

const saveProgress = (episodeId, userId, progress) => {
  return db
    .table('userGameData')
    .where({ episodeId, userId })
    .update({ progress, updatedAt: db.fn.now(3) })
    .returning(['id', 'userId', 'episodeId', 'progress']);
};

const loadProgress = (userId, episodeId) => {
  return db
    .select('id', 'userId', 'episodeId', 'progress')
    .table('userGameData')
    .where({ userId, episodeId });
};

const reset = userId => {
  return db
    .table('userGameData')
    .where({ userId })
    .update({ progress: 0, updatedAt: db.fn.now(3) });
};

const queries = {
  activate,
  isActivated,
  saveProgress,
  loadProgress,
  reset,
};

export default queries;
