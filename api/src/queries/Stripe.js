import knex from 'knex';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const createCustomer = (stripeId, userId) => {
  return db.table('stripeCustomers').insert({
    id: stripeId,
    userId,
  });
};

const createProduct = id => {
  return db.table('stripeProducts').insert({ id });
};

const createPrice = data => {
  return db.table('stripePrices').insert({
    id: data.id,
    productId: data.productId,
    unitAmount: data.unitAmount,
    lookupKey: data.lookupKey,
    ...(data.currency && { currency: data.currency }),
    ...(data.interval && { interval: data.interval }),
    ...(data.intervalCount && { intervalCount: data.intervalCount }),
    ...(data.type && { type: data.type }),
  });
};

const createCoupon = data => {
  return db.table('stripeCoupons').insert(data);
};

const createPromoCode = data => {
  return db.table('stripePromoCodes').insert(data);
};

const createSubscription = data => {
  return db.table('stripeSubscriptions').insert(data);
};

const createInvoice = data => {
  return db.table('stripeInvoices').insert(data);
};

const deleteCoupon = async id => {
  await db.table('stripePromoCodes').where({ couponId: id }).del();
  return db.table('stripeCoupons').where({ id }).del();
};

const deleteProduct = id => {
  return db.table('stripeProducts').where({ id }).del();
};

const deletePrice = id => {
  return db.table('stripePrices').where({ id }).del();
};

const deletePricesByProductId = productId => {
  return db.table('stripePrices').where({ productId }).del();
};

const updateProduct = (id, active) => {
  return db
    .table('stripeProducts')
    .update({ active, updatedAt: db.fn.now(3) })
    .where({ id });
};

const updatePrice = (id, unitAmount) => {
  return db
    .table('stripePrices')
    .update({ unitAmount, updatedAt: db.fn.now(3) })
    .where({ id });
};

const updatePromoCode = (id, data) => {
  return db
    .table('stripePromoCodes')
    .update({ ...data, updatedAt: db.fn.now(3) })
    .where({ id });
};

const updateSubscription = (id, data) => {
  return db
    .table('stripeSubscriptions')
    .update({ ...data, updatedAt: db.fn.now(3) })
    .where({ id });
};

const updateInvoice = (id, data) => {
  return db
    .table('stripeInvoices')
    .update({ ...data, updatedAt: db.fn.now(3) })
    .where({ id });
};

const findCustomerByStripeId = id => {
  return db.select().table('stripeCustomers').where({ id });
};

const findCustomerByUUID = userId => {
  return db.select().table('stripeCustomers').where({ userId });
};

const findPricesByProductId = (productId, interval) => {
  if (interval) {
    return db.select().table('stripePrices').where({ productId, interval });
  }
  return db.select().table('stripePrices').where({ productId });
};

const findPriceById = id => {
  return db.select().table('stripePrices').where({ id });
};

const findPromoCode = (code, active = undefined) => {
  return db
    .select()
    .from('stripePromoCodes')
    .where({ code, ...(active !== undefined && { active }) });
};

const findCouponById = id => {
  return db.select().from('stripeCoupons').where({ id });
};

const findAllSubscriptionsByParentId = parentId => {
  return db.select().from('stripeSubscriptions').where({ parentId });
};

const findAllInvoicesBySubId = id => {
  return db.select().from('stripeInvoices').where({ subscriptionId: id });
};

const findSubscriptionById = id => {
  return db.select().from('stripeSubscriptions').where({ id });
};

const queries = {
  createCustomer,
  createProduct,
  createPrice,
  createCoupon,
  createPromoCode,
  createSubscription,
  createInvoice,
  deleteCoupon,
  deleteProduct,
  deletePrice,
  deletePricesByProductId,
  updateProduct,
  updatePrice,
  updatePromoCode,
  updateSubscription,
  updateInvoice,
  findCustomerByStripeId,
  findCustomerByUUID,
  findPricesByProductId,
  findPriceById,
  findPromoCode,
  findCouponById,
  findAllSubscriptionsByParentId,
  findAllInvoicesBySubId,
  findSubscriptionById,
};

export default queries;
