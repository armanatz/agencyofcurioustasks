import knex from 'knex';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const create = data => {
  return db
    .table('episodeActivationCodes')
    .insert(data)
    .returning(['id', 'episodeId', 'code', 'fromPartner', 'partnerId']);
};

const del = async id => {
  return db.table('episodeActivationCodes').where({ id }).del();
};

const getAll = () => {
  return db
    .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
    .table('episodeActivationCodes');
};

const findByCode = code => {
  return db
    .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
    .table('episodeActivationCodes')
    .where({ code });
};

const findByEpisodeId = episodeId => {
  return db
    .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
    .table('episodeActivationCodes')
    .where({ episodeId });
};

const findByPartnerId = partnerId => {
  return db
    .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
    .table('episodeActivationCodes')
    .where({ partnerId });
};

const findByEpisodeAndPartnerId = (episodeId, partnerId) => {
  return db
    .select('id', 'episodeId', 'code', 'fromPartner', 'partnerId')
    .table('episodeActivationCodes')
    .where({ episodeId })
    .andWhere({ partnerId });
};

const queries = {
  create,
  del,
  getAll,
  findByCode,
  findByEpisodeId,
  findByPartnerId,
  findByEpisodeAndPartnerId,
};

export default queries;
