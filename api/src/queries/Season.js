import knex from 'knex';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const create = data => {
  return db
    .table('seasons')
    .insert(data)
    .returning([
      'id',
      'stripeProductId',
      'title',
      'description',
      'seasonNumber',
      'totalPlayableEpisodes',
      'coverImageFileName',
      'coverImageUrl',
      'active',
      'comingSoon',
    ]);
};

const update = async (id, data) => {
  return db
    .table('seasons')
    .where({ id })
    .update({ ...data, updatedAt: db.fn.now(3) })
    .returning([
      'id',
      'stripeProductId',
      'title',
      'description',
      'seasonNumber',
      'totalPlayableEpisodes',
      'coverImageFileName',
      'coverImageUrl',
      'active',
      'comingSoon',
    ]);
};

const del = async id => {
  return db.table('seasons').where({ id }).del();
};

const all = () => {
  return db
    .select(
      'id',
      'stripeProductId',
      'title',
      'description',
      'seasonNumber',
      'totalPlayableEpisodes',
      'coverImageFileName',
      'coverImageUrl',
      'active',
      'comingSoon',
    )
    .table('seasons');
  // return db
  //   .table('seasons')
  //   .leftJoin(
  //     'stripePrices',
  //     'stripePrices.productId',
  //     'seasons.stripeProductId',
  //   )
  //   .select(
  //     'seasons.id',
  //     'seasons.title',
  //     'seasons.description',
  //     'seasons.seasonNumber',
  //     'seasons.totalPlayableEpisodes',
  //     'seasons.coverImageFileName',
  //     'seasons.coverImageUrl',
  //     'seasons.active',
  //     'seasons.comingSoon',
  //     db.raw(
  //       `COALESCE(ARRAY_AGG(
  //         JSON_BUILD_OBJECT(
  //           'id', "stripePrices".id,
  //           'productId', "stripePrices"."productId",
  //           'unitAmount', "stripePrices"."unitAmount",
  //           'interval', "stripePrices".interval,
  //           'intervalCount', "stripePrices"."intervalCount",
  //           'type', "stripePrices".type,
  //           'currency', "stripePrices".currency,
  //         )
  //       ) FILTER (WHERE seasons."stripeProductId" IS NOT null), '{}') AS prices`,
  //     ),
  //   )
  //   .groupBy('seasons.id');
};

const findById = async id => {
  return db
    .select(
      'id',
      'stripeProductId',
      'title',
      'description',
      'seasonNumber',
      'totalPlayableEpisodes',
      'coverImageFileName',
      'coverImageUrl',
      'active',
      'comingSoon',
    )
    .table('seasons')
    .where({ id });
};

const findByStripeId = async id => {
  return db
    .select(
      'id',
      'stripeProductId',
      'title',
      'description',
      'seasonNumber',
      'totalPlayableEpisodes',
      'coverImageFileName',
      'coverImageUrl',
      'active',
      'comingSoon',
    )
    .table('seasons')
    .where({ stripeProductId: id });
};

const findByNumber = seasonNumber => {
  return db
    .select(
      'id',
      'stripeProductId',
      'title',
      'description',
      'seasonNumber',
      'totalPlayableEpisodes',
      'coverImageFileName',
      'coverImageUrl',
      'active',
      'comingSoon',
    )
    .table('seasons')
    .where({ seasonNumber });
};

const queries = {
  create,
  update,
  del,
  all,
  findById,
  findByStripeId,
  findByNumber,
};

export default queries;
