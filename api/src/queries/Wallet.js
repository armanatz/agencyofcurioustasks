import knex from 'knex';

import knexConfig from '../config/knexfile.js';

const db = knex(knexConfig);

const create = (userId, balance = 0) => {
  return db.table('wallets').insert({ userId, balance });
};

const balance = userId => {
  return db.select('id', 'balance').from('wallets').where({ userId });
};

const update = async (userId, operation, amount) => {
  let currentBalance = await db
    .select()
    .from('wallets')
    .where({ userId })
    .pluck('balance');

  currentBalance = currentBalance[0];

  let balance = currentBalance;

  if (operation === 'add') {
    balance += amount;
  } else if (operation === 'subtract') {
    balance -= amount;
  }

  if (balance < 0) {
    balance = 0;
  }

  return db
    .table('wallets')
    .where({ userId })
    .update({ balance, updatedAt: db.fn.now(3) });
};

const queries = {
  create,
  balance,
  update,
};

export default queries;
