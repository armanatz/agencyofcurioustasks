import AWS from 'aws-sdk';

import vars from '../config/vars.js';

export default new AWS.S3({
  endpoint: new AWS.Endpoint(vars.digitalocean.spacesEndpoint),
  credentials: new AWS.Credentials(
    vars.digitalocean.spacesKey,
    vars.digitalocean.spacesSecret,
    null,
  ),
});
