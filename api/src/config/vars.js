// Express server variables
const NODE_ENV = process.env.NODE_ENV;
const SERVER_PORT = process.env.SERVER_PORT;
const API_PATH_PREFIX = process.env.API_PATH_PREFIX;
const API_VERSION = process.env.API_VERSION;
const SERVER_DOMAIN = process.env.SERVER_DOMAIN;
const SERVER_NAME = process.env.SERVER_NAME;
const SERVER_PROTO = process.env.SERVER_PROTO;
const ADMIN_PASSWORD = process.env.ADMIN_PASSWORD;
const LOG_LEVEL = process.env.LOG_LEVEL;

// Postgres DB variables
const POSTGRES_SSL = process.env.POSTGRES_SSL;
const POSTGRES_HOST = process.env.POSTGRES_HOST;
const POSTGRES_PORT = process.env.POSTGRES_PORT;
const POSTGRES_USER = process.env.POSTGRES_USER;
const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
const POSTGRES_DB = process.env.POSTGRES_DB;

// FusionAuth variables
const FUSIONAUTH_APP_URL = process.env.FUSIONAUTH_APP_URL;
const FUSIONAUTH_PORT = process.env.FUSIONAUTH_PORT;
const FUSIONAUTH_API_KEY = process.env.FUSIONAUTH_API_KEY;
const FUSIONAUTH_APP_ID = process.env.FUSIONAUTH_APP_ID;
const JWT_SIGNING_KEY = process.env.JWT_SIGNING_KEY;

// Stripe variables
const STRIPE_PUBLIC_KEY = process.env.STRIPE_PUBLIC_KEY;
const STRIPE_PRIVATE_KEY = process.env.STRIPE_PRIVATE_KEY;
const STRIPE_WEBHOOK_SECRET = process.env.STRIPE_WEBHOOK_SECRET;
const STRIPE_PRICE_LOOKUP_KEY_SUFFIX =
  NODE_ENV === 'development'
    ? `${process.env.STRIPE_PRICE_LOOKUP_KEY_SUFFIX}_`
    : '';

// DigitalOcean variables
const DO_SPACES_ENDPOINT = process.env.DO_SPACES_ENDPOINT;
const DO_SPACES_REGION = process.env.DO_SPACES_REGION;
const DO_SPACES_NAME = process.env.DO_SPACES_NAME;
const DO_SPACES_KEY = process.env.DO_SPACES_KEY;
const DO_SPACES_SECRET = process.env.DO_SPACES_SECRET;

// Mailgun variables
const MAILGUN_DOMAIN = process.env.MAILGUN_DOMAIN;
const MAILGUN_API_KEY = process.env.MAILGUN_API_KEY;
const MAILGUN_PUBLIC_KEY = process.env.MAILGUN_PUBLIC_KEY;

// Mailchimp variables
const MAILCHIMP_API_KEY = process.env.MAILCHIMP_API_KEY;
const MAILCHIMP_SERVER = process.env.MAILCHIMP_SERVER;

const SERVER = {
  env: NODE_ENV,
  port: SERVER_PORT,
  apiPathPrefix: API_PATH_PREFIX,
  apiVersion: API_VERSION,
  domain: SERVER_DOMAIN,
  name: SERVER_NAME,
  proto: SERVER_PROTO,
  adminPassword: ADMIN_PASSWORD,
  logLevel: LOG_LEVEL,
};

const POSTGRES = {
  ssl: POSTGRES_SSL,
  host: POSTGRES_HOST,
  port: POSTGRES_PORT,
  user: POSTGRES_USER,
  password: POSTGRES_PASSWORD,
  database: POSTGRES_DB,
};

const FUSIONAUTH = {
  appUrl: FUSIONAUTH_APP_URL,
  port: FUSIONAUTH_PORT,
  apiKey: FUSIONAUTH_API_KEY,
  appId: FUSIONAUTH_APP_ID,
  jwtKey: JWT_SIGNING_KEY,
};

const STRIPE = {
  publicKey: STRIPE_PUBLIC_KEY,
  privateKey: STRIPE_PRIVATE_KEY,
  webhookSecret: STRIPE_WEBHOOK_SECRET,
  priceLookupKeySuffix: STRIPE_PRICE_LOOKUP_KEY_SUFFIX,
};

const DIGITALOCEAN = {
  spacesEndpoint: DO_SPACES_ENDPOINT,
  spacesRegion: DO_SPACES_REGION,
  spacesName: DO_SPACES_NAME,
  spacesKey: DO_SPACES_KEY,
  spacesSecret: DO_SPACES_SECRET,
};

const MAILGUN = {
  domain: MAILGUN_DOMAIN,
  apiKey: MAILGUN_API_KEY,
  pubKey: MAILGUN_PUBLIC_KEY,
};

const MAILCHIMP = {
  apiKey: MAILCHIMP_API_KEY,
  server: MAILCHIMP_SERVER,
};

const vars = {
  server: SERVER,
  postgres: POSTGRES,
  fusionauth: FUSIONAUTH,
  stripe: STRIPE,
  digitalocean: DIGITALOCEAN,
  mailgun: MAILGUN,
  mailchimp: MAILCHIMP,
};

export default vars;
