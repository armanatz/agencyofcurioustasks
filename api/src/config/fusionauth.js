import { FusionAuthClient } from '@fusionauth/typescript-client';

import vars from './vars.js';

const { apiKey, appUrl, port } = vars.fusionauth;

export default new FusionAuthClient(apiKey, `${appUrl}:${port}`);
