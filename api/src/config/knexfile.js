import path from 'path';
import pg from 'pg';

import vars from './vars.js';

const { ssl, host, port, user, password, database } = vars.postgres;

pg.defaults.ssl = ssl === 'true';

export default {
  client: 'pg',
  connection: {
    host,
    port,
    user,
    password,
    database,
    ssl:
      ssl === 'true'
        ? {
            rejectUnauthorized: false,
          }
        : false,
  },
  pool: {
    min: 2,
    max: 22,
  },
  migrations: {
    tableName: 'knex_migrations',
    directory: '../migrations',
  },
  seeds: {
    directory: '../seeds',
  },
  timezone: 'UTC',
};
