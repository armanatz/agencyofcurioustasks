import mailchimp from '@mailchimp/mailchimp_marketing';

import vars from './vars.js';

const { apiKey, server } = vars.mailchimp;

export default new mailchimp.setConfig({ apiKey, server });
