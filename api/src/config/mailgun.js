import formData from 'form-data';
import Mailgun from 'mailgun.js';

import vars from './vars.js';

const mailgun = new Mailgun(formData);

export default mailgun.client({
  username: 'api',
  key: vars.mailgun.apiKey,
  public_key: vars.mailgun.pubKey,
});
