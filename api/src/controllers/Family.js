import jwt from 'jsonwebtoken';
import createError from 'http-errors';

import vars from '../config/vars.js';
import Auth from '../config/fusionauth.js';
import Address from '../queries/Address.js';

import Product from '../queries/Product.js';

const getCurrentUsersFamily = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    const authGetFamily = await Auth.retrieveFamilies(decodedJWT.sub);
    const family = authGetFamily.response.families[0];

    let members = await Promise.all(
      family.members.map(async member => {
        const authGetUserById = await Auth.retrieveUser(member.userId);

        const { user } = authGetUserById.response;

        const registration = user.registrations.filter(
          reg => reg.applicationId === vars.fusionauth.appId,
        )[0];

        return {
          active: user.active,
          ...(user.email && {
            addresses: await Address.findByUUID(member.userId, false),
            email: user.email,
            mobilePhone: user.mobilePhone,
          }),
          firstName: user.firstName,
          fullName: user.fullName,
          id: user.id,
          ...(user.username && {
            birthDate: user.birthDate,
            data: user.data,
            imageUrl: user.imageUrl,
            ownedProducts: await Product.findByUUID(member.userId),
            username: user.username,
          }),
          lastName: user.lastName,
          roles: registration.roles,
          timezone: user.timezone,
          verified: user.verified,
        };
      }),
    );

    if (req.query.onlyChildren) {
      members = members.filter(member => !member.email);
    }

    return res.status(200).json({
      id: family.id,
      members,
    });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  getCurrentUsersFamily,
};
