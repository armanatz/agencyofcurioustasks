import fs from 'fs/promises';
import path from 'path';
import jwt from 'jsonwebtoken';
import createError from 'http-errors';
import { ExportToCsv } from 'export-to-csv';
import * as randomCodes from 'referral-codes';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc.js';
import timezone from 'dayjs/plugin/timezone.js';

import vars from '../config/vars.js';
import Auth from '../config/fusionauth.js';
import Mail from '../helpers/mail.js';
import Mailchimp from '../helpers/mailchimp.js';

import Episode from '../queries/Episode.js';
import Order from '../queries/Order.js';
import Referral from '../queries/Referral.js';
import Wallet from '../queries/Wallet.js';
import Setting from '../queries/Setting.js';
import Stripe from '../queries/Stripe.js';
import Product from '../queries/Product.js';

dayjs.extend(utc);
dayjs.extend(timezone);

const createOrder = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    const parentId = decodedJWT.sub;
    const { childId, seasonNumber } = req.body;

    let episodeIds = [];

    const parentDetails = await Auth.retrieveUser(parentId);

    const mailchimpData = {
      email: parentDetails.response.user.email,
      firstName: parentDetails.response.user.firstName,
      ...(parentDetails.response.user.lastName && {
        lastName: parentDetails.response.user.lastName,
      }),
    };

    if (seasonNumber === 0) {
      episodeIds.push(req.body.episodeId);
      const episodeDetails = await Episode.findById(req.body.episodeId);

      await Mailchimp.addUserToMailchimp({
        ...mailchimpData,
        tags: [episodeDetails.title],
      });
    } else {
      const dbReq = await Episode.findBySeasonNumber(seasonNumber);
      dbReq
        .sort((a, b) => (a.episodeNumber > b.episodeNumber ? 1 : -1))
        .forEach(episode => episodeIds.push(episode.id));

      await Mailchimp.addUserToMailchimp({
        ...mailchimpData,
        tags: [`Season ${seasonNumber}`],
      });
    }

    let orderCode = randomCodes.generate({
      length: 8,
      charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
    });

    let doesOrderCodeExist = await Order.findByOrderCode(orderCode[0]);

    while (doesOrderCodeExist.length > 0) {
      orderCode = randomCodes.generate({
        length: 7,
        charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      });

      doesOrderCodeExist = await Order.findByOrderCode(orderCode[0]);
    }

    let promoUsed = false;
    let promoId = null;

    if (req.body.discountCode) {
      const discountDetails = await Stripe.findPromoCode(req.body.discountCode);

      if (discountDetails.length !== 0) {
        await Stripe.updatePromoCode(discountDetails[0].id, {
          timesRedeemed: discountDetails[0].timesRedeemed + 1,
        });
        promoUsed = true;
        promoId = discountDetails[0].id;
      }
    }

    const order = await Order.create({
      parentId,
      childId,
      episodeIds,
      orderCode: orderCode[0],
      promoUsed,
      promoId,
    });

    const assignmentArr = episodeIds.map(id => ({
      userId: childId,
      episodeId: id,
    }));

    await Product.assignToChild(assignmentArr, childId);

    const wasReferred = await Referral.check(parentId);

    if (wasReferred.length > 0) {
      if (!wasReferred[0].placedOrder) {
        await Referral.update(wasReferred[0].id, true);

        const referree = await Referral.findByCode(wasReferred[0].codeUsed);

        const referralDiscount = await Setting.findByName('referralDiscount');

        await Wallet.update(
          referree[0].userId,
          'add',
          referralDiscount[0].value,
        );
      }
    }

    const orderProcessingDays = await Setting.findByName('orderProcessingDays');

    await Mail.send({
      to: parentDetails.response.user.email,
      subject: 'Your Order Summary',
      templateName: 'order_summary',
      templateContext: {
        fullName: parentDetails.response.user.fullName,
        orderId: orderCode[0],
        orderProcessingDays: orderProcessingDays[0].value,
        orderDate: dayjs
          .tz(order.date, parentDetails.response.user.timezone)
          .format('DD/MM/YYYY HH:mm A'),
      },
    });

    return res.status(200).json(order);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getOrders = async (req, res, next) => {
  try {
    const filters = {
      ...req.query,
    };

    if (req.query.email) {
      const authReq = await Auth.retrieveUserByEmail(req.query.email);
      delete filters.email;
      filters.parentId = authReq.response.user.id;
    }

    if (req.query.username) {
      const authReq = await Auth.retrieveUserByUsername(req.query.username);
      delete filters.username;
      filters.childId = authReq.response.user.id;
    }

    const orders = await Order.all(filters);

    const result = await Promise.all(
      orders.map(async order => {
        const obj = { ...order };
        delete obj.parentId;
        delete obj.childId;
        const parent = await Auth.retrieveUser(order.parentId);
        const child = await Auth.retrieveUser(order.childId);

        return {
          ...obj,
          parent: parent.response.user,
          child: child.response.user,
          admin:
            order.lastUpdateBy === null
              ? null
              : (await Auth.retrieveUser(order.lastUpdateBy)).response.user,
        };
      }),
    );

    return res.status(200).json(result);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getStatusCodes = async (req, res, next) => {
  try {
    const statusCodes = await Order.statusCodes();
    return res.status(200).json(statusCodes);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const updateOrderItem = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    const orderItem = await Order.updateItem(req.params.id, {
      ...req.body,
      lastUpdateBy: decodedJWT.sub,
    });

    const order = await Order.findByOrderId(orderItem[0].orderId);
    const parentDetails = await Auth.retrieveUser(order[0].parentId);

    if (req.body.statusCode === 3 && !orderItem[0].shippingEmailSent) {
      await Mail.send({
        to: parentDetails.response.user.email,
        subject: `Order ${order[0].orderCode} just shipped out!`,
        templateName: 'order_shipped',
        templateContext: {
          fullName: parentDetails.response.user.fullName,
          orderId: order[0].orderCode,
          courier: req.body.courier || orderItem[0].courier,
          trackingUrl: req.body.trackingUrl || orderItem[0].trackingUrl,
          trackingCode: req.body.trackingCode || orderItem[0].trackingCode,
        },
      });
      await Order.updateItem(req.params.id, {
        shippingEmailSent: true,
      });
    }

    if (req.body.statusCode === 4 && !orderItem[0].deliveredEmailSent) {
      await Mail.send({
        to: parentDetails.response.user.email,
        subject: `Order ${order[0].orderCode} has been delivered!`,
        templateName: 'order_delivered',
        templateContext: {
          fullName: parentDetails.response.user.fullName,
          proto: vars.server.proto,
          serverName: vars.server.name,
        },
      });
      await Order.updateItem(req.params.id, {
        deliveredEmailSent: true,
      });
    }

    return res.status(200).end();
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const exportOrders = async (req, res, next) => {
  try {
    const orders = await Order.all();

    const result = await Promise.all(
      orders.map(async order => {
        const obj = { ...order };
        delete obj.parentId;
        delete obj.childId;
        const parent = await Auth.retrieveUser(order.parentId);
        const child = await Auth.retrieveUser(order.childId);

        return {
          order_code: obj.orderCode,
          order_date: dayjs(obj.orderDate).format('DD-MM-YYYY'),
          episode_title: obj.episodeTitle,
          status_name: obj.statusName,
          max_ship_by_date: dayjs(obj.maxShipByDate).format('DD-MM-YYYY'),
          parent_name: parent.response.user.fullName,
          child_name: child.response.user.fullName,
          notes: obj.notes,
          tracking_code: obj.trackingCode,
          last_update_by:
            order.lastUpdateBy === null
              ? null
              : (await Auth.retrieveUser(order.lastUpdateBy)).response.user,
          last_update_date: dayjs(obj.lastUpdateDate).format('DD-MM-YYYY'),
        };
      }),
    );

    const fileName = `${dayjs().format('YYYYMMDD')}_OrderList.csv`;
    const absPath = path.join(path.resolve('./temp'), fileName);

    const options = {
      title: fileName,
      useKeysAsHeaders: true,
    };

    const csvExporter = new ExportToCsv(options);

    const csv = csvExporter.generateCsv(result, true);

    await fs.writeFile(absPath, csv);

    return res.download(absPath, fileName, err => {
      if (!err) {
        fs.unlink(absPath, err => {
          if (err) {
            return next(
              createError(err.statusCode ? err.statusCode : 500, err, {
                level: 'ERROR',
              }),
            );
          }
        });
      }
    });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  createOrder,
  getOrders,
  getStatusCodes,
  updateOrderItem,
  exportOrders,
};
