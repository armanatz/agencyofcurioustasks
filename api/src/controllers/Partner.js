import createError from 'http-errors';

import Partner from '../queries/Partner.js';

const getPartners = async (req, res, next) => {
  try {
    const partners = await Partner.all(req.query);
    return res.status(200).json(partners);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getPartnerById = async (req, res, next) => {
  try {
    const partner = await Partner.findById(req.params.id);
    return res.status(200).json(partner[0]);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const createPartner = async (req, res, next) => {
  try {
    const partner = await Partner.create(req.body);
    return res.status(200).json(partner[0]);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const editPartner = async (req, res, next) => {
  try {
    const partner = await Partner.update(req.params.id, {
      ...req.body,
      ...(req.body.useActivationCode === true && { referralCode: null }),
    });
    return res.status(200).json(partner[0]);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const deletePartner = async (req, res, next) => {
  try {
    await Partner.del(req.params.id);
    return res.status(200).end();
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  getPartners,
  getPartnerById,
  createPartner,
  editPartner,
  deletePartner,
};
