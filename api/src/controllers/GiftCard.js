import jwt from 'jsonwebtoken';
import createError from 'http-errors';
import dayjs from 'dayjs';

import vars from '../config/vars.js';

import GiftCard from '../queries/GiftCard.js';
import Wallet from '../queries/Wallet.js';

const createGiftCard = async (req, res, next) => {
  try {
    const giftCard = await GiftCard.create(req.body.value);
    return res.status(200).json(giftCard);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const retrieveAllGiftCards = async (req, res, next) => {
  try {
    const giftCards = await GiftCard.all(req.query.redeemed);
    return res.status(200).json(giftCards);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const deleteGiftCard = async (req, res, next) => {
  try {
    await GiftCard.del(req.params.id);
    return res.status(200).json({ message: 'Deleted gift card.' });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const redeemGiftCard = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    const giftCard = await GiftCard.findByCode(req.body.code);

    if (giftCard.length === 0) {
      return next(
        createError(404, 'This gift card is invalid or does not exist.', {
          level: 'INFO',
        }),
      );
    }

    if (!giftCard[0].redeemed && giftCard[0].hidden) {
      return next(
        createError(403, 'This gift card is invalid or does not exist.', {
          level: 'INFO',
        }),
      );
    }

    if (giftCard[0].redeemed) {
      return next(
        createError(403, 'This gift card has already been redeemed.', {
          level: 'INFO',
        }),
      );
    }

    await GiftCard.update(giftCard[0].id, {
      redeemed: true,
      redeemedBy: decodedJWT.sub,
      redeemedAt: dayjs().toISOString(),
    });

    await Wallet.update(decodedJWT.sub, 'add', giftCard[0].value);
    return res.status(200).end();
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  createGiftCard,
  retrieveAllGiftCards,
  deleteGiftCard,
  redeemGiftCard,
};
