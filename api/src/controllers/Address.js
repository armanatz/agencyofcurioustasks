import jwt from 'jsonwebtoken';
import createError from 'http-errors';

import vars from '../config/vars.js';

import Address from '../queries/Address.js';

const getCurrentUsersAddress = async (req, res, next) => {
  const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
    algorithms: ['HS256'],
  });

  try {
    const address = await Address.findByUUID(decodedJWT.sub);
    return res.status(200).json(address);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getUsersAddress = async (req, res, next) => {
  try {
    const address = await Address.findByUUID(req.params.userId);
    return res.status(200).json(address);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const createAddress = async (req, res, next) => {
  let id = null;

  if (req.params.userId) {
    id = req.params.userId;
  } else {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    id = decodedJWT.sub;
  }

  try {
    const address = await Address.create(id, req.body);
    return res.status(200).json(address);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const updateAddress = async (req, res, next) => {
  let id = null;

  if (req.params.userId) {
    id = req.params.userId;
  } else {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    id = decodedJWT.sub;
  }

  try {
    const address = await Address.update(id, req.body);
    return res.status(200).json(address);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  getCurrentUsersAddress,
  getUsersAddress,
  createAddress,
  updateAddress,
};
