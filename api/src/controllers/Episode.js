import createError from 'http-errors';
import StripeClient from 'stripe';

import vars from '../config/vars.js';
import doSpaces from '../helpers/doSpaces.js';

import Episode from '../queries/Episode.js';
import Stripe from '../queries/Stripe.js';

const stripeSdk = new StripeClient(vars.stripe.privateKey);

const createEpisode = async (req, res, next) => {
  try {
    const data = { ...req.body };

    let stripeProductId = null;

    if (req.body.seasonId === '1') {
      const stripeProduct = await stripeSdk.products.create({
        name: req.body.title,
        description: req.body.description,
        metadata: {
          episodeNumber: data.episodeNumber,
        },
      });

      stripeProductId = stripeProduct.id;

      const lookupKey = `${vars.stripe.priceLookupKeySuffix}S0_E${req.body.episodeNumber}`;

      const stripePricing = await stripeSdk.prices.create({
        unit_amount: req.body.price,
        currency: 'myr',
        product: stripeProductId,
        lookup_key: lookupKey,
      });

      await Stripe.createProduct(stripeProductId);
      await Stripe.createPrice({
        id: stripePricing.id,
        productId: stripeProductId,
        lookupKey,
        unitAmount: stripePricing.unit_amount,
        type: 'one_time',
        interval: null,
        intervalCount: null,
      });
    }

    delete data.price;

    const episode = await Episode.create({
      ...data,
      seasonId: parseInt(req.body.seasonId),
      stripeProductId,
      active: req.body.active === 'true',
      comingSoon: req.body.comingSoon === 'true',
      images: req.files,
    });
    return res.status(200).json(episode);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const editEpisode = async (req, res, next) => {
  try {
    const episodeToUpdate = await Episode.findById(req.params.id);

    let { filesToDelete } = req.body;

    if (filesToDelete) {
      filesToDelete = JSON.parse(filesToDelete);
    }

    const data = {
      ...req.body,
      ...(req.body.active && { active: req.body.active === 'true' }),
      ...(req.body.comingSoon && {
        comingSoon: req.body.comingSoon === 'true',
      }),
      ...(req.body.seasonId && { seasonId: parseInt(req.body.seasonId) }),
      ...(filesToDelete && { filesToDelete }),
      ...(req.files && { images: req.files }),
    };

    const { stripeProductId } = episodeToUpdate;

    if (episodeToUpdate.seasonNumber === 0) {
      if (
        Object.keys(data).some(key => ['title', 'description'].includes(key))
      ) {
        await stripeSdk.products.update(stripeProductId, {
          ...(data.title && { name: data.title }),
          ...(data.description && { description: data.description }),
        });
      }

      if (data.price) {
        const stripePrice = await Stripe.findPricesByProductId(stripeProductId);

        await stripeSdk.prices.update(stripePrice[0].id, { active: false });

        await Stripe.deletePrice(stripePrice[0].id);

        const lookupKey = `${vars.stripe.priceLookupKeySuffix}S0_E${episodeToUpdate.episodeNumber}`;

        const stripePricing = await stripeSdk.prices.create({
          unit_amount: req.body.price,
          currency: 'myr',
          product: stripeProductId,
          lookup_key: lookupKey,
          transfer_lookup_key: true,
        });

        await Stripe.createPrice({
          id: stripePricing.id,
          productId: stripeProductId,
          lookupKey,
          unitAmount: stripePricing.unit_amount,
          type: 'one_time',
          interval: null,
          intervalCount: null,
        });

        delete data.price;
      }
    }

    if (Object.keys(data).length > 0) {
      const updatedEpisode = await Episode.update(req.params.id, data);
      return res.status(200).json(updatedEpisode);
    }
    return res.status(200).json(episodeToUpdate);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const deleteEpisode = async (req, res, next) => {
  try {
    const episodeToDelete = await Episode.findById(req.params.id);

    const fileNames = episodeToDelete.images.map(file => file.fileName);

    await doSpaces.deleteFiles(fileNames, '/products/episodes');

    if (episodeToDelete.seasonId === 1) {
      await stripeSdk.products.update(episodeToDelete.stripeProductId, {
        active: false,
      });

      await Stripe.deleteProduct(episodeToDelete.stripeProductId);
      await Stripe.deletePricesByProductId(episodeToDelete.stripeProductId);
    }

    await Episode.del(req.params.id);
    return res.status(200).end();
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getEpisodes = async (req, res, next) => {
  try {
    const episodes = await Episode.all();
    return res.status(200).json(episodes);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getEpisodesBySeasonId = async (req, res, next) => {
  try {
    const episodes = await Episode.findBySeasonId(req.params.seasonId);
    return res.status(200).json(episodes);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getEpisodesBySeasonNumber = async (req, res, next) => {
  try {
    const episodes = await Episode.findBySeasonNumber(req.params.seasonNumber);
    return res.status(200).json(episodes);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getEpisodeById = async (req, res, next) => {
  try {
    const episodes = await Episode.findById(req.params.id);
    return res.status(200).json(episodes);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  createEpisode,
  editEpisode,
  deleteEpisode,
  getEpisodes,
  getEpisodesBySeasonId,
  getEpisodesBySeasonNumber,
  getEpisodeById,
};
