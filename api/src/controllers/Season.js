import createError from 'http-errors';
import StripeClient from 'stripe';

import vars from '../config/vars.js';
import doSpaces from '../helpers/doSpaces.js';

import Season from '../queries/Season.js';
import Stripe from '../queries/Stripe.js';

const stripeSdk = new StripeClient(vars.stripe.privateKey);

const getSeasons = async (req, res, next) => {
  try {
    const seasons = await Season.all();
    return res
      .status(200)
      .json(seasons.sort((a, b) => (a.seasonNumber > b.seasonNumber ? 1 : -1)));
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getSeasonById = async (req, res, next) => {
  try {
    const season = await Season.findById(req.params.id);
    return res.status(200).json(season[0]);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getSeasonByNumber = async (req, res, next) => {
  try {
    const season = await Season.findByNumber(req.params.number);
    return res.status(200).json(season[0]);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const createSeason = async (req, res, next) => {
  try {
    const data = { ...req.body };

    let stripeProductId = null;

    if (req.body.seasonNumber !== '0') {
      const stripeProduct = await stripeSdk.products.create({
        name: req.body.title,
        description: req.body.description,
        metadata: {
          seasonNumber: data.seasonNumber,
        },
      });

      stripeProductId = stripeProduct.id;

      const lookupKey = `${vars.stripe.priceLookupKeySuffix}S${req.body.seasonNumber}_monthly`;

      const stripePricing = await stripeSdk.prices.create({
        unit_amount: req.body.price,
        currency: 'myr',
        recurring: { interval: 'month', interval_count: 1 },
        product: stripeProductId,
        lookup_key: lookupKey,
      });

      await Stripe.createProduct(stripeProductId);
      await Stripe.createPrice({
        id: stripePricing.id,
        productId: stripeProductId,
        lookupKey,
        unitAmount: stripePricing.unit_amount,
        interval: 'month',
        intervalCount: 1,
      });
    }

    delete data.price;

    const season = await Season.create({
      ...data,
      stripeProductId,
      coverImageFileName: req.file.key.substring(
        req.file.key.lastIndexOf('/') + 1,
      ),
      coverImageUrl: req.file.location,
    });
    return res.status(200).json(season[0]);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const editSeason = async (req, res, next) => {
  try {
    const seasonToUpdate = await Season.findById(req.params.id);

    const data = {
      ...req.body,
      ...(req.file && {
        coverImageFileName: req.file.key.substring(
          req.file.key.lastIndexOf('/') + 1,
        ),
        coverImageUrl: req.file.location,
      }),
    };

    const { stripeProductId } = seasonToUpdate[0];

    if (seasonToUpdate[0].seasonNumber !== 0) {
      if (
        Object.keys(data).some(key => ['title', 'description'].includes(key))
      ) {
        await stripeSdk.products.update(stripeProductId, {
          ...(data.title && { name: data.title }),
          ...(data.description && { description: data.description }),
        });
      }

      if (data.price) {
        const stripePrice = await Stripe.findPricesByProductId(
          stripeProductId,
          'month',
        );

        await stripeSdk.prices.update(stripePrice[0].id, { active: false });

        await Stripe.deletePrice(stripePrice[0].id);

        const lookupKey = `${vars.stripe.priceLookupKeySuffix}S${req.body.seasonNumber}_monthly`;

        const stripePricing = await stripeSdk.prices.create({
          unit_amount: req.body.price,
          currency: 'myr',
          recurring: { interval: 'month', interval_count: 1 },
          product: stripeProductId,
          lookup_key: lookupKey,
          transfer_lookup_key: true,
        });

        await Stripe.createPrice({
          id: stripePricing.id,
          productId: stripeProductId,
          lookupKey,
          unitAmount: stripePricing.unit_amount,
          interval: 'month',
          intervalCount: 1,
        });

        delete data.price;
      }
    }

    if (Object.keys(data).length > 0) {
      const updatedSeason = await Season.update(req.params.id, data);
      return res.status(200).json(updatedSeason);
    }
    return res.status(200).json(seasonToUpdate[0]);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const deleteSeason = async (req, res, next) => {
  try {
    const seasonToDelete = await Season.findById(req.params.id);

    if (seasonToDelete[0].coverImageFileName !== 'product_placeholder.png') {
      await doSpaces.deleteFiles(
        [seasonToDelete[0].coverImageFileName],
        '/products/seasons',
      );
    }

    if (seasonToDelete[0].seasonNumber !== 0) {
      await stripeSdk.products.update(seasonToDelete[0].stripeProductId, {
        active: false,
      });

      await Stripe.deleteProduct(seasonToDelete[0].stripeProductId);
      await Stripe.deletePricesByProductId(seasonToDelete[0].stripeProductId);
    }

    await Season.del(req.params.id);
    return res.status(200).end();
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  getSeasons,
  getSeasonById,
  getSeasonByNumber,
  createSeason,
  editSeason,
  deleteSeason,
};
