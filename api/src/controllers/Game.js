import createError from 'http-errors';

import Auth from '../config/fusionauth.js';

import Game from '../queries/Game.js';
import Product from '../queries/Product.js';

const activateEpisode = async (req, res, next) => {
  try {
    const isOwned = await Product.findByUUID(req.body.userId);
    if (isOwned.length > 0) {
      await Game.activate(req.body.code, req.params.episodeId, req.body.userId);
      return res.status(200).end();
    }

    return next(
      createError(403, 'You do not own this episode yet.', { level: 'INFO' }),
    );
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getAllEpisodesActivated = async (req, res, next) => {
  try {
    const episodesActivated = await Game.isActivated(req.params.userId);

    return res.status(200).json(episodesActivated);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getEpisodeActivated = async (req, res, next) => {
  try {
    const episodeActivated = await Game.isActivated(
      req.params.userId,
      req.params.episodeId,
    );

    return res.status(200).send(episodeActivated.length > 0);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const loadEpisodeProgress = async (req, res, next) => {
  try {
    const progress = await Game.loadProgress(
      req.params.userId,
      req.params.episodeId,
    );

    if (progress.length > 0) {
      return res.status(200).json(progress[0]);
    }

    return res.status(200).json({});
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const saveEpisodeProgress = async (req, res, next) => {
  try {
    await Game.saveProgress(
      req.params.episodeId,
      req.body.userId,
      req.body.progress,
    );

    return res.status(200).end();
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const resetUsersProgress = async (req, res, next) => {
  try {
    const user = await Auth.retrieveUserByUsername(req.params.username);
    await Game.reset(user.response.user.id);
    return res.status(200).json({
      message: `Game progress reset for user: ${req.params.username}`,
    });
  } catch (err) {
    if (err.statusCode === 404) {
      return next(
        createError(404, 'Invalid username entered.', {
          level: 'INFO',
        }),
      );
    }
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  activateEpisode,
  getAllEpisodesActivated,
  getEpisodeActivated,
  loadEpisodeProgress,
  saveEpisodeProgress,
  resetUsersProgress,
};
