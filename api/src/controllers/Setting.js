import createError from 'http-errors';

import Setting from '../queries/Setting.js';

const retrieveSettings = async (req, res, next) => {
  try {
    const settings = await Setting.all();
    return res.status(200).json(settings);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const retrieveSetting = async (req, res, next) => {
  try {
    if (Object.keys(req.params).includes('name')) {
      const setting = await Setting.findByName(req.params.name);
      return res.status(200).send(setting[0].value);
    }

    const setting = await Setting.findById(req.params.id);
    return res.status(200).send(setting[0].value);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const createSetting = async (req, res, next) => {
  try {
    const setting = await Setting.create(req.body);
    return res.status(200).json(setting);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const updateSetting = async (req, res, next) => {
  try {
    const setting = await Setting.update(req.body.data);
    return res.status(200).json(setting);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const deleteSetting = async (req, res, next) => {
  try {
    await Setting.del(req.params.id);
    return res.status(200).end();
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  retrieveSettings,
  retrieveSetting,
  createSetting,
  updateSetting,
  deleteSetting,
};
