import createError from 'http-errors';

import Auth from '../config/fusionauth.js';

import Product from '../queries/Product.js';
import ActivationCode from '../queries/ActivationCode.js';

const getAllActivationCodes = async (req, res, next) => {
  try {
    const activationCodes = await ActivationCode.getAll();
    return res.status(200).json(activationCodes);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getEpisodeActivationCode = async (req, res, next) => {
  try {
    const activationCodes = await ActivationCode.findByEpisodeId(
      req.params.episodeId,
    );
    return res.status(200).json(activationCodes);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const createActivationCode = async (req, res, next) => {
  try {
    if (req.body.partnerId) {
      const find = await ActivationCode.findByEpisodeAndPartnerId(
        req.body.episodeId,
        req.body.partnerId,
      );
      if (find.length > 0) {
        return next(
          createError(409, 'Partner already has a code for this episode', {
            level: 'INFO',
          }),
        );
      }
    } else {
      const find = await ActivationCode.findByEpisodeId(req.body.episodeId);
      if (find.length > 0) {
        return next(
          createError(409, 'Episode already has an activation code assigned.', {
            level: 'INFO',
          }),
        );
      }
    }

    const activationCode = await ActivationCode.create(req.body);
    return res.status(200).json(activationCode[0]);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const deleteActivationCode = async (req, res, next) => {
  try {
    await ActivationCode.del(req.params.id);
    return res.status(200).end();
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const assignProduct = async (req, res, next) => {
  try {
    let { userId } = req.body;

    if (req.body.username) {
      userId = await Auth.retrieveUserByUsername(req.body.username);
      userId = userId.response.user.id;
    }

    const activationCode = await ActivationCode.findByCode(
      req.body.activationCode,
    );

    if (activationCode.length > 0) {
      const ownedProducts = await Product.findByUUID(userId);

      if (ownedProducts.length > 0) {
        if (
          ownedProducts.some(el => el.episodeId === activationCode[0].episodeId)
        ) {
          return next(
            createError(409, 'Episode already added', { level: 'INFO' }),
          );
        }
      }

      if (activationCode[0].fromPartner || req.query.admin) {
        const assignment = await Product.assignToChild({
          userId,
          episodeId: activationCode[0].episodeId,
          codeUsed: req.body.activationCode,
        });

        return res.status(200).json(assignment[0]);
      }
    }

    return next(
      createError(404, 'Activation code is not valid.', { level: 'INFO' }),
    );
  } catch (err) {
    if (err.statusCode === 404) {
      return next(
        createError(404, 'Username is invalid.', {
          level: 'INFO',
        }),
      );
    }
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getOwnedProducts = async (req, res, next) => {
  try {
    const ownedProducts = await Product.findByUUID(req.params.userId);

    return res.status(200).json(ownedProducts);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  getAllActivationCodes,
  getEpisodeActivationCode,
  createActivationCode,
  deleteActivationCode,
  assignProduct,
  getOwnedProducts,
};
