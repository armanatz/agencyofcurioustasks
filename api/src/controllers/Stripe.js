import jwt from 'jsonwebtoken';
import createError from 'http-errors';
import StripeClient from 'stripe';
import dayjs from 'dayjs';

import vars from '../config/vars.js';
import Auth from '../config/fusionauth.js';
import Mail from '../helpers/mail.js';

import Address from '../queries/Address.js';
import Stripe from '../queries/Stripe.js';
import Wallet from '../queries/Wallet.js';
import Order from '../queries/Order.js';
import Season from '../queries/Season.js';
import Episode from '../queries/Episode.js';

const stripeSdk = new StripeClient(vars.stripe.privateKey);

const getStripeCustomer = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    const customer = await stripeSdk.customers.retrieve('cus_JByyvtoDVyzkuV');

    return res.status(200).json({ ...customer });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getStripeProducts = async (req, res, next) => {
  try {
    const products = await stripeSdk.products.list({
      active: true,
      limit: 100,
    });

    const prices = await stripeSdk.prices.list({
      active: true,
      limit: 100,
    });

    const data = prices.data.map(price => {
      let obj = {};
      products.data.forEach(product => {
        if (price.product === product.id) {
          obj = {
            productActive: product.active,
            productId: product.id,
            productName: product.name,
            priceId: price.id,
            priceName: price.nickname,
            priceActive: price.active,
            price: price.unit_amount,
            priceDecimal: price.unit_amount_decimal,
            pricingType: price.type,
            currency: price.currency,
            priceRecurring: price.recurring,
          };
        }
      });
      return obj;
    });

    return res.status(200).json(data);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getStripeCustomerSubs = async (req, res, next) => {
  try {
    const subscriptions = await stripeSdk.subscriptions.list({
      customer: 'cus_JByyvtoDVyzkuV',
    });

    return res.status(200).json(subscriptions);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const createStripeCustomer = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    const authGetUserById = await Auth.retrieveUser(decodedJWT.sub);
    const addresses = await Address.findByUUID(decodedJWT.sub, false);

    if (addresses.length === 0) {
      return res.status(400).end();
    }

    const customerFromDB = {
      ...authGetUserById.response.user,
      addresses: [...addresses],
    };

    const doesCustomerExist = await Stripe.findCustomerByUUID(decodedJWT.sub);

    if (doesCustomerExist.length > 0) {
      const customer = await stripeSdk.customers.retrieve(
        doesCustomerExist[0].id,
      );
      return res.status(200).json(customer);
    }

    const customer = await stripeSdk.customers.create({
      email: customerFromDB.email,
      name: customerFromDB.fullName,
      phone: customerFromDB.mobilePhone,
      shipping: {
        address: {
          line1: customerFromDB.addresses[0].line1,
          ...(customerFromDB.addresses[0].line2 !== null && {
            line2: customerFromDB.addresses[0].line2,
          }),
          city: customerFromDB.addresses[0].city,
          country: 'MY',
          postal_code: customerFromDB.addresses[0].postCode,
          state: customerFromDB.addresses[0].state,
        },
        name: customerFromDB.fullName,
        phone: customerFromDB.mobilePhone,
      },
    });

    await Stripe.createCustomer(customer.id, decodedJWT.sub);

    return res.status(200).json(customer);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const createStripePaymentIntent = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    let walletBalance = await Wallet.balance(decodedJWT.sub);
    walletBalance = walletBalance[0].balance;

    const price = await stripeSdk.prices.retrieve(req.body.priceId);
    let { unit_amount } = price;

    if (walletBalance > 0) {
      if (walletBalance < unit_amount / 100) {
        unit_amount = unit_amount - walletBalance * 100;
      } else if (walletBalance >= unit_amount / 100) {
        unit_amount = 0;
      }
      await Wallet.update(decodedJWT.sub, 'subtract', price.unit_amount / 100);
    }

    if (req.body.discountCode) {
      const promoCode = await Stripe.findPromoCode(req.body.discountCode);

      const coupon = await stripeSdk.coupons.retrieve(promoCode[0].couponId);

      if (coupon.amount_off !== null) {
        unit_amount = unit_amount - coupon.amount_off;
      } else if (coupon.percent_off !== null) {
        unit_amount = unit_amount * (1 - coupon.percent_off / 100);
      }
    }

    if (unit_amount < 0) {
      unit_amount = 0;
    }

    if (unit_amount === 0) {
      return res.status(200).json({ clientSecret: 'n/a' });
    }

    const paymentIntent = await stripeSdk.paymentIntents.create({
      customer: req.body.customerId,
      setup_future_usage: 'off_session',
      amount: unit_amount,
      currency: price.currency,
    });

    return res.status(201).json({ clientSecret: paymentIntent.client_secret });
  } catch (err) {
    if (err.raw) {
      return next(
        createError(err.statusCode, err.raw.message, { level: 'ERROR' }),
      );
    }
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const createStripeSetupIntent = async (req, res, next) => {
  try {
    let returnUrl = `${vars.server.proto}://${
      vars.server.env === 'development' &&
      vars.server.name !== 'staging.agencyofcurioustasks.com'
        ? vars.server.domain
        : vars.server.name
    }/${req.body.fromPage}`;

    const setupIntent = await stripeSdk.setupIntents.create({
      customer: req.body.customerId,
      payment_method: req.body.paymentMethodId,
      confirm: true,
      usage: 'off_session',
      return_url: returnUrl,
    });

    return res.status(200).json(setupIntent);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const createStripeSubscription = async (req, res, next) => {
  try {
    await stripeSdk.paymentMethods.attach(req.body.paymentMethodId, {
      customer: req.body.customerId,
    });

    let couponId = undefined;

    if (req.body.discountCode) {
      const promoCode = await Stripe.findPromoCode(req.body.discountCode);
      ({ couponId } = promoCode[0]);
    }

    // Create the subscription
    const subscription = await stripeSdk.subscriptions.create({
      customer: req.body.customerId,
      default_payment_method: req.body.paymentMethodId,
      items: [{ price: req.body.priceId }],
      cancel_at: req.body.cancelByDate,
      ...(couponId && { coupon: couponId }),
      metadata: {
        childId: req.body.childId,
        childName: req.body.childName,
      },
      expand: ['latest_invoice.payment_intent'],
    });

    if (
      subscription.latest_invoice.payment_intent === 'requires_payment_method'
    ) {
      if (
        subscription.latest_invoice.payment_intent.last_payment_error !== null
      ) {
        return next(
          createError(
            402,
            subscription.latest_invoice.payment_intent.last_payment_error
              .message,
            { level: 'INFO' },
          ),
        );
      }
    }

    let parentId = await Stripe.findCustomerByStripeId(req.body.customerId);
    parentId = parentId[0].userId;

    await Stripe.createSubscription({
      id: subscription.id,
      parentId,
      childId: req.body.childId,
      productId: subscription.items.data[0].price.product,
      status: subscription.status,
      startDate: dayjs.unix(subscription.start_date).toISOString(),
      endDate: dayjs.unix(subscription.cancel_at).toISOString(),
    });

    return res.status(200).json(subscription);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const retrieveStripeCustomerPaymentMethods = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    const customer = await Stripe.findCustomerByUUID(decodedJWT.sub);

    if (customer.length > 0) {
      const paymentMethods = await stripeSdk.paymentMethods.list({
        customer: customer[0].id,
        type: 'card',
        limit: 100,
      });

      return res.status(200).json({
        hasMore: paymentMethods.has_more,
        data: paymentMethods.data.map(pm => ({
          id: pm.id,
          billingDetails: pm.billing_details,
          brand: pm.card.brand,
          expMonth: pm.card.exp_month,
          expYear: pm.card.exp_year,
          last4: pm.card.last4,
        })),
      });
    }

    return res.status(200).json({ hasMore: false, data: [] });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const retrieveAllSubscriptions = async (req, res, next) => {
  const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
    algorithms: ['HS256'],
  });

  try {
    let parent = await Auth.retrieveUser(decodedJWT.sub);
    parent = parent.response.user;

    let subscriptions = await Stripe.findAllSubscriptionsByParentId(
      decodedJWT.sub,
    );

    subscriptions = await Promise.all(
      subscriptions.map(async el => {
        const subscription = await stripeSdk.subscriptions.retrieve(el.id, {
          expand: ['latest_invoice.payment_intent'],
        });

        let child = await Auth.retrieveUser(el.childId);
        child = child.response.user;

        return {
          subscription,
          parent,
          child,
        };
      }),
    );

    return res.status(200).json(subscriptions);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const retrieveStripeSubscription = async (req, res, next) => {
  try {
    const subscription = await stripeSdk.subscriptions.retrieve(req.params.id, {
      expand: ['latest_invoice.payment_intent'],
    });

    return res.status(200).json(subscription);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const deleteCustomerPaymentMethod = async (req, res, next) => {
  try {
    await stripeSdk.paymentMethods.detach(req.params.id);
    return res.status(200).end();
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const webhookListerner = async (req, res, next) => {
  try {
    let event = req.body;

    if (vars.stripe.webhookSecret) {
      const signature = req.headers['stripe-signature'];

      event = stripeSdk.webhooks.constructEvent(
        req.body,
        signature,
        vars.stripe.webhookSecret,
      );
    }

    switch (event.type) {
      case 'coupon.created': {
        const doesExist = await Stripe.findCouponById(event.data.object.id);

        if (doesExist.length > 0) {
          return res.status(200).end();
        }

        await Stripe.createCoupon({
          id: event.data.object.id,
          name: event.data.object.name,
        });

        return res.status(200).end();
      }
      case 'coupon.deleted': {
        const doesExist = await Stripe.findCouponById(event.data.object.id);

        if (doesExist.length === 0) {
          return res.status(200).end();
        }

        await Stripe.deleteCoupon(event.data.object.id);
        return res.status(200).end();
      }
      case 'promotion_code.created': {
        const doesExist = await Stripe.findPromoCode(event.data.object.code);

        if (doesExist.length > 0) {
          return res.status(200).end();
        }

        await Stripe.createPromoCode({
          id: event.data.object.id,
          couponId: event.data.object.coupon.id,
          code: event.data.object.code,
        });

        return res.status(200).end();
      }
      case 'promotion_code.updated': {
        const doesExist = await Stripe.findPromoCode(event.data.object.code);

        if (doesExist.length === 0) {
          return res.status(200).end();
        }

        await Stripe.updatePromoCode(event.data.object.id, {
          active: event.data.object.active,
        });

        return res.status(200).end();
      }
      case 'invoice.created': {
        let product;
        let episode;

        if (event.data.object.subscription === null) {
          product = await Episode.findByStripeId(
            event.data.object.lines.data[0].price.product,
          );
          episode = product[0];
        } else {
          product = await Season.findByStripeId(
            event.data.object.lines.data[0].price.product,
          );
          const invoices = await Stripe.findAllInvoicesBySubId(
            event.data.object.subscription,
          );
          const episodesInSeason = await Episode.findBySeasonId(product[0].id);
          episode = episodesInSeason[invoices.length];
        }

        await Stripe.createInvoice({
          id: event.data.object.id,
          subscriptionId: event.data.object.subscription,
          episodeId: episode.id,
          status: event.data.object.status,
          periodStartDate: dayjs
            .unix(event.data.object.period_start)
            .toISOString(),
          periodEndDate: dayjs.unix(event.data.object.period_end).toISOString(),
        });

        await stripeSdk.invoices.update(event.data.object.id, {
          metadata: {
            episodeId: episode.id,
            episodeName: episode.title,
          },
        });

        return res.status(200).end();
      }
      case 'invoice.finalized': {
        await Stripe.updateInvoice(event.data.object.id, {
          status: event.data.object.status,
        });
        return res.status(200).end();
      }
      case 'invoice.paid': {
        if (event.data.object.subscription !== null) {
          const episodeId = event.data.object.metadata.episodeId;

          const subscription = await Stripe.findSubscriptionById(
            event.data.object.subscription,
          );

          let orders = await Order.findByParentId(subscription[0].parentId);
          orders = orders.filter(
            el =>
              el.childId === subscription[0].childId &&
              dayjs(el.createdAt).startOf('day').format('YYYY-MM-DD') ===
                dayjs(subscription[0].startDate)
                  .startOf('day')
                  .format('YYYY-MM-DD'),
          );

          const orderItems = await Order.findAllItemsByOrderId(orders[0].id);
          const thisOrderItem = orderItems.find(
            el => el.episodeId === episodeId,
          );

          await Order.updateItem(thisOrderItem.id, {
            statusCode: 1,
            lastUpdateBy: null,
          });
        }

        await Stripe.updateInvoice(event.data.object.id, {
          status: event.data.object.status,
          paymentDate: dayjs
            .unix(event.data.object.status_transitions.paid_at)
            .toISOString(),
        });
        return res.status(200).end();
      }
      case 'invoice.payment_failed': {
        break;
      }
      case 'invoice.voided': {
        await Stripe.updateInvoice(event.data.object.id, {
          status: event.data.object.status,
        });
        return res.status(200).end();
      }
      case 'customer.subscription.updated': {
        await Stripe.updateSubscription(event.data.object.id, {
          status: event.data.object.status,
          ...(event.data.object.status === 'canceled' && {
            cancelDate: dayjs.unix(event.data.object.canceled_at).toISOString(),
          }),
        });
        return res.status(200).end();
      }
      default:
        console.log({ [event.type]: event });
        return res.status(200).end();
    }
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const retrieveProductPrice = async (req, res, next) => {
  try {
    const price = await Stripe.findPricesByProductId(req.params.id);
    return res.status(200).json(price);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const checkDiscountCode = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    const userId = decodedJWT.sub;

    const promoCode = await Stripe.findPromoCode(req.body.code, true);

    if (promoCode.length === 0) {
      return next(
        createError(404, 'Discount code is invalid.', { level: 'INFO' }),
      );
    }

    const coupon = await Stripe.findCouponById(promoCode[0].couponId);

    const stripePromoCode = await stripeSdk.promotionCodes.retrieve(
      promoCode[0].id,
      {
        expand: ['coupon.applies_to'],
      },
    );

    if (stripePromoCode.coupon.applies_to) {
      if (
        !stripePromoCode.coupon.applies_to.products.includes(req.body.productId)
      ) {
        return next(
          createError(
            403,
            'This discount code does not apply for this product.',
            { level: 'INFO' },
          ),
        );
      }
    }

    if (stripePromoCode.customer !== null) {
      const customerId = await Stripe.findCustomerByUUID(userId);
      if (customerId[0].id !== stripePromoCode.customer) {
        return next(
          createError(403, 'You are not eligible for this discount.', {
            level: 'INFO',
          }),
        );
      }
    }

    if (stripePromoCode.coupon.redeem_by !== null) {
      if (dayjs.unix(stripePromoCode.coupon.redeem_by).isBefore(dayjs())) {
        return next(
          createError(403, 'Discount code has expired.', { level: 'INFO' }),
        );
      }
    }

    if (stripePromoCode.expires_at !== null) {
      if (dayjs.unix(stripePromoCode.expires_at).isBefore(dayjs())) {
        return next(
          createError(403, 'Discount code has expired.', { level: 'INFO' }),
        );
      }
    }

    if (stripePromoCode.coupon.max_redemptions !== null) {
      if (coupon[0].timesRedeemed === stripePromoCode.max_redemptions) {
        return next(
          createError(403, 'Discount code has been full redeemed.', {
            level: 'INFO',
          }),
        );
      }
    }

    if (stripePromoCode.max_redemptions !== null) {
      if (promoCode[0].timesRedeemed === stripePromoCode.max_redemptions) {
        return next(
          createError(403, 'Discount code has been full redeemed.', {
            level: 'INFO',
          }),
        );
      }
    }

    if (stripePromoCode.restrictions.first_time_transaction) {
      const hasOrder = await Order.findByParentId(userId);
      if (hasOrder.length !== 0) {
        return next(
          createError(
            403,
            'Discount code is only applicable for your first-time purchase.',
            { level: 'INFO' },
          ),
        );
      }
    }

    if (stripePromoCode.restrictions.minimum_amount !== null) {
      if (req.body.unitAmount < stripePromoCode.restrictions.minimum_amount) {
        return next(
          createError(
            403,
            `Discount code is only applicable to orders above ${stripePromoCode.restrictions.minimum_amount_currency.toUpperCase()} ${
              stripePromoCode.restrictions.minimum_amount / 100
            }.`,
            { level: 'INFO' },
          ),
        );
      }
    }

    return res.status(200).json(stripePromoCode);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const cancelSubscription = async (req, res, next) => {
  const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
    algorithms: ['HS256'],
  });

  try {
    let user = await Auth.retrieveUser(decodedJWT.sub);
    user = user.response.user;

    const subscription = await stripeSdk.subscriptions.del(req.params.id);

    await Stripe.updateSubscription(req.params.id, {
      cancelDate: dayjs.unix(subscription.canceled_at).toISOString(),
      status: subscription.status,
    });

    await Mail.send({
      to: user.email,
      subject: 'Your Agency of Curious Tasks subscription has been cancelled',
      templateName: 'subscription_canceled',
      templateContext: {
        fullName: user.fullName,
      },
    });

    return res.status(200).json(subscription);
  } catch (err) {
    return next(createError(err.statusCode || 500, err, { level: 'ERROR' }));
  }
};

export default {
  getStripeCustomer,
  getStripeProducts,
  getStripeCustomerSubs,
  createStripeCustomer,
  createStripePaymentIntent,
  createStripeSetupIntent,
  createStripeSubscription,
  retrieveStripeCustomerPaymentMethods,
  retrieveAllSubscriptions,
  retrieveStripeSubscription,
  deleteCustomerPaymentMethod,
  webhookListerner,
  retrieveProductPrice,
  checkDiscountCode,
  cancelSubscription,
};
