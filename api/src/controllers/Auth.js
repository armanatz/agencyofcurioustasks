import createError from 'http-errors';

import vars from '../config/vars.js';
import Auth from '../config/fusionauth.js';

import mail from '../helpers/mail.js';

const loginUser = async (req, res, next) => {
  const { loginId, password, remember } = req.body;

  if (!loginId) {
    return next(createError(400, 'No login ID provided.', { level: 'INFO' }));
  }

  if (!password) {
    return next(createError(400, 'No password provided.', { level: 'INFO' }));
  }

  try {
    const authLogin = await Auth.login({
      applicationId: vars.fusionauth.appId,
      loginId,
      password,
    });

    if (!authLogin.response.user.verified) {
      return res
        .status(403)
        .json({ message: 'Email has not been verified.', email: loginId });
    }

    const cookieOptions = {
      httpOnly: true,
      secure: vars.server.env !== 'development',
    };

    return res
      .status(200)
      .cookie('_rtk', authLogin.response.refreshToken, {
        ...cookieOptions,
        ...(remember && { maxAge: 1000 * 60 * 43200 }),
      })
      .cookie('_atk', authLogin.response.token, {
        ...cookieOptions,
        ...(remember && { maxAge: 1000 * 60 * 30 }),
      })
      .cookie(
        '_meta',
        JSON.stringify({
          ...(remember && { remember }),
        }),
        {
          maxAge: 1000 * 60 * 43200 * 120,
          secure: vars.server.env !== 'development',
        },
      )
      .json(authLogin.response);
  } catch (err) {
    if (err.statusCode) {
      switch (err.statusCode) {
        case 404:
          return next(
            createError(404, 'Invalid login ID or password provided.', {
              level: 'INFO',
            }),
          );
        default:
          return next(createError(err.statusCode, err, { level: 'ERROR' }));
      }
    }
    return next(err);
  }
};

const logoutUser = async (req, res, next) => {
  try {
    await Auth.logout(false, req.cookies._rtk);
    return res.clearCookie('_rtk').clearCookie('_atk').status(200).json({
      message: 'Logged out.',
    });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getAuthStatus = async (req, res, next) => {
  try {
    if (req.cookies._atk) {
      const authGetUserWithJWT = await Auth.retrieveUserUsingJWT(
        req.cookies._atk,
      );
      if (authGetUserWithJWT.response) {
        return res.status(200).json({ isAuthenticated: true });
      }
    }
    return res.status(200).json({ isAuthenticated: false });
  } catch (err) {
    if (Object.keys(err).length > 1) {
      if (
        err.statusCode === 400 &&
        Object.keys(err.exception.fieldErrors).includes('refreshToken')
      ) {
        if (
          err.exception.fieldErrors.refreshToken[0].code ===
          '[invalid]refreshToken'
        ) {
          return res.status(200).json({ isAuthenticated: false });
        }
      }
      return next(createError(err.statusCode, err, { level: 'ERROR' }));
    }
    return res.status(200).json({ isAuthenticated: false });
  }
};

const passwordForgot = async (req, res, next) => {
  try {
    const authGetUserWithEmail = await Auth.retrieveUserByEmail(req.body.email);
    const { user } = authGetUserWithEmail.response;
    const forgotPassword = await Auth.forgotPassword({
      loginId: user.email,
      sendForgotPasswordEmail: false,
    });
    const { changePasswordId } = forgotPassword.response;

    const templateContext = {
      fullName: user.fullName,
      resetLink: `${vars.server.proto}://${
        vars.server.env === 'development' &&
        vars.server.name !== 'staging.agencyofcurioustasks.com'
          ? vars.server.domain
          : vars.server.name
      }/reset-password?id=${changePasswordId}`,
    };

    await mail.send({
      to: user.email,
      subject: 'Reset your ACT account password',
      templateName: 'reset_password',
      templateContext,
    });

    return res.status(200).json({
      message: `An email has been sent to ${user.email} with a link to reset your password`,
    });
  } catch (err) {
    if (err.statusCode === 404) {
      return res.status(200).json({
        message: `An email has been sent to ${req.body.email} with a link to reset your password`,
      });
    }
    return next(createError(err.statusCode, err, { level: 'ERROR' }));
  }
};

const passwordReset = async (req, res, next) => {
  try {
    await Auth.changePassword(req.body.changePasswordId, {
      password: req.body.password,
    });

    return res.status(200).json({
      message:
        'Your password has been successfully reset. You may now use your new password to login.',
    });
  } catch (err) {
    if (err.statusCode === 404) {
      return next(
        createError(
          err.statusCode,
          'Password reset link has expired. Please request a new one.',
          { level: 'INFO' },
        ),
      );
    }
    return next(createError(err.statusCode, err, { level: 'ERROR' }));
  }
};

export default {
  loginUser,
  logoutUser,
  getAuthStatus,
  passwordForgot,
  passwordReset,
};
