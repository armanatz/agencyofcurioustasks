import jwt from 'jsonwebtoken';
import createError from 'http-errors';

import vars from '../config/vars.js';
import Auth from '../config/fusionauth.js';
import Mail from '../helpers/mail.js';
import Mailchimp from '../helpers/mailchimp.js';

import User from '../queries/User.js';
import Address from '../queries/Address.js';
import Referral from '../queries/Referral.js';
import Partner from '../queries/Partner.js';
import Product from '../queries/Product.js';
import Wallet from '../queries/Wallet.js';
import Setting from '../queries/Setting.js';

const createUser = async (req, res, next) => {
  const { type, password, firstName, lastName, timezone } = req.body;

  if (!type) {
    return next(
      createError(
        400,
        'User type was not specified for this resource yet it is required.',
        { level: 'INFO' },
      ),
    );
  }

  const loginId = type === 'Adult' ? req.body.email : req.body.username;

  if (!loginId) {
    return next(
      createError(
        400,
        `${type === 'Adult' ? 'Email' : 'Username'} is required.`,
        { level: 'INFO' },
      ),
    );
  }

  if (!password) {
    return next(createError(400, 'Password is required.', { level: 'INFO' }));
  }

  if (!firstName) {
    return next(createError(400, 'First name is required.', { level: 'INFO' }));
  }

  if (type === 'Adult' && !req.body.mobilePhone) {
    return next(
      createError(400, 'Phone number is required.', { level: 'INFO' }),
    );
  }

  if (type === 'Child') {
    if (!req.body.birthDate) {
      return next(
        createError(400, 'Birth date is required.', { level: 'INFO' }),
      );
    }

    if (!req.body.gender) {
      return next(createError(400, 'Gender is required.', { level: 'INFO' }));
    }
  }

  if (req.body.email) {
    const isPartner = await Partner.findByEmail(req.body.email);
    const isBlocked = isPartner.some(entry => entry.blockRegistration);

    if (isBlocked) {
      return next(
        createError(
          401,
          'This email is blocked from registration. If you think this is a mistake, please contact us for support.',
        ),
      );
    }
  }

  const doesUserExist = await User.findByLoginId(loginId);

  if (doesUserExist.length !== 0) {
    return next(
      createError(
        409,
        `${
          type === 'Adult' ? 'Email' : 'Username'
        } has already been taken by another user.`,
        { level: 'INFO' },
      ),
    );
  }

  try {
    let parentData = undefined;

    if (type === 'Child') {
      parentData = await Auth.retrieveUserUsingJWT(req.cookies._atk);
    }

    const skipVerification =
      type === 'Child' ||
      vars.server.env === 'development' ||
      vars.server.name === 'staging.agencyofcurioustasks.com';

    const authRegister = await Auth.register(null, {
      generateAuthenticationToken: false,
      registration: {
        applicationId: vars.fusionauth.appId,
        roles: [type],
        timezone: timezone ? timezone : 'Asia/Kuala_Lumpur',
        preferredLanguages: ['en'],
      },
      user: {
        ...(type === 'Adult'
          ? { email: loginId, mobilePhone: req.body.mobilePhone }
          : { username: loginId }),
        password,
        birthDate: type === 'Adult' ? '1900-01-01' : req.body.birthDate,
        firstName,
        ...(lastName && { lastName, fullName: `${firstName} ${lastName}` }),
        ...(!lastName && { fullName: firstName }),
        encryptionScheme: 'bcrypt',
        factor: 10,
        timezone: timezone ? timezone : 'Asia/Kuala_Lumpur',
        preferredLanguages: ['en'],
        ...(type === 'Child' && {
          parentEmail: parentData.response.user.email,
          imageUrl: req.body.imageUrl,
          data: {
            gender: req.body.gender,
          },
        }),
      },
      skipVerification,
    });

    await User.create({
      id: authRegister.response.user.id,
      loginId,
      role: type,
    });

    if (type === 'Adult') {
      await Referral.create(authRegister.response.user.id);
      await Wallet.create(authRegister.response.user.id);

      if (req.body.referralCode) {
        const doesReferralCodeExist = await Referral.findByCode(
          req.body.referralCode,
        );

        if (doesReferralCodeExist.length > 0) {
          await Referral.capture(
            authRegister.response.user.id,
            req.body.referralCode,
          );

          let referralDiscount = await Setting.findByName('referralDiscount');
          referralDiscount = referralDiscount[0].value;

          await Wallet.update(
            authRegister.response.user.id,
            'add',
            referralDiscount,
          );
        }
      }

      await Auth.createFamily(null, {
        familyMember: {
          userId: authRegister.response.user.id,
          owner: true,
          role: type,
        },
      });
    } else {
      const getFamily = await Auth.retrieveFamilies(
        parentData.response.user.id,
      );
      await Auth.addUserToFamily(getFamily.response.families[0].id, {
        familyMember: {
          userId: authRegister.response.user.id,
          role: type,
        },
      });
    }

    if (
      type === 'Adult' &&
      vars.server.name !== 'staging.agencyofcurioustasks.com'
    ) {
      const emailVerificationId = await Auth.generateEmailVerificationId(
        authRegister.response.user.email,
      );

      await Mail.send({
        to: authRegister.response.user.email,
        subject: 'Welcome to the Agency of Curious Tasks',
        templateName: 'welcome',
        templateContext: {
          fullName: authRegister.response.user.fullName,
          verifyLink: `${vars.server.proto}://${
            vars.server.env === 'development' &&
            vars.server.name !== 'staging.agencyofcurioustasks.com'
              ? vars.server.domain
              : vars.server.name
          }/verify/email?id=${emailVerificationId.response.verificationId}`,
        },
      });
    }

    return res.status(201).json({
      message:
        'Please check your inbox for a verification email in order to access your account.',
    });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const authDeleteUser = await Auth.deleteUser(req.params.userId);
    await User.del(req.params.userId);
    return res.status(200).json(authDeleteUser);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getCurrentUser = async (req, res, next) => {
  const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
    algorithms: ['HS256'],
  });

  try {
    const authGetUserById = await Auth.retrieveUser(decodedJWT.sub);

    const { user } = authGetUserById.response;

    const registration = user.registrations.filter(
      reg => reg.applicationId === vars.fusionauth.appId,
    )[0];

    const returnObj = {
      active: user.active,
      ...(user.email && {
        addresses: await Address.findByUUID(user.id, false),
        email: user.email,
        mobilePhone: user.mobilePhone,
      }),
      firstName: user.firstName,
      fullName: user.fullName,
      id: user.id,
      ...(user.username && {
        birthDate: user.birthDate,
        data: user.data,
        imageUrl: user.imageUrl,
        ownedProducts: await Product.findByUUID(user.id),
        username: user.username,
      }),
      lastName: user.lastName,
      roles: registration.roles,
      timezone: user.timezone,
      verified: user.verified,
    };

    return res.status(200).json(returnObj);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getUser = async (req, res, next) => {
  try {
    const authGetUserById = await Auth.retrieveUser(req.params.userId);

    const { user } = authGetUserById.response;

    const registration = user.registrations.filter(
      reg => reg.applicationId === vars.fusionauth.appId,
    )[0];

    const returnObj = {
      active: user.active,
      ...(user.email && {
        addresses: await Address.findByUUID(user.id, false),
        email: user.email,
        mobilePhone: user.mobilePhone,
      }),
      firstName: user.firstName,
      fullName: user.fullName,
      id: user.id,
      ...(user.username && {
        birthDate: user.birthDate,
        data: user.data,
        imageUrl: user.imageUrl,
        ownedProducts: await Product.findByUUID(user.id),
        username: user.username,
      }),
      lastName: user.lastName,
      roles: registration.roles,
      timezone: user.timezone,
      verified: user.verified,
    };

    return res.status(200).json(returnObj);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const updateCurrentUser = async (req, res, next) => {
  const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
    algorithms: ['HS256'],
  });

  const { firstName, lastName } = req.body;
  const data = { ...req.body };

  if (data.gender) {
    data.data = { gender: data.gender };
    delete data.gender;
  }

  const authGetUserById = await Auth.retrieveUser(decodedJWT.sub);

  try {
    if (firstName || lastName) {
      const { user } = authGetUserById.response;
      data.fullName = `${firstName ? firstName : user.firstName} ${
        lastName ? lastName : user.lastName
      }`;
    }

    const authUpdateUser = await Auth.patchUser(decodedJWT.sub, {
      user: data,
    });

    if (
      authGetUserById.response.user.passwordLastUpdateInstant !==
      authUpdateUser.response.user.passwordLastUpdateInstant
    ) {
      return res
        .clearCookie('_rtk')
        .clearCookie('_atk')
        .status(200)
        .json(authUpdateUser.response);
    }

    return res.status(200).json(authUpdateUser.response);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const updateUser = async (req, res, next) => {
  const { firstName, lastName } = req.body;
  const data = { ...req.body };

  if (data.gender) {
    data.data = { gender: data.gender };
    delete data.gender;
  }

  try {
    if (firstName || lastName) {
      const authGetUserById = await Auth.retrieveUser(req.params.userId);
      const { user } = authGetUserById.response;
      data.fullName = `${firstName ? firstName : user.firstName} ${
        lastName ? lastName : user.lastName
      }`;
    }

    const authUpdateUser = await Auth.patchUser(req.params.userId, {
      user: data,
    });

    return res.status(200).json(authUpdateUser.response);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getCurrentUsersReferralCode = async (req, res, next) => {
  const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
    algorithms: ['HS256'],
  });

  try {
    const referralCode = await Referral.findByUUID(decodedJWT.sub);

    return res.status(200).send(referralCode[0].code);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getUsersReferralCode = async (req, res, next) => {
  try {
    const referralCode = await Referral.findByUUID(req.params.userId);

    return res.status(200).send(referralCode[0].code);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getReferralCount = async (req, res, next) => {
  try {
    const count = await Referral.count(req.params.code);

    return res.status(200).send(count);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const contactRequest = async (req, res, next) => {
  try {
    const templateContext = {
      name: req.body.name,
      email: req.body.email,
      phone: req.body.phone,
      message: req.body.message,
    };

    await Mail.send({
      from: `${req.body.name} <${req.body.email}>`,
      to: 'enquiries@agencyofcurioustasks.com',
      subject: 'Enquiry about ACT (Contact Form)',
      templateName: 'contact_form',
      templateContext,
    });

    return res.status(200).json({
      message:
        'Thanks for contacting us. One of our agents will get back to you shortly',
    });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const retrieveCurrentUsersWalletBalance = async (req, res, next) => {
  try {
    const decodedJWT = jwt.verify(req.cookies._atk, vars.fusionauth.jwtKey, {
      algorithms: ['HS256'],
    });

    const balance = await Wallet.balance(decodedJWT.sub);
    return res.status(200).json(balance[0]);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const addUserToMailchimp = async (req, res, next) => {
  try {
    const mailchimpRes = await Mailchimp.addUserToMailchimp({
      email: req.body.email,
      tags: req.body.tags,
      ...(req.body.firstName && { firstName: req.body.firstName }),
      ...(req.body.lastName && { lastName: req.body.lastName }),
    });

    return res.status(200).json(mailchimpRes);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const verifyUsersEmail = async (req, res, next) => {
  try {
    await Auth.verifyEmail(req.body.verificationId);

    return res.status(200).json({
      message:
        'Email verification was successful. Please login using the email and password provided during signup.',
    });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const resendVerificationEmail = async (req, res, next) => {
  try {
    const authReq = await Auth.retrieveUserByEmail(req.body.email);

    const emailVerificationId = await Auth.generateEmailVerificationId(
      authReq.response.user.email,
    );

    await Mail.send({
      to: authReq.response.user.email,
      subject: 'Welcome to the Agency of Curious Tasks',
      templateName: 'welcome',
      templateContext: {
        fullName: authReq.response.user.fullName,
        verifyLink: `${vars.server.proto}://${
          vars.server.env === 'development' &&
          vars.server.name !== 'staging.agencyofcurioustasks.com'
            ? vars.server.domain
            : vars.server.name
        }/verify/email?id=${emailVerificationId.response.verificationId}`,
      },
    });

    return res.status(200).json({
      message: `Verification email has been sent to the inbox of ${authReq.response.user.email}. Please check your inbox or spam/junk folder for the email.`,
    });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getAllUsers = async (req, res, next) => {
  try {
    let users = User.getAll(req.query);
    users = await Promise.all(
      (
        await users
      ).map(async el => {
        const authGetUserById = await Auth.retrieveUser(el.id);
        const { user } = authGetUserById.response;
        const registration = user.registrations.filter(
          reg => reg.applicationId === vars.fusionauth.appId,
        )[0];

        return {
          active: user.active,
          ...(user.email && {
            addresses: await Address.findByUUID(user.id, false),
            email: user.email,
            mobilePhone: user.mobilePhone,
          }),
          firstName: user.firstName,
          fullName: user.fullName,
          id: user.id,
          ...(user.username && {
            birthDate: user.birthDate,
            data: user.data,
            imageUrl: user.imageUrl,
            ownedProducts: await Product.findByUUID(user.id),
            username: user.username,
          }),
          lastName: user.lastName,
          roles: registration.roles,
          timezone: user.timezone,
          verified: user.verified,
        };
      }),
    );

    return res.status(200).json(users);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getAllAdmins = async (req, res, next) => {
  try {
    let admins = await User.findAdmins();
    admins = await Promise.all(
      admins.map(async el => {
        const authGetUserById = await Auth.retrieveUser(el.id);
        const { user } = authGetUserById.response;
        const registration = user.registrations.filter(
          reg => reg.applicationId === vars.fusionauth.appId,
        )[0];

        return {
          active: user.active,
          ...(user.email && {
            addresses: await Address.findByUUID(user.id, false),
            email: user.email,
            mobilePhone: user.mobilePhone,
          }),
          firstName: user.firstName,
          fullName: user.fullName,
          id: user.id,
          ...(user.username && {
            birthDate: user.birthDate,
            data: user.data,
            imageUrl: user.imageUrl,
            ownedProducts: await Product.findByUUID(user.id),
            username: user.username,
          }),
          lastName: user.lastName,
          roles: registration.roles,
          timezone: user.timezone,
          verified: user.verified,
        };
      }),
    );

    return res.status(200).json(admins);
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const getAllAdminRoles = async (req, res, next) => {
  try {
    const authRoles = await Auth.retrieveApplication(vars.fusionauth.appId);
    return res
      .status(200)
      .json(
        authRoles.response.application.roles.filter(
          role =>
            role.name !== 'Adult' &&
            role.name !== 'Child' &&
            role.name !== 'Superuser',
        ),
      );
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const addOrUpdateAdmin = async (req, res, next) => {
  try {
    let userDetails = await Auth.retrieveUserByEmail(
      req.body.email || req.params.email,
    );
    userDetails = userDetails.response.user;

    if (userDetails.registrations[0].roles.includes(req.body.role)) {
      return next(
        createError(
          409,
          `This user has already been assigned the ${req.body.role} role.`,
          {
            level: 'INFO',
          },
        ),
      );
    }

    await Auth.updateRegistration(userDetails.id, {
      registration: {
        applicationId: vars.fusionauth.appId,
        roles: [...userDetails.registrations[0].roles, req.body.role],
      },
    });

    if (req.method === 'POST') {
      await User.createAdmin({
        id: userDetails.id,
        email: userDetails.email,
        roles: [...userDetails.registrations[0].roles, req.body.role]
          .filter(role => role !== 'Adult')
          .toString(),
      });
    } else {
      await User.updateAdmin(req.params.email, {
        roles: [...userDetails.registrations[0].roles, req.body.role]
          .filter(role => role !== 'Adult')
          .sort((a, b) => a.localeCompare(b))
          .toString(),
      });
    }

    return res
      .status(200)
      .json({ message: `User added to the ${req.body.role} role.` });
  } catch (err) {
    if (err.statusCode === 404) {
      return next(
        createError(
          404,
          'A user with the specified email was not found in the system. Have they signed up?',
          {
            level: 'INFO',
          },
        ),
      );
    }

    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

const deleteAdmin = async (req, res, next) => {
  try {
    let userDetails = await Auth.retrieveUser(req.params.id);
    userDetails = userDetails.response.user;

    await Auth.updateRegistration(userDetails.id, {
      registration: {
        applicationId: vars.fusionauth.appId,
        roles: userDetails.registrations[0].roles.filter(
          role => role === 'Adult',
        ),
      },
    });

    await User.deleteAdmin(req.params.id);

    return res
      .status(200)
      .json({ message: 'User removed entirely as an admin.' });
  } catch (err) {
    return next(
      createError(err.statusCode ? err.statusCode : 500, err, {
        level: 'ERROR',
      }),
    );
  }
};

export default {
  createUser,
  deleteUser,
  getCurrentUser,
  getUser,
  updateCurrentUser,
  updateUser,
  getCurrentUsersReferralCode,
  getUsersReferralCode,
  getReferralCount,
  contactRequest,
  retrieveCurrentUsersWalletBalance,
  addUserToMailchimp,
  verifyUsersEmail,
  resendVerificationEmail,
  getAllUsers,
  getAllAdmins,
  getAllAdminRoles,
  addOrUpdateAdmin,
  deleteAdmin,
};
