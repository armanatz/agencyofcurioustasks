import fsPromise from 'fs/promises';
import path from 'path';
import mjml2html from 'mjml';
import Handlebars from 'handlebars';

import mailgun from '../config/mailgun.js';

import vars from '../config/vars.js';

const send = async ({
  from = 'The ACT Team <no-reply@agencyofcurioustasks.com>',
  to,
  subject,
  templateName,
  templateContext,
}) => {
  try {
    const pathToMJML = `./src/emails/${templateName}.mjml`;

    const fileToRead = await fsPromise.readFile(
      path.resolve(pathToMJML),
      'utf-8',
    );

    const htmlOutput = mjml2html(fileToRead);

    Handlebars.registerHelper('nl2br', (text, isXhtml) => {
      const breakTag = isXhtml ? '<br />' : '<br>';
      const withBr = Handlebars.escapeExpression(text).replace(
        /([^>\r\n]?)(\r\n|\n\r|\r|\n)/g,
        '$1' + breakTag + '$2',
      );
      return new Handlebars.SafeString(withBr);
    });

    const template = Handlebars.compile(htmlOutput.html);
    const html = template(templateContext);

    return await mailgun.messages.create(vars.mailgun.domain, {
      from,
      to,
      subject,
      // text,
      html,
    });
  } catch (err) {
    throw new Error(err);
  }
};

export default {
  send,
};
