import fs from 'fs';

import vars from '../config/vars.js';
import doClient from '../config/digitalocean.js';

const defaultParams = {
  Bucket: vars.digitalocean.spacesName,
};

const upload = async (file, key) => {
  const uploadParams = {
    ...defaultParams,
    Key: key,
    ACL: 'public-read',
  };

  let fileStream = fs.createReadStream(file.path);

  fileStream.on('error', err => {
    throw new Error(err);
  });

  uploadParams.Body = fileStream;

  fs.unlink(file.path, err => {
    if (err) {
      throw new Error(err);
    }
  });

  const upload = await doClient.upload(uploadParams).promise();

  return upload;
};

const deleteFiles = async (fileNames = [], path = '') => {
  const objectsToDelete = fileNames.map(name => ({
    Key: `${vars.server.env}${path}/${name}`,
  }));

  const request = await doClient
    .deleteObjects({
      ...defaultParams,
      Delete: { Objects: objectsToDelete, Quiet: true },
    })
    .promise();

  return request;
};

export default {
  upload,
  deleteFiles,
};
