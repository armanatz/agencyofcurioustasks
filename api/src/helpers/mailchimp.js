import Mailchimp from 'mailchimp-api-v3';
import md5 from 'md5';

import vars from '../config/vars.js';

const { apiKey, server } = vars.mailchimp;

const mailchimp = new Mailchimp(apiKey);

const listId = 'ef00616b5b';

const addUserToMailchimp = async ({ email, tags, firstName, lastName }) => {
  const emailHash = md5(email.toLowerCase());

  const addOrUpdateUser = await mailchimp.put(
    `/lists/${listId}/members/${emailHash}?skip_merge_validation=true`,
    {
      email_address: email,
      status_if_new: 'subscribed',
      ...(firstName && {
        merge_fields: {
          FNAME: firstName,
          ...(lastName && { LNAME: lastName }),
        },
      }),
    },
  );

  const currentTags = addOrUpdateUser.tags.map(tag => tag.name);

  const tagsToInsert = tags.filter(tag => !currentTags.includes(tag));

  await mailchimp.post(`/lists/${listId}/members/${emailHash}/tags`, {
    tags: tagsToInsert.map(tag => ({ name: tag, status: 'active' })),
  });

  return {
    message:
      'You have successfully subscribed to our newsletter. Stay tuned for updates and exciting offers from us!',
  };
};

export default { addUserToMailchimp };
