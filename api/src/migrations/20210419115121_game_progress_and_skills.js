export function up(knex) {
  return Promise.all([
    knex.schema
      .createTable('skills', table => {
        table.increments('id').primary();
        table.string('name');
        table.string('humanName');
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('userGameSkills', table => {
        table.bigIncrements('id').primary();
        table
          .uuid('userId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .index();
        table
          .integer('skillId')
          .references('id')
          .inTable('skills')
          .onDelete('CASCADE');
        table.integer('value').defaultTo(0);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('userGameData', table => {
        table.bigIncrements('id').primary();
        table
          .uuid('userId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .index();
        table
          .integer('episodeId')
          .references('id')
          .inTable('episodes')
          .onDelete('SET NULL');
        table.integer('progress');
        table.string('inventory');
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .then(() => {
        return knex.table('skills').insert([
          { name: 'problemSolving', humanName: 'Problem Solving' },
          { name: 'observation', humanName: 'Observation' },
          { name: 'mathGenius', humanName: 'Math Genius' },
          { name: 'criticalThinking', humanName: 'Critical Thinking' },
        ]);
      }),
  ]);
}

export function down(knex) {
  return knex.schema
    .table('userSkills', table => {
      table.dropForeign('userId');
      table.dropForeign('skillId');
    })
    .table('userGameData', table => {
      table.dropForeign('userId');
      table.dropForeign('episodeId');
    })
    .dropTableIfExists('skills')
    .dropTableIfExists('userSkills')
    .dropTableIfExists('userGameData');
}
