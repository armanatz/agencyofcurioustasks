export function up(knex) {
  return Promise.all([
    knex.schema
      .table('orders', table => {
        table.dropForeign('promoId');
      })
      .table('partners', table => {
        table.dropForeign('promoId');
      })
      .table('stripePromoCodes', table => {
        table.dropPrimary('stripePromoCodes_pkey');
        table.dropIndex('code');
      })

      .renameTable('stripePromoCodes', 'old_stripePromoCodes')

      .createTable('stripePromoCodes', table => {
        table.string('id').primary();
        table.string('couponId');
        table.string('code').index();
        table.integer('timesRedeemed').defaultTo(0);
        table.boolean('active').defaultTo(true);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .then(async () => {
        const oldItems = await knex.select().from('old_stripePromoCodes');
        await knex.table('stripePromoCodes').insert(oldItems);
        await knex.schema.table('orders', table => {
          table
            .foreign('promoId')
            .references('id')
            .inTable('stripePromoCodes')
            .onDelete('SET NULL');
        });
        await knex.schema.table('partners', table => {
          table
            .foreign('promoId')
            .references('id')
            .inTable('stripePromoCodes')
            .onDelete('SET NULL');
        });
        return knex.schema.dropTableIfExists('old_stripePromoCodes');
      }),
  ]);
}

export function down(knex) {
  return knex.schema.table('stripePromoCodes', table => {
    table.dropColumn('active');
  });
}
