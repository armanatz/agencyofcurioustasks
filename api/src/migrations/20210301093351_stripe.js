export function up(knex) {
  return knex.schema
    .createTable('stripeCustomers', table => {
      table.string('id').primary();
      table
        .uuid('userId')
        .references('id')
        .inTable('users')
        .onDelete('SET NULL')
        .index();
      table
        .timestamp('createdAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
      table
        .timestamp('updatedAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
    })

    .createTable('stripeCards', table => {
      table.string('id').primary();
      table
        .uuid('userId')
        .references('id')
        .inTable('users')
        .onDelete('CASCADE')
        .index();
      table.string('last4').notNullable();
      table.integer('expMonth').notNullable();
      table.integer('expYear').notNullable();
      table.string('brand');
      table
        .timestamp('createdAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
      table
        .timestamp('updatedAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
    })

    .createTable('stripeProducts', table => {
      table.string('id').primary();
      table
        .timestamp('createdAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
      table
        .timestamp('updatedAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
    })

    .createTable('stripePrices', table => {
      table.string('id').primary();
      table
        .string('productId')
        .references('id')
        .inTable('stripeProducts')
        .onDelete('CASCADE')
        .index();
      table.string('lookupKey').index();
      table.string('currency').defaultTo('myr').notNullable();
      table.string('interval');
      table.integer('intervalCount');
      table.string('type').defaultTo('recurring').notNullable();
      table.integer('unitAmount').defaultTo(0).notNullable();
      table
        .timestamp('createdAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
      table
        .timestamp('updatedAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
    })

    .createTable('stripeCoupons', table => {
      table.string('id').primary();
      table.string('name');
      table.integer('timesRedeemed').defaultTo(0);
      table
        .timestamp('createdAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
      table
        .timestamp('updatedAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
    })

    .createTable('stripePromoCodes', table => {
      table.string('id').primary();
      table.string('couponId');
      table.string('code').index();
      table.integer('timesRedeemed').defaultTo(0);
      table
        .timestamp('createdAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
      table
        .timestamp('updatedAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
    });
}

export function down(knex) {
  return knex.schema
    .table('stripeCustomers', table => {
      table.dropForeign('userId');
    })
    .table('stripeCards', table => {
      table.dropForeign('userId');
    })
    .table('stripePrices', table => {
      table.dropForeign('productId');
    })
    .dropTableIfExists('stripeCustomers')
    .dropTableIfExists('stripeCards')
    .dropTableIfExists('stripeProducts')
    .dropTableIfExists('stripePrices')
    .dropTableIfExists('stripeCoupons')
    .dropTableIfExists('stripePromoCodes');
}
