export function up(knex) {
  return knex.schema
    .createTable('stripeSubscriptions', table => {
      table.string('id').primary();
      table
        .uuid('parentId')
        .references('id')
        .inTable('users')
        .onDelete('SET NULL');
      table
        .uuid('childId')
        .references('id')
        .inTable('users')
        .onDelete('SET NULL');
      table
        .string('productId')
        .references('id')
        .inTable('stripeProducts')
        .onDelete('SET NULL');
      table.string('status');
      table.timestamp('startDate', { precision: 3, useTz: true });
      table.timestamp('endDate', { precision: 3, useTz: true });
      table.timestamp('cancelDate', { precision: 3, useTz: true });
      table
        .timestamp('createdAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
      table
        .timestamp('updatedAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
    })

    .createTable('stripeInvoices', table => {
      table.string('id').primary();
      table
        .string('subscriptionId')
        .references('id')
        .inTable('stripeSubscriptions')
        .onDelete('CASCADE');
      table
        .integer('episodeId')
        .references('id')
        .inTable('episodes')
        .onDelete('SET NULL');
      table.string('status');
      table.timestamp('paymentDate', { precision: 3, useTz: true });
      table.timestamp('periodStartDate', { precision: 3, useTz: true });
      table.timestamp('periodEndDate', { precision: 3, useTz: true });
      table
        .timestamp('createdAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
      table
        .timestamp('updatedAt', { precision: 3, useTz: true })
        .defaultTo(knex.fn.now(3));
    });
}

export function down(knex) {
  return knex.schema
    .table('stripeSubscriptions', table => {
      table.dropForeign('parentId');
      table.dropForeign('childId');
      table.dropForeign('productId');
    })
    .table('stripeInvoices', table => {
      table.dropForeign('subscriptionId');
      table.dropForeign('episodeId');
    })
    .dropTableIfExists('stripeSubscriptions')
    .dropTableIfExists('stripeInvoices');
}
