export function up(knex) {
  return Promise.all([
    knex.schema
      .createTable('seasons', table => {
        table.increments('id').primary();
        table
          .string('stripeProductId')
          .references('id')
          .inTable('stripeProducts')
          .defaultTo(null)
          .onDelete('SET NULL');
        table.string('title').notNullable();
        table.text('description').notNullable();
        table.integer('seasonNumber').unique().notNullable();
        table.integer('totalPlayableEpisodes').notNullable();
        table.string('coverImageFileName').notNullable();
        table.text('coverImageUrl').notNullable();
        table.boolean('active').defaultTo(false);
        table.boolean('comingSoon').defaultTo(false);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('episodes', table => {
        table.increments('id').primary();
        table
          .integer('seasonId')
          .references('id')
          .inTable('seasons')
          .onDelete('CASCADE')
          .index();
        table
          .string('stripeProductId')
          .references('id')
          .inTable('stripeProducts')
          .defaultTo(null)
          .onDelete('SET NULL');
        table.string('title').notNullable();
        table.text('description').notNullable();
        table.string('episodeNumber').notNullable();
        table.text('boxContents');
        table.text('onlineGames');
        table.boolean('active').defaultTo(false);
        table.boolean('comingSoon').defaultTo(false);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('episodeImages', table => {
        table.bigIncrements('id').primary();
        table
          .integer('episodeId')
          .references('id')
          .inTable('episodes')
          .onDelete('CASCADE')
          .index();
        table.string('fileName').notNullable();
        table.text('imageUrl').notNullable();
        table.boolean('coverPhoto').defaultTo(false);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('episodeActivationCodes', table => {
        table.increments('id').primary();
        table
          .integer('episodeId')
          .references('id')
          .inTable('episodes')
          .onDelete('CASCADE');
        table.string('code').notNullable().unique().index();
        table.boolean('fromPartner').defaultTo(false);
        table
          .bigInteger('partnerId')
          .references('id')
          .inTable('partners')
          .onDelete('CASCADE');
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('episodesOwned', table => {
        table.bigIncrements('id').primary();
        table
          .uuid('userId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .index();
        table
          .integer('episodeId')
          .references('id')
          .inTable('episodes')
          .onDelete('CASCADE');
        table.string('codeUsed');
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('episodesActivated', table => {
        table.bigIncrements('id').primary();
        table
          .uuid('userId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .index();
        table
          .integer('episodeId')
          .references('id')
          .inTable('episodes')
          .onDelete('CASCADE');
        table.string('codeUsed').notNullable();
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .then(() => {
        return knex.table('seasons').insert({
          title: 'Special Missions',
          description:
            'The Agency of Curious Tasks is always on high alert for any new happenings occurring around the world. As an ACT member, you will be taken across Iguana Island to solve various mysteries and conundrums. Here you will find active missions that we need your help with agent. So if you are ready to get going, choose your mission and receive your briefing package.',
          seasonNumber: 0,
          totalPlayableEpisodes: 1,
          coverImageFileName: 'product_placeholder.png',
          coverImageUrl:
            'https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/products/product_placeholder.png',
          active: false,
          comingSoon: false,
        });
      }),
  ]);
}

export function down(knex) {
  return knex.schema
    .table('seasons', table => {
      table.dropForeign('stripeProductId');
    })
    .table('episodes', table => {
      table.dropForeign('seasonId');
      table.dropForeign('stripeProductId');
    })
    .table('episodeActivationCodes', table => {
      table.dropForeign('episodeId');
      table.dropForeign('partnerId');
    })
    .table('episodeImages', table => {
      table.dropForeign('episodeId');
    })
    .table('episodesOwned', table => {
      table.dropForeign('userId');
      table.dropForeign('episodeId');
    })
    .table('episodesActivated', table => {
      table.dropForeign('userId');
      table.dropForeign('episodeId');
    })
    .dropTableIfExists('seasons')
    .dropTableIfExists('episodes')
    .dropTableIfExists('episodeActivationCodes')
    .dropTableIfExists('episodeImages')
    .dropTableIfExists('episodesOwned')
    .dropTableIfExists('episodesActivated');
}
