import * as referralCodes from 'referral-codes';

import Auth from '../config/fusionauth.js';

export function up(knex) {
  return Promise.all([
    knex.schema
      .createTable('partners', table => {
        table.bigIncrements('id').primary();
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('category').notNullable();
        table
          .string('promoId')
          .references('id')
          .inTable('stripePromoCodes')
          .onDelete('SET NULL')
          .index();
        table.boolean('useActivationCode').defaultTo(false);
        table.boolean('blockRegistration').defaultTo(false);
        table.boolean('enabled').defaultTo(true);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('referrals', table => {
        table.bigIncrements('id').primary();
        table
          .uuid('userId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE');
        table.string('codeUsed').index().notNullable();
        table.boolean('placedOrder').defaultTo(false);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('referralCodes', table => {
        table.bigIncrements('id').primary();
        table
          .uuid('userId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE');
        table.string('code').unique().index().notNullable();
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .then(async () => {
        const admin = await Auth.retrieveUserByEmail('act@fliped.my');

        let referralCode = referralCodes.generate({
          length: 7,
          charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
        });

        referralCode = referralCode[0];

        return knex.table('referralCodes').insert({
          userId: admin.response.user.id,
          code: referralCode,
        });
      }),
  ]);
}

export function down(knex) {
  return knex.schema
    .table('partners', table => {
      table.dropForeign('promoId');
    })
    .table('referrals', table => {
      table.dropForeign('userId');
    })
    .table('referralCodes', table => {
      table.dropForeign('userId');
    })
    .dropTableIfExists('partners')
    .dropTableIfExists('referrals')
    .dropTableIfExists('referralCodes');
}
