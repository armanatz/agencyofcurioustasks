import Auth from '../config/fusionauth.js';

export function up(knex) {
  return Promise.all([
    knex.schema
      .createTable('wallets', table => {
        table.bigIncrements('id').primary();
        table
          .uuid('userId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .index();
        table.float('balance').defaultTo(0);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('giftCards', table => {
        table.bigIncrements('id').primary();
        table.string('code').index();
        table.float('value').defaultTo(0);
        table.string('currency').defaultTo('myr').notNullable();
        table.boolean('redeemed').defaultTo(false);
        table.boolean('hidden').default(false);
        table
          .uuid('redeemedBy')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .defaultTo(null);
        table
          .timestamp('redeemedAt', { precision: 3, useTz: true })
          .defaultTo(null);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .then(async () => {
        const admin = await Auth.retrieveUserByEmail('act@fliped.my');

        return knex
          .table('wallets')
          .insert({ userId: admin.response.user.id, balance: 0 });
      }),
  ]);
}

export function down(knex) {
  return knex.schema
    .table('wallets', table => {
      table.dropForeign('userId');
    })
    .table('giftCards', table => {
      table.dropForeign('redeemedBy');
    })
    .dropTableIfExists('wallets')
    .dropTableIfExists('giftCards');
}
