export function up(knex) {
  return Promise.all([
    knex.schema
      .createTable('globalSettings', table => {
        table.increments('id').primary();
        table.string('name').unique().notNullable();
        table.string('humanName').notNullable();
        table.string('value');
      })
      .then(() => {
        return knex.table('globalSettings').insert([
          {
            name: 'referralDiscount',
            humanName: 'Referral Discount',
            value: '10',
          },
          {
            name: 'orderProcessingDays',
            humanName: 'Order Processing Days',
            value: '7',
          },
        ]);
      }),
  ]);
}

export function down(knex) {
  return knex.schema.dropTableIfExists('globalSettings');
}
