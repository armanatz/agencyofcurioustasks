export function up(knex) {
  return Promise.all([
    knex.schema
      .table('orderItems', table => {
        table.dropPrimary('orderItems_pkey');
        table.dropIndex('orderId');
        table.dropForeign('orderId');
        table.dropForeign('episodeId');
        table.dropForeign('statusCode');
        table.dropForeign('lastUpdateBy');
      })

      .renameTable('orderItems', 'old_orderItems')

      .then(async () => {
        const oldItems = await knex.select().from('old_orderItems');
        const lastId = await knex
          .select('id')
          .from('old_orderItems')
          .orderBy('createdAt', 'desc')
          .limit(1);

        await knex.schema.dropTableIfExists('old_orderItems');

        await knex.schema.createTable('orderItems', table => {
          table.bigIncrements('id').primary();
          table
            .bigInteger('orderId')
            .references('id')
            .inTable('orders')
            .onDelete('CASCADE')
            .index();
          table
            .integer('episodeId')
            .references('id')
            .inTable('episodes')
            .onDelete('SET NULL');
          table.text('notes');
          table
            .integer('statusCode')
            .references('code')
            .inTable('statusCodes')
            .defaultTo(0)
            .onDelete('SET NULL');
          table.timestamp('maxShipByDate', { precision: 3, useTz: true });
          table.string('courier');
          table.string('trackingUrl');
          table.string('trackingCode');
          table.boolean('shippingEmailSent').defaultTo(false);
          table.boolean('deliveredEmailSent').defaultTo(false);
          table
            .uuid('lastUpdateBy')
            .references('id')
            .inTable('admins')
            .onDelete('SET NULL');
          table
            .timestamp('createdAt', { precision: 3, useTz: true })
            .defaultTo(knex.fn.now(3));
          table
            .timestamp('updatedAt', { precision: 3, useTz: true })
            .defaultTo(knex.fn.now(3));
        });

        await knex.raw(
          `ALTER SEQUENCE "orderItems_id_seq" RESTART WITH ${
            lastId.length > 0 ? parseInt(lastId[0].id, 10) + 1 : 1
          };`,
        );

        return knex.table('orderItems').insert(oldItems);
      }),
  ]);
}

export function down(knex) {
  return knex.schema.table('orderItems', table => {
    table.dropColumn('courier');
    table.dropColumn('trackingUrl');
    table.dropColumn('shippingEmailSent');
    table.dropColumn('deliveredEmailSent');
  });
}
