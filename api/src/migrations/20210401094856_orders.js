export function up(knex) {
  return Promise.all([
    knex.schema
      .createTable('orders', table => {
        table.bigIncrements('id').primary();
        table
          .uuid('parentId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .index();
        table
          .uuid('childId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE');
        table.string('orderCode').unique();
        table.boolean('promoUsed').defaultTo(false);
        table
          .string('promoId')
          .references('id')
          .inTable('stripePromoCodes')
          .onDelete('SET NULL');
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('statusCodes', table => {
        table.integer('code').primary();
        table.string('name').notNullable();
      })

      .createTable('orderItems', table => {
        table.bigIncrements('id').primary();
        table
          .bigInteger('orderId')
          .references('id')
          .inTable('orders')
          .onDelete('CASCADE')
          .index();
        table
          .integer('episodeId')
          .references('id')
          .inTable('episodes')
          .onDelete('SET NULL');
        table.text('notes');
        table
          .integer('statusCode')
          .references('code')
          .inTable('statusCodes')
          .defaultTo(0)
          .onDelete('SET NULL');
        table.timestamp('maxShipByDate', { precision: 3, useTz: true });
        table.string('trackingCode');
        table
          .uuid('lastUpdateBy')
          .references('id')
          .inTable('admins')
          .onDelete('SET NULL');
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })
      .then(() => {
        return knex.table('statusCodes').insert([
          { code: 0, name: 'Pending Payment' },
          { code: 1, name: 'Awaiting Fulfillment' },
          { code: 2, name: 'Awaiting Shipment' },
          { code: 3, name: 'Shipped' },
          { code: 4, name: 'Completed' },
          { code: 5, name: 'Customer Cancelled' },
          { code: 6, name: 'Seller Cancelled' },
          { code: 7, name: 'Refunded' },
          { code: 8, name: 'Partially Refunded' },
          { code: 9, name: 'Disputed' },
        ]);
      }),
  ]);
}

export function down(knex) {
  return knex.schema
    .table('orders', table => {
      table.dropForeign('parentId');
      table.dropForeign('childId');
      table.dropForeign('promoId');
    })
    .table('orderItems', table => {
      table.dropForeign('orderId');
      table.dropForeign('episodeId');
      table.dropForeign('statusCode');
      table.dropForeign('lastUpdateBy');
    })
    .dropTableIfExists('orders')
    .dropTableIfExists('statusCodes')
    .dropTableIfExists('orderItems');
}
