import vars from '../config/vars.js';
import Auth from '../config/fusionauth.js';

export function up(knex) {
  return Promise.all([
    knex.schema
      .createTable('users', table => {
        table.uuid('id').primary();
        table.string('loginId').unique().notNullable();
        table.string('role').notNullable();
        table.string('stripeCustomerId').defaultTo(null);
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('admins', table => {
        table.uuid('id').primary();
        table.string('email').unique().notNullable();
        table.string('roles');
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .createTable('addresses', table => {
        table.bigIncrements('id').primary();
        table
          .uuid('userId')
          .references('id')
          .inTable('users')
          .onDelete('CASCADE')
          .index();
        table.string('line1').notNullable();
        table.string('line2').defaultTo(null);
        table.string('postCode').notNullable();
        table.string('city').notNullable();
        table.string('state').defaultTo(null);
        table.string('country').notNullable();
        table
          .timestamp('createdAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
        table
          .timestamp('updatedAt', { precision: 3, useTz: true })
          .defaultTo(knex.fn.now(3));
      })

      .then(async () => {
        const authRegister = await Auth.register(null, {
          generateAuthenticationToken: false,
          registration: {
            applicationId: vars.fusionauth.appId,
            roles: ['Adult', 'Superuser'],
            timezone: 'Asia/Kuala_Lumpur',
            preferredLanguages: ['en'],
          },
          user: {
            email: 'act@fliped.my',
            mobilePhone: '+60123456789',
            password: vars.server.adminPassword,
            birthDate: '1900-01-01',
            firstName: 'ACT',
            lastName: 'Admin',
            fullName: 'ACT Admin',
            encryptionScheme: 'bcrypt',
            factor: 10,
            timezone: 'Asia/Kuala_Lumpur',
            preferredLanguages: ['en'],
          },
          skipVerification: true,
        });

        await Auth.createFamily(null, {
          familyMember: {
            userId: authRegister.response.user.id,
            owner: true,
            role: 'Adult',
          },
        });

        await knex.table('users').insert({
          id: authRegister.response.user.id,
          loginId: authRegister.response.user.email,
          role: 'Adult',
        });

        return knex.table('admins').insert({
          id: authRegister.response.user.id,
          email: authRegister.response.user.email,
          role: 'Superuser',
        });
      }),
  ]);
}

export function down(knex) {
  return Promise.all([
    knex.schema
      .table('addresses', table => {
        table.dropForeign('userId');
      })
      .dropTableIfExists('users')
      .dropTableIfExists('admins')
      .dropTableIfExists('addresses')
      .then(async () => {
        const adminUser = await Auth.retrieveUserByEmail('act@fliped.my');
        await Auth.deleteUser(adminUser.response.user.id);
      }),
  ]);
}
