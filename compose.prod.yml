version: "3.9"

services:
  fusionauth:
    image: fusionauth/fusionauth-app:1.27.2@sha256:ec6e213bcb0048b738347b43cae832958dbbe7a3306d9fab5e8d8ebba67ce122
    container_name: fusionauth
    logging:
      driver: "json-file"
      options:
        max-size: "1m"
        max-file: "10"
    environment:
      DATABASE_URL: ${DATABASE_URL}
      DATABASE_USERNAME: ${DATABASE_USERNAME}
      DATABASE_PASSWORD: ${DATABASE_PASSWORD}
      FUSIONAUTH_APP_MEMORY: ${FUSIONAUTH_APP_MEMORY}
      FUSIONAUTH_APP_RUNTIME_MODE: production
      FUSIONAUTH_APP_SILENT_MODE: "true"
      SEARCH_TYPE: database
    expose:
      - ${FUSIONAUTH_PORT}
    networks:
      - app
    restart: unless-stopped
    volumes:
      - fa_data:/usr/local/fusionauth/config

  api:
    image: act-api:${NODE_ENV}
    container_name: api
    logging:
      driver: "json-file"
      options:
        max-size: "1m"
        max-file: "10"
    build:
      context: ./api
      args:
        NODE_ENV: ${NODE_ENV}
    user: node
    init: true
    depends_on:
      - fusionauth
    environment:
      NODE_ENV: ${NODE_ENV}
      SERVER_PORT: ${SERVER_PORT}
      API_PATH_PREFIX: ${API_PATH_PREFIX}
      API_VERSION: ${API_VERSION}
      SERVER_DOMAIN: ${SERVER_DOMAIN}
      SERVER_NAME: ${SERVER_NAME}
      SERVER_PROTO: ${SERVER_PROTO}
      ADMIN_PASSWORD: ${ADMIN_PASSWORD}
      LOG_LEVEL: ${LOG_LEVEL}
      POSTGRES_SSL: ${POSTGRES_SSL}
      POSTGRES_HOST: ${POSTGRES_HOST}
      POSTGRES_PORT: ${POSTGRES_PORT}
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
      POSTGRES_DB: ${POSTGRES_DB}
      FUSIONAUTH_APP_URL: ${FUSIONAUTH_APP_URL}
      FUSIONAUTH_PORT: ${FUSIONAUTH_PORT}
      FUSIONAUTH_API_KEY: ${FUSIONAUTH_API_KEY}
      FUSIONAUTH_APP_ID: ${FUSIONAUTH_APP_ID}
      JWT_SIGNING_KEY: ${JWT_SIGNING_KEY}
      STRIPE_PUBLIC_KEY: ${STRIPE_PUBLIC_KEY}
      STRIPE_PRIVATE_KEY: ${STRIPE_PRIVATE_KEY}
      STRIPE_WEBHOOK_SECRET: ${STRIPE_WEBHOOK_SECRET}
      STRIPE_PRICE_LOOKUP_KEY_SUFFIX: ${STRIPE_PRICE_LOOKUP_KEY_SUFFIX}
      DO_SPACES_ENDPOINT: ${DO_SPACES_ENDPOINT}
      DO_SPACES_REGION: ${DO_SPACES_REGION}
      DO_SPACES_NAME: ${DO_SPACES_NAME}
      DO_SPACES_KEY: ${DO_SPACES_KEY}
      DO_SPACES_SECRET: ${DO_SPACES_SECRET}
      MAILGUN_DOMAIN: ${MAILGUN_DOMAIN}
      MAILGUN_API_KEY: ${MAILGUN_API_KEY}
      MAILGUN_PUBLIC_KEY: ${MAILGUN_PUBLIC_KEY}
      MAILCHIMP_API_KEY: ${MAILCHIMP_API_KEY}
      MAILCHIMP_SERVER: ${MAILCHIMP_SERVER}
    networks:
      - app
    restart: unless-stopped
    volumes:
      - ./api/src:/usr/src/app/src
      - ./api/package.json:/usr/src/app/package.json
      - ./api/package-lock.json:/usr/src/app/package-lock.json
    tty: true
    command: ["node", "./src/index.js"]

  nginx:
    image: act-client:${NODE_ENV}
    container_name: client
    logging:
      driver: "json-file"
      options:
        max-size: "1m"
        max-file: "10"
    build:
      context: ./client
      args:
        NODE_ENV: ${NODE_ENV}
        DISABLE_ESLINT_PLUGIN: "true"
        REACT_APP_SERVER_NAME: ${SERVER_NAME}
        REACT_APP_API_URL: ${API_PATH_PREFIX}
        REACT_APP_API_VERSION: ${API_VERSION}
        REACT_APP_STRIPE_PRICE_LOOKUP_KEY_SUFFIX: ${STRIPE_PRICE_LOOKUP_KEY_SUFFIX}
        REACT_APP_STRIPE_PUBLIC_KEY: ${STRIPE_PUBLIC_KEY}
    depends_on:
      - fusionauth
      - api
    environment:
      SERVER_PORT: ${SERVER_PORT}
      SERVER_DOMAIN: ${SERVER_DOMAIN}
      SERVER_NAME: ${SERVER_NAME}
      FUSIONAUTH_PORT: ${FUSIONAUTH_PORT}
    ports:
      - 80:80
      - 443:443
      - ${FUSIONAUTH_PORT}:${FUSIONAUTH_PORT}
    networks:
      - app
    restart: unless-stopped
    volumes:
      - /dev/null:/etc/nginx/conf.d/default.conf
      - ./nginx/prod.conf.template:/etc/nginx/templates/prod.conf.template
      - /var/www/html
    tty: true
    command: nginx -g 'daemon off;'

volumes:
  es_data:
  fa_data:

networks:
  app:
