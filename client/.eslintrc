{
  "env": {
    "browser": true,
    "node": true,
    "es6": true
  },
  "parser": "babel-eslint",
  "plugins": ["react"],
  "extends": ["eslint:recommended", "plugin:react/recommended", "prettier"],
  "rules": {
    "default-case": "warn",
    "dot-notation": "warn",
    "eqeqeq": "error",
    "no-cond-assign": ["error", "except-parens"],
    "no-console": "off",
    "no-else-return": "warn",
    "no-empty-function": "error",
    "no-extra-bind": "error",
    "no-invalid-this": "off",
    "no-lone-blocks": "error",
    "no-multi-str": "warn",
    "no-new": "error",
    "no-param-reassign": "off",
    "no-unused-vars": ["warn", { "vars": "local", "args": "none" }],
    "no-useless-concat": "error",
    "react/prop-types": "off",
    "react/display-name": "off",

    "array-bracket-newline": ["off", "consistent"],
    "array-element-newline": ["off", { "multiline": true, "minItems": 3 }],
    "camelcase": ["warn", { "properties": "never" }],
    "capitalized-comments": [
      "off",
      "never",
      {
        "line": {
          "ignorePattern": ".*",
          "ignoreInlineComments": true,
          "ignoreConsecutiveComments": true
        },
        "block": {
          "ignorePattern": ".*",
          "ignoreInlineComments": true,
          "ignoreConsecutiveComments": true
        }
      }
    ],
    "func-name-matching": [
      "off",
      "always",
      {
        "includeCommonJSModuleExports": false
      }
    ],
    "func-names": "warn",
    "func-style": ["off", "expression"],
    "id-blacklist": "off",
    "id-length": "off",
    "id-match": "off",
    "jsx-quotes": ["off", "prefer-double"],
    "line-comment-position": [
      "off",
      {
        "position": "above",
        "ignorePattern": "",
        "applyDefaultPatterns": true
      }
    ],
    "lines-between-class-members": [
      "error",
      "always",
      { "exceptAfterSingleLine": false }
    ],
    "lines-around-comment": "off",
    "lines-around-directive": [
      "error",
      {
        "before": "always",
        "after": "always"
      }
    ],
    "max-depth": ["off", 4],
    "max-len": [
      "warn",
      80,
      2,
      {
        "ignoreUrls": true,
        "ignoreComments": false,
        "ignoreRegExpLiterals": true,
        "ignoreStrings": true,
        "ignoreTemplateLiterals": true
      }
    ],
    "max-lines": [
      "off",
      {
        "max": 300,
        "skipBlankLines": true,
        "skipComments": true
      }
    ],
    "max-nested-callbacks": "off",
    "max-params": ["off", 3],
    "max-statements": ["off", 10],
    "max-statements-per-line": ["off", { "max": 1 }],
    "multiline-comment-style": ["off", "starred-block"],
    "multiline-ternary": ["off", "never"],
    "new-cap": [
      "error",
      {
        "newIsCap": true,
        "newIsCapExceptions": [],
        "capIsNew": false,
        "capIsNewExceptions": [
          "Immutable.Map",
          "Immutable.Set",
          "Immutable.List"
        ]
      }
    ],
    "newline-after-var": "off",
    "newline-before-return": "off",
    "no-array-constructor": "error",
    "no-bitwise": "error",
    "no-continue": "error",
    "no-inline-comments": "off",
    "no-lonely-if": "error",
    "no-multi-assign": ["error"],
    "no-negated-condition": "off",
    "no-nested-ternary": "off",
    "no-new-object": "error",
    "no-plusplus": "error",
    "no-restricted-syntax": [
      "error",
      {
        "selector": "ForInStatement",
        "message": "for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array."
      },
      {
        "selector": "ForOfStatement",
        "message": "iterators/generators require regenerator-runtime, which is too heavyweight for this guide to allow them. Separately, loops should be avoided in favor of array iterations."
      },
      {
        "selector": "LabeledStatement",
        "message": "Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand."
      },
      {
        "selector": "WithStatement",
        "message": "`with` is disallowed in strict mode because it makes code impossible to predict and optimize."
      }
    ],
    "no-tabs": ["error", {"allowIndentationTabs": true}],
    "no-ternary": "off",
    "no-underscore-dangle": [
      "error",
      {
        "allow": [],
        "allowAfterThis": false,
        "allowAfterSuper": false,
        "enforceInMethodNames": false
      }
    ],
    "no-unneeded-ternary": ["error", { "defaultAssignment": false }],
    "one-var": ["error", "never"],
    "operator-assignment": ["error", "always"],
    "padding-line-between-statements": "off",
    "quotes": [
      "error",
      "single",
      { "avoidEscape": true, "allowTemplateLiterals": false }
    ],
    "radix": "off",
    "require-jsdoc": "off",
    "sort-keys": ["off", "asc", { "caseSensitive": false, "natural": true }],
    "sort-vars": "off",
    "space-infix-ops": "off",
    "spaced-comment": [
      "error",
      "always",
      {
        "line": {
          "exceptions": ["-", "+"],
          "markers": ["=", "!"]
        },
        "block": {
          "exceptions": ["-", "+"],
          "markers": ["=", "!"],
          "balanced": true
        }
      }
    ],
    "wrap-regex": "off"
  }
}
