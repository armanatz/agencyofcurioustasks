import { extendTheme } from '@chakra-ui/react';

import colors from './colors';
import Button from './Button';
import Input from './Input';
import Select from './Select';
import Textarea from './Textarea';
import NumberInput from './NumberInput';

const customTheme = extendTheme({
  styles: {
    global: {
      body: {
        bg: 'gray.50',
      },
      button: {
        _focus: {
          outline: 'none',
        },
      },
    },
  },
  fonts: {
    body: 'Comfortaa, sans-serif',
    heading: 'Changa, sans-serif',
  },
  colors,
  shadows: {
    outline: 'none',
  },
  components: {
    Button,
    Input,
    Select,
    Textarea,
    NumberInput,
    Checkbox: {
      defaultProps: {
        colorScheme: 'niagara',
      },
    },
  },
});

export default customTheme;
