const Textarea = {
  variants: {
    baseStyle: {
      fontWeight: '800',
    },
    filled: {
      bg: 'linen.500',
      color: 'cerulean.500',
      borderColor: 'linen.600',
      _hover: {
        bg: 'linen.500',
      },
      _focus: {
        bg: 'white',
        borderColor: 'cerulean.500',
      },
      _valid: {
        bg: 'goldenrod.500',
        color: 'cerulean.500',
      },
      _invalid: {
        bg: 'gray.100',
        color: 'froly.500',
        borderColor: 'froly.500',
      },
      _placeholder: {
        color: 'linen.700',
      },
    },
  },
  defaultProps: {
    variant: 'filled',
  },
};

export default Textarea;
