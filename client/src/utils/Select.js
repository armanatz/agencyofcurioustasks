const Select = {
  variants: {
    baseStyle: {
      field: {
        fontWeight: '800',
      },
      addon: {
        fontWeight: '800',
      },
    },
    filled: {
      field: {
        bg: 'linen.500',
        color: 'cerulean.500',
        borderColor: 'linen.600',
        _hover: {
          bg: 'linen.500',
        },
        _focus: {
          bg: 'white',
          borderColor: 'cerulean.500',
        },
        _valid: {
          bg: 'goldenrod.500',
          color: 'cerulean.500',
        },
        _invalid: {
          bg: 'gray.100',
          color: 'froly.500',
          borderColor: 'froly.500',
        },
        _placeholder: {
          color: 'linen.700',
        },
      },
      addon: {
        bg: 'wattle.500',
        borderColor: 'linen.600',
        color: 'cerulean.500',
      },
    },
  },
  defaultProps: {
    variant: 'filled',
  },
};

export default Select;
