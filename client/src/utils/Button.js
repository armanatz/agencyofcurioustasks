const primarySolid = {
  backgroundColor: 'wattle.500',
  color: 'cerulean.500',
  borderColor: 'wattle.500',
  borderWidth: 2,
  _hover: {
    _disabled: {
      backgroundColor: 'wattle.500',
      color: 'cerulean.500',
      borderColor: 'wattle.500',
    },
    backgroundColor: 'cerulean.500',
    color: 'wattle.500',
    borderColor: 'cerulean.500',
  },
  _active: {
    backgroundColor: 'cerulean.800',
    color: 'white',
    borderColor: 'cerulean.800',
  },
};

const primaryOutline = {
  backgroundColor: 'transparent',
  color: 'cerulean.500',
  textShadow: '0px 0px 5px black',
  borderColor: 'wattle.500',
  borderWidth: 2,
  _hover: {
    _disabled: {
      backgroundColor: 'transparent',
      color: 'cerulean.500',
      textShadow: '0px 0px 5px black',
      borderColor: 'wattle.500',
    },
    backgroundColor: 'cerulean.500',
    color: 'wattle.500',
    textShadow: 'none',
    borderColor: 'cerulean.500',
  },
  _active: {
    backgroundColor: 'cerulean.800',
    color: 'white',
    textShadow: 'none',
    borderColor: 'cerulean.800',
  },
};

const secondarySolid = {
  backgroundColor: 'niagara.500',
  color: 'goldenrod.500',
  borderColor: 'niagara.500',
  borderWidth: 2,
  _hover: {
    _disabled: {
      backgroundColor: 'niagara.500',
      borderColor: 'niagara.500',
    },
    backgroundColor: 'cerulean.500',
    borderColor: 'cerulean.500',
    borderWidth: 2,
  },
  _active: {
    backgroundColor: 'cerulean.800',
    color: 'white',
    borderColor: 'cerulean.800',
    borderWidth: 2,
  },
};

const secondaryOutline = {
  backgroundColor: 'transparent',
  color: 'goldenrod.500',
  textShadow: '0px 0px 5px black',
  borderColor: 'niagara.500',
  borderWidth: 2,
  _hover: {
    _disabled: {
      backgroundColor: 'transparent',
      color: 'goldenrod.500',
      textShadow: '0px 0px 5px black',
      borderColor: 'niagara.500',
    },
    backgroundColor: 'cerulean.500',
    color: 'goldenrod.500',
    textShadow: 'none',
    borderColor: 'cerulean.500',
    borderWidth: 2,
  },
  _active: {
    backgroundColor: 'cerulean.800',
    color: 'white',
    textShadow: 'none',
    borderColor: 'cerulean.800',
    borderWidth: 2,
  },
};

const dangerSolid = {
  backgroundColor: 'froly.500',
  color: 'white',
  borderColor: 'froly.500',
  borderWidth: 2,
  _hover: {
    _disabled: {
      backgroundColor: 'froly.500',
      color: 'white',
    },
    backgroundColor: 'froly.400',
    borderColor: 'froly.400',
  },
  _active: {
    backgroundColor: 'froly.700',
    color: 'white',
    borderColor: 'froly.700',
  },
};

const dangerOutline = {
  backgroundColor: 'transparent',
  color: 'froly.500',
  textShadow: '0px 0px 5px black',
  borderColor: 'froly.500',
  borderWidth: 2,
  _hover: {
    _disabled: {
      backgroundColor: 'transparent',
      color: 'froly.500',
      textShadow: '0px 0px 5px black',
    },
    backgroundColor: 'froly.500',
    color: 'white',
    textShadow: 'none',
    borderColor: 'froly.500',
    borderWidth: 2,
  },
  _active: {
    backgroundColor: 'froly.700',
    color: 'white',
    textShadow: 'none',
    borderColor: 'froly.700',
    borderWidth: 2,
  },
};

const navLink = {
  _hover: {
    backgroundColor: 'goldenrod.500',
    color: 'cerulean.500',
  },
  _active: {
    backgroundColor: 'transparent',
    color: 'goldenrod.500',
  },
};

const navLinkActive = {
  backgroundColor: 'transparent',
  color: 'goldenrod.500',
  _hover: {
    backgroundColor: 'goldenrod.500',
    color: 'cerulean.500',
  },
};

const navLinkKC = {
  _hover: {
    color: 'goldenrod.500',
  },
  _active: {
    color: 'goldenrod.700',
  },
};

const navLinkActiveKC = {
  color: 'goldenrod.500',
};

const Button = {
  baseStyle: {
    fontFamily: 'Changa',
    fontSize: 'md',
    fontWeight: '800',
    rounded: 'md',
  },
  variants: {
    primarySolid,
    primaryOutline,
    secondarySolid,
    secondaryOutline,
    dangerSolid,
    dangerOutline,
    navLink,
    navLinkActive,
    navLinkKC,
    navLinkActiveKC,
  },
  defaultProps: {
    variant: 'primarySolid',
  },
};

export default Button;
