const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/auth`;

export const fetchAuthStatus = async () => {
  const response = await fetch(`${ENDPOINT}/check`, { credentials: 'include' });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.status === 200) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
