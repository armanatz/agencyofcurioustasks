const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/gift_cards`;

export const fetchAllGiftCards = async redeemed => {
  let query = '';

  if (redeemed === 'true') {
    query = '?redeemed=true';
  } else if (redeemed === 'false') {
    query = '?redeemed=false';
  }

  const response = await fetch(`${ENDPOINT}${query}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
