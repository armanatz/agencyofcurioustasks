const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/partners`;

export const fetchPartners = async (filters = '') => {
  const response = await fetch(`${ENDPOINT}?${filters}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchPartnerById = async id => {
  const response = await fetch(`${ENDPOINT}/${id}`, { credentials: 'include' });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
