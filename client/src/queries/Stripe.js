const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/stripe`;

export const fetchStripeProducts = async () => {
  const response = await fetch(`${ENDPOINT}/products`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchCustomersPaymentMethods = async () => {
  const response = await fetch(`${ENDPOINT}/customer/payment_methods`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchPricesByProductId = async productId => {
  const response = await fetch(`${ENDPOINT}/price/${productId}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchAllSubscriptions = async () => {
  const response = await fetch(`${ENDPOINT}/subscriptions`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
