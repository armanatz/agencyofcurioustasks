const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/games`;

export const activateGame = async (code, episodeId, userId) => {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');

  return fetch(`${ENDPOINT}/activate/${episodeId}`, {
    headers,
    method: 'POST',
    body: JSON.stringify({ code, userId }),
    credentials: 'include',
  });
};

export const fetchAllEpisodesActivated = async userId => {
  const response = await fetch(`${ENDPOINT}/activated/${userId}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchIsEpisodeActivated = async (userId, episodeId) => {
  const response = await fetch(`${ENDPOINT}/activated/${userId}/${episodeId}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const loadGame = async (userId, episodeId) => {
  const response = await fetch(`${ENDPOINT}/load/${userId}/${episodeId}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const saveGame = async (episodeId, userId, progress) => {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');

  return fetch(`${ENDPOINT}/save/${episodeId}`, {
    headers,
    method: 'POST',
    body: JSON.stringify({ userId, progress }),
    credentials: 'include',
  });
};
