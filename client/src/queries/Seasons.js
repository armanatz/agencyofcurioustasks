const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/seasons`;

export const fetchSeasons = async () => {
  const response = await fetch(ENDPOINT, { credentials: 'include' });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchSeasonById = async id => {
  const response = await fetch(`${ENDPOINT}/id/${id}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchSeasonByNumber = async seasonNumber => {
  const response = await fetch(`${ENDPOINT}/number/${seasonNumber}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
