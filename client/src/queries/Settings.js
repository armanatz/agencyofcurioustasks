const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/settings`;

export const fetchGlobalSettings = async () => {
  const response = await fetch(ENDPOINT, { credentials: 'include' });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchSingleGlobalSetting = async (type, key) => {
  const response = await fetch(`${ENDPOINT}/${type}/${key}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
