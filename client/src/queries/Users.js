const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users`;

export const fetchLoggedInUser = async () => {
  const response = await fetch(`${ENDPOINT}/current`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchUserById = async id => {
  const response = await fetch(`${ENDPOINT}/${id}`, { credentials: 'include' });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchReferralCode = async () => {
  const response = await fetch(`${ENDPOINT}/referrals/code/current`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.text();
  }
  throw new Error('Something went wrong');
};

export const fetchReferralCount = async code => {
  const response = await fetch(`${ENDPOINT}/referrals/count/${code}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.text();
  }
  throw new Error('Something went wrong');
};

export const fetchCurrentUsersWalletBalance = async () => {
  const response = await fetch(`${ENDPOINT}/wallet`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchAllAdmins = async () => {
  const response = await fetch(`${ENDPOINT}/admins`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchAllAdminRoles = async () => {
  const response = await fetch(`${ENDPOINT}/admins/roles`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchAllUsers = async (filters = undefined) => {
  const response = await fetch(`${ENDPOINT}?${filters || ''}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
