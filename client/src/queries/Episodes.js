const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/episodes`;

export const fetchAllEpisodes = async () => {
  const response = await fetch(ENDPOINT, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchEpisodesBySeasonId = async seasonId => {
  const response = await fetch(`${ENDPOINT}/season_id/${seasonId}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchEpisodesBySeasonNumber = async seasonNumber => {
  const response = await fetch(`${ENDPOINT}/season_number/${seasonNumber}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchEpisodeById = async episodeId => {
  const response = await fetch(`${ENDPOINT}/id/${episodeId}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
