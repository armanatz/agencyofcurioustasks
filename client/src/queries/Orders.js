const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/orders`;

export const fetchOrders = async (filters = '') => {
  const response = await fetch(`${ENDPOINT}?${filters}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchStatusCodes = async () => {
  const response = await fetch(`${ENDPOINT}/status_codes`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
