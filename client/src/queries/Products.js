const ENDPOINT = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/products`;

export const fetchActivationCodes = async () => {
  const response = await fetch(`${ENDPOINT}/activation_codes`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchActivationCodeByEpisodeId = async episodeId => {
  const response = await fetch(
    `${ENDPOINT}/activation_codes/episode/${episodeId}`,
    {
      credentials: 'include',
    },
  );
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

export const fetchOwnedProducts = async userId => {
  const response = await fetch(`${ENDPOINT}/owned/${userId}`, {
    credentials: 'include',
  });
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};
