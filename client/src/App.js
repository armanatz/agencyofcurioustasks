import React, { Suspense, lazy } from 'react';
import { Route, Switch, Redirect, useLocation } from 'react-router-dom';
import { useQueryClient } from 'react-query';
import { Box, Center, Spinner } from '@chakra-ui/react';
import { useQuery } from 'react-query';
import ReactPixel from 'react-facebook-pixel';
import TagManager from 'react-gtm-module';
import { Helmet } from 'react-helmet';

import ScrollToTop from './components/ScrollToTop';
import NavBar from './components/NavBar';
import NavBarKC from './components/NavBarKC';
import Footer from './components/Footer';

import { fetchAuthStatus } from './queries/Auth';
import { fetchLoggedInUser } from './queries/Users';
import LogoutButton from './components/LogoutButton';

// Public Pages
const NotFound = lazy(() => import('./pages/NotFound'));
const Home = lazy(() => import('./pages/Landing/Home'));
const About = lazy(() => import('./pages/Landing/About'));
const StoreHome = lazy(() => import('./pages/Landing/Store/Home'));
const SeasonOverview = lazy(() =>
  import('./pages/Landing/Store/SeasonOverview'),
);
const FAQ = lazy(() => import('./pages/Landing/FAQ'));
const Login = lazy(() => import('./pages/Landing/Auth/Login'));
const Signup = lazy(() => import('./pages/Landing/Auth/Signup'));
const ForgotPassword = lazy(() =>
  import('./pages/Landing/Auth/ForgotPassword'),
);
const ResetPassword = lazy(() => import('./pages/Landing/Auth/ResetPassword'));
const EmailVerification = lazy(() =>
  import('./pages/Landing/Auth/EmailVerification'),
);
const Terms = lazy(() => import('./pages/Landing/Terms'));
const Privacy = lazy(() => import('./pages/Landing/Privacy'));

// Private Pages

// Parent pages
const Dashboard = lazy(() => import('./pages/App/Dashboard'));
const Children = lazy(() => import('./pages/App/Children'));
const ChildrenAdd = lazy(() => import('./pages/App/Children/Add'));
const ChildrenEdit = lazy(() => import('./pages/App/Children/Edit'));
const Profile = lazy(() => import('./pages/App/Account/Profile'));
const Billing = lazy(() => import('./pages/App/Account/Billing'));
const CheckoutSingle = lazy(() => import('./pages/App/Store/CheckoutSingle'));
const CheckoutSubscription = lazy(() =>
  import('./pages/App/Store/CheckoutSubscription'),
);
const AssignProduct = lazy(() => import('./pages/App/Store/AssignProduct'));
const ThankYou = lazy(() => import('./pages/App/Store/ThankYou'));

// Children pages
const GameSeasonMenu = lazy(() => import('./pages/App/Games/SeasonMenu'));
const GameEpisodeMenu = lazy(() => import('./pages/App/Games/EpisodeMenu'));
const Game = lazy(() => import('./pages/App/Games/Game'));
const KnowledgeCenter = lazy(() => import('./pages/App/KnowledgeCenter'));
const KnowledgeCenterSeasonOverview = lazy(() =>
  import('./pages/App/KnowledgeCenter/SeasonOverview'),
);
const KnowledgeCenterKitSelect = lazy(() =>
  import('./pages/App/KnowledgeCenter/KitSelect'),
);
const KnowledgeCenterKitHome = lazy(() =>
  import('./pages/App/KnowledgeCenter/KitHome'),
);
const KnowledgeCenterHowItWorks = lazy(() =>
  import('./pages/App/KnowledgeCenter/HowItWorks'),
);
const KnowledgeCenterHistory = lazy(() =>
  import('./pages/App/KnowledgeCenter/History'),
);
const KnowledgeCenterScience = lazy(() =>
  import('./pages/App/KnowledgeCenter/Science'),
);
const KnowledgeCenterTrivia = lazy(() =>
  import('./pages/App/KnowledgeCenter/Trivia'),
);
const KnowledgeCenterResources = lazy(() =>
  import('./pages/App/KnowledgeCenter/Resources'),
);

// Admin Pages
const Products = lazy(() => import('./pages/App/Admin/Products'));
const SeasonAdd = lazy(() => import('./pages/App/Admin/SeasonAdd'));
const SeasonEdit = lazy(() => import('./pages/App/Admin/SeasonEdit'));
const EpisodeAdd = lazy(() => import('./pages/App/Admin/EpisodeAdd'));
const EpisodeEdit = lazy(() => import('./pages/App/Admin/EpisodeEdit'));
const Partners = lazy(() => import('./pages/App/Admin/Partners'));
const PartnerAdd = lazy(() => import('./pages/App/Admin/PartnerAdd'));
const PartnerEdit = lazy(() => import('./pages/App/Admin/PartnerEdit'));
const ActivationCodeAdd = lazy(() =>
  import('./pages/App/Admin/ActivationCodeAdd'),
);
const Orders = lazy(() => import('./pages/App/Admin/Orders'));
const Accounts = lazy(() => import('./pages/App/Admin/Accounts'));
const Settings = lazy(() => import('./pages/App/Admin/Settings'));

const PrivateRouteParent = ({ children, ...rest }) => {
  const queryClient = useQueryClient();
  const authStatus = queryClient.getQueryData('authStatus');
  const user = queryClient.getQueryData('user');

  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (!authStatus?.isAuthenticated) {
          return (
            <Redirect
              to={{
                pathname: '/login',
                state: { from: location },
              }}
            />
          );
        }

        if (user?.roles.includes('Adult')) {
          return children;
        } else if (user?.roles.includes('Child')) {
          return <Redirect to="/games" />;
        }
      }}
    />
  );
};

const PrivateRouteChild = ({ children, ...rest }) => {
  const queryClient = useQueryClient();
  const authStatus = queryClient.getQueryData('authStatus');
  const user = queryClient.getQueryData('user');

  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (!authStatus?.isAuthenticated) {
          return (
            <Redirect
              to={{
                pathname: '/login',
                state: { from: location },
              }}
            />
          );
        }

        if (user?.roles.includes('Child')) {
          return children;
        } else if (user?.roles.includes('Adult')) {
          return <Redirect to="/dashboard" />;
        }
      }}
    />
  );
};

const AdminRoute = ({ usersRoles, rolesAllowed, children, ...rest }) => {
  const queryClient = useQueryClient();
  const authStatus = queryClient.getQueryData('authStatus');

  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (authStatus?.isAuthenticated) {
          const canAccess = rolesAllowed
            ? rolesAllowed.some(role => usersRoles?.includes(role))
            : true;
          if (canAccess) {
            return children;
          }
          return <Redirect to="/" />;
        }
        return (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        );
      }}
    />
  );
};

const App = () => {
  const location = useLocation();

  if (process.env.NODE_ENV !== 'development') {
    TagManager.initialize({
      gtmId: 'GTM-WRL273G',
    });
    ReactPixel.init('1867351963428110', {}, { autoConfig: true });
    ReactPixel.pageView();
  }

  const { data: authStatus, status: authStatusQueryStatus } = useQuery(
    'authStatus',
    fetchAuthStatus,
    {
      retry: 1,
      refetchOnWindowFocus: true,
      refetchOnMount: true,
      refetchOnReconnect: true,
    },
  );

  const { data: user, status: userQueryStatus } = useQuery(
    'user',
    fetchLoggedInUser,
    {
      enabled: !!authStatus?.isAuthenticated,
    },
  );

  if (authStatusQueryStatus === 'loading' || userQueryStatus === 'loading') {
    return (
      <Box position="relative" minH="100vh">
        <NavBar />
        <Center h="100vh">
          <Spinner color="niagara.500" size="xl" />
        </Center>
      </Box>
    );
  }

  const paddingBottom = authStatus?.isAuthenticated
    ? { base: '104px', md: '24px' }
    : { base: '520px', md: '230px' };

  const usersRoles = user ? user?.roles : [];

  let childTopMenu = <LogoutButton />;

  if (location.pathname.split('/')[1] === 'knowledge-center') {
    childTopMenu = <NavBarKC showLinks />;
  }

  return (
    <>
      <Helmet>
        <title>Agency of Curious Tasks</title>
        <meta
          name="description"
          content="ACT is a 21st century story-based e-learning style crafted for parent and child to collaborate using STEM kits for hands-on learning through play."
        />
      </Helmet>
      <Box
        position="relative"
        minH="100vh"
        pb={usersRoles.includes('Child') ? 0 : paddingBottom}
      >
        {!usersRoles.includes('Child') && <NavBar showLinks />}
        {usersRoles.includes('Child') && childTopMenu}
        <Suspense
          fallback={
            <Center h="100vh">
              <Spinner color="niagara.500" size="xl" />
            </Center>
          }
        >
          <ScrollToTop />
          <Switch>
            {/* PUBLIC ROUTE DEFINITIONS */}
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/about">
              <About />
            </Route>
            <Route exact path="/store">
              <StoreHome />
            </Route>
            <Route exact path="/store/season/:number">
              <SeasonOverview />
            </Route>
            <Route exact path="/faq">
              <FAQ />
            </Route>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route exact path="/signup">
              <Signup />
            </Route>
            <Route exact path="/forgot-password">
              <ForgotPassword />
            </Route>
            <Route exact path="/reset-password">
              <ResetPassword />
            </Route>
            <Route exact path="/verify/email">
              <EmailVerification />
            </Route>
            <Route exact path="/terms">
              <Terms />
            </Route>
            <Route exact path="/privacy">
              <Privacy />
            </Route>

            {/* PRIVATE ROUTE DEFINITIONS FOR PARENT ACCOUNTS */}
            <PrivateRouteParent exact path="/dashboard">
              <Dashboard />
            </PrivateRouteParent>
            <PrivateRouteParent exact path="/children">
              <Children />
            </PrivateRouteParent>
            <PrivateRouteParent exact path="/children/add">
              <ChildrenAdd />
            </PrivateRouteParent>
            <PrivateRouteParent exact path="/children/edit/:username">
              <ChildrenEdit />
            </PrivateRouteParent>
            <PrivateRouteParent exact path="/account/profile">
              <Profile />
            </PrivateRouteParent>
            <PrivateRouteParent exact path="/account/billing">
              <Billing />
            </PrivateRouteParent>
            <PrivateRouteParent exact path="/store/checkout/single">
              <CheckoutSingle />
            </PrivateRouteParent>
            <PrivateRouteParent exact path="/store/checkout/subscription">
              <CheckoutSubscription />
            </PrivateRouteParent>
            <PrivateRouteParent exact path="/store/assign">
              <AssignProduct />
            </PrivateRouteParent>
            <PrivateRouteParent exact path="/store/checkout/success">
              <ThankYou />
            </PrivateRouteParent>

            {/* PRIVATE ROUTE DEFINITIONS FOR CHILD ACCOUNTS */}
            <PrivateRouteChild exact path="/games">
              <GameSeasonMenu />
            </PrivateRouteChild>
            <PrivateRouteChild exact path="/games/season/:season">
              <GameEpisodeMenu />
            </PrivateRouteChild>
            <PrivateRouteChild
              exact
              path="/games/season/:season/episode/:episode"
            >
              <Game />
            </PrivateRouteChild>
            <PrivateRouteChild exact path="/knowledge-center">
              <KnowledgeCenter />
            </PrivateRouteChild>
            <PrivateRouteChild exact path="/knowledge-center/season/:season">
              <KnowledgeCenterSeasonOverview />
            </PrivateRouteChild>
            <PrivateRouteChild
              exact
              path="/knowledge-center/season/:season/episode/:episode"
            >
              <KnowledgeCenterKitSelect />
            </PrivateRouteChild>
            <PrivateRouteChild
              exact
              path="/knowledge-center/season/:season/episode/:episode/:kit"
            >
              <KnowledgeCenterKitHome />
            </PrivateRouteChild>
            <PrivateRouteChild
              exact
              path="/knowledge-center/season/:season/episode/:episode/:kit/how-it-works"
            >
              <KnowledgeCenterHowItWorks />
            </PrivateRouteChild>
            <PrivateRouteChild
              exact
              path="/knowledge-center/season/:season/episode/:episode/:kit/history"
            >
              <KnowledgeCenterHistory />
            </PrivateRouteChild>
            <PrivateRouteChild
              exact
              path="/knowledge-center/season/:season/episode/:episode/:kit/science"
            >
              <KnowledgeCenterScience />
            </PrivateRouteChild>
            <PrivateRouteChild
              exact
              path="/knowledge-center/season/:season/episode/:episode/:kit/trivia"
            >
              <KnowledgeCenterTrivia />
            </PrivateRouteChild>
            <PrivateRouteChild
              exact
              path="/knowledge-center/season/:season/episode/:episode/:kit/resources"
            >
              <KnowledgeCenterResources />
            </PrivateRouteChild>

            {/* ADMIN ROUTE DEFINITIONS */}
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={[
                'Superuser',
                'Admin',
                'Editor',
                'Marketer',
                'Fulfillment',
              ]}
              exact
              path="/admin/products"
            >
              <Products />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={['Superuser', 'Admin', 'Editor']}
              exact
              path="/admin/products/season/add"
            >
              <SeasonAdd />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={[
                'Superuser',
                'Admin',
                'Editor',
                'Marketer',
                'Fulfillment',
              ]}
              exact
              path="/admin/products/season/edit/:id"
            >
              <SeasonEdit />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={['Superuser', 'Admin', 'Editor']}
              exact
              path="/admin/products/episode/add"
            >
              <EpisodeAdd />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={[
                'Superuser',
                'Admin',
                'Editor',
                'Marketer',
                'Fulfillment',
              ]}
              exact
              path="/admin/products/episode/edit/:id"
            >
              <EpisodeEdit />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={['Superuser', 'Admin', 'Marketer']}
              exact
              path="/admin/partners"
            >
              <Partners />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={['Superuser', 'Admin', 'Marketer']}
              exact
              path="/admin/partners/add"
            >
              <PartnerAdd />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={['Superuser', 'Admin', 'Marketer']}
              exact
              path="/admin/partners/edit/:id"
              s
            >
              <PartnerEdit />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={['Superuser', 'Admin', 'Editor']}
              exact
              path="/admin/products/activation-code/add"
            >
              <ActivationCodeAdd />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={['Superuser', 'Admin', 'Fulfillment', 'Support']}
              exact
              path="/admin/orders"
            >
              <Orders />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={['Superuser', 'Admin', 'Support']}
              exact
              path="/admin/accounts"
            >
              <Accounts />
            </AdminRoute>
            <AdminRoute
              usersRoles={usersRoles}
              rolesAllowed={['Superuser', 'Admin', 'Support']}
              exact
              path="/admin/settings"
            >
              <Settings />
            </AdminRoute>
            <Route>
              <NotFound />
            </Route>
          </Switch>
        </Suspense>
        {!usersRoles.includes('Child') && <Footer />}
      </Box>
    </>
  );
};

export default App;
