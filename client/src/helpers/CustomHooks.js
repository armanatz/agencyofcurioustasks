import { useToast } from '@chakra-ui/react';
import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useQueryClient, useMutation } from 'react-query';
import actual from 'actual';

export const useQueryParam = () => {
  return new URLSearchParams(useLocation().search);
};

export const useWindowDimensions = () => {
  const [width, setWidth] = useState(actual('width', 'px'));
  const [height, setHeight] = useState(actual('height', 'px'));

  useEffect(() => {
    let timeoutId = null;

    const resizeListener = () => {
      clearTimeout(timeoutId);
      timeoutId = setTimeout(() => {
        setWidth(actual('width', 'px'));
        setHeight(actual('height', 'px'));
      }, 150);
    };

    window.addEventListener('resize', resizeListener);

    return () => {
      window.removeEventListener('resize', resizeListener);
    };
  }, []);

  return { width, height };
};

export const useLogout = () => {
  const queryClient = useQueryClient();
  const toast = useToast();

  return useMutation(
    () => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/auth/logout`,
        {
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries('authStatus');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );
};
