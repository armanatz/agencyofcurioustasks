import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import {
  Box,
  Heading,
  Text,
  Stack,
  Flex,
  Button,
  VStack,
  Container,
  SimpleGrid,
  HStack,
  Icon,
  Image,
} from '@chakra-ui/react';
import { ReactSVG } from 'react-svg';
import { IoIosRocket } from 'react-icons/io';
import { useQueryClient } from 'react-query';

import SpyIcon from '../../assets/landing/spy.svg';
import LaptopIcon from '../../assets/landing/laptop.svg';
import CreativityIcon from '../../assets/landing/creativity.svg';
import GameIcon from '../../assets/landing/game.svg';

import Laptop2Icon from '../../assets/landing/laptop2.svg';
import SubscriptionIcon from '../../assets/landing/subscription.svg';
import BoxIcon from '../../assets/landing/box.svg';

const Home = () => {
  const queryClient = useQueryClient();
  const authStatus = queryClient.getQueryData('authStatus');
  const user = queryClient.getQueryData('user');

  const features = [
    {
      text: 'Join a secret agency and solve mysterious missions using clues, riddles and puzzles.',
      icon: SpyIcon,
    },
    {
      text: 'Gain access to a web-based interactive learning platform.',
      icon: LaptopIcon,
    },
    {
      text: 'Apply STEM knowledge through hands on applicationas the exciting story unravels.',
      icon: CreativityIcon,
    },
    {
      text: 'Enjoy fun digital games while nurturing cognitive abilities.',
      icon: GameIcon,
    },
  ];

  const howItWorks = [
    {
      text: 'Each mission (episode) is specifically crafted for parent and child to collaborate on.',
      icon: SpyIcon,
    },
    {
      text: 'Register for Season 1 but only be charged monthly over 9 months.*',
      icon: LaptopIcon,
    },
    {
      text: 'Every box is an episode of Season 1 that is interconnected to a bigger story that unravels over 9 episodes.',
      icon: CreativityIcon,
    },
    {
      text: 'Every episode will be delivered monthly.',
      icon: GameIcon,
    },
  ];

  if (authStatus?.isAuthenticated) {
    if (user?.roles.includes('Child')) {
      return <Redirect to="/games" />;
    } else if (user?.roles.includes('Adult')) {
      return <Redirect to="/dashboard" />;
    }
  }

  return (
    <Box pt="82px">
      <Flex
        w="full"
        h={{ base: '55vh', md: '91vh' }}
        backgroundImage={
          'url(https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/home_hero.jpg)'
        }
        backgroundSize="cover"
        backgroundPosition="center center"
        mb={10}
        boxShadow="inset 0 0 0 1000px rgba(0, 0, 0, 0.3)"
      >
        <VStack
          w="full"
          justify="center"
          px={{ base: 4, md: 8, lg: 0 }}
          bgGradient="linear(to-r, blackAlpha.600, transparent)"
          position="relative"
        >
          <Stack maxW="2xl" align="center" spacing={2}>
            <Heading
              textAlign="center"
              color="white"
              fontWeight={700}
              lineHeight={1.2}
              fontSize={{ base: '2xl', md: '4xl' }}
            >
              Preparing young minds for the 21st century through creativity and
              role-play.
            </Heading>
            <Text color="white">Delivered to your doorstep, monthly</Text>
            <Button as={Link} to="/signup" textAlign="center">
              Get Started
            </Button>
          </Stack>
          <Box
            position="absolute"
            bottom="0"
            bgColor="cerulean.500"
            w="full"
            py={8}
            px={4}
            display={{ base: 'none', md: 'block' }}
          >
            <SimpleGrid templateColumns="100px 1fr 100px">
              <Flex align="center" justify="center">
                <Image
                  src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/element_icon.png"
                  w="50px"
                />
              </Flex>
              <Box textAlign="center">
                <Heading color="wattle.500" fontSize="28px">
                  Education needs to shift from a 20th century mindset that is
                  memory-based
                </Heading>
                <Text color="white">
                  ACT{"'"}s mission is to fix that through a modern and
                  interactive experience
                </Text>
              </Box>
              <Flex align="center" justify="center">
                <Image
                  src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/element_icon.png"
                  w="50px"
                />
              </Flex>
            </SimpleGrid>
          </Box>
        </VStack>
      </Flex>
      <Box mb={10} px={4}>
        <Stack spacing={4} as={Container} maxW="3xl" textAlign="center">
          <Heading fontSize={{ base: '2xl', md: '3xl' }}>
            ACT is an immersive story-based series of STEM kits that takes the
            shape of secret missions
          </Heading>
          {/* <Text fontSize="xl">Inspire STEM learning in your child</Text> */}
        </Stack>
        <Container maxW="5xl" mt={10}>
          <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10} my={10}>
            {features.map((feature, i) => (
              <HStack key={i} align="center">
                <Box px={2}>
                  <Flex
                    rounded="full"
                    backgroundColor="goldenrod.500"
                    w="80px"
                    h="80px"
                    justify="center"
                    align="center"
                    textAlign="center"
                  >
                    <ReactSVG
                      src={feature.icon}
                      beforeInjection={svg => {
                        svg.setAttribute(
                          'style',
                          'width: 60px; height: 60px; fill: #F87375;',
                        );
                      }}
                    />
                  </Flex>
                </Box>
                <VStack align="start">
                  <Text>{feature.text}</Text>
                </VStack>
              </HStack>
            ))}
          </SimpleGrid>
          <Box textAlign="center">
            <Button
              as={Link}
              to="/signup"
              variant="secondarySolid"
              w={{ base: 'full', md: 'auto' }}
            >
              Begin Adventure
            </Button>
          </Box>
        </Container>
      </Box>
      <Stack
        minH="40vh"
        flexDir={{ base: 'column', md: 'row' }}
        backgroundColor="goldenrod.500"
        textAlign={['center', 'left']}
      >
        <Flex flex={1}>
          <Image
            alt="Iguana Island map"
            objectFit="cover"
            w="full"
            h="full"
            src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/map.jpg"
          />
        </Flex>
        <Flex p={8} flex={1} align="center" justify="center">
          <Stack spacing={6} w="full" maxW="lg">
            <Heading fontSize={{ base: '2xl', md: '3xl', lg: '4xl' }}>
              Season 1: The Curious Case of the Keystone
            </Heading>
            <Text fontSize={{ lg: 'xl' }}>
              A scientist has been reported missing while working on a rare
              element on a remote island.
            </Text>
            <Text>
              Your mission should you choose to accept it is to investigate his
              disappearance and retrieve the element
            </Text>
            <Stack direction={{ base: 'column', md: 'row' }} spacing={4}>
              <Button as={Link} to="/signup" variant="secondarySolid">
                Begin Adventure
              </Button>
            </Stack>
          </Stack>
        </Flex>
      </Stack>
      <Stack
        minH="40vh"
        direction={{ base: 'column-reverse', md: 'row' }}
        backgroundColor="azalea.500"
        textAlign={['center', 'left']}
      >
        <Flex p={8} flex={1} align="center" justify="center">
          <Stack spacing={6} w="full" maxW="lg">
            <Heading fontSize={{ base: '2xl', md: '3xl', lg: '4xl' }}>
              Make Your Own Adventure With Your Child
            </Heading>
            <Text fontSize={{ base: 'md', lg: 'lg' }}>
              Bond over STEM projects with your child using principles of
              science and learn how to apply it in an immersive online and
              offline journey
            </Text>
            <Stack direction={{ base: 'column', md: 'row' }} spacing={4}>
              <Button as={Link} to="/signup" variant="secondarySolid">
                Begin Adventure
              </Button>
            </Stack>
          </Stack>
        </Flex>
        <Flex flex={1}>
          <Image
            alt="Family enjoying ACT together"
            objectFit="cover"
            w="full"
            h="full"
            src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/home1.jpg"
          />
        </Flex>
      </Stack>
      <Stack
        minH="40vh"
        direction={{ base: 'column', md: 'row' }}
        backgroundColor="wattle.500"
        textAlign={['center', 'left']}
      >
        <Flex flex={1}>
          <Image
            alt="STEM graphic"
            objectFit="cover"
            w="full"
            h="full"
            src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/stem.png"
          />
        </Flex>
        <Flex p={8} flex={1} align="center" justify="center">
          <Stack spacing={6} w="full" maxW="lg">
            <Heading fontSize={{ base: '2xl', md: '3xl', lg: '4xl' }}>
              What is STEM?
            </Heading>
            <Text fontSize={{ lg: 'xl' }}>
              STEM stands for Science, Technology, Engineering {'&'} Math
            </Text>
            <Text>
              It{"'"}s important to develop STEM education from a young age to
              nurture your child{"'"}s learning potential
            </Text>
            <Stack direction={{ base: 'column', md: 'row' }} spacing={4}>
              <Text fontWeight={700} fontFamily="Changa">
                Start them young {'&'} watch them soar
              </Text>
            </Stack>
          </Stack>
        </Flex>
      </Stack>
      <Flex justify="center" backgroundColor="linen.500" py={10} mb={10}>
        <Image
          objectFit="cover"
          w="full"
          h="full"
          src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/components.png"
        />
      </Flex>
      <Box mb={10} px={4}>
        <Stack spacing={4} as={Container} maxW="3xl" textAlign="center">
          <Heading fontSize="3xl">Sounds Cool, But How Does It Work?</Heading>
        </Stack>
        <Stack spacing={10}>
          <Container maxW="5xl" mt={10}>
            <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10}>
              {howItWorks.map((feature, i) => (
                <HStack key={i} align="center">
                  <Box px={2}>
                    <Flex
                      rounded="full"
                      backgroundColor="goldenrod.500"
                      w="80px"
                      h="80px"
                      justify="center"
                      align="center"
                      textAlign="center"
                    >
                      <Icon
                        as={IoIosRocket}
                        color="froly.500"
                        fontSize="60px"
                      />
                    </Flex>
                  </Box>
                  <VStack align="start">
                    <Text>{feature.text}</Text>
                  </VStack>
                </HStack>
              ))}
            </SimpleGrid>
          </Container>
          <Box textAlign="center">
            <Text fontSize="14px" color="gray.500" mb={10}>
              * Free to opt-out prior to the next scheduled charge.
            </Text>
            <Button
              as={Link}
              to="/signup"
              variant="secondarySolid"
              w={{ base: 'full', md: 'auto' }}
            >
              Begin Adventure
            </Button>
          </Box>
        </Stack>
      </Box>
      <Flex
        py={16}
        px={4}
        backgroundColor="froly.500"
        align="center"
        flexDir="column"
        color="white"
      >
        <Heading fontSize="3xl" textAlign="center">
          Simple to Start. Fulfilling to Finish.
        </Heading>
        <SimpleGrid columns={{ base: 1, md: 3 }} spacing={10} my={10}>
          <Stack
            alignItems="center"
            direction={{ base: 'row', md: 'column' }}
            spacing={6}
          >
            <Box>
              <Flex
                w={24}
                h={24}
                align="center"
                justify="center"
                color="white"
                bg="wattle.500"
                mb={2}
                rounded="full"
              >
                <ReactSVG
                  src={Laptop2Icon}
                  beforeInjection={svg => {
                    svg.setAttribute(
                      'style',
                      'width: 60px; height: 60px; fill: #047FAA;',
                    );
                  }}
                />
              </Flex>
            </Box>
            <Box
              maxW={{ base: '65%', sm: '350px' }}
              textAlign={['left', 'center']}
            >
              <Text fontWeight={800} mb={2} fontSize="20px">
                Step 1
              </Text>
              <Text>Sign up and register your child as a secret agent.</Text>
            </Box>
          </Stack>
          <Stack
            alignItems="center"
            direction={{ base: 'row', md: 'column' }}
            spacing={6}
          >
            <Box>
              <Flex
                w={24}
                h={24}
                align="center"
                justify="center"
                color="white"
                bg="wattle.500"
                mb={2}
                rounded="full"
              >
                <ReactSVG
                  src={SubscriptionIcon}
                  beforeInjection={svg => {
                    svg.setAttribute(
                      'style',
                      'width: 60px; height: 60px; fill: #047FAA;',
                    );
                  }}
                />
              </Flex>
            </Box>
            <Box
              maxW={{ base: '65%', sm: '350px' }}
              textAlign={['left', 'center']}
            >
              <Text fontWeight={800} mb={2} fontSize="20px">
                Step 2
              </Text>
              <Text>Subscribe to your first series of boxes in minutes.</Text>
            </Box>
          </Stack>
          <Stack
            alignItems="center"
            direction={{ base: 'row', md: 'column' }}
            spacing={6}
          >
            <Box>
              <Flex
                w={24}
                h={24}
                align="center"
                justify="center"
                color="white"
                bg="wattle.500"
                mb={2}
                rounded="full"
              >
                <ReactSVG
                  src={BoxIcon}
                  beforeInjection={svg => {
                    svg.setAttribute(
                      'style',
                      'width: 60px; height: 60px; fill: #047FAA;',
                    );
                  }}
                />
              </Flex>
            </Box>
            <Box
              maxW={{ base: '65%', sm: '350px' }}
              textAlign={['left', 'center']}
            >
              <Text fontWeight={800} mb={2} fontSize="20px">
                Step 3
              </Text>
              <Text>
                Receive a box every month filled with fun, educational projects
                and activities.
              </Text>
            </Box>
          </Stack>
        </SimpleGrid>
        <Button as={Link} to="/signup" w={{ base: 'full', md: 'auto' }}>
          Begin Adventure
        </Button>
      </Flex>
    </Box>
  );
};

export default Home;
