import React from 'react';
import { Link } from 'react-router-dom';
import {
  Box,
  Flex,
  VStack,
  Stack,
  Heading,
  Text,
  Container,
  Link as ChakraLink,
} from '@chakra-ui/react';
import { Helmet } from 'react-helmet';

const Privacy = () => {
  return (
    <>
      <Helmet>
        <title>Privacy Policy - Agency of Curious Tasks</title>
      </Helmet>
      <Box pt="82px" pb={['20px', '60px']}>
        <Flex w="full" minH="30vh" bgColor="goldenrod.500" py={16} mb={10}>
          <VStack w="full" justify="center" px={{ base: 4, md: 8 }}>
            <Stack maxW="2xl" align="center" spacing={6} textAlign="center">
              <Heading
                color="white"
                fontWeight={700}
                lineHeight={1.2}
                fontSize={{ base: '3xl', md: '4xl' }}
              >
                Privacy Policy
              </Heading>
            </Stack>
          </VStack>
        </Flex>
        <Container maxW="6xl">
          <VStack spacing={10} align="flex-start">
            <Text>Last Updated on 21 June 2021</Text>
            <Text>
              <strong>1. Introduction</strong>
              <br />
              <br />
              We,{' '}
              <strong>
                FlipEd Sdn Bhd (1153403-H) ({'"'}Company{'"'}, {'"'}We{'"'},{' '}
                {'"'}Our{'"'} or {'"'}Us{'"'})
              </strong>
              , reserve the right to modify this privacy policy ({'"'}Policy
              {'"'}) at any time. We encourage you to check this page
              periodically for any changes. This Policy is incorporated into,
              and subject to, the{' '}
              <ChakraLink
                as={Link}
                to="/terms"
                target="_blank"
                rel="noopener noreferrer"
                color="cerulean.500"
              >
                Terms {'&'} Conditions
              </ChakraLink>{' '}
              . By using or navigating Our website located at{' '}
              <ChakraLink
                as={Link}
                to="/"
                target="_blank"
                rel="noopener noreferrer"
                color="cerulean.500"
              >
                https://www.agencyofcurioustasks.com/
              </ChakraLink>{' '}
              any mobile applications, plug-ins, or other means or any other
              products or services offered by Us (collectively, the {'"'}
              <strong>Services</strong>
              {'"'}), you acknowledge that you have read, understand, and agree
              to be bound by this Policy or any modified Policy as posted. If
              you do not agree to these terms, please do not use or access the
              Services.
            </Text>
            <Text>
              <strong>2. Information Collected</strong>
              <br />
              <br />
              We do not collect personally identifiable information except where
              you specifically provide Us with such information. With your
              consent, We may collect your full name, e-mail address, street
              address, telephone number, and other contact information. Our
              payment gateways, which are operated by third parties, may also
              collect personal data from you (please refer to the payment
              gateway’s privacy policy for more information). We generally
              collect this personal information on our registration, enquiries
              and order forms when you sign up to purchase and receive products
              or services through Us.
              <br />
              <br />
              Information such as IP addresses, screen resolution, browser
              language, etc.) and anonymous demographic information (such as
              your postal code, the web site that you came from, etc.) is
              collected during your usage of the Services.
            </Text>
            <Text>
              <strong>3. Use of Information</strong>
              <br />
              <br />
              We use personal information and other demographic or profile
              information you provide to Us to fulfill the sales order or
              services, to attempt to prevent fraudulent transactions, to
              improve the content of Our web page in order to enhance your
              experience, and to contact you when necessary.
              <br />
              <br />
              Whenever possible, We use non-identifiable information (such as IP
              addresses and anonymous demographic information) rather than
              personal information. Non-identifiable information is used to
              tailor your experiences with Our Services by showing content in
              which We think you will be interested and displaying content
              according to your preferences. We may use aggregate data for a
              variety of purposes, including analysing user behaviour and
              characteristics in order to measure interest in (and use of) the
              various portions and areas of our Services. We also use the
              information collected to evaluate and improve our Services and
              analyse traffic to Our Services.
            </Text>
            <Text>
              <strong>4. Security</strong>
              <br />
              <br />
              We are committed to maintaining the security of your information
              and have measures in place to protect against the loss, misuse,
              and alteration of the information under Our control. Our employees
              who have access to, or are associated with, the processing of
              personal information are contractually obligated to respect the
              confidentiality of your information and abide by the privacy
              standards We have established. Please be aware that no security
              measures are perfect or impenetrable. Therefore, although We use
              industry standard practices to protect your privacy, We cannot
              (and do not) guarantee the absolute security of personal
              information.
            </Text>
            <Text>
              <strong>5. Sharing of Information with Third Parties</strong>
              <br />
              <br />
              We may share your information with our agents, processors, or
              contractors in order to 1process the sales order or provide the
              services. We may also transfer information between our affiliates
              and related companies. You should note, however, that We are
              opposed to spam mail activities and does not participate in such
              mailings, nor do We release 2the use of data for such purposes.
              <br />
              <br />
              We may use statistical analysis of aggregate information to inform
              advertisers of aggregate user demographics and 2behavior, as well
              as the number of users that have been exposed to or clicked on
              their advertising banners. We will provide only aggregate data
              from these analyses to third parties.
              <br />
              <br />
              We may share information We collect with third party service
              providers to manage certain aspects of the services We provide,
              such as maintaining Our servers and processing or fulfilling
              orders for products and services you purchase through the
              Services. We ensure that the third party service providers that We
              share your information with have privacy policies in place
              substantially similar to Our own. With respect to any information
              that you provide to Our payment gateway providers, such
              information will be stored and used only to the extent necessary
              to process payments made to Us, and in accordance with the payment
              gateway provider’s own privacy policy.
              <br />
              <br />
              We may transfer your information to a third party as a result of a
              sale, acquisition, merger, or re-organisation involving Us, Our
              affiliates, related companies, or any of Our respective assets.
              <br />
              <br />
              We may also disclose your information in special cases if required
              to do so by law, court order, or other governmental authority, or
              when We believe in good faith that disclosing this information is
              otherwise necessary or advisable, such as to identify, contact, or
              bring legal action against someone who may be causing injury to –
              or interfering with – Our rights or property, the Services,
              another user, or anyone else that could be harmed by such
              activities (for example, identify theft or fraud).
            </Text>
            <Text>
              <strong>6. Use of Cookies</strong>
              <br />
              <br />
              {'"'}Cookies{'"'} are pieces of information generated by web
              servers and stored in your computer for future access. Cookies
              cannot view or retrieve data from other cookies, nor can they
              capture files or data stored on your computer. We, our partners,
              and our suppliers use cookie technology to enhance your online
              experience by making it easier for you to navigate through our
              Services or make a feature work better. We also use cookies to
              track affiliates that refer you to Our Services. These particular
              cookies are saved on your computer and collect no data from you.
              Generally, cookies can be disabled. The {'"'}help{'"'} portion of
              the toolbar on most browsers will tell you how to prevent your
              browser from accepting new cookies, how to have the browser notify
              you when you receive a new cookie, or how to disable cookies
              altogether. If you would like to use the Services, you may do so
              without accepting cookies. However, you should understand that if
              you choose not to accept cookies, some areas of the Services may
              not function properly or optimally and you will not be permitted
              to access certain secured areas of the Services.
            </Text>
            <Text>
              <strong>7. Use of Web Beacons</strong>
              <br />
              <br />
              {'"'}Web beacons{'"'} (also known as {'"'}web bugs{'"'}) are small
              images, generally a single transparent pixel, served to you as
              part of a web page or other document. Web beacons may be used in
              parts of Our Services for purposes such as site traffic reporting,
              unique visitor counts, advertising auditing and reporting, and
              personalization. These beacons do not collect personally
              identifiable information, and do not transfer any personally
              identifiable information to third parties. If beacons are used by
              any of Our advertising services suppliers, that supplier’s privacy
              policy governs their use.
            </Text>
            <Text>
              <strong>8. Use of Advertisements</strong>
              <br />
              <br />
              Any advertisements that may appear in the Services will generally
              be delivered (or {'"'}served{'"'}) directly to you by third party
              advertisers, and they automatically receive your IP address when
              this happens. These third party advertisers may also download
              cookies to your computer or use other technologies to measure the
              effectiveness of their advertisements and to personalise
              advertising content. Doing this allows them to recognise your
              computer each time they send you an advertisement in order to
              measure the effectiveness of their advertisements and to
              personalise advertising content. In this way, they may compile
              information about where individuals using your computer or browser
              saw their advertisements and determine which advertisements were
              clicked. Third-party advertisers have no access to the information
              you have provided to Us unless you choose to provide it to them
              yourself directly.
            </Text>
            <Text>
              <strong>9. Retention of Information</strong>
              <br />
              <br />
              Different information is used for different purposes, and is
              subject to different standards and regulations. In general,
              information is retained for such period of time as is felt
              necessary to provide you with services you request, to comply with
              applicable regulations or legislation, and to ensure that you have
              a reasonable opportunity to access the information. If you no
              longer consent to Us retaining your personal information, you can
              request that it be removed by contacting Us at{' '}
              <ChakraLink
                href="mailto:enquiries@agencyofcurioustasks.com"
                color="cerulean.500"
              >
                enquiries@agencyofcurioustasks.com
              </ChakraLink>
              <br />
              <br />
              If you send Us correspondence, including e-mails and faxes, We may
              retain such information along with any records of your account. We
              may also retain customer service correspondence and other
              correspondence involving you, Our partners, and Our suppliers. We
              may retain these records in order to provide you with services you
              request, to measure and improve our customer service, and to
              investigate potential fraud and violation of Our Terms {'&'}
              Conditions. We may, over time, delete these records.
            </Text>
            <Text>
              <strong>10. Third Party Websites</strong>
              <br />
              <br />
              The Services may contain links to other Internet websites. By
              clicking on a third party advertising banner or certain other
              links, you will be redirected out of the Services and to such
              third party websites.
              <br />
              <br />
              We are not responsible for the privacy policies of other websites
              or services. You should make sure that you read and understand any
              applicable third party privacy policies, and you should direct any
              questions or concerns to the relevant third party administrators
              or webmasters prior to providing any personally identifiable
              information.
            </Text>
            <Text>
              <strong>11. Unsubscribing</strong>
              <br />
              <br />
              With your permission, We sometimes e-mail you in order to provide
              targeted marketing from our Services. We only send such e-mails
              when you have specifically requested them. All e‑mails you receive
              from Us will include specific instructions on how to unsubscribe
              and you may unsubscribe at any time.
            </Text>
          </VStack>
        </Container>
      </Box>
    </>
  );
};

export default Privacy;
