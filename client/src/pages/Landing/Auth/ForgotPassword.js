import React, { useState } from 'react';
import {
  Box,
  Heading,
  Text,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  Flex,
  useToast,
} from '@chakra-ui/react';
import { useMutation } from 'react-query';
import { Helmet } from 'react-helmet';

import ForgotPasswordForm from '../../../components/Auth/ForgotPasswordForm';

const ForgotPassword = () => {
  const [submitting, setSubmitting] = useState(false);
  const toast = useToast();

  const mutation = useMutation(
    data => {
      setSubmitting(true);
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/auth/password/forgot`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async res => {
        if (res.ok) {
          setSubmitting(false);

          const data = await res.json();

          return toast({
            title: 'Reset Password Link Sent!',
            status: 'success',
            description: data.message,
            duration: 5000,
            isClosable: true,
          });
        }

        setSubmitting(false);
        const body = await res.json();
        return toast({
          title: 'Could not signup',
          status: 'error',
          description: `${body.message}`,
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    return mutation.mutate(values);
  };

  return (
    <>
      <Helmet>
        <title>Forgot Password - Agency of Curious Tasks</title>
      </Helmet>
      <Box
        px={{ base: '6', lg: '8' }}
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box maxW={{ sm: 'md' }} mx={{ sm: 'auto' }} w={{ sm: 'full' }}>
          <Heading mt="6" textAlign="center" size="xl" fontWeight="extrabold">
            Forgot your password?
          </Heading>
        </Box>
        <Box maxW={{ sm: 'md' }} mx={{ sm: 'auto' }} mt="8" w={{ sm: 'full' }}>
          <Box
            borderWidth={0.5}
            rounded="md"
            borderColor="grey.70"
            px={[4, 8]}
            py={[4, 8]}
            backgroundColor="white"
          >
            <Tabs isFitted variant="soft-rounded">
              <TabList mb={6}>
                <Tab
                  _selected={{
                    bg: 'cerulean.500',
                    color: 'white',
                  }}
                  borderLeftWidth="0"
                >
                  Parent
                </Tab>
                <Tab
                  _selected={{
                    bg: 'cerulean.500',
                    color: 'white',
                  }}
                  borderRightWidth="0"
                >
                  Child
                </Tab>
              </TabList>
              <TabPanels>
                <TabPanel p="0">
                  <Box
                    borderTopRadius="0"
                    borderTopWidth="0"
                    borderBottomRadius="lg"
                  >
                    <ForgotPasswordForm
                      onSubmit={onSubmit}
                      submitting={submitting}
                    />
                  </Box>
                </TabPanel>
                <TabPanel p="0">
                  <Flex minH="136px" align="center">
                    <Text>
                      Please ask your parent to help you reset your password
                      from within their account.
                    </Text>
                  </Flex>
                </TabPanel>
              </TabPanels>
            </Tabs>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default ForgotPassword;
