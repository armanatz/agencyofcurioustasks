import React, { useEffect, useState } from 'react';
import {
  Box,
  Heading,
  Text,
  Link as ChakraLink,
  useToast,
} from '@chakra-ui/react';
import { Link, Redirect } from 'react-router-dom';
import { useMutation, useQueryClient } from 'react-query';
import { Helmet } from 'react-helmet';

import LoginForm from '../../../components/Auth/LoginForm';

const Login = () => {
  const [submitting, setSubmitting] = useState(false);
  const toast = useToast();
  const queryClient = useQueryClient();
  const authStatus = queryClient.getQueryData('authStatus');
  const user = queryClient.getQueryData('user');

  const loginMutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/auth/login`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async data => {
        setSubmitting(false);
        if (data.ok) {
          return queryClient.invalidateQueries('authStatus');
        }

        const body = await data.json();

        if (
          data.status === 403 &&
          body.message === 'Email has not been verified.'
        ) {
          return toast({
            render: () => (
              <Box color="white" bg="orange.300" p={4} rounded="md">
                <Text>
                  Email is not yet verified.{' '}
                  <ChakraLink
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ textDecoration: 'none' }}
                    onClick={() => resendVerificationEmail(body.email)}
                  >
                    Click here
                  </ChakraLink>{' '}
                  to resend the verification link.
                </Text>
              </Box>
            ),
            duration: 10000,
          });
        }

        return toast({
          title: 'Could not log in',
          status: 'error',
          description: `${body.message}`,
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  useEffect(() => {
    queryClient.invalidateQueries('authStatus');
    queryClient.invalidateQueries('user', {
      refetchInactive: false,
      refetchActive: false,
    });
    queryClient.resetQueries('user');
  }, [queryClient]);

  const resendVerificationEmail = async email => {
    try {
      const req = await fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users/verify/email/resend`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify({ email }),
        },
      );

      const res = await req.json();

      if (req.ok) {
        return toast({
          description: res.message,
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      }

      return toast({
        description: res.message,
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
    } catch (err) {
      return toast({
        title: 'Woops, an error occurred :(',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  };

  const onLogin = values => {
    setSubmitting(true);
    return loginMutation.mutate(values);
  };

  if (authStatus?.isAuthenticated) {
    if (user?.roles.includes('Child')) {
      return <Redirect to="/games" />;
    } else if (user?.roles.includes('Adult')) {
      return <Redirect to="/dashboard" />;
    }
  }

  return (
    <>
      <Helmet>
        <title>Log In - Agency of Curious Tasks</title>
      </Helmet>
      <Box
        px={{ base: '6', lg: '8' }}
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box maxW={{ sm: 'md' }} mx={{ sm: 'auto' }} w={{ sm: 'full' }}>
          <Heading mt={10} textAlign="center" size="xl" fontWeight="extrabold">
            Log in to your account
          </Heading>
          <Text mt="4" align="center" maxW="md" fontWeight="medium">
            <span>Don&apos;t have an account?</span>
            <ChakraLink
              as={Link}
              marginStart="1"
              to="/signup"
              color="cerulean.500"
              display={{ base: 'block', sm: 'revert' }}
            >
              Sign up
            </ChakraLink>
          </Text>
        </Box>
        <Box maxW={{ sm: 'md' }} mx={{ sm: 'auto' }} mt="8" w={{ sm: 'full' }}>
          <Box borderWidth={0.5} borderRadius="lg" borderColor="grey.70">
            <Box
              py="8"
              px={{ base: '4', md: '10' }}
              borderRadius="lg"
              bg="white"
            >
              <LoginForm onSubmit={onLogin} submitting={submitting} />
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Login;
