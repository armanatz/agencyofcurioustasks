import React, { useState } from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import {
  Box,
  Heading,
  Text,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  Link,
  SimpleGrid,
  Flex,
  Icon,
  useToast,
} from '@chakra-ui/react';
import { ImQuotesLeft } from 'react-icons/im';
import { useMutation } from 'react-query';
import { Helmet } from 'react-helmet';

import SignupForm from '../../../components/Auth/SignupForm';

const Signup = () => {
  const [submitting, setSubmitting] = useState(false);
  const history = useHistory();
  const toast = useToast();

  const mutation = useMutation(
    data => {
      setSubmitting(true);
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async res => {
        if (res.ok) {
          setSubmitting(false);

          const data = await res.json();

          toast({
            title: 'Thank you for signing up!',
            status: 'success',
            description: data.message,
            duration: 5000,
            isClosable: true,
          });

          return history.push('/login');
        }

        setSubmitting(false);
        const body = await res.json();
        return toast({
          title: 'Could not signup',
          status: 'error',
          description: `${body.message}`,
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    const data = { ...values };

    delete data.terms;
    delete data.privacy;

    if (values.mobilePhone1) {
      delete data.mobilePhone1;
    }

    data.type = 'Adult';

    delete data.confirmPassword;

    if (!values.referralCode) {
      delete data.referralCode;
    }

    if (values.lastName.length === 0) {
      delete data.lastName;
    }

    return mutation.mutate(data);
  };

  return (
    <>
      <Helmet>
        <title>Sign Up - Agency of Curious Tasks</title>
      </Helmet>
      <Box
        px={{ base: '6', lg: '8' }}
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <SimpleGrid columns={{ base: 1, xl: 2 }} spacing={10}>
          <Box display="flex" flexDir="column" alignItems="center">
            <Box
              bg="white"
              rounded="lg"
              shadow="base"
              py={10}
              mx={{ base: 0, xl: 12, '2xl': 32 }}
            >
              <Box px={[4, '50px']}>
                {/* <Center textAlign="center">
                <Image src={Logo} w="200px" />
              </Center> */}
                <Heading
                  mt="6"
                  textAlign="center"
                  size="lg"
                  fontWeight="extrabold"
                >
                  Your child&apos;s journey at the Agency of Curious Tasks
                  starts here!
                </Heading>
              </Box>
              <Box mt="8" px={[4, 8]}>
                <Tabs isFitted variant="soft-rounded">
                  <TabList>
                    <Tab
                      _selected={{
                        bg: 'cerulean.500',
                        color: 'white',
                      }}
                      mr={2}
                    >
                      Parent
                    </Tab>
                    <Tab
                      _selected={{
                        bg: 'cerulean.500',
                        color: 'white',
                      }}
                    >
                      Child
                    </Tab>
                  </TabList>
                  <TabPanels>
                    <TabPanel p="0">
                      <Box mt={8}>
                        <SignupForm
                          onSubmit={onSubmit}
                          isLoading={submitting}
                        />
                      </Box>
                    </TabPanel>
                    <TabPanel p="0">
                      <Flex pt={8} alignItems="flex-start" flexDir="column">
                        <Heading size="md">Attention!</Heading>
                        <Text>
                          In order to sign up, you need to ask your parent to
                          create an account for you after they have created one
                          for themselves.
                        </Text>
                      </Flex>
                    </TabPanel>
                  </TabPanels>
                </Tabs>
              </Box>
            </Box>
            <Box mx="auto">
              <Text mt="4" align="center" maxW="md" fontWeight="medium">
                <span>Already have an account?</span>
                <Link
                  as={RouterLink}
                  marginStart="1"
                  to="/login"
                  color="cerulean.500"
                  display={{ base: 'block', sm: 'revert' }}
                >
                  Log in
                </Link>
              </Text>
            </Box>
          </Box>
          <Box display={{ base: 'none', xl: 'block' }} mt={32} mx="auto">
            <Box>
              <Box as="blockquote" display="flex" flex="1 1 0%">
                <Box mr={4} fontSize={36} color="goldenrod.400">
                  <Icon as={ImQuotesLeft} />
                </Box>
                <Box>
                  <Text mt={10} fontSize="lg" maxW="lg">
                    I love seeing the genuine excitement and enthusiasm on her
                    face when it{"'"}s delivered each month. It was cool that
                    she was working on a STEM project without even realizing it.
                    I can
                    {"'"}t recommend this learning concept highly enough for
                    curious-minded kids.
                  </Text>
                  <Flex mt={8}>
                    {/* <Box mr={4}>
                      <Avatar size="lg" />
                    </Box> */}
                    <Flex flexDir="column" justifyContent="center">
                      <Text fontWeight="bold">Joshua Fernandez</Text>
                      <Text>Parent of an amazing child</Text>
                    </Flex>
                  </Flex>
                </Box>
              </Box>
            </Box>
          </Box>
        </SimpleGrid>
      </Box>
    </>
  );
};

export default Signup;
