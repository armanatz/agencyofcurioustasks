import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Box, Heading, useToast } from '@chakra-ui/react';
import { useMutation } from 'react-query';
import { Helmet } from 'react-helmet';

import ResetPasswordForm from '../../../components/Auth/ResetPasswordForm';

import { useQueryParam } from '../../../helpers/CustomHooks';

const ResetPassword = () => {
  const [submitting, setSubmitting] = useState(false);
  const toast = useToast();
  const queryParam = useQueryParam();
  const history = useHistory();

  const mutation = useMutation(
    data => {
      setSubmitting(true);
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/auth/password/reset`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async res => {
        const data = await res.json();

        if (res.ok) {
          setSubmitting(false);
          toast({
            description: data.message,
            status: 'success',
            duration: 5000,
            isClosable: true,
          });
          return history.push('/login');
        }

        setSubmitting(false);
        return toast({
          description: `${data.message}`,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    const data = { ...values };
    delete data.confirmPassword;

    data.changePasswordId = queryParam.get('id');

    return mutation.mutate(data);
  };

  return (
    <>
      <Helmet>
        <title>Reset Password - Agency of Curious Tasks</title>
      </Helmet>
      <Box
        px={{ base: '6', lg: '8' }}
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box maxW={{ sm: 'md' }} mx={{ sm: 'auto' }} w={{ sm: 'full' }}>
          <Heading mt="6" textAlign="center" size="xl" fontWeight="extrabold">
            Reset your password
          </Heading>
        </Box>
        <Box maxW={{ sm: 'md' }} mx={{ sm: 'auto' }} mt="8" w={{ sm: 'full' }}>
          <Box
            borderWidth={0.5}
            rounded="md"
            borderColor="grey.70"
            px={[4, 8]}
            py={[4, 8]}
            backgroundColor="white"
          >
            <ResetPasswordForm onSubmit={onSubmit} submitting={submitting} />
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default ResetPassword;
