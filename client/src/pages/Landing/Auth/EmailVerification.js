import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useToast } from '@chakra-ui/react';

import Loading from '../../../components/Loading';

import { useQueryParam } from '../../../helpers/CustomHooks';

const ForgotPassword = () => {
  const queryParam = useQueryParam();
  const history = useHistory();
  const toast = useToast();

  const verificationId = queryParam.get('id');

  useEffect(() => {
    window
      .fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users/verify/email`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify({ verificationId }),
        },
      )
      .then(res => {
        if (res.ok) {
          toast({
            title: 'Email verified',
            description:
              'Please login using the email and password you used to signup.',
            status: 'success',
            duration: 5000,
            isClosable: true,
          });
          return history.replace('/login');
        }

        toast({
          title: 'Could not verify email.',
          description: 'The verification link must have expired.',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });

        return history.replace('/login');
      })
      .catch(err => {
        return toast({
          title: 'Woops, something went wrong :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      });
  }, [verificationId]);

  return <Loading />;
};

export default ForgotPassword;
