import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import {
  Box,
  Heading,
  Text,
  Stack,
  Flex,
  VStack,
  Container,
  SimpleGrid,
  Icon,
  Image,
  // Avatar,
  Link as ChakraLink,
  useToast,
  Tooltip,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import { HiLocationMarker } from 'react-icons/hi';
import { RiPhoneFill } from 'react-icons/ri';
import { IoLogoWhatsapp, IoIosMail } from 'react-icons/io';
import { Helmet } from 'react-helmet';

import ContactForm from '../../components/Landing/ContactForm';

// const Testimonial = ({ children }) => {
//   return <Box>{children}</Box>;
// };

// const TestimonialContent = ({ children }) => {
//   return (
//     <Stack
//       bg="white"
//       boxShadow="lg"
//       p={8}
//       rounded="xl"
//       align="center"
//       pos="relative"
//       _after={{
//         content: '""',
//         w: 0,
//         h: 0,
//         borderLeft: 'solid transparent',
//         borderLeftWidth: 16,
//         borderRight: 'solid transparent',
//         borderRightWidth: 16,
//         borderTop: 'solid',
//         borderTopWidth: 16,
//         borderTopColor: 'white',
//         pos: 'absolute',
//         bottom: '-16px',
//         left: '50%',
//         transform: 'translateX(-50%)',
//       }}
//     >
//       {children}
//     </Stack>
//   );
// };

// const TestimonialText = ({ children }) => {
//   return (
//     <Text textAlign="center" fontSize="sm">
//       {children}
//     </Text>
//   );
// };

// const TestimonialAvatar = ({ src, name, title }) => {
//   return (
//     <Flex align="center" mt={8} direction="column">
//       <Avatar src={src} alt={name} mb={2} />
//       <Stack spacing={-1} align="center">
//         <Text fontWeight={600}>{name}</Text>
//         <Text fontSize="sm" color="gray.500">
//           {title}
//         </Text>
//       </Stack>
//     </Flex>
//   );
// };

const About = () => {
  const [submitting, setSubmitting] = useState(false);

  const queryClient = useQueryClient();
  const toast = useToast();

  const mutation = useMutation(
    data => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users/contact_request`,
        {
          method: 'POST',
          headers,
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async res => {
        setSubmitting(false);

        const data = await res.json();

        return toast({
          title:
            res.status === 200 ? 'Message Received' : 'Message failed to send',
          description: data.message,
          status: res.status === 200 ? 'success' : 'error',
          duration: 5000,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const authStatus = queryClient.getQueryData('authStatus');

  if (authStatus?.isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  const onSubmit = values => {
    setSubmitting(true);
    return mutation.mutate(values);
  };

  return (
    <>
      <Helmet>
        <title>About - Agency of Curious Tasks</title>
        <meta property="og:title" content="About - Agency of Curious Tasks" />
        <meta
          property="og:description"
          content="It all started with a little vision to change education as we know it. Generally, our children are used to learning new things in the same old ways. The rote-learning system of memorising and regurgitating facts does not help a child to comprehend lessons."
        />
        <meta
          property="og:image"
          content="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/about1.jpg"
        />
      </Helmet>
      <Box pt="82px">
        <Flex
          w="full"
          minH="30vh"
          backgroundImage="url(https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/about_hero.jpg)"
          backgroundSize="cover"
          backgroundPosition="center center"
          py={16}
          boxShadow="inset 0 0 0 1000px rgba(0, 0, 0, 0.5)"
        >
          <VStack w="full" justify="center" px={{ base: 4, md: 8 }}>
            <Stack maxW="2xl" align="center" spacing={6} textAlign="center">
              <Heading
                color="white"
                fontWeight={700}
                lineHeight={1.2}
                fontSize={{ base: '3xl', md: '4xl' }}
              >
                Our Story
              </Heading>
            </Stack>
          </VStack>
        </Flex>
        <Container maxW="5xl" py={12}>
          <Heading textAlign="center">
            A Group of Educators with Big Goals
          </Heading>
          <SimpleGrid
            columns={{ base: 1, md: 2 }}
            spacing={{ base: 4, md: 10 }}
            mt={10}
          >
            <Stack spacing={4}>
              <Text textAlign="justify">
                It all started with a little vision to change education as we
                know it. Generally, our children are used to learning new things
                in the same old ways. The rote-learning system of memorising and
                regurgitating facts does not help a child to comprehend lessons.
              </Text>
              <Text textAlign="justify">
                We further wanted to solve the challenges faced by many parents,
                specifically relating to time. Most parents have to shuttle
                their child(ren) between tuition, music classes and other
                enrichment programs. Mostly packing as much as possible in
                weekends. We felt strongly that this became a hurdle and
                separated a child’s learning time from quality family time.
              </Text>
              <Text textAlign="justify">
                Thus, began the journey of FlipEd to create an educational
                platform that sparks children’s curiosity in how things work and
                ignite their passion for learning and exploring technology.
              </Text>
            </Stack>
            <Stack spacing={4}>
              <Text textAlign="justify">
                We spent all of 2020 developing a truly immersive and
                interactive experience that was not only fun but educational
                too, for both the parents and child. We hope that parents enjoy
                this new experience with their children, and together, find a
                way to instil a passion for discovery and learning in their
                children’s lives.
              </Text>
              <Box>
                <Image
                  alt="feature image"
                  src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/images/about1.jpg"
                />
              </Box>
            </Stack>
          </SimpleGrid>
        </Container>
        {/* <Box bg="azalea.500">
        <Container maxW="7xl" py={16} as={Stack} spacing={12}>
          <Stack spacing={0} align="center" textAlign="center">
            <Heading>Don{"'"}t Take Our Word For It</Heading>
            <Text>Happy parents and educators is what we strive for</Text>
          </Stack>
          <Stack
            direction={{ base: 'column', md: 'row' }}
            spacing={{ base: 10, md: 4, lg: 10 }}
          >
            <Testimonial>
              <TestimonialContent>
                <TestimonialText>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Auctor neque sed imperdiet nibh lectus feugiat nunc sem.
                </TestimonialText>
              </TestimonialContent>
              <TestimonialAvatar
                src={
                  'https://images.unsplash.com/photo-1586297135537-94bc9ba060aa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=100&q=80'
                }
                name="Jane Cooper"
                title="Parent of two smart children"
              />
            </Testimonial>
            <Testimonial>
              <TestimonialContent>
                <TestimonialText>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Auctor neque sed imperdiet nibh lectus feugiat nunc sem.
                </TestimonialText>
              </TestimonialContent>
              <TestimonialAvatar
                src={
                  'https://images.unsplash.com/photo-1586297135537-94bc9ba060aa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=100&q=80'
                }
                name="Jane Cooper"
                title="Principal of School"
              />
            </Testimonial>
            <Testimonial>
              <TestimonialContent>
                <TestimonialText>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Auctor neque sed imperdiet nibh lectus feugiat nunc sem.
                </TestimonialText>
              </TestimonialContent>
              <TestimonialAvatar
                src={
                  'https://images.unsplash.com/photo-1586297135537-94bc9ba060aa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=100&q=80'
                }
                name="Jane Cooper"
                title="Parent of two smart children"
              />
            </Testimonial>
          </Stack>
        </Container>
      </Box>
      <Flex minH="40vh" alignItems="center" backgroundColor="linen.500" mb={10}>
        <Container maxW="7xl" py={16} as={Stack} spacing={12}>
          <Stack spacing={0} align="center" textAlign="center">
            <Heading>What We Have Accomplished So Far</Heading>
            <Text>And we are never going to stop</Text>
          </Stack>
          <Stack
            direction={{ base: 'column', md: 'row' }}
            spacing={{ base: 10, md: 4, lg: 10 }}
            justifyContent="space-evenly"
          >
            <Box textAlign="center">
              <Heading size="3xl">500</Heading>
              <Text color="froly.500">Happy Customers</Text>
            </Box>
            <Box textAlign="center">
              <Heading size="3xl">25</Heading>
              <Text color="froly.500">Excellence Awards</Text>
            </Box>
            <Box textAlign="center">
              <Heading size="3xl">0</Heading>
              <Text color="froly.500">Projects we didn{"'"}t believe in</Text>
            </Box>
          </Stack>
        </Container>
      </Flex> */}
        <Box backgroundColor="goldenrod.500" py={10}>
          <Container maxW="5xl">
            <SimpleGrid
              templateColumns={{ base: '1fr', sm: '1fr 1fr' }}
              gap={10}
            >
              <Box rounded="lg" bg="cerulean.200" boxShadow="lg" p={8}>
                <Heading mb={4}>Get in Touch</Heading>
                <Stack spacing={4}>
                  <Box>
                    <ChakraLink href="tel:+6010-245-2458">
                      <Icon as={RiPhoneFill} mr={1} />
                      +6010-245-2458
                    </ChakraLink>
                    <br />
                    <ChakraLink
                      href="https://api.whatsapp.com/send?phone=60102452458"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <Icon as={IoLogoWhatsapp} mr={1} />
                      +6010-245-2458
                    </ChakraLink>
                  </Box>
                  <Box>
                    <Tooltip
                      label="enquiries@agencyofcurioustasks.com"
                      placement="top"
                    >
                      <ChakraLink href="mailto:enquiries@agencyofcurioustasks.com">
                        <Icon as={IoIosMail} mr={1} />
                        Email Us
                      </ChakraLink>
                    </Tooltip>
                  </Box>
                  <Box mt={4}>
                    <Text>
                      <Icon as={HiLocationMarker} mr={1} />
                      Visit Us
                    </Text>
                    <Text>
                      15-2 Jalan Sri Hartamas 7
                      <br />
                      Taman Sri Hartamas
                      <br />
                      50480 Kuala Lumpur
                      <br />
                      Malaysia
                    </Text>
                  </Box>
                </Stack>
              </Box>
              <Box id="contact">
                <ContactForm onSubmit={onSubmit} submitting={submitting} />
              </Box>
            </SimpleGrid>
          </Container>
        </Box>
      </Box>
    </>
  );
};

export default About;
