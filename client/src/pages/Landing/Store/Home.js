import React from 'react';
import { useQuery } from 'react-query';
import {
  Container,
  Box,
  Flex,
  SimpleGrid,
  Heading,
  Text,
  Stack,
  VStack,
} from '@chakra-ui/react';
import { Helmet } from 'react-helmet';

import SeasonCard from '../../../components/Store/SeasonCard';
import Loading from '../../../components/Loading';

import { fetchSeasons } from '../../../queries/Seasons';

const StoreHome = () => {
  const { data: seasons, status: seasonsQueryStatus } = useQuery(
    'seasons',
    fetchSeasons,
  );

  if (seasonsQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>Store - Agency of Curious Tasks</title>
        <meta property="og:title" content="Store - Agency of Curious Tasks" />
        <meta
          property="og:description"
          content="You can choose between a Single Mission or a Season. A Season is
            where a mission unfolds over a series of episodes. Each Season comes
            with a variety of unique episodes full of rich content for your
            child to become engaged in."
        />
        <meta
          property="og:image"
          content="https://i.pinimg.com/originals/85/70/e8/8570e8a8f3beb2485e73e5861b6cd3bd.jpg"
        />
      </Helmet>
      <Box pt="82px" pb={['20px', '60px']}>
        <Flex
          w="full"
          minH="30vh"
          backgroundImage={
            'url(https://i.pinimg.com/originals/85/70/e8/8570e8a8f3beb2485e73e5861b6cd3bd.jpg)'
          }
          backgroundSize="cover"
          backgroundPosition="center center"
          py={16}
          mb={10}
          boxShadow="inset 0 0 0 1000px rgba(0, 0, 0, 0.5)"
        >
          <VStack w="full" justify="center" px={{ base: 4, md: 8 }}>
            <Stack maxW="2xl" align="center" spacing={6} textAlign="center">
              <Heading
                color="white"
                fontWeight={700}
                lineHeight={1.2}
                fontSize={{ base: '3xl', md: '4xl' }}
              >
                Choose Your Mission
              </Heading>
            </Stack>
          </VStack>
        </Flex>
        <Container centerContent maxW="6xl">
          <Text>
            You can choose between a Single Mission or a Season. A Season is
            where a mission unfolds over a series of episodes. Each Season comes
            with a variety of unique episodes full of rich content for your
            child to become engaged in.
            <br />
            <br />
            We have one Single Mission available currently and will be launching
            Season 1 soon. We have many exciting stories in the works and plan
            to publish new Single Missions and Seasons in the near future.
          </Text>
          <SimpleGrid gap={6} my={6} w="full">
            {seasons
              .sort((a, b) => (a.seasonNumber > b.seasonNumber ? 1 : -1))
              .map(
                season =>
                  season.active && <SeasonCard key={season.id} {...season} />,
              )}
          </SimpleGrid>
        </Container>
      </Box>
    </>
  );
};

export default StoreHome;
