import React, { Fragment } from 'react';
import { useQueryClient, useQuery } from 'react-query';
import { useHistory, useLocation } from 'react-router-dom';
import {
  Container,
  Box,
  Grid,
  Heading,
  Text,
  Stack,
  VStack,
} from '@chakra-ui/react';
import { Helmet } from 'react-helmet';

import EpisodeCard from '../../../components/Store/EpisodeCard';

import { fetchUsersChildren } from '../../../queries/Families';
import { fetchPricesByProductId } from '../../../queries/Stripe';
import { fetchSeasonByNumber } from '../../../queries/Seasons';
import { fetchEpisodesBySeasonId } from '../../../queries/Episodes';

import Loading from '../../../components/Loading';

const SeasonOverview = () => {
  const queryClient = useQueryClient();
  const location = useLocation();
  const history = useHistory();

  const authStatus = queryClient.getQueryData('authStatus');

  const { data: usersChildren, status: usersChildrenQueryStatus } = useQuery(
    'usersChildren',
    fetchUsersChildren,
    { enabled: authStatus?.isAuthenticated },
  );

  const seasonNumber = location.pathname.substring(
    location.pathname.lastIndexOf('/') + 1,
  );

  const { data: season, status: seasonQueryStatus } = useQuery(
    ['season', seasonNumber],
    () => fetchSeasonByNumber(seasonNumber),
  );

  const { data: prices, status: pricesQueryStatus } = useQuery(
    ['prices', season?.stripeProductId],
    () => fetchPricesByProductId(season?.stripeProductId),
    { enabled: !!season && season.stripeProductId !== null },
  );

  const seasonId = season?.id;

  const { data: episodesInSeason, status: episodesInSeasonQueryStatus } =
    useQuery(
      ['episodesInSeason', seasonId],
      () => fetchEpisodesBySeasonId(seasonId),
      { enabled: !!seasonId },
    );

  if (
    usersChildrenQueryStatus === 'loading' ||
    seasonQueryStatus === 'loading' ||
    episodesInSeasonQueryStatus === 'loading' ||
    pricesQueryStatus === 'loading'
  ) {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>{season.title} - Agency of Curious Tasks</title>
        <meta name="description" content={season.description} />
        <meta
          property="og:title"
          content={`${season.title} - Agency of Curious Tasks`}
        />
        <meta property="og:description" content={season.description} />
        <meta property="og:image" content={season.coverImageUrl} />
      </Helmet>
      <Box pt="82px" pb={['20px', '60px']}>
        <Box
          w="full"
          minH="30vh"
          backgroundImage={`url(${season.coverImageUrl})`}
          backgroundSize="cover"
          backgroundPosition="center center"
          py={16}
          mb={10}
          boxShadow="inset 0 0 0 1000px rgba(0, 0, 0, 0.5)"
        >
          <Container maxW="4xl">
            <VStack w="full" justify="center" px={{ base: 4, md: 8 }}>
              <Stack maxW="2xl" align="center" spacing={6} textAlign="center">
                <Heading
                  color="white"
                  fontWeight={700}
                  lineHeight={1.2}
                  fontSize={{ base: '3xl', md: '4xl' }}
                >
                  {season.title}
                </Heading>
                <Text color="white" whiteSpace="pre-wrap" fontSize="14px">
                  {season.description}
                </Text>
                {episodesInSeason.length > 0 &&
                parseInt(seasonNumber, 10) !== 0 ? (
                  <Box bg="white" rounded="md" p={4}>
                    <Heading fontSize="xl" fontWeight="bold">
                      Price:
                    </Heading>
                    {prices.map((price, i) => (
                      <Text
                        fontSize="lg"
                        key={price.id}
                        mb={i === prices.length - 1 ? 0 : 2}
                        style={{ marginTop: 0 }}
                      >
                        {price.currency.toUpperCase()} {price.unitAmount / 100}{' '}
                        every
                        {price.intervalCount !== 1
                          ? ` ${price.intervalCount} `
                          : ' '}{' '}
                        {`${price.interval}${
                          price.intervalCount !== 1 ? 's' : ''
                        }`}
                      </Text>
                    ))}
                  </Box>
                ) : null}
              </Stack>
            </VStack>
          </Container>
        </Box>
        <Box px={6} w="full">
          {episodesInSeason.length > 0 ? (
            <Grid
              templateColumns={{ base: '1fr', sm: 'repeat(3, 1fr)' }}
              gap={6}
              my={6}
            >
              {episodesInSeason
                .sort((a, b) => (a.episodeNumber > b.episodeNumber ? 1 : -1))
                .map(episode => {
                  if (episode.active) {
                    return (
                      <EpisodeCard
                        key={episode.id}
                        episodeData={episode}
                        canSubscribe={
                          !authStatus?.isAuthenticated ||
                          usersChildren?.members.length !== 0
                        }
                        onSubscribeBtnClick={price => {
                          if (authStatus?.isAuthenticated) {
                            if (usersChildren.members.length !== 0) {
                              if (parseInt(seasonNumber, 10) !== 0) {
                                return history.push(
                                  `/store/checkout/subscription?season=${season.seasonNumber}`,
                                );
                              }
                              return history.push(
                                `/store/checkout/single?episode=${episode.episodeNumber}`,
                                {
                                  usersChildren,
                                  episode,
                                  pricing: price,
                                },
                              );
                            }
                            return history.push('/children');
                          }
                          return history.push('/signup');
                        }}
                      />
                    );
                  }
                })}
            </Grid>
          ) : (
            <Container centerContent maxW="2xl" textAlign="center">
              <Text>No Episodes currently in this season</Text>
            </Container>
          )}
        </Box>
      </Box>
    </>
  );
};

export default SeasonOverview;
