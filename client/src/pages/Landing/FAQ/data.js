import React, { Fragment } from 'react';
import { Link, Text, OrderedList, ListItem } from '@chakra-ui/react';

export const productQuestions = [
  {
    title: 'What is the Agency of Curious Tasks?',
    description:
      'The Agency of Curious Tasks is an immersive story-based series of STEM kits that takes the shape of secret missions. The activities involve the combined use of both the physical kit and the ACT website.',
  },
  {
    title: 'How much time do I need to allocate for each episode?',
    description:
      "Each episode consists of 1 STEM project, 1 mini game and some various activities to challenge the child. The time needed to complete each activity will differ as we try to make these activities not only educational but also interactive as well, to challenge your child's cognitive thinking.\n\nYour child should be able to complete it in a single sitting of an hour. We have also curated relevant online resources to help your child's learning progression for each STEM project and this will always be accessible through the Knowledge Centre button. The mini game will also be freely accessible and can create an interest to return to the website and attempt to better their scores, while waiting for the next episode to be shipped.",
  },
  {
    title: 'What is contained in each episode?',
    description: (
      <>
        <Text>Each episode will include the following components:</Text>
        <br />
        <OrderedList listStyleType="upper-alpha">
          <ListItem>
            1x STEM project included in the kit, that is related to the
            storyline
          </ListItem>
          <ListItem>
            At least 2x activities (puzzle, riddle, secret messages, math games,
            etc). You will use these and other clues provided to solve the
            mystery of each mission.
          </ListItem>
          <ListItem>
            1x mini game on the gamesite, unique to each episode
          </ListItem>
          <ListItem>
            The Knowledge Centre on the website with curated video content for a
            better learning experience.
          </ListItem>
        </OrderedList>
      </>
    ),
  },
  {
    title: 'Are all required materials provided in the box?',
    description:
      'For the main project, we provide all the required materials. However, there will be certain instances where we may need your child to utilize items that can be easily found at home. For example, stationary (scissors, pencil, rulers, markers etc.), measuring cups/spoons, bowls or plates.',
  },
  {
    title: 'What age range is ACT suitable for?',
    description:
      'The Agency of Curious Tasks is designed for children between the ages of 7 - 9 years old. Children out of this age range are welcome to try their hand as well. Younger children may require adult supervision, whereas older children would be able to complete the activities on their own.\n\nWe have plans to expand our offering for the entire family to enjoy together with more challenging projects and activities. Stay tuned!',
  },
  {
    title: 'Is adult supervision required for each activity?',
    description:
      'While adult supervision is not necessary, we highly encourage parents to be involved in the process! It is a great way to bond with your children through learning.',
  },
  {
    title: 'What device is required for the online portion of the game?',
    description:
      'We have designed the entire experience to be mobile responsive, so the gamesite can be accessed with any of the commonly offered tablets and smartphones. However, it would be better enjoyed through a laptop, given the screen size.',
  },
  {
    title: 'How does one order an episode?',
    description: (
      <OrderedList listStyleType="upper-alpha">
        <ListItem>
          You will need to register an account and sign your up child for the
          chosen mission.
        </ListItem>
        <ListItem>
          Provide your payment and shipping details and you will be charged
          based on the chosen mission. A single charge for any of the Single
          Missions or monthly for a defined period if purchasing a Season.
        </ListItem>
        <ListItem>You can opt-out of your monthly orders anytime.</ListItem>
      </OrderedList>
    ),
  },
  {
    title: "What if my child can't access the Gamesite?",
    description: (
      <Text>
        In the unlikely instance that your child{"'"}s login does not initiate
        the Gamesite, please contact us at{' '}
        <Link
          href="mailto:service@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          service@agencyofcurioustasks.com
        </Link>
        .
      </Text>
    ),
  },
  {
    title: 'What if my child forgets their password?',
    description:
      "As the Parent, you have the ability to fix that. Just login using your email and password then navigate to the child account and you can reset the password through the edit function. It's as easy as that.",
  },
  {
    title: 'How creative can we be with the child Username?',
    description:
      "As long as the child's username is a unique single word, you can be as creative as your imagination lets you. Once chosen however, you cannot change it. You will always be able to see the child username when logged in as the Parent. Navigate to the child account and the username will be in brackets next to your child's name.",
  },
];

export const paymentQuestions = [
  {
    title: 'How do I purchase an episode?',
    description: (
      <Text>
        We offer two ways to experience the Agency of Curious Tasks. The Single
        Missions are one-off purchases and Seasons is a monthly payment plan
        option, wherein you order the entire Season but will be charged monthly.
        You can opt-out freely anytime and will only be charged for episodes
        already delivered.
        <br />
        <br />
        Thank you for your interest. We{"'"}ve tried to make it easier for you
        to sign up with multiple options.
      </Text>
    ),
  },
  {
    title: 'Can I have a trial of the product?',
    description:
      'Yes you may. You can try any of the Single Missions but we would recommend that you start with the Sneak Peek.',
  },
  {
    title: 'How much does each episode cost?',
    description:
      'The Sneak Peek retails at RM68 and Season 1 retails at RM95/per episode. We carry out various promotions and offers, so make sure you SIGN UP TODAY to enjoy the best benefits.',
  },
  {
    title:
      "What's the benefits of 'subscribing'? Can I opt not to 'subscribe'?",
    description:
      "The payment model is NOT a subscription scheme. You can purchase Single Missions for a one-off payment without any commitments. Should you choose the Seasons, we provide you with a monthly installment plan for each episode of a Season. By signing up for this plan, you and your child get to enjoy each episode consistently without any delays. (Don't worry, there is no interest charge!)",
  },
  {
    title: 'Can I pick which box I get for each month?',
    description:
      "The Agency of Curious Tasks is story-based and as such each box is an interconnected series of episodes. For the best experience we don't allow selecting your choice of episodes.",
  },
  {
    title: 'How do I pay for each episode?',
    description:
      "Once you have registered an account and signed your child up for a mission, you will be asked for your credit/debit card details during the checkout process. If you've chosen a Single Mission, you will be charged that one time. If you've chosen a Season, after the first initial charge, you will then be charged every month on the same date as your original sign up date for the defined number of episodes in the chosen Season.",
  },
  {
    title: 'Can I change my credit card details?',
    description:
      "There are various reasons why credit card details need to be changed and you can do so through the 'Parent Dashboard' > 'Billing' section. Do get in touch with us if you have any issues with this process.",
  },
];

export const shippingQuestions = [
  {
    title: 'What is the delivery time?',
    description:
      'The Agency of Curious Tasks team aims to deliver each order between 5-7 working days of each payment cycle. Deliveries usually arrive during business days and hours. We are however not in control of any external elements like any extreme weather conditions. In addition, if there are any restrictions imposed under MCO or otherwise, regular delivery times may vary slightly.',
  },
  {
    title:
      'Where does Agency of Curious Tasks ship to? What are the shipping fees like?',
    description: (
      <Text>
        Currently, we are only shipping within Malaysia. Delivery charges are
        included in the retail price. However, after the initial promotional
        period expected to end by 31st December 2021, delivery to East Malaysia
        will incur an additional charge of RM5 per box.
        <br />
        <br />
        We are working towards future International delivery. If you{"'"}re
        living outside of Malaysia and would like to try our boxes, do drop us
        an email at{' '}
        <Link
          href="mailto:enquiries@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          enquiries@agencyofcurioustasks.com
        </Link>
        .
      </Text>
    ),
  },
  {
    title: 'When will I receive the subsequent boxes?',
    description:
      'You can expect to receive each subsequent box within 5-7 working days of each payment cycle which happens on the same date as your original sign up date.',
  },
  {
    title: 'Does the Agency of Curious Tasks ship internationally?',
    description:
      'We are currently working on this and will be updating all our platforms once the service is available.',
  },
  {
    title: 'Can I track my box?',
    description: (
      <Text>
        Until we formalise arrangements and agreements with a suitably
        proficient logistics partner, you may check on your order through
        <Link
          href="mailto:service@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          service@agencyofcurioustasks.com
        </Link>
        .
        <br />
        <br />
        Once we have secured a logistics partner, you will be able to check on
        your order status in the Parent Dashboard on the website.
        Simultaneously, we will send you an email with shipping details and a
        tracking link for you.
      </Text>
    ),
  },
  {
    title: 'How do I change my address?',
    description:
      "You can change your address and other details, via the 'Parent Dashboard' section on our website.",
  },
];

export const returnQuestions = [
  {
    title: 'What are your returns/refund policy?',
    description: (
      <Text>
        We do not accept returns or exchange for any products sold. We don{"'"}t
        provide refunds unless it is a payment related issue, and if so please
        write to us at{' '}
        <Link
          href="mailto:service@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          service@agencyofcurioustasks.com
        </Link>
        .
        <br />
        <br />
        If any part is damaged, please email us at{' '}
        <Link
          href="mailto:service@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          service@agencyofcurioustasks.com
        </Link>{' '}
        and send us a picture of the part in question and upon validation we
        will proceed to send a replacement part at no extra charge.
      </Text>
    ),
  },
  {
    title: 'If items received are incomplete. What should I do?',
    description: (
      <Text>
        We are truly sorry that you had to experience this inconvenience! Please
        drop us an email at{' '}
        <Link
          href="mailto:service@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          service@agencyofcurioustasks.com
        </Link>{' '}
        with your full name, order ID, list of missing item(s) and a picture of
        the box contents no later than 7 days after receiving the box. We will
        send you either the missing items or a new box at no extra cost.
      </Text>
    ),
  },
  {
    title: 'Some of the items received are damaged, what should I do?',
    description: (
      <Text>
        We are truly sorry about this. Do drop us an email at{' '}
        <Link
          href="mailto:service@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          service@agencyofcurioustasks.com
        </Link>{' '}
        with your full name, order ID and a picture of the damaged items no
        later than 7 days after receiving the box. We will send you either the
        replacement for the damaged items or a new box at no extra cost.
      </Text>
    ),
  },
];

export const affiliateQuestions = [
  {
    title: 'Do you collaborate with schools?',
    description: (
      <Text>
        Yes we do. In fact, if you are a representative from a school, do get in
        touch with us at{' '}
        <Link
          href="mailto:enquiries@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          enquiries@agencyofcurioustasks.com
        </Link>{' '}
        as we would love to partner with you and your establishment.
        <br />
        <br />
        Alternatively, if you would like to recommend us approaching any
        particular schools, do email us at{' '}
        <Link
          href="mailto:enquiries@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          enquiries@agencyofcurioustasks.com
        </Link>
        . Thank you.
      </Text>
    ),
  },
  {
    title: 'Do you have a referral program?',
    description:
      'Yes, we do. It is straightforward and easy to earn redemption value. Invite your friends and family, and they will receive RM10 off their first purchase and you will also receive a redeemable value of RM10 credited into your account for every successful sign up. This redeemable cash value can be accumulated and used to pay for a Single Mission.',
  },
  {
    title: "How do I 'Gift' the Agency of Curious Tasks to friends or family?",
    description: (
      <Text>
        How nice of you :-)
        <br />
        <br />
        Our gifting procedure is via a Gift Card with the credited gift value.
        At present, we manage {"'"}gifting{"'"} manually wherein you will
        transfer the intended gift value to our bank account and we will issue a
        Gift Card of that value which you will be able to share with your friend
        or family. The recipient will then need to register an account and
        sign-up their child for a mission and utilise the Gift Card upon check
        out to enjoy the benefits. Any unutilised amount will be retained in the
        recipient{"'"}s stored value.
        <br />
        <br />
        Please contact us at{' '}
        <Link
          href="mailto:enquiries@agencyofcurioustasks.com"
          color="cerulean.500"
        >
          enquiries@agencyofcurioustasks.com
        </Link>{' '}
        to purchase a Gift Card.
      </Text>
    ),
  },
  {
    title: 'How can I give feedback on the product and experience?',
    description: (
      <>
        <Text>
          We love improving and welcome your feedback. Do email us your feedback
          at{' '}
          <Link
            href="mailto:service@agencyofcurioustasks.com"
            color="cerulean.500"
          >
            service@agencyofcurioustasks.com
          </Link>
          .
          <br />
          <br />
          Alternatively, do feel free to drop us a recommendation on our social
          media platforms:
        </Text>
        <OrderedList>
          <ListItem>
            <Link
              href="https://www.facebook.com/agencyofcurioustasks"
              color="cerulean.500"
            >
              Facebook Page
            </Link>
          </ListItem>
          <ListItem>
            <Link
              href="https://www.instagram.com/agencyofcurioustasks"
              color="cerulean.500"
            >
              Instagram Page
            </Link>
          </ListItem>
        </OrderedList>
      </>
    ),
  },
];
