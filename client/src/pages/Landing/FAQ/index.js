import React from 'react';
import { Redirect } from 'react-router-dom';
import { Box, Heading, Stack, Flex, VStack, Container } from '@chakra-ui/react';
import { useQueryClient } from 'react-query';
import { Helmet } from 'react-helmet';

import FAQSection from '../../../components/Landing/FAQSection';

import {
  productQuestions,
  paymentQuestions,
  shippingQuestions,
  returnQuestions,
  affiliateQuestions,
} from './data';

const FAQ = () => {
  const queryClient = useQueryClient();
  const authStatus = queryClient.getQueryData('authStatus');

  if (authStatus?.isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <>
      <Helmet>
        <title>FAQ - Agency of Curious Tasks</title>
        <meta property="og:title" content="FAQ - Agency of Curious Tasks" />
        <meta
          property="og:description"
          content="Have Questions? We Have Answers!"
        />
        <meta
          property="og:image"
          content="https://schools.firstnews.co.uk/wp-content/uploads/sites/3/2020/02/child-with-question-mark.jpg"
        />
      </Helmet>
      <Box pt="82px" pb={['20px', '60px']}>
        <Flex
          w="full"
          minH="30vh"
          backgroundImage={
            'url(https://schools.firstnews.co.uk/wp-content/uploads/sites/3/2020/02/child-with-question-mark.jpg)'
          }
          backgroundSize="cover"
          backgroundPosition="center center"
          py={16}
          mb={10}
          boxShadow="inset 0 0 0 1000px rgba(0, 0, 0, 0.5)"
        >
          <VStack w="full" justify="center" px={{ base: 4, md: 8 }}>
            <Stack maxW="2xl" align="center" spacing={6} textAlign="center">
              <Heading
                color="white"
                fontWeight={700}
                lineHeight={1.2}
                fontSize={{ base: '3xl', md: '4xl' }}
              >
                Have Questions? We Have Answers!
              </Heading>
            </Stack>
          </VStack>
        </Flex>
        <Container maxW="6xl">
          <FAQSection
            id="product"
            title="About the Product"
            colorScheme="wattle"
            hoverBgColor="cerulean"
            data={productQuestions}
            containerProps={{ mb: 10 }}
          />
          <FAQSection
            id="payment"
            title="About Payment"
            colorScheme="cerulean"
            hoverBgColor="wattle"
            data={paymentQuestions}
            containerProps={{ mb: 10 }}
          />
          <FAQSection
            id="shipping"
            title="About Shipping & Delivery"
            colorScheme="azalea"
            hoverBgColor="froly"
            data={shippingQuestions}
            containerProps={{ mb: 10 }}
          />
          <FAQSection
            id="returns"
            title="About Returns, Refunds, & Exchanges"
            colorScheme="goldenrod"
            hoverBgColor="niagara"
            data={returnQuestions}
            containerProps={{ mb: 10 }}
          />
          <FAQSection
            id="affiliates"
            title="About Affiliates, Referrals, Gifting and Other Matters"
            colorScheme="niagara"
            hoverBgColor="goldenrod"
            data={affiliateQuestions}
          />
        </Container>
      </Box>
    </>
  );
};

export default FAQ;
