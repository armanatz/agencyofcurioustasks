import React from 'react';
import { Link } from 'react-router-dom';
import {
  Box,
  Flex,
  VStack,
  Stack,
  Heading,
  Text,
  Container,
  Link as ChakraLink,
} from '@chakra-ui/react';
import { Helmet } from 'react-helmet';

const Terms = () => {
  return (
    <>
      <Helmet>
        <title>Terms {'&'} Conditions - Agency of Curious Tasks</title>
      </Helmet>
      <Box pt="82px" pb={['20px', '60px']}>
        <Flex w="full" minH="30vh" bgColor="niagara.500" py={16} mb={10}>
          <VStack w="full" justify="center" px={{ base: 4, md: 8 }}>
            <Stack maxW="2xl" align="center" spacing={6} textAlign="center">
              <Heading
                color="white"
                fontWeight={700}
                lineHeight={1.2}
                fontSize={{ base: '3xl', md: '4xl' }}
              >
                Terms {'&'} Conditions
              </Heading>
            </Stack>
          </VStack>
        </Flex>
        <Container maxW="6xl">
          <VStack spacing={6} align="flex-start">
            <Text>Last Updated on 21 June 2021</Text>
            <Text>
              <strong>
                FlipEd Sdn Bhd (1153403-H) ({'"'}Company{'"'}, {'"'}We{'"'},{' '}
                {'"'}Our{'"'} or {'"'}Us{'"'})
              </strong>
              provides access to information through our website accessible at
              the URL{' '}
              <ChakraLink
                as={Link}
                to="/"
                target="_blank"
                rel="noopener noreferrer"
                color="cerulean.500"
              >
                https://www.agencyofcurioustasks.com/
              </ChakraLink>{' '}
              (the {'"'}
              <strong>Website</strong>
              {'"'}) and we will provide the ability to use certain
              functionalities of the Website. Use of the Website, information,
              services, and content distributed through or in conjunction with
              or made available on the Website or through any mobile
              applications, plug-ins, or other means (collectively, the {'"'}
              <strong>Services</strong>
              {'"'}). The Website and Services are made available to you only
              under the following terms and conditions, and any other notices or
              amendments posted through the Services (the {'"'}Terms {'&'}
              Conditions{'"'}).
            </Text>
            <Text>
              By accessing and using the Services, or any of the information
              and/or content presented in any areas of the Services, you are
              acknowledging that you have read and agree to be bound by these
              Terms {'&'} Conditions, Our Privacy Policy, and other notices
              posted through the Services. You further warrant and covenant that
              you have the power and authority to enter into this agreement. If
              you do not acknowledge and agree with the above, you may not
              access or use Our content or Services.
            </Text>
            <Heading fontSize="24px">
              THESE TERMS AND CONDITIONS IMPOSE LEGAL OBLIGATIONS UPON YOU, IT
              IS IMPORTANT THAT YOU READ THEM CAREFULLY.
            </Heading>
            <Text>
              We shall have the right, at Our sole discretion, to modify, add,
              or remove any terms or conditions of these Terms {'&'} Conditions
              at any time ({'"'}
              <strong>Changes</strong>
              {'"'}). You will be notified of any Changes through a posting on
              the Website. The use by you of the Services shall constitute your
              acceptance of the Terms {'&'} Conditions. If you do not agree to
              any or all of the Terms {'&'} Conditions, you must stop using the
              Services immediately.
            </Text>
            <VStack spacing={10} align="flex-start">
              <Heading fontSize="24px">
                I. ACCESS TO OUR ONLINE SERVICES AND TOOLS
              </Heading>
              <Text>
                <strong>1. Accounts</strong>
                <br />
                <br />
                In order to use certain features of the Site or Services, you
                must register for an account with Company ({'"'}Account{'"'})
                and provide certain information about yourself as prompted by
                the Site registration form. You represent and warrant that: (a)
                all required registration information you submit is truthful and
                accurate; and (b) you will maintain the accuracy of such
                information. You are responsible for maintaining the
                confidentiality of your Account login information and are fully
                responsible for all activities that occur under your Account. 
                You agree to immediately notify Company of any unauthorized use,
                or suspected unauthorized use of your Account or any other
                breach of security. Company cannot and will not be liable for
                any loss or damage arising from your failure to comply with the
                above requirements.
              </Text>
              <Text>
                <strong>2. Access to Features of Online Services</strong>
                <br />
                <br />
                Subject to these Terms {'&'} Conditions, we grant you a limited,
                non-exclusive, nontransferable personal license to (a) access
                and use the Website and Services and (b) download, install and
                operate any software (in object code form only), scripts and
                other content that we may from time to time have specifically
                identified within the Website as available for download ({'"'}
                Downloadable Tools{'"'}), if any. The Downloadable Tools are
                deemed part of the {'"'}Service{'"'}. Your use of any
                Downloadable Tools may be subject to additional terms and
                conditions that accompany such Downloadable Tools.
              </Text>
              <Text>
                <strong>3. General Restrictions on Use</strong>
                <br />
                <br />
                The rights granted to you by these Terms {'&'} Conditions will
                remain in force only for so long as these Terms {'&'} Conditions
                remain in effect. You may not rent, transfer, assign,
                commercially exploit, resell or sublicense access to the Service
                to any third-party. You may use the Website and the Services
                (including the Downloadable Tools) only for your personal,
                non-commercial purposes. You further agree not to combine or
                integrate the Website and the Services (including the
                Downloadable Tools) with hardware, software or other technology
                or materials not provided by us. You may not modify or create
                any derivative product based on the Website, the Website and the
                Services (including the Downloadable Tools). You may not
                decompile, disassemble, reverse engineer or otherwise attempt to
                obtain or perceive the source code from which any component of
                the Website and the Services (including the Downloadable Tools)
                is compiled or interpreted, and nothing in these Terms {'&'}
                Conditions should be interpreted as granting you any right to
                obtain or use source code. Except as expressly stated herein, no
                part of the Website or Services (including the Downloadable
                Tools) may be copied, reproduced, distributed, republished,
                downloaded, displayed, posted or transmitted in any form or by
                any means. Any future release, update, or other addition to
                functionality of the Website or Services shall be subject to
                these Terms {'&'} Conditions. You agree not to use the Website
                and the Services (including the Downloadable Tools) to: (a)
                violate any local, state, national or international law; (b)
                stalk, harass or harm another individual; (c) collect or store
                personal data about other users; (d) impersonate any person or
                entity, or otherwise misrepresent your affiliation with a person
                or entity; or (e) interfere with or disrupt the Services or
                servers or networks connected to the Services, or disobey any
                requirements, procedures, policies or regulations of networks
                connected to the Services. Without our written consent, you may
                not (i) use any high volume, automated, or electronic means to
                access the Services (including, without limitation, robots,
                spiders or scripts); or (ii) frame the Website, place pop-up
                windows over its pages, or otherwise affect the display of its
                pages. You promise that any information about yourself that you
                voluntarily provide to us will be true, accurate, complete and
                current.
              </Text>
              <Text>
                <strong>4. Use of Third-Party Offerings</strong>
                <br />
                <br />
                You may be able to access websites, content, products or
                services provided by third-parties through links that are made
                available on the Website. We refer to all such websites,
                content, services and products as {'"'}Third-Party Offerings.
                {'"'} Any hypertext links or pointers to Third-Party Offerings
                are provided to you for convenience only and do not constitute
                an endorsement or approval by Us of: (I) the organisations that
                operate such websites; (II) the information, content, privacy
                policies or other terms of use on such websites; or (III) such
                third party products and services. For example, we may permit
                third parties to advertise their products and services on the
                Website, and those advertisements may contain links to the
                website(s) of the advertisers. If you elect to use such
                Third-Party Offerings, you understand that your use of them will
                be subject to any terms and conditions required by the
                applicable third-party provider(s). You understand that we are
                not the provider of, and are not responsible for, any such
                Third-Party Offerings and that these Terms {'&'} Conditions do
                not themselves grant you any rights to access, use or purchase
                any Third-Party Offerings.
                <br />
                <br />
                As We have no control or responsibility over, and do not
                investigate, monitor, or check for accuracy third party websites
                or information and/or content maintained by other organisations,
                or for products and services offered by third parties, and We do
                not assume any liability for your use of any of the foregoing.
                If you decide to exit the Services and access third party
                websites, you acknowledge and agree that you do so at your own
                risk and you should be aware that these Terms {'&'} Conditions
                no longer govern.
              </Text>
              <Text>
                <strong>5. Ownership</strong>
                <br />
                <br />
                As between you and us, we and/or our vendors and suppliers, as
                applicable, retain all right, title and interest in and to the
                Website and the Service (including the Downloadable Tools), and
                all related intellectual property rights, other than User
                Content. Company and its suppliers reserve all rights not
                granted in these Terms. Unless you first obtain the copyright
                owner’s prior written consent, you may not copy, distribute,
                publicly perform, publicly display, digitally perform (in the
                case of sound recordings), or create derivative works from any
                copyrighted work made available or accessible via the Site or
                the Services. If you provide Company any feedback or suggestions
                regarding the Site or Services ({'"'}Feedback{'"'}), you hereby
                assign to Company all rights in the Feedback and agree that
                Company shall have the right to use such Feedback and related
                information in any manner it deems appropriate. Company will
                treat any Feedback you provide to Company as non-confidential
                and non-proprietary. You agree that you will not submit to
                Company any information or ideas that you consider to be
                confidential or proprietary.
              </Text>
              <Text>
                <strong>6. User Consent</strong>
                <br />
                <br />
                {'"'}User Content{'"'} means any and all information and content
                that a user submits to, or uses with, the Website or Services
                (e.g., content in the user’s profile, photographs, or other
                postings). You are solely responsible for your User Content and
                assume all risks associated with use of your User Content. You
                hereby represent and warrant that your User Content does not
                violate the Acceptable Use Policy (defined below). You may not
                state or imply that your User Content is in any way provided,
                sponsored or endorsed by Company. Because you alone are
                responsible for your User Content (and not Company), you may
                expose yourself to liability if, for example, your User Content
                violates the Acceptable Use Policy. Company is not obligated to
                backup any User Content and User Content may be deleted at
                anytime. You are solely responsible for creating backup copies
                of your User Content if you desire.
              </Text>
              <Text>
                <strong>7. License of User Consent to Us</strong>
                <br />
                <br />
                You hereby grant, and you represent and warrant that you have
                the right to grant, to Company an irrevocable, nonexclusive,
                royalty-free and fully paid, worldwide license and authorization
                to reproduce, distribute, publicly display and perform, prepare
                derivative works of, incorporate into other works, and otherwise
                use your User Content (including your and/or your child’s name,
                voice, photograph, and likeness) and to grant sublicenses of the
                foregoing, for the purposes of including your User Content in
                the Website, Services, Company’s other products and services,
                and Company’s marketing materials or endorsements. You agree to
                irrevocably waive (and cause to be waived) any claims and
                assertions of moral rights or attribution with respect to your
                User Content.
              </Text>
              <Text>
                <strong>8. Acceptable Use Policy</strong>
                <br />
                <br />
                The following sets forth Company’s {'"'}Acceptable Use Policy
                {'"'}: You agree not to use the Website or Services to collect,
                upload, transmit, display, or distribute any User Content (i)
                that violates any third-party right, including any copyright,
                trademark, patent, trade secret, moral right, privacy right,
                right of publicity, or any other intellectual property or
                proprietary right; (ii) that is unlawful, harassing, abusive,
                tortious, threatening, harmful, invasive of another’s privacy,
                vulgar, defamatory, false, intentionally misleading, trade
                libelous, pornographic, obscene, patently offensive, promotes
                racism, bigotry, hatred, or physical harm of any kind against
                any group or individual or is otherwise objectionable; (iii)
                that is harmful to minors in any way; (iv) that is in violation
                of any law, regulation, or obligations or restrictions imposed
                by any third party; (v) that constitutes a computer virus, worm,
                or any software intended to damage or alter a computer system or
                data; or (vi) that constitutes unsolicited or unauthorized
                advertising, promotional materials, junk mail, spam, chain
                letters, pyramid schemes, or any other form of duplicative or
                unsolicited messages, whether commercial or otherwise.
              </Text>
              <Text>
                <strong>9. Remedies for Violations</strong>
                <br />
                <br />
                We reserve the right to seek all remedies available at law and
                in equity for violations, or for suspected violations, of these
                Terms {'&'} Conditions, including but not limited to the right
                to block access from a particular IP address to the Services and
                their features.
              </Text>
            </VStack>
            <VStack spacing={10} align="flex-start">
              <Heading fontSize="24px">
                II. TERMS AND CONDITIONS OF SALE
              </Heading>
              <Text>
                <strong>1. CERTAIN PRODUCT DISCLAIMERS</strong>
                <br />
                <br />
                WARNING: CHOKING HAZARD – SMALL PARTS. NOT FOR CHILDREN UNDER 3
                YEARS. YOU ACKNOWLEDGE THAT THE PRODUCTS ARE NOT DESIGNED,
                MANUFACTURED OR INTENDED FOR USE BY CHILDREN UNDER THE AGE OF
                THREE (3) AND MAY CONTAIN SMALL PARTS. IN ADDITION TO ALL OTHER
                LIMITATIONS AND DISCLAIMERS IN THIS AGREEMENT, COMPANY SHALL NOT
                BE LIABLE TO YOU OR ANY THIRD PARTY, IN WHOLE OR IN PART, FOR
                ANY CLAIMS, LIABILITY, DAMAGES, LOSS OR COSTS ARISING FROM SUCH
                USE.
              </Text>
              <Text>
                <strong>2. Products and Pricing</strong>
                <br />
                <br />
                All products listed on the Site ({'"'}Products{'"'}), their
                descriptions, and their prices are each subject to change. 
                Company reserves the right, at any time, to modify, suspend, or
                discontinue the sale of any Product with or without notice. You
                agree that Company will not be liable to you or to any third
                party for any modification, suspension, or discontinuance of any
                Product (except as set forth in Section IV). In the event a
                Product is listed at an incorrect price or with incorrect
                information due to typographical error or error in pricing or
                Product information received from our suppliers, we shall have
                the right, prior to the acceptance of your order (as described
                below), to decline or cancel any such orders, whether or not the
                order has been confirmed and/or your credit card charged. If
                your credit card has already been charged for the order and we
                cancel your order, we shall immediately issue a credit to your
                credit card account in the amount of the charge.
              </Text>
              <Text>
                <strong>3. Orders</strong>
                <br />
                <br />
                When you make an order, you are making an offer to purchase, and
                such offer is subject to our acceptance. Your receipt of an
                order confirmation from us does not signify our acceptance of
                your order, nor does it constitute confirmation of our offer to
                sell. We reserve the right at any time after receipt of your
                order to accept or decline or cancel your order (in whole or in
                part) for any reason. We may require additional verifications or
                information before accepting any order. Your order is not
                accepted until we send you shipping information for the order
                (or the accepted portion thereof). Notwithstanding the
                foregoing, you agree that, if we cancel all or a part of your
                order, your sole and exclusive remedy is either that (a) we will
                issue a credit to your credit card account in the amount charged
                for the cancelled portion (if your credit card has already been
                charged for the order) or (b) we will not charge your credit
                card for the cancelled portion of the order.
              </Text>
              <Text>
                <strong>4. Payment Terms</strong>
                <br />
                <br />
                For each Product you order on the Website, you agree to pay the
                price applicable for the Product as of the time you submitted
                your order ({'"'}Product Price{'"'}), the delivery fees for the
                delivery service you select ({'"'}Delivery Fees{'"'}), and any
                applicable Taxes (defined below). Your next billing date will be
                displayed in your Account page next to each of your active
                Orders. We reserve the right to change the timing of our
                billing, in particular, if your Payment Method has not
                successfully settled, there are unforeseen delays that hinder
                our ability to ship your product or there are changes to your
                account or Orders. Please also note that if your Order includes
                a discounted Product Price for a promotional period, once the
                promotional period expires, your Order will renew at the full
                Product Price. 
                <strong>
                  As such, if you purchase an Order, until such time as you
                  terminate your Order in accordance with the directions on this
                  website, you hereby authorize, agree and assent to the Company
                  automatically billing your credit card submitted as part of
                  the order process for such amounts that are due.
                </strong>
                You will be solely responsible for payment of all taxes (other
                than taxes based on Company’s income), fees, duties, and other
                governmental charges, and any related penalties and interest,
                arising from the Product purchase ({'"'}Taxes{'"'}) not withheld
                by Company. All payments are non-refundable.
              </Text>
              <Text>
                <strong>5. Shipping Policy</strong>
                <br />
                <br />
                Products will be shipped in accordance with the shipping method
                you selected when placing the order. Any delivery dates provided
                by Company are estimates. Company reserves the right to make
                deliveries in installments. Company will send you an email when
                your order has shipped and you may review your order and
                shipping information on your Account.
              </Text>
              <Text>
                <strong>6. Cancellation Policy</strong>
                <br />
                <br />
                You can cancel an Order anytime after the first box ships.
                Please cancel using our online service the day before your
                billing date if you do not wish to receive that month’s box.
              </Text>
              <Text>
                <strong>7. Return Policy</strong>
                <br />
                <br />
                (i) Return Policy. Unless the Product information page states
                that a Product is a Final Sale, Company will accept returns only
                for store credit and only in accordance with the Return
                Procedures below. If a Product is a Final Sale, then the sale is
                final and no returns will be accepted. Provided that Company
                confirms that your Product was not a Final Sale and was returned
                in accordance with the Return Procedures below, your sole and
                exclusive remedy is that we will issue you a store credit in the
                amount charged for the applicable Product; provided that the
                credited amount will not include the applicable Delivery Fee,
                which is nonrefundable. Store credit may only be used for future
                purchase of Products on the Site (excluding gift cards) and are
                not transferable.
                <br />
                <br />
                (ii) Exchanges. We do not accept any Product exchanges.
                <br />
                <br />
                (iii) Damaged Products. If the Product arrives damaged or not
                substantially as described on the Product information page (
                {'"'}Damaged Product{'"'}), Company will accept returns for a
                full refund only in accordance with the Return Procedures below.
                Provided that Company confirms that your Product was a Damaged
                Product and was returned in accordance with the Return
                Procedures below, your sole and exclusive remedy is that (a) we
                will issue a refund to your credit card in the amount charged
                for the Damaged Product (if your credit card has already been
                charged for the Product) or (b) we will not charge your credit
                card for the Damaged Product. The refunded amount will include
                the applicable Delivery Fee.
                <br />
                <br />
                (iv) Return Procedures. The following sets forth the required
                {'"'}Return Procedures{'"'}: All returns must be made within 14
                days after the Product shipment date. All returned Products must
                be unused and returned in accordance with the instructions
                received from contacting customer service. You are solely
                responsible for the cost of shipping the returned Product. All
                Products not returned in accordance with the Return Procedures
                shall be sent back to you and no credit or refund will be
                issued.
              </Text>
              <Text>
                <strong>8. Promotions</strong>
                <br />
                <br />
                Please read the official rules that accompany each special
                offer, coupon, discount, contest and sweepstakes that we may
                offer or conduct. Special offers, coupons, or discounts cannot
                be used in conjunction with other offers. Limit one promotion
                per order.
              </Text>
            </VStack>
            <VStack spacing={10} align="flex-start">
              <Heading fontSize="24px">
                III. DISCLAIMERS, LIMITATIONS AND EXCLUSIONS OF LIABILITY
              </Heading>
              <Text>
                <strong>1. Disclaimer</strong>
                <br />
                <br />
                THE SERVICES AND ALL INFORMATION AND CONTENT ARE PROVIDED ON AN
                {'"'}AS IS{'"'} AND {'"'}AS AVAILABLE{'"'} BASIS. WE, OUR
                AFFILIATES, AGENTS AND/OR SUPPLIERS, OFFICERS, DIRECTORS, AND
                EMPLOYEES EXPRESSLY DISCLAIM ANY AND ALL WARRANTIES, WHETHER
                EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE WARRANTIES
                OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
                NON-INFRINGEMENT OF THE RIGHTS OF THIRD PARTIES. WE DISCLAIM ALL
                RESPONSIBILITY FOR, BUT NOT LIMITED TO, ANY LOSS, INJURY, CLAIM,
                LIABILITY, OR DAMAGE OF ANY KIND RESULTING FROM, ARISING OUT OF,
                OR ANY WAY RELATED TO: (I) ANY ERRORS IN OR OMISSIONS FROM THE
                SERVICES AND THEIR INFORMATION AND CONTENT, INCLUDING BUT NOT
                LIMITED TO TECHNICAL INACCURACIES AND TYPOGRAPHICAL ERRORS; (II)
                ANY THIRD PARTY WEBSITES OR THIRD PARTY APPLICATIONS SOFTWARE
                AND CONTENT, DIRECTLY OR INDIRECTLY, ACCESSED THROUGH HYPERTEXT
                LINKS IN THE SERVICES, INCLUDING BUT NOT LIMITED TO ANY ERRORS
                IN OR OMISSIONS THEREFROM; (III) THE UNAVAILABILITY OF THE
                SERVICES, INFORMATION AND CONTENT, OR ANY PORTION THEREOF; (IV)
                YOUR USE OF THE SERVICES OR RELIANCE ON ANY INFORMATION AND
                CONTENT); (V) PROBLEMS OR TECHNICAL MALFUNCTION OF ANY TELEPHONE
                NETWORK OR LINES, COMPUTER ON-LINE SYSTEMS, SERVERS, INTERNET
                ACCESS PROVIDERS, COMPUTER EQUIPMENT, SOFTWARE, OR ANY
                COMBINATION THEREOF, INCLUDING WITHOUT LIMITATION ANY INJURY OR
                DAMAGE TO YOUR, OR ANY OTHER PERSON{"'"}S, COMPUTER AS A RESULT
                OF USING THE SERVICES, OR; (VI) YOUR USE OF ANY EQUIPMENT OR
                SOFTWARE IN CONNECTION WITH THE SERVICES.
                <br />
                <br />
                ALL WARRANTIES, TERMS AND CONDITIONS, EXPRESS OR IMPLIED BY
                STATUTE, COMMON LAW OR OTHERWISE, ARE EXCLUDED TO THE MAXIMUM
                EXTENT PERMITTED UNDER APPLICABLE LAWS. WE DO NOT REPRESENT OR
                WARRANT THAT SOFTWARE, INFORMATION AND CONTENT, OR MATERIALS IN
                THE SERVICES ARE ACCURATE, COMPLETE, RELIABLE, TIMELY, CURRENT,
                ERROR-FREE, OR OTHERWISE RELIABLE, OR THAT THE SERVICES ARE FREE
                OF VIRUSES OR OTHER HARMFUL COMPONENTS. THEREFORE, YOU SHOULD
                EXERCISE CAUTION IN THE USE AND DOWNLOADING OF ANY SOFTWARE,
                INFORMATION AND CONTENT, OR MATERIALS AND USE
                INDUSTRY-RECOGNIZED SOFTWARE TO DETECT AND DISINFECT VIRUSES. AS
                A VISITOR TO THE SERVICES, YOU ACKNOWLEDGE AND AGREE THAT ANY
                RELIANCE ON OR USE BY YOU OF ANY INFORMATION AND CONTENT SHALL
                BE ENTIRELY AT YOUR OWN RISK.
                <br />
                <br />
                YOU FURTHER ACKNOWLEDGE AND AGREE THAT IN NO EVENT SHALL WE BE
                LIABLE FOR ANY LOSS, INJURY, CLAIM, LIABILITY, OR DAMAGE OF ANY
                KIND RESULTING FROM YOUR USE OF THE SERVICES. WE SHALL NOT BE
                LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, INCIDENTAL, OR
                CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER (INCLUDING, WITHOUT
                LIMITATION, LEGAL FEES AND COURT COSTS) IN ANY WAY DUE TO,
                RESULTING FROM, OR ARISING IN CONNECTION WITH: (I) THE USE OF OR
                INABILITY TO USE THE SERVICES, OR ANY INFORMATION AND CONTENT
                CONTAINED THEREIN; (II) YOUR PARTICIPATION OR RELIANCE ON ANY
                INFORMATION AND CONTENT ACCESSED IN CONNECTION WITH THE
                SERVICES; (III) ANY OTHER MATTER RELATING TO THE SERVICES AND/OR
                ANY INFORMATION AND CONTENT, REGARDLESS OF WHETHER ANY OF THE
                FOREGOING IS DETERMINED TO CONSTITUTE A FUNDAMENTAL BREACH OR
                FAILURE OF ESSENTIAL PURPOSE.
                <br />
                <br />
                TO THE EXTENT THIS LIMITATION ON LIABILITY IS PROHIBITED, OUR
                SOLE OBLIGATION TO YOU FOR DAMAGES SHALL BE LIMITED TO RINGGIT
                MALAYSIA TEN (RM10.00) (OR THE EQUIVALENT IN ANY OTHER CURRENCY)
              </Text>
              <Text>
                <strong>2. Exclusions</strong>
                <br />
                <br />
                THE LIMITATIONS OR EXCLUSIONS OF WARRANTIES AND LIABILITY
                CONTAINED IN THESE TERMS DO NOT AFFECT OR PREJUDICE THE
                STATUTORY RIGHTS OF A CONSUMER, I.E., A PERSON ACQUIRING GOODS
                OTHERWISE THAN IN THE COURSE OF A BUSINESS. THE LIMITATIONS OR
                EXCLUSIONS OF WARRANTIES AND REMEDIES CONTAINED IN THESE TERMS
                SHALL APPLY TO CUSTOMER ONLY TO THE EXTENT SUCH LIMITATIONS OR
                EXCLUSIONS AND REMEDIES ARE PERMITTED UNDER THE LAWS OF THE
                JURISDICTION WHERE CUSTOMER IS LOCATED. IF APPLICABLE LAW
                REQUIRES ANY WARRANTIES WITH RESPECT TO THE PRODUCT, ALL SUCH
                WARRANTIES ARE LIMITED IN DURATION TO NINETY (90) DAYS FROM THE
                DATE OF DELIVERY. CERTAIN STATES AND/OR JURISDICTIONS DO NOT
                ALLOW THE EXCLUSION OF IMPLIED WARRANTIES OR LIMITATION OF
                LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE
                EXCLUSIONS SET FORTH ABOVE MAY NOT APPLY TO YOU.
              </Text>
              <Text>
                <strong>3. Indemnification by User</strong>
                <br />
                <br />
                You agree to indemnify, defend, and hold{' '}
                <strong>FlipEd Sdn Bhd (1153403-V)</strong> and{' '}
                <strong>Our</strong> affiliates, business partners, officers,
                directors, employees, and agents harmless from any loss,
                liability, claim, action, suit, demand, damage, or expense
                (including reasonable legal fees, costs of investigation and
                court costs) asserted by any third party relating in any way to,
                or in respect of, your use of the Services, any third party
                applications, software, information, content, and/or materials
                you post or share on or through the Services, or breach of these
                Terms {'&'} Conditions. We reserve the right to assume the
                exclusive defence and control of any matter subject to
                indemnification by you, which shall not excuse your indemnity
                obligations.
              </Text>
            </VStack>
            <VStack spacing={10} align="flex-start">
              <Heading fontSize="24px">IV. TERM AND TERMINATION</Heading>
              <Text>
                These Terms will become effective and binding when you use the
                Site or Service, when you voluntarily provide any information
                about yourself to us, or when you indicate your agreement by
                following any instructions we place on the Site (such as buttons
                labeled {'"'}I Agree{'"'}) (which ever occurs first). We reserve
                the right to terminate these Terms, your Account, and your
                access to the Site and the Service at any time without notice.
                You may delete your Account at any time, for any reason, by
                following the instructions on the Site. If we have suspended or
                terminated these Terms, your Account, the Site, or the Service
                other than for your breach of these Terms, we will refund you a
                pro-rata share of any amounts you have pre-paid for a
                Subscription to the Service (if any). You understand that any
                termination of your Account involves deletion of your User
                Content associated therewith from our live databases. Your
                rights under these Terms will automatically and immediately
                terminate if you fail to comply with your promises and
                obligations stated in these Terms. The provisions of sections
                I.3, I.4, I. 5, I.6, II.1, III, and V will survive the
                termination of these Terms.
              </Text>
            </VStack>
            <VStack spacing={10} align="flex-start">
              <Heading fontSize="24px">V. MISCELLANEOUS MATTERS</Heading>
              <Text>
                <strong>1. Governing Law</strong>
                <br />
                <br />
                These Terms {'&'} Conditions shall be governed by and construed
                in accordance with the laws of Malaysia, without regard to
                conflict of laws provisions thereto. All rights and remedies,
                whether conferred by this agreement, by any other instrument or
                by law, shall be cumulative, and may be exercised singly or
                concurrently. The parties hereby irrevocably agree to the
                exclusive jurisdiction of the courts of the Malaysia.
              </Text>
              <Text>
                <strong>2. Modifications to Terms</strong>
                <br />
                <br />
                We may change these Terms from time to time. Any such changes
                will become effective upon the earlier of thirty (30) calendar
                days following our dispatch of an e-mail notice to you (if
                applicable) or thirty (30) calendar days following our posting
                of notice of the changes on our Site. These changes will be
                effective immediately for new users of our Site or Services. If
                you object to any such changes, your sole recourse will be to
                cease using the Site and the Services (including the
                Downloadable Tools). Continued use of the Site or the Services
                (including any Downloadable Tools) following posting of any such
                changes will indicate your acknowledgement of such changes and
                your agreement to be bound by the revised Terms, inclusive of
                such changes. In addition, certain features of the Services may
                be subject to additional terms of use. By using such features,
                or any part thereof, you agree to be bound by the additional
                terms of use applicable to such features. In the event that any
                of the additional terms of use governing such area conflict with
                these Terms, the additional terms will govern.
              </Text>
              <Text>
                <strong>3. Service Modifications</strong>
                <br />
                <br />
                You acknowledge and agree that We reserve the right, in its sole
                discretion and at any time, to modify, change, add to,
                terminate, or discontinue (with respect to an individual user or
                all users) the Services and/or any associated services,
                Information And Content, or technical specifications with or
                without notice to you, and that We will not be responsible or
                liable, directly or indirectly, to you or any other person in
                any way for any loss or damage of any kind incurred as a result
                of, or in connection with, any such modification or
                discontinuance.
              </Text>
              <Text>
                <strong>4. Privacy and Personal Information</strong>
                <br />
                <br />
                You acknowledge that you have read and agree to all of the terms
                and conditions of Our Privacy Policy, which is located at{' '}
                <ChakraLink
                  as={Link}
                  to="/privacy"
                  target="_blank"
                  rel="noopener noreferrer"
                  color="cerulean.500"
                >
                  https://www.agencyofcurioustasks.com/privacy
                </ChakraLink>{' '}
                and that you agree to the incorporation of the Privacy Policy
                into these Terms {'&'} Conditions.
              </Text>
              <Text>
                <strong>
                  5. Confidentiality and Transmissions over the Internet
                </strong>
                <br />
                <br />
                The transmission of data or information (including
                communications by e-mail) over the Internet or other publicly
                accessible networks is inherently not secure, and is subject to
                possible loss, interception, or alteration while in transit.
                Accordingly, We do not assume any liability for any damage you
                may experience or costs you may incur as a result of any
                transmissions over the Internet or other publicly accessible
                networks, including without limitation transmissions involving
                the exchange of e-mail with Us, containing your personal
                information. While We shall take commercially reasonable efforts
                to safeguard the privacy of the information you provide to Us
                and shall treat such information in accordance with Our Privacy
                Policy, in no event will the information you provide to Us be
                deemed to be confidential, create any fiduciary obligations to
                you on Our part, or result in any liability to you on Our part
                in the event that such information is inadvertently released by
                Us or accessed by third parties without Our consent.
              </Text>
              <Text>
                <strong>6. Proprietary Rights</strong>
                <br />
                <br />
                The content made available on our Website{' '}
                <ChakraLink
                  as={Link}
                  to="/"
                  target="_blank"
                  rel="noopener noreferrer"
                  color="cerulean.500"
                >
                  https://www.agencyofcurioustasks.com/
                </ChakraLink>
                , including but not limited to Our logo, our get-up, and other
                identifying marks of Ours are and shall remain the trade-marks
                and trade names and exclusive property of Ours, the and any
                unauthorised use of which is prohibited. Other trade-marks on
                the Services are the property of their respective owners. All
                intellectual property rights in content in the Services, are
                owned by us or our licensors. You are only granted a limited
                personal, non-exclusive, non-sub-licensable, non-transferable,
                licence to use such content provided you comply with any terms
                of licence and (where subject to fees) make due payment of fees.
                Without prejudice to the generality of the foregoing, content
                include without limitation all artwork, literary works, text,
                sound recording, designs, inventions, programs, compiled
                binaries, interface layout, interface text, documentation,
                graphics and any other subject matter capable of intellectual
                property rights protection including where registrations may be
                applied for or secured. The information and content may be used
                by you only for your personal, non-commercial use. Any rights
                not expressly granted to you are reserved by Us.
              </Text>
              <Text>
                <strong>7. Unlawful Activity</strong>
                <br />
                <br />
                We reserve the right to investigate complaints or reported
                violations of these Terms {'&'} Conditions and to take any
                action We deem appropriate, including but not limited to
                reporting any suspected unlawful activity to law enforcement
                officials, regulators, or other third parties, and disclosing
                any information necessary or appropriate to such persons or
                entities relating to your profiles, e-mail addresses, usage
                history, posted materials, IP addresses, and traffic
                information.
              </Text>
              <Text>
                <strong>8. General</strong>
                <br />
                <br />
                These Terms {'&'} Conditions incorporate by reference all
                notices and disclaimers contained in the Services, including but
                not limited to the Privacy Policy and, except for any agreements
                with Us that expressly reference these Terms {'&'} Conditions,
                constitute the entire agreement between you and Us, with respect
                to access to, and use of, the Services. Except as specifically
                set forth herein, these Terms {'&'} Conditions supersede any
                prior agreements, including prior oral and/or written statements
                or representations not contained herein, between you and Us
                relating to the Services. This agreement is not intended to
                create a partnership, joint venture or agency relationship
                between you and Us.
                <br />
                <br />
                If any provision of these Terms {'&'} Conditions is invalid,
                illegal or unenforceable in any respect under any applicable
                statute or rule of law, the provision shall be deemed omitted to
                the extent that it is invalid, illegal or unenforceable. In such
                a case, the remaining provisions of these Terms {'&'} Conditions
                shall be construed in a manner as to give greatest effect to the
                original intention of the parties hereto. Our failure to insist
                upon, or enforce, strict performance of any right or provision
                of these Terms {'&'} Conditions shall not constitute or be
                construed or deemed to be a waiver of such right or provision in
                the future or a waiver of any other right or provision under
                these Terms {'&'}
                Conditions. You agree that these Terms {'&'} Conditions, Our
                Privacy Policy and other notices posted through the Services
                have been drawn up in English. Although translations in other
                languages of any of the foregoing documents may be available,
                such translations may not be up to date or complete.
                Accordingly, you agree that in the event of any conflict between
                the English language version of the foregoing documents and any
                other translations thereto, the English language version of such
                documents shall govern.
              </Text>
              <Text>
                <strong>9. Force Majeure</strong>
                <br />
                <br />
                In addition to applicable disclaimers stated above, Our
                performance under these Terms {'&'} Conditions shall be excused
                in the event of interruption and/or delay due to, or resulting
                from, causes beyond its reasonable control, including but not
                limited to acts of God, acts of any government, epidemic,
                pandemic, war or other hostility, civil disorder, the elements,
                fire, flood, earthquake, explosion, embargo, acts of terrorism,
                power failure, equipment failure, industrial or labour disputes
                or controversies, acts of any third party data provider(s) or
                other third party information provider(s), third party software,
                or communication method interruptions.
              </Text>
              <Text>
                <strong>10. Contact</strong>
                <br />
                <br />
                If you have any questions or concerns about the Website,
                Services, or these Terms {'&'} Conditions, please feel free to
                contact us at{' '}
                <ChakraLink
                  href="mailto:enquiries@agencyofcurioustasks.com"
                  color="cerulean.500"
                >
                  enquiries@agencyofcurioustasks.com
                </ChakraLink>
                .
                <br />
                <br />
                Address: 15-2, Jalan Sri Hartamas 7, Taman Sri Hartamas, 50480
                Kuala Lumpur
                <br />
                Telephone:{' '}
                <ChakraLink href="tel:+60362062339" color="cerulean.500">
                  03-6206 2339
                </ChakraLink>
              </Text>
            </VStack>
          </VStack>
        </Container>
      </Box>
    </>
  );
};

export default Terms;
