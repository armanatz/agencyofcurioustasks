import React from 'react';
import { Link } from 'react-router-dom';
import {
  Container,
  Box,
  Text,
  Image,
  Stack,
  Heading,
  Link as ChakraLink,
} from '@chakra-ui/react';
import { Helmet } from 'react-helmet';

import Travolta from '../assets/common/confused_travolta.gif';

const NotFound = () => {
  return (
    <>
      <Helmet>
        <title>Page Not Found - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
        px={{ base: 1, sm: 4 }}
      >
        <Stack spacing={10} textAlign="center" mt={10} mb={12}>
          <Image src={Travolta} w="200px" mx="auto" />
          <Box maxW="500px" px={6}>
            <Heading size="2xl" color="froly.500">
              Oh no!
            </Heading>
            <Text fontSize="18px">
              Looks like you have taken a wrong turn and we couldn{"'"}t figure
              out where you were going.
            </Text>
          </Box>
          <ChakraLink as={Link} to="/" color="cerulean.500" fontWeight="bold">
            Let{"'"}s take you back somewhere safe, shall we?
          </ChakraLink>
        </Stack>
      </Container>
    </>
  );
};

export default NotFound;
