import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import {
  Container,
  Box,
  Flex,
  Heading,
  ButtonGroup,
  Button,
  VStack,
  useToast,
} from '@chakra-ui/react';
import { useQuery, useQueryClient } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import PartnersTable from '../../../components/Admin/Partners/Table';
import PartnersFilter from '../../../components/Admin/Partners/Filter';

import { fetchPartners } from '../../../queries/Partners';

const AdminPartners = () => {
  const [submitting, setSubmitting] = useState(false);

  const toast = useToast();
  const queryClient = useQueryClient();

  const { data: partners, status: partnersQueryStatus } = useQuery(
    'partners',
    () => fetchPartners(),
  );

  if (partnersQueryStatus === 'loading') {
    return <Loading />;
  }

  const onFilterAction = async (action, values) => {
    setSubmitting(true);
    try {
      if (action === 'submit') {
        let filter = '';

        Object.keys(values).forEach((key, i) => {
          if (i === 0) {
            filter += `${key}=${encodeURIComponent(values[key])}`;
          } else {
            filter += `&${key}=${encodeURIComponent(values[key])}`;
          }
        });

        await queryClient.fetchQuery('partners', () => fetchPartners(filter));

        return setSubmitting(false);
      }

      await queryClient.fetchQuery('partners', () => fetchPartners());

      return setSubmitting(false);
    } catch (err) {
      setSubmitting(false);
      return toast({
        title: 'Woops, an error occurred :(',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  };

  return (
    <>
      <Helmet>
        <title>Partner Management | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <VStack spacing={6} w="full">
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <Flex
              flexDir={['column', 'row']}
              justifyContent="space-between"
              alignItems="center"
              py={6}
            >
              <Heading>Partner Management</Heading>
              <ButtonGroup>
                <Button as={Link} to="/admin/partners/add">
                  Add Partner
                </Button>
              </ButtonGroup>
            </Flex>
          </Box>
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <PartnersFilter
              submitting={submitting}
              onSubmit={values => onFilterAction('submit', values)}
              onReset={() => onFilterAction('reset')}
            />
          </Box>
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <Box my="6">
              <Box maxW="full" pb={6} px={0}>
                <Box overflowX="scroll" maxW="full" display="block">
                  <PartnersTable data={partners} />
                </Box>
              </Box>
            </Box>
          </Box>
        </VStack>
      </Container>
    </>
  );
};

export default AdminPartners;
