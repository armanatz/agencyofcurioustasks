import React, { useState } from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import { useHistory, useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import EpisodeEditForm from '../../../components/Admin/Products/Episodes/EditForm';

import { fetchEpisodeById } from '../../../queries/Episodes';
import { fetchPricesByProductId } from '../../../queries/Stripe';

const EpisodeEdit = () => {
  const [submitting, setSubmitting] = useState(false);

  const queryClient = useQueryClient();
  const location = useLocation();
  const history = useHistory();
  const toast = useToast();

  const episodeId = location.pathname.substring(
    location.pathname.lastIndexOf('/') + 1,
  );

  const { data: episode, status: episodeQueryStatus } = useQuery(
    ['episode', episodeId],
    () => fetchEpisodeById(episodeId),
  );

  const { data: prices, status: pricesQueryStatus } = useQuery(
    ['prices', episode?.stripeProductId],
    () => fetchPricesByProductId(episode.stripeProductId),
    {
      enabled: !!episode && episode?.stripeProductId !== null,
    },
  );

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/episodes/${episode?.id}`,
        {
          method: 'PATCH',
          credentials: 'include',
          body: data,
        },
      );
    },
    {
      onSuccess: async req => {
        setSubmitting(false);

        if (req.ok) {
          queryClient.invalidateQueries('episode');
          queryClient.invalidateQueries('episodes');
          history.push('/admin/products');
          return toast({
            title: 'Episode Updated',
            status: 'success',
            duration: 5000,
            isClosable: true,
          });
        }

        const res = await req.json();

        return toast({
          description: res.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    setSubmitting(true);

    if (!values.coverImage) {
      setSubmitting(false);
      return toast({
        title: 'Cover Image is required',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    const data = { ...values };

    delete data.coverImage;
    delete data.otherImages;

    const imageArr = [values.coverImage, ...values.otherImages];

    const newFiles = imageArr.filter(el => !el.imageUrl);

    const filesToDelete = episode.images.filter(obj1 => {
      return !imageArr.some(obj2 => {
        if (obj2.imageUrl) {
          return obj1.imageUrl === obj2.imageUrl;
        }
        return false;
      });
    });

    if (filesToDelete.length > 0) {
      data.filesToDelete = filesToDelete;
    }

    if (newFiles.length > 0) {
      data.images = newFiles;
    }

    const keys = Object.keys(values);

    keys.forEach(key => {
      if (key === 'price') {
        if (prices) {
          if (prices.length > 0) {
            if (data.price === prices[0].unitAmount) {
              delete data.price;
            }
          }
        } else {
          delete data.price;
        }
      }

      if (
        data[key] === '' ||
        data[key] === undefined ||
        data[key] === null ||
        data[key] === episode[key]
      ) {
        delete data[key];
      }
    });

    if (Object.entries(data).length !== 0) {
      const formData = new FormData();
      Object.keys(data).forEach(key => {
        if (key === 'images') {
          data.images.forEach(image => formData.append('images', image));
        } else if (key === 'filesToDelete') {
          formData.append('filesToDelete', JSON.stringify(filesToDelete));
        } else {
          formData.append(key, data[key]);
        }
      });
      return mutation.mutate(formData);
    }

    setSubmitting(false);

    return toast({
      title: 'Nothing was changed',
      status: 'warning',
      duration: 5000,
      isClosable: true,
    });
  };

  if (episodeQueryStatus === 'loading' || pricesQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>Edit Episode | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Edit Episode</Heading>
          </Flex>
          <Divider />
          <Box my="6">
            <EpisodeEditForm
              data={{ ...episode, prices: prices || [] }}
              onSubmit={onSubmit}
              submitting={submitting}
            />
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default EpisodeEdit;
