import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  ButtonGroup,
  Button,
  Text,
  Tooltip,
  Select,
  HStack,
  Center,
  Spinner,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useQuery } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import SeasonsTable from '../../../components/Admin/Products/Seasons/Table';
import EpisodesTable from '../../../components/Admin/Products/Episodes/Table';
import ActivationCodesTable from '../../../components/Admin/Products/ActivationCodes/Table';

import { fetchSeasons } from '../../../queries/Seasons';
import {
  fetchAllEpisodes,
  fetchEpisodesBySeasonId,
} from '../../../queries/Episodes';
import { fetchActivationCodes } from '../../../queries/Products';
import { fetchPartners } from '../../../queries/Partners';

const AdminProducts = () => {
  const [episodeTableLoading, setEpisodeTableLoading] = useState(false);

  const queryClient = useQueryClient();
  const toast = useToast();

  const { data: seasons, status: seasonsQueryStatus } = useQuery(
    'seasons',
    fetchSeasons,
  );

  const { data: episodes, status: episodesQueryStatus } = useQuery(
    'episodes',
    fetchAllEpisodes,
  );

  const { data: partners, status: partnersQueryStatus } = useQuery(
    'partners',
    () => fetchPartners(),
  );

  const { data: activationCodes, status: activationCodesQueryStatus } =
    useQuery('activationCodes', fetchActivationCodes);

  useEffect(() => {
    return () => {
      queryClient.fetchQuery('episodes', fetchAllEpisodes);
    };
  }, []);

  const user = queryClient.getQueryData('user');

  const canAdd = user.roles.some(role => {
    switch (role) {
      case 'Superuser':
      case 'Admin':
      case 'Editor':
        return true;
      default:
        return false;
    }
  });

  if (
    seasonsQueryStatus === 'loading' ||
    episodesQueryStatus === 'loading' ||
    activationCodesQueryStatus === 'loading' ||
    partnersQueryStatus === 'loading'
  ) {
    return <Loading />;
  }

  const onEpisodeFilter = async e => {
    setEpisodeTableLoading(true);
    try {
      if (e.target.value === 'All') {
        await queryClient.fetchQuery('episodes', fetchAllEpisodes);
      } else {
        await queryClient.fetchQuery('episodes', () =>
          fetchEpisodesBySeasonId(e.target.value),
        );
      }
      setEpisodeTableLoading(false);
    } catch (err) {
      setEpisodeTableLoading(false);
      return toast({
        title: 'Woops, an error occurred :(',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  };

  return (
    <>
      <Helmet>
        <title>Product Management | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex
            flexDir={['column', 'row']}
            justifyContent="space-between"
            alignItems="center"
            py={6}
          >
            <Heading>Product Management</Heading>
            {canAdd && (
              <>
                <ButtonGroup>
                  <Button as={Link} to="/admin/products/season/add">
                    Add Season
                  </Button>
                  <Button as={Link} to="/admin/products/episode/add">
                    Add Episode
                  </Button>
                  {episodes.length > 0 ? (
                    <Button as={Link} to="/admin/products/activation-code/add">
                      Add Activation Code
                    </Button>
                  ) : (
                    <Tooltip
                      label="To add an activation code, you need to create an episode first"
                      placement="top"
                      backgroundColor="niagara.500"
                      closeOnClick={false}
                    >
                      <Button>Add Activation Code</Button>
                    </Tooltip>
                  )}
                </ButtonGroup>
              </>
            )}
          </Flex>
          <Divider />
          <Box my="6">
            <Tabs variant="solid-rounded" colorScheme="niagara">
              <TabList>
                <Tab>Seasons</Tab>
                <Tab>Episodes</Tab>
                <Tab>Activation Codes</Tab>
              </TabList>
              <TabPanels>
                <TabPanel maxW="full" pb={6} px={0}>
                  {seasons.length > 0 ? (
                    <Box overflowX="scroll" maxW="full" display="block">
                      <SeasonsTable data={seasons} />
                    </Box>
                  ) : (
                    <Box textAlign="center">
                      <Text>No seasons to display</Text>
                    </Box>
                  )}
                </TabPanel>
                <TabPanel maxW="full" pb={6} px={0}>
                  <Box my={4}>
                    <HStack>
                      <Text fontSize="14px">Season:</Text>
                      <Select size="sm" w="30%" onChange={onEpisodeFilter}>
                        <option>All</option>
                        {seasons.map(el => (
                          <option key={el.id} value={el.id}>
                            {el.title}
                          </option>
                        ))}
                      </Select>
                    </HStack>
                  </Box>
                  {episodeTableLoading ? (
                    <Center>
                      <Spinner color="niagara.500" size="xl" />
                    </Center>
                  ) : (
                    <>
                      {episodes.length > 0 ? (
                        <Box overflowX="scroll" maxW="full" display="block">
                          <EpisodesTable data={episodes} />
                        </Box>
                      ) : (
                        <Box textAlign="center">
                          <Text>No episodes to display</Text>
                        </Box>
                      )}
                    </>
                  )}
                </TabPanel>
                <TabPanel maxW="full" pb={6} px={0}>
                  {activationCodes.length > 0 ? (
                    <Box overflowX="scroll" maxW="full" display="block">
                      <ActivationCodesTable
                        data={activationCodes}
                        partners={partners}
                      />
                    </Box>
                  ) : (
                    <Box textAlign="center">
                      <Text>No activation codes to display</Text>
                    </Box>
                  )}
                </TabPanel>
              </TabPanels>
            </Tabs>
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default AdminProducts;
