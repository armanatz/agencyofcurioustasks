import React, { useState } from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import ActivationCodeAddForm from '../../../components/Admin/Products/ActivationCodes/AddForm';

const ActivationCodeAdd = () => {
  const [submitting, setSubmitting] = useState(false);
  const queryClient = useQueryClient();
  const toast = useToast();
  const history = useHistory();

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/products/activation_codes`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async res => {
        setSubmitting(false);

        if (res.status !== 200) {
          const error = await res.json();
          return toast({
            description: error.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }
        queryClient.invalidateQueries('activationCodes');
        history.push('/admin/products');
        return toast({
          title: 'Activation Code Added',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    setSubmitting(true);
    return mutation.mutate(values);
  };

  return (
    <>
      <Helmet>
        <title>Add Activation Code | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Add Activation Code</Heading>
          </Flex>
          <Divider />
          <Box my="6">
            <ActivationCodeAddForm
              onSubmit={onSubmit}
              submitting={submitting}
            />
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default ActivationCodeAdd;
