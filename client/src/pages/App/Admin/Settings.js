import React, { useState } from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  Button,
  VStack,
  Modal,
  ModalOverlay,
  ModalHeader,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  useToast,
  useDisclosure,
  Select,
} from '@chakra-ui/react';
import { useQuery, useQueryClient, useMutation } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import SettingsForm from '../../../components/Admin/SettingsForm';
import GiftCardsTable from '../../../components/Admin/GiftCards/Table';
import GiftCardAddForm from '../../../components/Admin/GiftCards/AddForm';

import { fetchGlobalSettings } from '../../../queries/Settings';
import { fetchAllGiftCards } from '../../../queries/GiftCards';

const AdminSettings = () => {
  const [settingFormSubmitting, setSettingFormSubmitting] = useState(false);
  const [giftCardAddFormSubmitting, setGiftCardAddFormSubmitting] =
    useState(false);
  const [giftCardFilter, setGiftCardFilter] = useState('all');

  const queryClient = useQueryClient();
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const { data: settings, status: settingsQueryStatus } = useQuery(
    'settings',
    fetchGlobalSettings,
  );

  const { data: giftCards, status: giftCardsQueryStatus } = useQuery(
    ['giftCards', giftCardFilter],
    () => fetchAllGiftCards(giftCardFilter),
  );

  const settingsMutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/settings`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async res => {
        setSettingFormSubmitting(false);

        const data = await res.json();

        if (res.status !== 200) {
          return toast({
            description: data.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }

        queryClient.invalidateQueries('settings');

        return toast({
          title: 'Settings Changed',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSettingFormSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const giftCardAddMutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/gift_cards`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async res => {
        setGiftCardAddFormSubmitting(false);

        const data = await res.json();

        if (res.status !== 200) {
          return toast({
            description: data.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }

        queryClient.invalidateQueries('giftCards');

        onClose();

        return toast({
          title: 'Gift Card Created',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setGiftCardAddFormSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSettingsFormSubmit = values => {
    setSettingFormSubmitting(true);
    const arr = Object.keys(values).map(key => ({
      name: key,
      value: values[key],
    }));
    return settingsMutation.mutate({ data: arr });
  };

  if (settingsQueryStatus === 'loading' || giftCardsQueryStatus === 'loading') {
    return <Loading />;
  }

  const onGiftCardAddFormSubmit = values => {
    setGiftCardAddFormSubmitting(true);
    return giftCardAddMutation.mutate(values);
  };

  return (
    <>
      <Helmet>
        <title>Settings | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Create Gift Card</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <GiftCardAddForm
                onSubmit={onGiftCardAddFormSubmit}
                submitting={giftCardAddFormSubmitting}
              />
            </ModalBody>
          </ModalContent>
        </Modal>
        <VStack spacing={6} w="full">
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <Flex
              flexDir={['column', 'row']}
              justifyContent="space-between"
              alignItems="center"
              py={6}
            >
              <Heading>Settings</Heading>
            </Flex>
            <Divider />
            <Box my="6">
              <SettingsForm
                data={settings}
                submitting={settingFormSubmitting}
                onSubmit={onSettingsFormSubmit}
              />
            </Box>
          </Box>
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <Flex
              flexDir={['column', 'row']}
              justifyContent="space-between"
              alignItems="center"
              py={6}
            >
              <Heading>Gift Cards</Heading>
              <Button onClick={onOpen}>Create Gift Card</Button>
            </Flex>
            <Divider />
            <Box my="6">
              <Select
                defaultValue={giftCardFilter}
                onChange={e => setGiftCardFilter(e.target.value)}
                size="sm"
                w="20%"
                mb={6}
              >
                <option value="all">All</option>
                <option value="true">Redeemed</option>
                <option value="false">Not Redeemed</option>
              </Select>
              <GiftCardsTable data={giftCards} />
            </Box>
          </Box>
        </VStack>
      </Container>
    </>
  );
};

export default AdminSettings;
