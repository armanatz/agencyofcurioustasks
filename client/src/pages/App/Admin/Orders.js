import React, { useState } from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Button,
  VStack,
  useToast,
} from '@chakra-ui/react';
import { useQuery, useQueryClient } from 'react-query';
// import download from 'downloadjs';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import OrdersTable from '../../../components/Admin/Orders/Table';
import OrdersFilter from '../../../components/Admin/Orders/Filter';

import { fetchOrders, fetchStatusCodes } from '../../../queries/Orders';

const AdminOrders = () => {
  const [submitting, setSubmitting] = useState(false);

  const toast = useToast();
  const queryClient = useQueryClient();

  const { data: orders, status: ordersQueryStatus } = useQuery('orders', () =>
    fetchOrders(),
  );

  const { data: statusCodes, status: statusCodesQueryStatus } = useQuery(
    'statusCodes',
    fetchStatusCodes,
  );

  if (ordersQueryStatus === 'loading' || statusCodesQueryStatus === 'loading') {
    return <Loading />;
  }

  let getFileName = header => {
    let contentDispostion = header.split(';');
    const fileNameToken = 'filename="';

    let fileName = 'OrderList.csv';

    contentDispostion.forEact(value => {
      if (value.trim().indexOf(fileNameToken) === 0) {
        fileName = decodeURIComponent(
          value.trim().replace(fileNameToken, '').slice(0, -1),
        );
      }
    });

    return fileName;
  };

  const exportAsCSV = async () => {
    try {
      const request = await fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/orders/export`,
        {
          credentials: 'include',
        },
      );

      const response = {
        filename: getFileName(request.headers.get('content-disposition')),
        blob: await request.blob(),
      };

      const newBlob = new Blob([response.blob], { type: 'text/csv' });

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
      } else {
        const objUrl = window.URL.createObjectURL(newBlob);

        let link = document.createElement('a');
        link.href = objUrl;
        link.download = response.filename;
        link.click();

        setTimeout(() => {
          window.URL.revokeObjectURL(objUrl);
        }, 250);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onFilterAction = async (action, values) => {
    setSubmitting(true);
    try {
      if (action === 'submit') {
        let filter = '';

        Object.keys(values).forEach((key, i) => {
          if (i === 0) {
            filter += `${key}=${encodeURIComponent(values[key])}`;
          } else {
            filter += `&${key}=${encodeURIComponent(values[key])}`;
          }
        });

        await queryClient.fetchQuery('orders', () => fetchOrders(filter));

        return setSubmitting(false);
      }

      await queryClient.fetchQuery('orders', () => fetchOrders());

      return setSubmitting(false);
    } catch (err) {
      setSubmitting(false);
      return toast({
        title: 'Woops, an error occurred :(',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  };

  return (
    <>
      <Helmet>
        <title>Order Management | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <VStack spacing={6} w="full">
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <Flex
              flexDir={['column', 'row']}
              justifyContent="space-between"
              alignItems="center"
              py={6}
            >
              <Heading>Order Management</Heading>
              <Button onClick={exportAsCSV}>Export as CSV</Button>
            </Flex>
          </Box>
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <OrdersFilter
              submitting={submitting}
              onSubmit={values => onFilterAction('submit', values)}
              onReset={() => onFilterAction('reset')}
            />
          </Box>
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <Box my="6">
              <Box maxW="full" pb={6} px={0}>
                <Box overflowX="scroll" maxW="full" display="block">
                  <OrdersTable data={orders} statusCodes={statusCodes} />
                </Box>
              </Box>
            </Box>
          </Box>
        </VStack>
      </Container>
    </>
  );
};

export default AdminOrders;
