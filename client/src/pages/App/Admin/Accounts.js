import React, { useState } from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Modal,
  ModalOverlay,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalContent,
  VStack,
  Button,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import ChildResetForm from '../../../components/Admin/Accounts/ChildResetForm';
import ProductAssignmentForm from '../../../components/Admin/Accounts/ProductAssignmentForm';
import AdminList from '../../../components/Admin/Accounts/AdminList';
import AdminAddForm from '../../../components/Admin/Accounts/AddForm';

import { fetchUsersChildren } from '../../../queries/Families';
import { fetchAllAdmins, fetchAllAdminRoles } from '../../../queries/Users';

const AdminAccounts = () => {
  const [adminAddFormSubmitting, setAdminAddFormSubmitting] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const queryClient = useQueryClient();
  const toast = useToast();

  const { data: usersChildren, status: usersChildrenQueryStatus } = useQuery(
    'usersChildren',
    fetchUsersChildren,
  );

  const { data: admins, status: adminsQueryStatus } = useQuery(
    'admins',
    fetchAllAdmins,
  );

  const { data: adminRoles, status: adminRolesQueryStatus } = useQuery(
    'adminRoles',
    fetchAllAdminRoles,
  );

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${
          process.env.REACT_APP_API_VERSION
        }/users/admins${data.method === 'PATCH' ? `/${data.email}` : ''}`,
        {
          method: data.method,
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify({
            role: data.role,
            ...(data.method === 'POST' && { email: data.email }),
          }),
        },
      );
    },
    {
      onSuccess: async res => {
        setAdminAddFormSubmitting(false);
        if (res.status !== 200) {
          const error = await res.json();
          return toast({
            description: error.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }

        queryClient.invalidateQueries('admins');

        onClose();

        return toast({
          title: 'Admin Added',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setAdminAddFormSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  if (
    usersChildrenQueryStatus === 'loading' ||
    adminsQueryStatus === 'loading' ||
    adminRolesQueryStatus === 'loading'
  ) {
    return <Loading />;
  }

  const user = queryClient.getQueryData('user');

  const onSubmit = values => {
    setAdminAddFormSubmitting(true);

    const doesAdminExist = admins.some(el => el.email === values.email);

    return mutation.mutate({
      ...values,
      method: doesAdminExist ? 'PATCH' : 'POST',
    });
  };

  return (
    <>
      <Helmet>
        <title>Account Management | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Add Admin</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <AdminAddForm
                onSubmit={onSubmit}
                submitting={adminAddFormSubmitting}
                roles={adminRoles}
              />
            </ModalBody>
          </ModalContent>
        </Modal>
        <VStack spacing={6} w="full">
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <Flex
              flexDir={['column', 'row']}
              justifyContent="space-between"
              alignItems="center"
              py={6}
            >
              <Heading>Account Management</Heading>
            </Flex>
          </Box>
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            py={6}
            w="full"
          >
            <Heading fontSize="20px">Reset a Child{"'"}s Progress</Heading>
            <br />
            <ChildResetForm usersChildren={usersChildren} />
          </Box>
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            py={6}
            w="full"
          >
            <Heading fontSize="20px">Assign Product to Child</Heading>
            <br />
            <ProductAssignmentForm />
          </Box>
          {user.roles.includes('Superuser') && (
            <Box
              bg="white"
              borderColor="gray.300"
              borderWidth={1}
              borderRadius="md"
              px={6}
              py={6}
              w="full"
            >
              <Flex
                flexDir={['column', 'row']}
                justifyContent="space-between"
                alignItems="center"
                pb={6}
              >
                <Heading fontSize="20px">Admin Accounts</Heading>
                <Button onClick={onOpen}>Add Admin</Button>
              </Flex>
              <AdminList data={admins} />
            </Box>
          )}
        </VStack>
      </Container>
    </>
  );
};

export default AdminAccounts;
