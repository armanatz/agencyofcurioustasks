import React, { useState } from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import EpisodeAddForm from '../../../components/Admin/Products/Episodes/AddForm';

import { fetchSeasons } from '../../../queries/Seasons';

const EpisodeAdd = () => {
  const [submitting, setSubmitting] = useState(false);
  const queryClient = useQueryClient();
  const toast = useToast();
  const history = useHistory();

  const cachedSeasons = queryClient.getQueryData('seasons');

  const { data: seasons, status: seasonsQueryStatus } = useQuery(
    'seasons',
    fetchSeasons,
    { enabled: !cachedSeasons },
  );

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/episodes`,
        {
          method: 'POST',
          credentials: 'include',
          body: data,
        },
      );
    },
    {
      onSuccess: async res => {
        setSubmitting(false);
        if (res.status !== 200) {
          const error = await res.json();
          return toast({
            description: error.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }

        queryClient.invalidateQueries('episodes');
        history.push('/admin/products');
        return toast({
          title: 'Episode Added',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  if (seasonsQueryStatus === 'loading') {
    return <Loading />;
  }

  const onSubmit = values => {
    if (!values.coverImage) {
      return toast({
        title: 'Cover Image is required',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    const data = { ...values };

    if (values.price) {
      if (values.seasonId !== 1) {
        delete data.price;
      } else {
        data.price = values.price * 100;
      }
    }

    const formData = new FormData();
    Object.keys(data).forEach(key => {
      if (key === 'otherImages') {
        data.otherImages.forEach(image =>
          formData.append('otherImages', image),
        );
      } else {
        formData.append(key, data[key]);
      }
    });

    setSubmitting(true);
    return mutation.mutate(formData);
  };

  return (
    <>
      <Helmet>
        <title>Add Episode | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Add Episode</Heading>
          </Flex>
          <Divider />
          <Box my="6">
            <EpisodeAddForm
              seasons={cachedSeasons || seasons}
              submitting={submitting}
              onSubmit={onSubmit}
            />
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default EpisodeAdd;
