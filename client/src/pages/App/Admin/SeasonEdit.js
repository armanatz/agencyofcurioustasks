import React, { useState } from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import { useHistory, useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import SeasonEditForm from '../../../components/Admin/Products/Seasons/EditForm';

import { fetchSeasonById } from '../../../queries/Seasons';
import { fetchPricesByProductId } from '../../../queries/Stripe';

const SeasonEdit = () => {
  const [submitting, setSubmitting] = useState(false);

  const queryClient = useQueryClient();
  const history = useHistory();
  const location = useLocation();
  const toast = useToast();

  const seasonId = location.pathname.substring(
    location.pathname.lastIndexOf('/') + 1,
  );

  const { data: season, status: seasonQueryStatus } = useQuery(
    ['season', seasonId],
    () => fetchSeasonById(seasonId),
  );

  const { data: prices, status: pricesQueryStatus } = useQuery(
    'prices',
    () => fetchPricesByProductId(season.stripeProductId),
    {
      enabled: !!season && season?.stripeProductId !== null,
    },
  );

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/seasons/${season?.id}`,
        {
          method: 'PATCH',
          credentials: 'include',
          body: data,
        },
      );
    },
    {
      onSuccess: () => {
        setSubmitting(false);
        queryClient.invalidateQueries('season');
        queryClient.invalidateQueries('seasons');
        history.push('/admin/products');
        return toast({
          title: 'Season Updated',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    setSubmitting(true);
    const data = { ...values };

    const keys = Object.keys(data);

    keys.forEach(key => {
      if (key === 'price') {
        if (prices) {
          if (prices.length > 0) {
            if (data.price === prices[0].unitAmount) {
              delete data.price;
            }
          }
        } else {
          delete data.price;
        }
      }

      if (
        data[key] === '' ||
        data[key] === undefined ||
        data[key] === null ||
        data[key] === season[key]
      ) {
        delete data[key];
      }
    });

    if (Object.entries(data).length !== 0) {
      const formData = new FormData();
      Object.keys(data).forEach(key => formData.append(key, data[key]));
      return mutation.mutate(formData);
    }

    setSubmitting(false);
    return toast({
      title: 'Nothing was changed',
      status: 'warning',
      duration: 5000,
      isClosable: true,
    });
  };

  if (seasonQueryStatus === 'loading' || pricesQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>Edit Season | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Edit Season</Heading>
          </Flex>
          <Divider />
          <Box my="6">
            <SeasonEditForm
              data={{ ...season, prices: prices || [] }}
              onSubmit={onSubmit}
              submitting={submitting}
            />
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default SeasonEdit;
