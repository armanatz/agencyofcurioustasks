import React, { useState } from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import { useHistory, useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import PartnerEditForm from '../../../components/Admin/Partners/EditForm';

import { fetchPartnerById } from '../../../queries/Partners';

const PartnerEdit = () => {
  const [submitting, setSubmitting] = useState(false);
  const queryClient = useQueryClient();
  const location = useLocation();
  const history = useHistory();
  const toast = useToast();

  const partnerId = location.pathname.substring(
    location.pathname.lastIndexOf('/') + 1,
  );

  const { data: partner, status: partnerQueryStatus } = useQuery(
    ['partner', partnerId],
    () => fetchPartnerById(partnerId),
  );

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/partners/${partner?.id}`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: () => {
        setSubmitting(false);
        queryClient.invalidateQueries('partner');
        queryClient.invalidateQueries('partners');
        history.push('/admin/partners');
        return toast({
          title: 'Partner Updated',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    setSubmitting(true);
    const data = { ...values };

    const keys = Object.keys(data);

    keys.forEach(key => {
      if (
        data[key] === '' ||
        data[key] === undefined ||
        data[key] === null ||
        data[key] === partner[key]
      ) {
        delete data[key];
      }
    });

    if (Object.entries(data).length !== 0) {
      return mutation.mutate(data);
    }

    setSubmitting(false);

    return toast({
      title: 'Nothing was changed',
      status: 'warning',
      duration: 5000,
      isClosable: true,
    });
  };

  if (partnerQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>Edit Partner | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Edit Partner</Heading>
          </Flex>
          <Divider />
          <Box my="6">
            <PartnerEditForm
              data={partner}
              onSubmit={onSubmit}
              submitting={submitting}
            />
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default PartnerEdit;
