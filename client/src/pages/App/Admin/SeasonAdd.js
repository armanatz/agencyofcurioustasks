import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import { Helmet } from 'react-helmet';

import SeasonAddForm from '../../../components/Admin/Products/Seasons/AddForm';

const SeasonAdd = () => {
  const [submitting, setSubmitting] = useState(false);
  const queryClient = useQueryClient();
  const toast = useToast();
  const history = useHistory();

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/seasons`,
        {
          method: 'POST',
          credentials: 'include',
          body: data,
        },
      );
    },
    {
      onSuccess: async res => {
        setSubmitting(false);

        if (res.status !== 200) {
          const error = await res.json();
          return toast({
            description: error.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }

        queryClient.invalidateQueries('seasons');
        history.push('/admin/products');
        return toast({
          title: 'Season Added',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    setSubmitting(true);

    const data = { ...values };

    if (values.seasonNumber === 0) {
      delete data.price;
    }

    if (!values.coverImage) {
      setSubmitting(false);
      return toast({
        title: 'Cover Image is required',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    const formData = new FormData();
    Object.keys(data).forEach(key => formData.append(key, data[key]));
    return mutation.mutate(formData);
  };

  return (
    <>
      <Helmet>
        <title>Add Season | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Add Season</Heading>
          </Flex>
          <Divider />
          <Box mb="6">
            <SeasonAddForm onSubmit={onSubmit} submitting={submitting} />
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default SeasonAdd;
