import React, { useState } from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import PartnerAddForm from '../../../components/Admin/Partners/AddForm';

const PartnerAdd = () => {
  const [submitting, setSubmitting] = useState(false);
  const queryClient = useQueryClient();
  const toast = useToast();
  const history = useHistory();

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/partners`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async res => {
        setSubmitting(false);

        const data = await res.json();

        if (res.status !== 200) {
          return toast({
            description: data.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }

        queryClient.invalidateQueries('partners');
        history.push('/admin/partners');
        return toast({
          title: 'Partner Added',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    setSubmitting(true);
    return mutation.mutate(values);
  };

  return (
    <>
      <Helmet>
        <title>Add Partner | Admin - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Add Partner</Heading>
          </Flex>
          <Divider />
          <Box my="6">
            <PartnerAddForm onSubmit={onSubmit} submitting={submitting} />
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default PartnerAdd;
