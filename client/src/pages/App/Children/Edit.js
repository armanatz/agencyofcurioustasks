import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import {
  Box,
  Button,
  Container,
  Divider,
  Flex,
  Heading,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  useToast,
  useDisclosure,
} from '@chakra-ui/react';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import dayjs from 'dayjs';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import AvatarSelector from '../../../components/Children/AvatarSelector';
import EditForm from '../../../components/Children/EditForm';

const fetchChild = async id => {
  const response = await fetch(
    `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users/${id}`,
    { credentials: 'include' },
  );
  if (response.status === 401) {
    throw new Error('Not authenticated');
  } else if (response.ok) {
    return response.json();
  }
  throw new Error('Something went wrong');
};

const ChildrenEdit = () => {
  const [selectedAvatar, setSelectedAvatar] = useState(undefined);
  const [confirmedAvatar, setConfirmedAvatar] = useState(undefined);
  const history = useHistory();
  const location = useLocation();
  const queryClient = useQueryClient();
  const toast = useToast();
  const { isOpen, onClose, onOpen } = useDisclosure();

  const id = location.pathname.match(/([^/]*)\/*$/)[1];

  const { data: child, status: childQueryStatus } = useQuery(
    [id, id],
    () => fetchChild(id),
    {
      cacheTime: 0,
    },
  );

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users/${id}`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(id);
        queryClient.invalidateQueries('usersChildren');
        return history.push('/children');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    const birthDate = dayjs(values.birthDate).format('YYYY-MM-DD');
    const data = { ...values };

    delete data.confirmPassword;
    data.birthDate = birthDate;
    data.imageUrl = confirmedAvatar;

    const keys = Object.keys(data);

    keys.forEach(key => {
      if (key !== 'gender') {
        if (
          data[key] === '' ||
          data[key] === undefined ||
          data[key] === null ||
          data[key] === child[key]
        ) {
          delete data[key];
        }
      } else if (data.gender === child.data.gender) {
        delete data.gender;
      }
    });

    if (Object.entries(data).length !== 0) {
      return mutation.mutate(data);
    }
  };

  if (childQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>Edit Child - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Modal isOpen={isOpen} onClose={onClose} size="4xl">
          <ModalOverlay />
          <ModalContent mx={['10px', 0]}>
            <ModalHeader fontFamily="Changa">Avatar Creator</ModalHeader>
            <ModalCloseButton onClick={onClose} />
            <ModalBody>
              <AvatarSelector onChange={val => setSelectedAvatar(val)} />
            </ModalBody>
            <ModalFooter>
              <Button
                variant="ghost"
                onClick={() => {
                  setSelectedAvatar(undefined);
                  return onClose();
                }}
              >
                Cancel
              </Button>
              <Button
                ml={3}
                onClick={() => {
                  setConfirmedAvatar(selectedAvatar);
                  setSelectedAvatar(undefined);
                  return onClose();
                }}
                isDisabled={!selectedAvatar}
              >
                Select
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex alignItems="center" py={6}>
            <Heading>Edit Child</Heading>
          </Flex>
          <Divider />
          <EditForm
            data={child}
            onOpenModal={onOpen}
            onSubmit={onSubmit}
            selectedAvatar={!confirmedAvatar ? child.imageUrl : confirmedAvatar}
          />
        </Box>
      </Container>
    </>
  );
};

export default ChildrenEdit;
