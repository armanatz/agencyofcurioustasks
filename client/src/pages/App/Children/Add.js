import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Box,
  Button,
  Container,
  Divider,
  Flex,
  Heading,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  useToast,
  useDisclosure,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import dayjs from 'dayjs';
import { Helmet } from 'react-helmet';

import AvatarSelector from '../../../components/Children/AvatarSelector';
import AddForm from '../../../components/Children/AddForm';

const ChildrenAdd = () => {
  const [selectedAvatar, setSelectedAvatar] = useState(undefined);
  const [confirmedAvatar, setConfirmedAvatar] = useState(undefined);
  const history = useHistory();
  const queryClient = useQueryClient();
  const toast = useToast();
  const { isOpen, onClose, onOpen } = useDisclosure();

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async res => {
        if (res.ok) {
          queryClient.invalidateQueries('usersChildren');
          return history.push('/children');
        }

        const data = await res.json();

        return toast({
          description: data.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    const birthDate = dayjs(values.birthDate).format('YYYY-MM-DD');
    const data = { type: 'Child', imageUrl: confirmedAvatar, ...values };

    delete data.confirmPassword;
    data.birthDate = birthDate;

    if (!confirmedAvatar) {
      return toast({
        description: 'Avatar is required',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    return mutation.mutate(data);
  };

  return (
    <>
      <Helmet>
        <title>Add Child - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Modal isOpen={isOpen} onClose={onClose} size="4xl">
          <ModalOverlay />
          <ModalContent mx={['10px', 0]}>
            <ModalHeader fontFamily="Changa">Avatar Creator</ModalHeader>
            <ModalCloseButton onClick={onClose} />
            <ModalBody>
              <AvatarSelector onChange={val => setSelectedAvatar(val)} />
            </ModalBody>
            <ModalFooter>
              <Button
                variant="ghost"
                onClick={() => {
                  setSelectedAvatar(undefined);
                  return onClose();
                }}
              >
                Cancel
              </Button>
              <Button
                ml={3}
                onClick={() => {
                  setConfirmedAvatar(selectedAvatar);
                  setSelectedAvatar(undefined);
                  return onClose();
                }}
                isDisabled={!selectedAvatar}
              >
                Select
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex alignItems="center" py={6}>
            <Heading>Add Child</Heading>
          </Flex>
          <Divider />
          <AddForm
            onSubmit={onSubmit}
            onOpen={onOpen}
            selectedAvatar={confirmedAvatar}
          />
        </Box>
      </Container>
    </>
  );
};

export default ChildrenAdd;
