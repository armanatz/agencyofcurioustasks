import React from 'react';
import {
  Box,
  Button,
  Center,
  Container,
  Divider,
  Flex,
  Heading,
  Icon,
  Text,
} from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { FaUsers } from 'react-icons/fa';
import { useQuery } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import Child from '../../../components/Children/Child';

import { fetchUsersChildren } from '../../../queries/Families';

const Children = () => {
  const { data: usersChildren, status: usersChildrenQueryStatus } = useQuery(
    'usersChildren',
    fetchUsersChildren,
  );

  if (usersChildrenQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>Your Children - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Manage Children</Heading>
            <Button as={Link} to="/children/add">
              Add Child
            </Button>
          </Flex>
          <Divider />
          {usersChildren?.members.length === 0 ? (
            <Center my={6}>
              <Box textAlign="center" w="md">
                <Icon as={FaUsers} fontSize="64px" />
                <Text fontSize={18}>
                  You have no child accounts created. Please use the Add Child
                  button to create one now.
                </Text>
              </Box>
            </Center>
          ) : (
            <Box mt="6">
              {usersChildren.members.map(child => (
                <Child key={child.id} data={child} />
              ))}
            </Box>
          )}
        </Box>
      </Container>
    </>
  );
};

export default Children;
