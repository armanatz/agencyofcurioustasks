import React, { useMemo, useState } from 'react';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import { Link, Redirect, useParams } from 'react-router-dom';
import {
  Button,
  Flex,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  ModalCloseButton,
  Heading,
  Text,
  Input,
  Textarea,
  VStack,
  FormControl,
  FormErrorMessage,
  Progress,
  useDisclosure,
  useToast,
  Box,
  IconButton,
  ButtonGroup,
  Tooltip,
} from '@chakra-ui/react';
import { isMobile } from 'react-device-detect';
import Unity, { UnityContext } from 'react-unity-webgl';
import { RiHome3Fill } from 'react-icons/ri';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import LoadingMessages from '../../../components/LoadingMessages';

import { useWindowDimensions } from '../../../helpers/CustomHooks';
import { fetchActivationCodeByEpisodeId } from '../../../queries/Products';
import {
  activateGame,
  fetchIsEpisodeActivated,
  loadGame,
  saveGame,
} from '../../../queries/Games';
import { fetchEpisodesBySeasonNumber } from '../../../queries/Episodes';
import { fetchOwnedProducts } from '../../../queries/Products';

const Game = () => {
  const [gameLoadingProgress, setGameLoadingProgress] = useState(0);
  const [loginInputValue, setLoginInputValue] = useState('');
  const [otherInputValue, setOtherInputValue] = useState('');
  const [showLoginInputError, setShowLoginInputError] = useState(false);
  const [showOtherInputError, setShowOtherInputError] = useState(false);
  const [question, setQuestion] = useState('');
  const [answer, setAnswer] = useState('');
  const [unityGO, setUnityGO] = useState('');

  const { season, episode: episodeNumber } = useParams();

  const {
    isOpen: isLoginModalOpen,
    onOpen: onLoginModalOpen,
    onClose: onLoginModalClose,
  } = useDisclosure();

  const {
    isOpen: isInputModalOpen,
    onOpen: onInputModalOpen,
    onClose: onInputModalClose,
  } = useDisclosure();

  const {
    isOpen: isErrorModalOpen,
    onOpen: onErrorModalOpen,
    onClose: onErrorModalClose,
  } = useDisclosure();

  const dimensions = useWindowDimensions();
  const queryClient = useQueryClient();
  const toast = useToast();

  const user = queryClient.getQueryData('user');

  const { data: ownedProducts, status: ownedProductsQueryStatus } = useQuery(
    'ownedProducts',
    () => fetchOwnedProducts(user.id),
  );

  const { data: episodes, status: episodesQueryStatus } = useQuery(
    'episodes',
    () => fetchEpisodesBySeasonNumber(season),
  );

  const currentEpisode = episodes?.filter(
    ep => ep.episodeNumber === episodeNumber,
  )[0];

  const { data: activationCodes, status: activationCodesQueryStatus } =
    useQuery(
      'activationCodes',
      () => fetchActivationCodeByEpisodeId(currentEpisode.id),
      {
        enabled: !!episodes,
      },
    );

  const { data: isEpisodeActivated, status: isEpisodeActivatedQueryStatus } =
    useQuery(
      'isEpisodeActivated',
      () => fetchIsEpisodeActivated(user.id, currentEpisode.id),
      {
        enabled: !!episodes,
      },
    );

  const { data: loadedGame, status: loadedGameQueryStatus } = useQuery(
    'loadedGame',
    () => loadGame(user.id, currentEpisode.id),
    {
      enabled: !!episodes,
    },
  );

  const activateMutation = useMutation(
    ({ code, episodeId, userId }) => {
      return activateGame(code, episodeId, userId);
    },
    {
      onSuccess: async res => {
        if (res.status !== 200) {
          return onErrorModalOpen();
        }
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const saveGameMutation = useMutation(
    ({ episodeId, userId, progress }) => {
      return saveGame(episodeId, userId, progress);
    },
    {
      onSuccess: async res => {
        if (res.status !== 200) {
          return toast({
            title: 'Error occurred saving game :(',
            description: `Status ${res.status} received.`,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const unityContext = useMemo(
    () =>
      new UnityContext({
        loaderUrl: `${process.env.PUBLIC_URL}/GameBuilds/Seasons/${season}/Episodes/${episodeNumber}/game.loader.js`,
        dataUrl: `${process.env.PUBLIC_URL}/GameBuilds/Seasons/${season}/Episodes/${episodeNumber}/game.data`,
        frameworkUrl: `${process.env.PUBLIC_URL}/GameBuilds/Seasons/${season}/Episodes/${episodeNumber}/game.framework.js`,
        codeUrl: `${process.env.PUBLIC_URL}/GameBuilds/Seasons/${season}/Episodes/${episodeNumber}/game.wasm`,
      }),
    [],
  );

  if (
    episodesQueryStatus === 'loading' ||
    activationCodesQueryStatus === 'loading' ||
    loadedGameQueryStatus === 'loading' ||
    isEpisodeActivatedQueryStatus === 'loading' ||
    ownedProductsQueryStatus === 'loading'
  ) {
    return <Loading />;
  }

  if (!ownedProducts.some(el => el.id === currentEpisode.id)) {
    return <Redirect to={`/games/season/${season}`} />;
  }

  unityContext.on('Alert', str => {
    window.alert(str);
  });

  unityContext.on('SendWebCommand', (cmd, param) => {
    switch (cmd) {
      case 'ShowLoginModal': {
        const arr = param.split('|');
        setUnityGO(arr[1]);
        onLoginModalOpen();
        break;
      }
      case 'ShowInputModal': {
        const arr = param.split('|');
        setQuestion(arr[0]);
        setAnswer(arr[1]);
        setUnityGO(arr[2]);
        onInputModalOpen();
        break;
      }
      case 'activationCodeEntered': {
        if (!isEpisodeActivated && param !== '00000') {
          const codeUsed = activationCodes.filter(
            el => el.code.toLowerCase() === param.toLowerCase(),
          );

          return activateMutation.mutate({
            code: codeUsed[0].code,
            episodeId: currentEpisode.id,
            userId: user.id,
          });
        }
        break;
      }
      case 'saveData': {
        const obj = JSON.parse(param);
        return saveGameMutation.mutate({
          episodeId: currentEpisode.id,
          userId: user.id,
          progress: obj.episodeProgress[0],
        });
      }
      case 'requestResource':
        switch (param) {
          case 'knowledgeCenter':
            return window.open('/knowledge-center', '_blank');
          default:
            break;
        }
        break;
      default:
        break;
    }
  });

  unityContext.on('IsMobilePlatform', () => {
    return isMobile;
  });

  unityContext.on('TellUnityCanGetData', () => {
    const obj = JSON.stringify({
      playerName: user.fullName,
      playerId: user.id,
      avatarURL:
        user.imageUrl ||
        'https://img.pngio.com/silhouette-user-person-silhouette-65210-png-images-pngio-head-silhouette-png-900_980.png',
      episodeProgress: loadedGame.progress ? [loadedGame.progress] : [0],
      hasActivated: isEpisodeActivated,
      activationCodes: activationCodes.map(el => el.code),
    });

    unityContext.send('Hub', 'Init', obj);
  });

  unityContext.on('progress', progress => {
    setGameLoadingProgress(Math.round(progress * 100));
  });

  unityContext.setFullscreen(false);

  const handleLoginBtnSubmit = () => {
    setShowLoginInputError(false);

    if (
      activationCodes.some(
        el => el.code.toLowerCase() === loginInputValue.toLowerCase(),
      )
    ) {
      unityContext.send(unityGO, 'Validate');
      setLoginInputValue('');
      return onLoginModalClose();
    }

    return setShowLoginInputError(true);
  };

  const handleOtherInputBtnSubmit = () => {
    setShowOtherInputError(false);

    if (otherInputValue.toLowerCase() === answer.toLowerCase()) {
      unityContext.send(unityGO, 'Validate');
      setOtherInputValue('');
      return onInputModalClose();
    }

    return setShowOtherInputError(true);
  };

  return (
    <>
      <Helmet>
        <title>Game - Agency of Curious Tasks</title>
      </Helmet>
      <Modal
        isOpen={isLoginModalOpen}
        onClose={onLoginModalClose}
        isCentered
        size="lg"
        closeOnEsc={false}
        closeOnOverlayClick={false}
      >
        <ModalOverlay />
        <ModalContent p={4}>
          <ModalBody textAlign="center">
            <VStack spacing={6}>
              <Heading>Please enter activation code</Heading>
              <FormControl isInvalid={showLoginInputError}>
                <Input
                  size="lg"
                  value={loginInputValue}
                  w="50%"
                  onChange={e => {
                    if (showLoginInputError) {
                      setShowLoginInputError(false);
                    }
                    return setLoginInputValue(e.target.value);
                  }}
                />
                <FormErrorMessage>
                  {showLoginInputError && (
                    <Text>Wrong activation code entered.</Text>
                  )}
                </FormErrorMessage>
              </FormControl>
              <ButtonGroup spacing={0}>
                <Button
                  as={Link}
                  to={`/games/season/${season}`}
                  size="lg"
                  borderTopLeftRadius="full"
                  borderBottomLeftRadius="full"
                  variant="secondarySolid"
                >
                  Back
                </Button>
                <Button
                  size="lg"
                  borderTopRightRadius="full"
                  borderBottomRightRadius="full"
                  onClick={handleLoginBtnSubmit}
                >
                  Submit
                </Button>
              </ButtonGroup>
            </VStack>
          </ModalBody>
        </ModalContent>
      </Modal>
      <Modal
        isOpen={isInputModalOpen}
        onClose={onInputModalClose}
        isCentered
        size="lg"
        closeOnEsc={true}
        closeOnOverlayClick={true}
      >
        <ModalOverlay />
        <ModalContent p={4}>
          <ModalBody textAlign="center">
            <ModalCloseButton />
            <VStack spacing={6}>
              <Heading fontSize="24px">{question}</Heading>
              <FormControl isInvalid={showOtherInputError}>
                <Textarea
                  size="lg"
                  value={otherInputValue}
                  onChange={e => {
                    if (showOtherInputError) {
                      setShowOtherInputError(false);
                    }
                    return setOtherInputValue(e.target.value);
                  }}
                />
                <FormErrorMessage>
                  {showOtherInputError &&
                    'Incorrect. Double check your answer.'}
                </FormErrorMessage>
              </FormControl>
              <Button
                size="lg"
                rounded="full"
                onClick={handleOtherInputBtnSubmit}
              >
                Submit
              </Button>
            </VStack>
          </ModalBody>
        </ModalContent>
      </Modal>
      <Modal
        isOpen={isErrorModalOpen}
        onClose={onErrorModalClose}
        isCentered
        size="2xl"
        closeOnEsc={false}
        closeOnOverlayClick={false}
      >
        <ModalOverlay />
        <ModalContent py={8} px={4} backgroundColor="froly.500">
          <ModalBody textAlign="center">
            <VStack spacing={6}>
              <Heading color="white">You do not yet have this episode</Heading>
              <Button as={Link} to="/games" rounded="full">
                Back to Main Menu
              </Button>
            </VStack>
          </ModalBody>
        </ModalContent>
      </Modal>
      {gameLoadingProgress !== 100 && (
        <Flex justify="center" align="center" h={dimensions.height}>
          <Box w="50%" textAlign="center">
            <Heading>Loading Hub</Heading>
            <LoadingMessages />
            <Progress value={gameLoadingProgress} colorScheme="cerulean" />
          </Box>
        </Flex>
      )}
      <Tooltip label="Back to Main Menu" placement="right">
        <Box position="fixed" left="10px" top="10px" zIndex="1000">
          <IconButton
            as={Link}
            to={`/games/season/${season}`}
            icon={<RiHome3Fill />}
            rounded="full"
            size="sm"
          />
        </Box>
      </Tooltip>
      <Flex
        h={dimensions.height}
        justify="center"
        alignItems="center"
        flexDirection="column"
        display={gameLoadingProgress !== 100 ? 'none' : 'flex'}
        backgroundColor="black"
      >
        <Flex
          w={dimensions.width}
          alignItems="center"
          position="relative"
          flexDir="column"
        >
          {!isErrorModalOpen && (
            <Unity
              unityContext={unityContext}
              style={{
                width: dimensions.width,
              }}
              tabIndex={1}
            />
          )}
        </Flex>
      </Flex>
    </>
  );
};

export default Game;
