import React from 'react';
import { Box, Heading, Text, Container, Grid } from '@chakra-ui/react';
import { useQueryClient, useQuery } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import SelectionCard from '../../../components/KnowledgeCenter/SelectionCard';

import { fetchSeasons } from '../../../queries/Seasons';

const SeasonMenu = () => {
  const queryClient = useQueryClient();

  const { data: seasons, status: seasonsQueryStatus } = useQuery(
    'seasons',
    fetchSeasons,
  );

  if (seasonsQueryStatus === 'loading') {
    return <Loading />;
  }

  const user = queryClient.getQueryData('user');

  return (
    <>
      <Helmet>
        <title>Season Select - Agency of Curious Tasks</title>
      </Helmet>
      <Container maxW="4xl" centerContent py={[6, 6, 20]} px={6}>
        <Box maxW="sm" textAlign="center" mb={14}>
          <Heading>
            Welcome
            <br />
            {user.fullName}!
          </Heading>
          <Text>Select a season to see your available missions</Text>
        </Box>
        <Grid
          gap={6}
          templateColumns={{ base: '1fr', sm: '1fr 1fr', md: 'repeat(4, 1fr)' }}
          w="full"
        >
          {seasons.map(season => {
            if (season.active) {
              return (
                <SelectionCard
                  key={season.id}
                  name={season.title}
                  imageUrl={season.coverImageUrl}
                  pathname={`/games/season/${season.seasonNumber}`}
                />
              );
            }
          })}
        </Grid>
      </Container>
    </>
  );
};

export default SeasonMenu;
