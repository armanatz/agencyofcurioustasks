import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import { useToast } from '@chakra-ui/react';
import { loadStripe } from '@stripe/stripe-js';
import { Elements, useStripe } from '@stripe/react-stripe-js';

import Loading from '../../components/Loading';

import { useQueryParam } from '../../helpers/CustomHooks';

const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLIC_KEY);

const Page = () => {
  const queryParam = useQueryParam();
  const stripe = useStripe();
  const toast = useToast();

  useEffect(() => {
    const getData = async () => {
      const redirectDetails = queryParam.get('redirect_details');
      const clientSecret = queryParam.get('setup_intent_client_secret');

      if (clientSecret) {
        const req = await stripe.retrieveSetupIntent(clientSecret);

        console.log(req);

        // if (req.setupIntent.last_setup_error !== null) {
        //   return toast({
        //     title: 'Payment Method Declined',
        //     description: req.setupIntent.last_setup_error.message,
        //     status: 'error',
        //     duration: 5000,
        //     isClosable: true,
        //   });
        // }

        // return toast({
        //   description: 'Payment Method Added',
        //   status: 'success',
        //   duration: 5000,
        //   isClosable: true,
        // });
      }
    };

    if (stripe) {
      getData();
    }
  }, [stripe]);

  return <Loading />;
};

const StripeSetup = () => {
  return (
    <Elements stripe={stripePromise}>
      <Page />
    </Elements>
  );
};

export default StripeSetup;
