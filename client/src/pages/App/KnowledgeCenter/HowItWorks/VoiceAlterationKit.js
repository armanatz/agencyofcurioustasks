import React from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Box,
  Flex,
  Heading,
  Text,
  Container,
  Grid,
  GridItem,
  VStack,
  Image,
  HStack,
  Link as ChakraLink,
} from '@chakra-ui/react';
import { isMobile } from 'react-device-detect';
import { Helmet } from 'react-helmet';

const VoiceAlterationKit = () => {
  const { season, episode, kit } = useParams();

  let diagramLayout = (
    <Grid gap={0} templateColumns="2fr 1fr">
      <GridItem>
        <VStack spacing="80px">
          <HStack align="flex-start">
            <Box w="60%">
              <Box p={4} mb={4} backgroundColor="niagara.200" rounded="xl">
                <Text>Opening</Text>
              </Box>
              <Text fontSize="14px">
                The opening collects the sound waves. It is also from this
                opening that the distorted voice comes out.
              </Text>
            </Box>
            <Box position="relative" w="40%">
              <Box
                position="absolute"
                top="26px"
                borderTop="2px dashed"
                borderTopColor="gray.500"
                w="full"
                h="2px"
              />
            </Box>
          </HStack>
          <HStack align="flex-start">
            <Box w="60%">
              <Box p={4} mb={4} backgroundColor="niagara.200" rounded="xl">
                <Text>Tube</Text>
              </Box>
              <Text fontSize="14px">
                The tube has to be hard enough so that the sound waves can
                bounce off the inside walls
              </Text>
            </Box>
            <Box position="relative" w="40%">
              <Box
                position="absolute"
                top="26px"
                borderTop="2px dashed"
                borderTopColor="gray.500"
                w="full"
                h="2px"
              />
            </Box>
          </HStack>
          <HStack align="flex-start">
            <Box w="60%">
              <Box p={4} mb={4} backgroundColor="niagara.200" rounded="xl">
                <Text>Membrane</Text>
              </Box>
              <Text fontSize="14px">
                Our acoustic membrane is made of a balloon. When the sound waves
                hit the balloon, it vibrates the balloon thus vibrating the air
                molecules outside of the balloon, which sounds a little
                different from our voice
              </Text>
            </Box>
            <Box position="relative" w="40%">
              <Box
                position="absolute"
                top="26px"
                borderTop="2px dashed"
                borderTopColor="gray.500"
                w="full"
                h="2px"
              />
            </Box>
          </HStack>
        </VStack>
      </GridItem>
      <GridItem>
        <Flex>
          <Image
            minW="50%"
            maxH="65%"
            src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/VoiceAlt_Tube.svg"
          />
        </Flex>
      </GridItem>
    </Grid>
  );

  if (isMobile) {
    diagramLayout = (
      <Flex>
        <Box
          backgroundRepeat="no-repeat"
          backgroundPosition="center"
          backgroundImage="url(https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/VoiceAlt_Tube.svg)"
          py={6}
        >
          <VStack spacing="80px">
            <Box w="50%">
              <Box p={4} mb={4} backgroundColor="niagara.200" rounded="xl">
                <Text>Opening</Text>
              </Box>
              <Box
                backgroundColor="rgba(239, 241, 176, 0.9)"
                rounded="xl"
                p={2}
              >
                <Text fontSize="14px">
                  The opening collects the sound waves. It is also from this
                  opening that the distorted voice comes out.
                </Text>
              </Box>
            </Box>
            <Box w="50%">
              <Box p={4} mb={4} backgroundColor="niagara.200" rounded="xl">
                <Text>Tube</Text>
              </Box>
              <Box
                backgroundColor="rgba(239, 241, 176, 0.9)"
                rounded="xl"
                p={2}
              >
                <Text fontSize="14px">
                  The tube has to be hard enough so that the sound waves can
                  bounce off the inside walls
                </Text>
              </Box>
            </Box>
            <Box w="50%">
              <Box p={4} mb={4} backgroundColor="niagara.200" rounded="xl">
                <Text>Membrane</Text>
              </Box>
              <Box
                backgroundColor="rgba(239, 241, 176, 0.9)"
                rounded="xl"
                p={2}
              >
                <Text fontSize="14px">
                  Our acoustic membrane is made of a balloon. When the sound
                  waves hit the balloon, it vibrates the balloon thus vibrating
                  the air molecules outside of the balloon, which sounds a
                  little different from our voice
                </Text>
              </Box>
            </Box>
          </VStack>
        </Box>
      </Flex>
    );
  }

  return (
    <>
      <Helmet>
        <title>
          Voice Alteration Kit | Knowledge Center - Agency of Curious Tasks
        </title>
      </Helmet>
      <Container maxW="6xl" py={[6, 20]} px={6}>
        <Box mb={4}>
          <Text>
            <ChakraLink as={Link} to="/knowledge-center" color="cerulean.500">
              Home
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}`}
              color="cerulean.500"
            >
              Season {season}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}`}
              color="cerulean.500"
            >
              Episode {episode}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}/${kit}`}
              color="cerulean.500"
            >
              {kit
                .split('-')
                .map(el => el.charAt(0).toUpperCase() + el.slice(1))
                .join(' ')}
            </ChakraLink>{' '}
            / <Text as="span">How It Works</Text>
          </Text>
        </Box>
        <Box mb={14}>
          <Heading size="4xl">How it works</Heading>
        </Box>
        <Grid
          gap={6}
          templateColumns={{ base: '1fr', md: '1fr 1fr', xl: '2fr 1fr' }}
        >
          <GridItem>{diagramLayout}</GridItem>
          <GridItem>
            <Flex flexDir="column" align="center" justify="center" h="100%">
              <Box
                p={6}
                mb={2}
                backgroundColor="niagara.200"
                rounded="xl"
                maxW={{ base: '80%', xl: 'full' }}
              >
                <Image src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/VoiceAlt_Real_Vertical.jpg" />
              </Box>
              <Text>Real sample</Text>
            </Flex>
          </GridItem>
        </Grid>
      </Container>
    </>
  );
};

export default VoiceAlterationKit;
