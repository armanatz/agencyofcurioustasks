import React from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Box,
  Flex,
  Heading,
  Text,
  Container,
  Grid,
  GridItem,
  VStack,
  Image,
  Stack,
  Link as ChakraLink,
  SimpleGrid,
} from '@chakra-ui/react';
import { isMobile } from 'react-device-detect';
import { Helmet } from 'react-helmet';

const DaVinciBridge = () => {
  const { season, episode, kit } = useParams();

  if (isMobile) {
    window.alert('This page is best viewed on a computer screen.');
  }

  return (
    <>
      <Helmet>
        <title>
          Da Vinci Bridge | Knowledge Center - Agency of Curious Tasks
        </title>
      </Helmet>
      <Container maxW="6xl" py={[6, 20]} px={6}>
        <Box mb={4}>
          <Text>
            <ChakraLink as={Link} to="/knowledge-center" color="cerulean.500">
              Home
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}`}
              color="cerulean.500"
            >
              Season {season}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}`}
              color="cerulean.500"
            >
              Episode {episode}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}/${kit}`}
              color="cerulean.500"
            >
              {kit
                .split('-')
                .map(el => el.charAt(0).toUpperCase() + el.slice(1))
                .join(' ')}
            </ChakraLink>{' '}
            / <Text as="span">How It Works</Text>
          </Text>
        </Box>
        <Box mb={14}>
          <Heading size="4xl">How it works</Heading>
        </Box>
        <Grid
          gap={10}
          templateColumns={{ base: '1fr', sm: '1fr', lg: '2fr 1fr' }}
        >
          <GridItem>
            <Stack align="center" spacing={24}>
              <Box>
                <Image src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/Bridge_Diagram.svg" />
                <SimpleGrid templateColumns="repeat(3, 1fr)" gap={10} mt={12}>
                  <VStack align="flex-start">
                    <Box position="relative">
                      <Box
                        position="absolute"
                        bottom="0"
                        left={{
                          base: '39px',
                          md: '92px',
                          lg: '80px',
                          xl: '92px',
                        }}
                        borderLeft="2px dashed"
                        borderLeftColor="gray.500"
                        w="full"
                        h={{
                          base: '115px',
                          md: '205px',
                          lg: '190px',
                          xl: '205px',
                        }}
                      />
                    </Box>
                    <Box>
                      <Box
                        p={4}
                        mb={4}
                        backgroundColor="niagara.200"
                        rounded="xl"
                      >
                        <Text>Rods</Text>
                      </Box>
                      <Text fontSize="14px">
                        These rods not only give horizontal support, it also
                        acts as a support that hold the beams up.
                      </Text>
                    </Box>
                  </VStack>
                  <VStack align="flex-start">
                    <Box position="relative">
                      <Box
                        position="absolute"
                        bottom="0"
                        left={{
                          base: '39px',
                          md: '104px',
                          lg: '84px',
                          xl: '104px',
                        }}
                        borderLeft="2px dashed"
                        borderLeftColor="gray.500"
                        w="full"
                        h={{
                          base: '175px',
                          md: '325px',
                          lg: '285px',
                          xl: '325px',
                        }}
                      />
                    </Box>
                    <Box>
                      <Box
                        p={4}
                        mb={4}
                        backgroundColor="niagara.200"
                        rounded="xl"
                      >
                        <Text>Load</Text>
                      </Box>
                      <Text fontSize="14px">
                        The amount of weight the structure has to carry.
                      </Text>
                    </Box>
                  </VStack>
                  <VStack align="flex-start">
                    <Box position="relative">
                      <Box
                        position="absolute"
                        bottom="0"
                        left={{
                          base: '39px',
                          md: '74px',
                          lg: '74px',
                          xl: '74px',
                        }}
                        borderLeft="2px dashed"
                        borderLeftColor="gray.500"
                        w="full"
                        h={{
                          base: '55px',
                          md: '65px',
                          lg: '65px',
                          xl: '65px',
                        }}
                      />
                    </Box>
                    <Box>
                      <Box
                        p={4}
                        mb={4}
                        backgroundColor="niagara.200"
                        rounded="xl"
                      >
                        <Text>Span</Text>
                      </Box>
                      <Text fontSize="14px">
                        The distance between two supports.
                      </Text>
                    </Box>
                  </VStack>
                </SimpleGrid>
              </Box>
              <Image
                w={{ base: 'full', sm: '60%' }}
                src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/Bridge_Beam_Diagram.svg"
              />
              <Container maxW="250px" centerContent>
                <VStack align="flex-start">
                  <SimpleGrid>
                    <Box position="relative">
                      <Box
                        position="absolute"
                        bottom="-14px"
                        left="-30px"
                        borderLeft="2px dashed"
                        borderLeftColor="gray.500"
                        w="full"
                        h="115px"
                        transform="rotate(-30deg)"
                        display={['none', 'block']}
                      />
                    </Box>
                    <Box position="relative">
                      <Box
                        position="absolute"
                        bottom="0"
                        left="108px"
                        borderLeft="2px dashed"
                        borderLeftColor="gray.500"
                        w="full"
                        h="115px"
                      />
                    </Box>
                    <Box position="relative">
                      <Box
                        position="absolute"
                        bottom="-14px"
                        left="248px"
                        borderLeft="2px dashed"
                        borderLeftColor="gray.500"
                        w="full"
                        h="115px"
                        transform="rotate(30deg)"
                        display={['none', 'block']}
                      />
                    </Box>
                  </SimpleGrid>
                  <Box>
                    <Box
                      p={4}
                      mb={4}
                      backgroundColor="niagara.200"
                      rounded="xl"
                    >
                      <Text>Notches</Text>
                    </Box>
                    <Text fontSize="14px">
                      An indentation or incision on an edge or surface. These
                      notches are here to help hold the rods and beams in place.
                    </Text>
                  </Box>
                </VStack>
              </Container>
            </Stack>
          </GridItem>
          <GridItem>
            <Flex flexDir="column" align="center" justify="center" h="100%">
              <Box
                p={6}
                mb={2}
                backgroundColor="niagara.200"
                rounded="xl"
                maxW={{ base: 'full', xl: 'full' }}
              >
                <Image src="https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/Bridge_Real.jpg" />
              </Box>
              <Text>Real sample</Text>
            </Flex>
          </GridItem>
        </Grid>
      </Container>
    </>
  );
};

export default DaVinciBridge;
