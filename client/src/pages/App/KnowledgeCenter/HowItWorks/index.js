import React, { lazy } from 'react';
import { useParams } from 'react-router-dom';
import { Box } from '@chakra-ui/react';

const VoiceAlterationKit = lazy(() => import('./VoiceAlterationKit'));
const DaVinciBridge = lazy(() => import('./DaVinciBridge'));

const HowItWorks = () => {
  const { kit } = useParams();

  switch (kit) {
    case 'voice-alteration-kit':
      return <VoiceAlterationKit />;
    case 'da-vinci-bridge':
      return <DaVinciBridge />;
    default:
      return <Box />;
  }
};

export default HowItWorks;
