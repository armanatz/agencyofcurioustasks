import React, { useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Box,
  Flex,
  Heading,
  Text,
  Container,
  VStack,
  HStack,
  Link as ChakraLink,
  Button,
  ButtonGroup,
  Icon,
  useToast,
  Stack,
} from '@chakra-ui/react';
import { HiOutlineLightBulb } from 'react-icons/hi';
import StepWizard from 'react-step-wizard';
import { Helmet } from 'react-helmet';

import * as data from './data';

const Intro = props => {
  return (
    <VStack spacing={20} align="flex-start">
      <Box>
        <Heading size="4xl">Trivia</Heading>
        <Heading>{props.heading}</Heading>
      </Box>
      <Box maxW="3xl">
        <Text fontSize="18px" color="niagara.500">
          Well agent, you have read through all of the materials provided in the
          knowledge center.
          <br />
          <br />
          Now let{"'"}s see if you are ready to test yourself and prove you have
          what it takes to be an agent of ACT.
        </Text>
        <Button
          mt={6}
          variant="secondarySolid"
          onClick={props.nextStep}
          w="20%"
        >
          Start
        </Button>
      </Box>
    </VStack>
  );
};

const Score = props => {
  return (
    <Flex justify="center" align="center" maxH="60vh">
      <Stack flexDir={{ base: 'column-reverse', md: 'row' }} spacing={6}>
        <VStack
          spacing={6}
          align={{ base: 'center', md: 'flex-start' }}
          textAlign={{ base: 'center', md: 'left' }}
          justify="center"
        >
          <Heading>
            You have successfully finished your quiz and scored:
          </Heading>
          <ButtonGroup spacing={6}>
            <Button as={Link} to={`/knowledge-center/${props.path}`}>
              Back to Kit Selection
            </Button>
            <Button as={Link} to="/knowledge-center">
              Back to Home
            </Button>
          </ButtonGroup>
        </VStack>
        <Flex
          w="full"
          justify={{ base: 'center', md: 'flex-end' }}
          align="center"
        >
          <Flex
            rounded="full"
            w={{ base: '200px', md: '300px', lg: '400px' }}
            h={{ base: '200px', md: '300px', lg: '400px' }}
            bgColor="niagara.500"
            color="white"
            justify="center"
            align="center"
          >
            <Text fontSize={{ base: '88px', lg: '128px' }}>
              {props.points}/{props.totalSteps - 2}
            </Text>
          </Flex>
        </Flex>
      </Stack>
    </Flex>
  );
};

const Choice = ({ text, onSelect, isSelected, isCorrect, isWrong }) => {
  const unselectedChoiceOpts = {
    bgColor: 'transparent',
    color: 'niagara.500',
    _hover: { bgColor: 'niagara.500', color: 'white' },
    borderColor: 'niagara.500',
  };

  const selectedChoiceOpts = {
    bgColor: 'niagara.500',
    color: 'white',
    borderColor: 'niagara.500',
  };

  const wrongChoiceOpts = {
    bgColor: 'froly.500',
    color: 'white',
    borderColor: 'froly.500',
  };

  const showAsSelected = isSelected || isCorrect;
  const showAsUnselected = !isSelected && !isCorrect;

  return (
    <Box
      {...(showAsSelected && { ...selectedChoiceOpts })}
      {...(showAsUnselected && { ...unselectedChoiceOpts })}
      {...(isWrong && { ...wrongChoiceOpts })}
      rounded="md"
      w="60%"
      borderWidth="2px"
      borderStyle="solid"
      p={4}
      boxShadow="lg"
      cursor="pointer"
      onClick={() => {
        return onSelect(text);
      }}
    >
      <Text>{text}</Text>
    </Box>
  );
};

const Question = props => {
  const toast = useToast();
  const [selectedAnswer, setSelectedAnswer] = useState(undefined);
  const [hasCheckedAnswer, setHasCheckedAnswer] = useState(false);

  const checkAnswer = () => {
    if (!selectedAnswer) {
      return toast({
        description: 'Please select an answer first.',
        status: 'error',
      });
    }

    if (!hasCheckedAnswer) {
      setHasCheckedAnswer(true);
      return props.onCheck(selectedAnswer === props.data.answer);
    }
  };

  return (
    <VStack spacing={10} align="flex-start" w="full">
      <Heading>
        {props.data.number}. {props.data.question}
      </Heading>
      <VStack align="flex-start" w="full" spacing={8}>
        {props.data.choices.map(choice => {
          return (
            <Choice
              key={choice}
              text={choice}
              onSelect={selection => setSelectedAnswer(selection)}
              isSelected={selectedAnswer === choice}
              isCorrect={hasCheckedAnswer && choice === props.data.answer}
              isWrong={hasCheckedAnswer && choice !== props.data.answer}
            />
          );
        })}
      </VStack>
      {hasCheckedAnswer && (
        <HStack bgColor="gray.300" p={4} spacing={6}>
          <Flex
            rounded="full"
            w="46px"
            h="46px"
            bgColor="white"
            color="niagara.500"
            justify="center"
            align="center"
          >
            <Icon as={HiOutlineLightBulb} fontSize="30px" />
          </Flex>
          <Text maxW="md">
            <strong>Answer:</strong> {props.data.answer}
            <br />
            <br />
            <Box as="span" fontSize="14px">
              {props.data.answerExtra}
            </Box>
          </Text>
        </HStack>
      )}
      <ButtonGroup spacing={10}>
        {/* <Button variant="ghost" onClick={props.firstStep}>
          Start Again
        </Button> */}
        {!hasCheckedAnswer ? (
          <Button variant="secondarySolid" onClick={checkAnswer}>
            Check Answer
          </Button>
        ) : (
          <Button
            variant="secondarySolid"
            onClick={() => {
              setHasCheckedAnswer(false);
              setSelectedAnswer(undefined);
              return props.nextStep();
            }}
          >
            {props.currentStep === props.totalSteps - 1
              ? 'Finish'
              : 'Next Question'}
          </Button>
        )}
      </ButtonGroup>
    </VStack>
  );
};

const Trivia = () => {
  const [correctlyAnswered, setCorrectlyAnswered] = useState(0);
  const { season, episode, kit } = useParams();

  const seasonData = data[`season${season}`];
  const { trivias } = seasonData[`episode${episode}`];

  const trivia = trivias[kit];

  return (
    <>
      <Helmet>
        <title>
          Trivia {trivia.heading} | Knowledge Center - Agency of Curious Tasks
        </title>
      </Helmet>
      <Container maxW="6xl" py={[6, 20]} px={6}>
        <Box mb={4}>
          <Text>
            <ChakraLink as={Link} to="/knowledge-center" color="cerulean.500">
              Home
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}`}
              color="cerulean.500"
            >
              Season {season}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}`}
              color="cerulean.500"
            >
              Episode {episode}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}/${kit}`}
              color="cerulean.500"
            >
              {kit
                .split('-')
                .map(el => el.charAt(0).toUpperCase() + el.slice(1))
                .join(' ')}
            </ChakraLink>{' '}
            / <Text as="span">Trivia</Text>
          </Text>
        </Box>
        <StepWizard>
          <Intro heading={trivia.heading} />
          {trivia.qnas.map(qna => (
            <Question
              key={qna.number}
              data={qna}
              onCheck={val => setCorrectlyAnswered(correctlyAnswered + val)}
            />
          ))}
          <Score
            points={correctlyAnswered}
            onResetPoints={() => setCorrectlyAnswered(0)}
            path={`season/${season}/episode/${episode}`}
          />
        </StepWizard>
      </Container>
    </>
  );
};

export default Trivia;
