import React from 'react';
import {
  Box,
  Heading,
  Text,
  Container,
  Grid,
  Link as ChakraLink,
} from '@chakra-ui/react';
import { Link, useParams } from 'react-router-dom';
import { useQuery, useQueryClient } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import SelectionCard from '../../../components/KnowledgeCenter/SelectionCard';

import { fetchEpisodesBySeasonNumber } from '../../../queries/Episodes';
import { fetchAllEpisodesActivated } from '../../../queries/Games';

const SeasonOverview = () => {
  const { season } = useParams();
  const queryClient = useQueryClient();

  const user = queryClient.getQueryData('user');

  const { data: episodes, status: episodesQueryStatus } = useQuery(
    'episodes',
    () => fetchEpisodesBySeasonNumber(season),
  );

  const { data: episodesActivated, status: episodesActivatedQueryStatus } =
    useQuery('episodesActivated', () => fetchAllEpisodesActivated(user.id));

  if (
    episodesQueryStatus === 'loading' ||
    episodesActivatedQueryStatus === 'loading'
  ) {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>
          Episode Selection | Knowledge Center - Agency of Curious Tasks
        </title>
      </Helmet>
      <Container maxW="4xl" centerContent py={[6, 20]} px={6}>
        <Box mb={4}>
          <Text>
            <ChakraLink as={Link} to="/knowledge-center" color="cerulean.500">
              Home
            </ChakraLink>{' '}
            / <Text as="span">Season {season}</Text>
          </Text>
        </Box>
        <Box maxW="sm" textAlign="center" mb={14}>
          <Heading>Episode Selection</Heading>
          <Text>Select the episode you would like to read more info on</Text>
        </Box>
        <Grid
          gap={6}
          templateColumns={{ base: '1fr', md: 'repeat(4, 1fr)' }}
          w="full"
        >
          {episodes.map(episode => {
            if (episode.active && !episode.comingSoon) {
              return (
                <SelectionCard
                  key={episode.id}
                  name={episode.title}
                  imageUrl={
                    episode.images.filter(image => image.coverPhoto)[0]
                      ?.imageUrl
                  }
                  pathname={`/knowledge-center/season/${season}/episode/${episode.episodeNumber}`}
                  locked={
                    !episodesActivated.some(el => el.episodeId === episode.id)
                  }
                />
              );
            }
          })}
        </Grid>
      </Container>
    </>
  );
};

export default SeasonOverview;
