import React from 'react';
import { Box, Link as ChakraLink, Text } from '@chakra-ui/react';

const Link = ({ href, children, ...rest }) => {
  return (
    <ChakraLink
      href={href}
      target="_blank"
      rel="noopener noreferrer"
      color="cerulean.500"
      {...rest}
    >
      {children}
    </ChakraLink>
  );
};

export const kits = [
  {
    name: 'Voice Alteration Kit',
    stub: 'voice-alteration-kit',
    pageSubHeading:
      'Learn about the STEM features of this device used to disguise your voice.',
    coverImage:
      'https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/VoiceAlt_Real_Horizontal.jpg',
    pages: [
      {
        name: 'How It Works',
        stub: 'how-it-works',
        coverImage:
          'https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/VoiceAlt_Real_Vertical.jpg',
      },
      {
        name: 'History',
        stub: 'history',
        coverImage:
          'https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/Spy.jpg',
      },
      {
        name: 'Science',
        stub: 'science',
        coverImage:
          'https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/SoundReflection.png',
      },
    ],
  },
  {
    name: 'Da Vinci Bridge',
    stub: 'da-vinci-bridge',
    pageSubHeading:
      'Learn about the STEM features of this device used to disguise your voice.',
    coverImage:
      'https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/Bridge_Real.jpg',
    pages: [
      {
        name: 'How It Works',
        stub: 'how-it-works',
        coverImage:
          'https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/Bridge_Diagram_ItemOnTop.png',
      },
      {
        name: 'History',
        stub: 'history',
        coverImage:
          'https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/LondonBridge.jpg',
      },
      {
        name: 'Science',
        stub: 'science',
        coverImage:
          'https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/knowledge-center/season-0/episode-1/Bridge_Real.jpg',
      },
    ],
  },
];

export const histories = {
  'voice-alteration-kit': {
    heading: 'of Disguises',
    para: (
      <Box>
        <Text textAlign="justify" whiteSpace="pre-wrap">
          The{' '}
          <Link href="https://kids.wordsmyth.net/we/?ent=identity">
            identity
          </Link>{' '}
          of an agent must always remain a{' '}
          <Link href="https://kids.wordsmyth.net/we/?ent=secret">secret</Link>.
          The information and knowledge{' '}
          <Link href="https://kids.wordsmyth.net/we/?ent=spies">spies</Link> or
          agents carry are too{' '}
          <Link href="https://kids.wordsmyth.net/we/?ent=valuable">
            valuable
          </Link>
          .
          <br />
          <br />
          Being able to{' '}
          <Link href="https://kids.wordsmyth.net/we/?ent=mask">mask</Link> or
          hide your real voice makes it difficult for others to know your
          identity.
        </Text>
      </Box>
    ),
    links: [
      {
        name: 'Spies & Secret Agents from the Past',
        href: 'https://www.youtube.com/watch?v=7folzRLtGxw',
        isVideo: true,
      },
      {
        name: 'How Spies Disguise Themselves',
        href: 'https://www.youtube.com/watch?v=JASUsVY5YJ8',
        isVideo: true,
      },
      {
        name: 'Spies of the American Revolution',
        href: 'https://www.ducksters.com/history/american_revolution/spies.php',
        isVideo: false,
      },
    ],
  },
  'da-vinci-bridge': {
    heading: 'of Bridges',
    para: (
      <Box>
        <Text textAlign="justify" whiteSpace="pre-wrap">
          Most likely, the earliest bridges were fallen trees and stepping
          stones, while{' '}
          <Link href="https://www.youtube.com/watch?v=WBrkYxf4798">
            Neolithic
          </Link>{' '}
          people built{' '}
          <Link href="https://kids.wordsmyth.net/we/?ent=boardwalk">
            boardwalk
          </Link>{' '}
          bridges across
          <Link href="https://kids.wordsmyth.net/we/?ent=marshland">
            marshland
          </Link>
          . The{' '}
          <Link href="https://www.greecehighdefinition.com/blog/2019/1/30/built-by-the-ancient-greeks-this-3300-year-old-chariot-bridge-is-still-in-use-today">
            Arkadiko Bridge
          </Link>{' '}
          dating from the 13th century BC, in the
          <Link href="https://kids.kiddle.co/Peloponnese">Peloponnese</Link>, in
          southern Greece is one of the oldest arch bridges still in existence
          and use.
          <br />
          <br />
          The Da Vinci Bridge is an{' '}
          <Link href="https://kids.wordsmyth.net/we/?level=2&rid=44675">
            unconventional
          </Link>{' '}
          bridge designed nearly five hundred years ago by Leonardo Da Vinci to
          be built quickly and easily.
        </Text>
      </Box>
    ),
    links: [
      {
        name: 'Leonardo Da Vinci (Video)',
        href: 'https://www.youtube.com/watch?v=beGWwEvffwc',
        isVideo: true,
      },
      {
        name: 'More about Leonardo Da Vinci',
        href:
          'https://www.history.com/topics/renaissance/leonardo-da-vinci#:~:text=Leonardo%20da%20Vinci%20(1452%2D1519)%20was%20born%20in%20Anchiano,an%20artist%2C%20inventor%20and%20thinker.',
        isVideo: false,
      },
      {
        name: 'History of Bridges (Video)',
        href: 'https://www.youtube.com/watch?v=PhvSpmKe9wA',
        isVideo: true,
      },
      {
        name: 'More about the History of Bridges',
        href: 'https://kids.kiddle.co/Bridge#History',
        isVideo: false,
      },
    ],
  },
};

export const sciences = {
  'voice-alteration-kit': {
    heading: 'of Sound',
    para: (
      <Box>
        <Text textAlign="justify" whiteSpace="pre-wrap">
          The way sound travels and how we hear sound involves quite a bit of
          science. By understanding what is sound and how it works, we can use
          this knowledge to further{' '}
          <Link href="https://kids.wordsmyth.net/we/?level=2&rid=11797#:~:text=definition%201%3A,in%20order%20to%20prevent%20recognition.&text=noun-,definition%201%3A,a%20great%20disguise%20for%20Halloween.">
            disguise
          </Link>{' '}
          our voice.
          <br />
          <br />
          Learn about the different concepts of sound to understand how our
          device makes noise:
        </Text>
      </Box>
    ),
    links: [
      {
        name: 'The Science of Sound',
        href: 'https://www.youtube.com/watch?v=xH8mT2IQz7Y',
        isVideo: true,
      },
      {
        name: 'Vibration',
        href: 'https://kidsdiscover.com/spotlight/sound-and-vibration/',
      },
      {
        name: 'Reflection',
        href: 'https://kids.kiddle.co/Reflection',
      },
      {
        name: 'Acoustic Membrane',
        href:
          'https://en.wikipedia.org/wiki/Acoustic_membrane#:~:text=An%20acoustic%20membrane%20is%20a,drum%2C%20microphone%2C%20or%20loudspeaker.',
      },
      {
        name: 'Accent',
        href: 'https://kids.kiddle.co/Accent',
      },
    ],
  },
  'da-vinci-bridge': {
    heading: 'of Bridges',
    para: (
      <Box>
        <Text textAlign="justify" whiteSpace="pre-wrap">
          Science is very important when bridges are being built. Bridges need
          to be strong to ensure that it doesn’t break when people or vehicles
          cross it.
          <br />
          <br />
          Here are some of the science you should learn up on:
        </Text>
      </Box>
    ),
    links: [
      {
        name: 'Tension and compression on bridges',
        href: 'https://youtu.be/33ZfJw8cEFY',
        isVideo: true,
      },
      {
        name: 'Friction',
        href: 'https://www.ducksters.com/science/friction.php',
      },
      {
        name: 'Collapsible',
        href: 'https://www.vocabulary.com/dictionary/collapsible',
      },
      {
        name: 'Reinforce',
        href: 'https://kids.wordsmyth.net/we/?level=2&rid=34709',
      },
      {
        name: 'Weight',
        href: 'https://kids.kiddle.co/Weight',
      },
      {
        name: 'Support',
        href: 'https://kids.wordsmyth.net/we/?ent=support',
      },
      {
        name: 'Interlock',
        href: 'https://kids.wordsmyth.net/we/?ent=interlock',
      },
      {
        name: 'Load',
        href: 'https://kids.kiddle.co/Structural_load',
      },
    ],
  },
};

export const trivias = {
  'voice-alteration-kit': {
    heading: 'about Sound',
    qnas: [
      {
        number: 1,
        question: 'Which phrase best describes sound (something you hear)?',
        choices: [
          'An object you can hold.',
          'Vibrations you can hear.',
          'Fragrance you can smell.',
        ],
        answer: 'Vibrations you can hear.',
        answerExtra:
          'Sound is technically vibrations. These vibrations moves in waves vibrating the air molecules into our ears.',
      },
      {
        number: 2,
        question:
          'Which musical instrument uses a membrane to help create sound?',
        choices: ['Piano.', 'Guitar.', 'Drums.'],
        answer: 'Drums.',
        answerExtra:
          'A drummer hits the skin or membrane of the drum to create loud vibrations.',
      },
      {
        number: 3,
        question:
          'Which part of our body projects our voice through vibrations?',
        choices: ['Vocal cords.', 'Ear drums.', 'Tongue.'],
        answer: 'Vocal cords.',
        answerExtra:
          'The vocal cords are two muscle tissue within our larynx which creates vibrations when we speak.',
      },
      {
        number: 4,
        question:
          'You hear it speak, for it has a hard tongue. But it cannot breathe, for it has not a lung. What is it?',
        choices: ['A clock.', 'A bell.', 'A shoe.'],
        answer: 'A bell.',
      },
      {
        number: 5,
        question: 'When sound waves bounce of hard objects, it is called a...',
        choices: ['Reflection.', 'Absorption.', 'Promotion.'],
        answer: 'Reflection.',
        answerExtra:
          'Sound reflects off hard objects that doesn’t allow it to pass through.',
      },
    ],
  },
  'da-vinci-bridge': {
    heading: 'about Bridges',
    qnas: [
      {
        number: 1,
        question:
          'Which of the following is NOT an idea from Leonardo Da Vinci?',
        choices: ['Flying machine.', 'Machine gun.', 'Computer.'],
        answer: 'Computer.',
        answerExtra:
          'The very first computer was first invented in 1822 by Charles Babbage.',
      },
      {
        number: 2,
        question: 'Which structure below is a bridge?',
        choices: ['Seri Wawasan.', 'Stadthuys.', 'Khoo Kongsi.'],
        answer: 'Seri Wawasan.',
        answerExtra:
          'Seri Wawasan Bridge is located in Putrajaya, Malaysia, which spans 165 m long.',
      },
      {
        number: 3,
        question: 'The distance between the two supports is called a...',
        choices: ['Deck.', 'Tension.', 'Span.'],
        answer: 'Span.',
        answerExtra:
          'The span of a bridge is measured between 2 supports. In the case of the da Vinci bridge, it would be the horizontal distance from one end to the other end of the bridge.',
      },
      {
        number: 4,
        question: 'Which of the following is a purpose of building a bridge?',
        choices: [
          'To cross a ravine.',
          'To climb a tree.',
          'To allow planes to land.',
        ],
        answer: 'To cross a ravine.',
        answerExtra:
          'A bridge can be built to cross over a ravine, river or other large gaps.',
      },
      {
        number: 5,
        question:
          'Which branch of engineering concerned with the design and construction of public structures (buildings, bridges, roads, etc.)?',
        choices: ['Aerospace.', 'Civil.', 'Electrical.'],
        answer: 'Civil.',
        answerExtra:
          'Civil engineers are responsible for the design and construction of public structures.',
      },
    ],
  },
};

export const resources = {
  'voice-alteration-kit': {
    topic: 'Sound',
    links: [
      {
        name: 'Sound waves',
        href: 'https://kids.kiddle.co/Sound',
      },
      {
        name: 'How Our Ear Works',
        href: 'https://www.youtube.com/watch?v=RiVx5Lih_44',
      },
      {
        name: 'How to See Sound',
        href: 'https://www.youtube.com/watch?v=j9IvcwZFx9s',
      },
      {
        name: 'Pitch',
        href: 'https://www.youtube.com/watch?v=yMLTF_0PAQw',
      },
    ],
  },
  'da-vinci-bridge': {
    topic: 'Bridges',
    links: [
      {
        name: 'Types of Bridges',
        href: 'https://carrotsareorange.com/learn-about-bridges/',
      },
      {
        name: 'Bridge Facts',
        href: 'https://kids.kiddle.co/Bridge',
      },
      {
        name: '10 unique bridges in the world',
        href: 'https://www.youtube.com/watch?v=UA2nX511vXQ',
      },
      {
        name: "Leonardo Da Vinci's inventions",
        href: 'http://www.leonardo-da-vinci.net/inventions/',
      },
      {
        name: 'Compression',
        href: 'https://kids.kiddle.co/Compression',
      },
      {
        name: 'Tension',
        href: 'https://kids.kiddle.co/Tension_(mechanics)',
      },
    ],
  },
};
