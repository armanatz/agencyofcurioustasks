import React from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Box,
  Heading,
  Text,
  Container,
  Grid,
  GridItem,
  Button,
  Link as ChakraLink,
} from '@chakra-ui/react';
import { Helmet } from 'react-helmet';

import SelectionCard from '../../../components/KnowledgeCenter/SelectionCard';

import * as data from './data';

const KitHome = () => {
  const { season, episode, kit } = useParams();

  const seasonData = data[`season${season}`];
  const { kits } = seasonData[`episode${episode}`];

  const thisKit = kits.filter(el => el.stub === kit)[0];

  return (
    <>
      <Helmet>
        <title>
          {thisKit.name} | Knowledge Center - Agency of Curious Tasks
        </title>
      </Helmet>
      <Container maxW="4xl" centerContent py={[6, 20]} px={6}>
        <Box mb={4}>
          <Text>
            <ChakraLink as={Link} to="/knowledge-center" color="cerulean.500">
              Home
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}`}
              color="cerulean.500"
            >
              Season {season}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}`}
              color="cerulean.500"
            >
              Episode {episode}
            </ChakraLink>{' '}
            / <Text as="span">{thisKit.name}</Text>
          </Text>
        </Box>
        <Box maxW="sm" textAlign="center" mb={14}>
          <Heading>{thisKit.name}</Heading>
          <Text>{thisKit.pageSubHeading}</Text>
        </Box>
        <Grid
          gap={6}
          templateColumns={{ base: '1fr', md: 'repeat(3, 1fr)' }}
          w="full"
          mb={6}
        >
          {thisKit.pages.map(page => (
            <SelectionCard
              key={page.name}
              name={page.name}
              imageUrl={page.coverImage}
              pathname={`/knowledge-center/season/${season}/episode/${episode}/${thisKit.stub}/${page.stub}`}
              h={['225px', '250px']}
            />
          ))}
        </Grid>
        <Grid
          gap={6}
          templateColumns={{ base: '1fr', md: 'repeat(2, 1fr)' }}
          w="full"
        >
          <GridItem>
            <Box
              rounded="xl"
              w="full"
              h={['225px', '225px']}
              p={6}
              color="white"
              backgroundColor="niagara.500"
            >
              <Text fontSize="18px" mb={6}>
                Think you got all the knowledge you need? Take some trivia.
              </Text>
              <Button
                as={Link}
                to={`/knowledge-center/season/${season}/episode/${episode}/${thisKit.stub}/trivia`}
                w="full"
                rounded="full"
              >
                Start Trivia
              </Button>
            </Box>
          </GridItem>
          <GridItem>
            <Box
              rounded="xl"
              w="full"
              h={['225px', '225px']}
              p={6}
              color="white"
              backgroundColor="gray.500"
            >
              <Text fontSize="18px" mb={6}>
                Do you have anything or any words you do not understand?
              </Text>
              <Button
                as={Link}
                to={`/knowledge-center/season/${season}/episode/${episode}/${thisKit.stub}/resources`}
                w="full"
                rounded="full"
                variant="secondarySolid"
              >
                Get More Info
              </Button>
            </Box>
          </GridItem>
        </Grid>
      </Container>
    </>
  );
};

export default KitHome;
