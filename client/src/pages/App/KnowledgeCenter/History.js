import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Box,
  Heading,
  Text,
  Container,
  Grid,
  GridItem,
  VStack,
  Link as ChakraLink,
  Button,
} from '@chakra-ui/react';
import ReactPlayer from 'react-player';
import { Helmet } from 'react-helmet';

import * as data from './data';

const History = () => {
  const [videoUrl, setVideoUrl] = useState('');
  const { season, episode, kit } = useParams();

  const seasonData = data[`season${season}`];
  const { histories } = seasonData[`episode${episode}`];

  const history = histories[kit];

  useEffect(() => {
    if (history) {
      const link = history.links.find(link => link.isVideo);
      setVideoUrl(link.href);
    }
  }, [history]);

  return (
    <>
      <Helmet>
        <title>
          History {history.heading} | Knowledge Center - Agency of Curious Tasks
        </title>
      </Helmet>
      <Container maxW="6xl" py={[6, 20]} px={6}>
        <Box mb={4}>
          <Text>
            <ChakraLink as={Link} to="/knowledge-center" color="cerulean.500">
              Home
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}`}
              color="cerulean.500"
            >
              Season {season}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}`}
              color="cerulean.500"
            >
              Episode {episode}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}/${kit}`}
              color="cerulean.500"
            >
              {kit
                .split('-')
                .map(el => el.charAt(0).toUpperCase() + el.slice(1))
                .join(' ')}
            </ChakraLink>{' '}
            / <Text as="span">History</Text>
          </Text>
        </Box>
        <Box mb={2}>
          <Heading size="4xl">History</Heading>
        </Box>
        <Grid gap={20} templateColumns={{ base: '1fr', sm: '450px 2fr' }}>
          <GridItem>
            <VStack spacing={12}>
              <Box>
                <Heading mb={12}>{history.heading}</Heading>
                {history.para}
              </Box>
              <Grid gap={4} templateColumns="1fr" w="full">
                {history.links.map(link => {
                  if (link.isVideo) {
                    return (
                      <GridItem key={link.name}>
                        <Button
                          variant="secondarySolid"
                          w="full"
                          onClick={() => setVideoUrl(link.href)}
                        >
                          {link.name}
                        </Button>
                      </GridItem>
                    );
                  }

                  return (
                    <GridItem key={link.name}>
                      <Button
                        variant="secondarySolid"
                        as={ChakraLink}
                        href={link.href}
                        target="_blank"
                        rel="noopener noreferrer"
                        w="full"
                      >
                        {link.name}
                      </Button>
                    </GridItem>
                  );
                })}
              </Grid>
            </VStack>
          </GridItem>
          <GridItem>
            <Box position="relative" pt="56.25%">
              {videoUrl !== '' && (
                <ReactPlayer
                  style={{
                    position: 'absolute',
                    top: '0',
                    left: '0',
                  }}
                  width="100%"
                  height="100%"
                  url={videoUrl}
                />
              )}
            </Box>
          </GridItem>
        </Grid>
      </Container>
    </>
  );
};

export default History;
