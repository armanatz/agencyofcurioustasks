import React from 'react';
import { Box, Heading, Text, Container, Grid } from '@chakra-ui/react';
import { useQuery } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import SelectionCard from '../../../components/KnowledgeCenter/SelectionCard';

import { fetchSeasons } from '../../../queries/Seasons';

const KnowledgeCenter = () => {
  const { data: seasons, status: seasonsQueryStatus } = useQuery(
    'seasons',
    fetchSeasons,
  );

  if (seasonsQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>Knowledge Center - Agency of Curious Tasks</title>
      </Helmet>
      <Container maxW="4xl" centerContent py={[6, 20]} px={6}>
        <Box maxW="sm" textAlign="center" mb={14}>
          <Heading>Knowledge Center</Heading>
          <Text>
            Select a season to get more resources and learning materials
          </Text>
        </Box>
        <Grid
          gap={6}
          templateColumns={{ base: '1fr', md: 'repeat(4, 1fr)' }}
          w="full"
        >
          {seasons.map(season => {
            if (season.active) {
              return (
                <SelectionCard
                  key={season.id}
                  name={season.title}
                  imageUrl={season.coverImageUrl}
                  pathname={`/knowledge-center/season/${season.seasonNumber}`}
                />
              );
            }
          })}
        </Grid>
      </Container>
    </>
  );
};

export default KnowledgeCenter;
