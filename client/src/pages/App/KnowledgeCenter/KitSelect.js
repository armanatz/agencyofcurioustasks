import React from 'react';
import {
  Box,
  Heading,
  Text,
  Container,
  Grid,
  Link as ChakraLink,
} from '@chakra-ui/react';
import { Link, Redirect, useParams } from 'react-router-dom';
import { useQuery, useQueryClient } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import SelectionCard from '../../../components/KnowledgeCenter/SelectionCard';

import { fetchOwnedProducts } from '../../../queries/Products';

import * as data from './data';

const KitSelect = () => {
  const { season, episode } = useParams();
  const queryClient = useQueryClient();

  const user = queryClient.getQueryData('user');

  const { data: ownedProducts, status: ownedProductsQueryStatus } = useQuery(
    'ownedProducts',
    () => fetchOwnedProducts(user.id),
  );

  const seasonData = data[`season${season}`];
  const { kits } = seasonData[`episode${episode}`];

  if (ownedProductsQueryStatus === 'loading') {
    return <Loading />;
  }

  if (!ownedProducts.some(el => el.episodeNumber === episode)) {
    return <Redirect to={`/knowledge-center/season/${season}`} />;
  }

  return (
    <>
      <Helmet>
        <title>
          Kit Selection | Knowledge Center - Agency of Curious Tasks
        </title>
      </Helmet>
      <Container maxW="4xl" centerContent py={[6, 20]} px={6}>
        <Box mb={4}>
          <Text>
            <ChakraLink as={Link} to="/knowledge-center" color="cerulean.500">
              Home
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}`}
              color="cerulean.500"
            >
              Season {season}
            </ChakraLink>{' '}
            / <Text as="span">Episode {episode}</Text>
          </Text>
        </Box>
        <Box maxW="sm" textAlign="center" mb={14}>
          <Heading>Select your kit</Heading>
          <Text>Get to know the kit you{"'"}re working on</Text>
        </Box>
        <Grid
          gap={6}
          templateColumns={{ base: '1fr', md: 'repeat(2, 1fr)' }}
          w="full"
        >
          {kits.map(kit => (
            <SelectionCard
              key={kit.name}
              name={kit.name}
              imageUrl={kit.coverImage}
              pathname={`/knowledge-center/season/${season}/episode/${episode}/${kit.stub}`}
            />
          ))}
        </Grid>
      </Container>
    </>
  );
};

export default KitSelect;
