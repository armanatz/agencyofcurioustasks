import React from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Box,
  Flex,
  Heading,
  Text,
  Container,
  Grid,
  GridItem,
  Link as ChakraLink,
  Button,
} from '@chakra-ui/react';
import { Helmet } from 'react-helmet';

import * as data from './data';

const Resources = () => {
  const { season, episode, kit } = useParams();

  const seasonData = data[`season${season}`];
  const { resources } = seasonData[`episode${episode}`];

  const thisKit = resources[kit];

  return (
    <>
      <Helmet>
        <title>Resources | Knowledge Center - Agency of Curious Tasks</title>
      </Helmet>
      <Container maxW="6xl" py={[6, 20]} px={6}>
        <Box mb={4}>
          <Text>
            <ChakraLink as={Link} to="/knowledge-center" color="cerulean.500">
              Home
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}`}
              color="cerulean.500"
            >
              Season {season}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}`}
              color="cerulean.500"
            >
              Episode {episode}
            </ChakraLink>{' '}
            /{' '}
            <ChakraLink
              as={Link}
              to={`/knowledge-center/season/${season}/episode/${episode}/${kit}`}
              color="cerulean.500"
            >
              {kit
                .split('-')
                .map(el => el.charAt(0).toUpperCase() + el.slice(1))
                .join(' ')}
            </ChakraLink>{' '}
            / <Text as="span">Resources</Text>
          </Text>
        </Box>
        <Flex flexDir="column" justify="center" minH="60vh">
          <Box mb={2}>
            <Heading size="4xl">Resources</Heading>
            <Heading>for {thisKit.topic}</Heading>
          </Box>
          <Text maxW="lg" mb={6}>
            Interested more on this topic? Here are some fun and educational
            links that will help you learn more about{' '}
            {thisKit.topic.toLowerCase()}:
          </Text>
          <Grid gap={6} templateColumns="1fr 1fr">
            {thisKit.links.map(link => (
              <GridItem key={link.name}>
                <Button
                  variant="secondarySolid"
                  as={ChakraLink}
                  href={link.href}
                  target="_blank"
                  rel="noopener noreferrer"
                  w="full"
                  h="50px"
                  fontSize="18px"
                >
                  {link.name}
                </Button>
              </GridItem>
            ))}
          </Grid>
        </Flex>
      </Container>
    </>
  );
};

export default Resources;
