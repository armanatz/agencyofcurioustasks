import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {
  Flex,
  Container,
  Box,
  Divider,
  Heading,
  Text,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  useToast,
  useDisclosure,
  Icon,
} from '@chakra-ui/react';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import { AiOutlineCheck } from 'react-icons/ai';
import dayjs from 'dayjs';
import { Helmet } from 'react-helmet';

import Loading from '../../components/Loading';
import AvatarSelector from '../../components/Children/AvatarSelector';
import AddForm from '../../components/Children/AddForm';

import { fetchUsersChildren } from '../../queries/Families';

const Dashboard = () => {
  const { data: usersChildren, status: usersChildrenQueryStatus } = useQuery(
    'usersChildren',
    fetchUsersChildren,
  );

  const [step, setStep] = useState(usersChildren?.members.length === 0 ? 1 : 2);
  const [selectedAvatar, setSelectedAvatar] = useState(undefined);
  const [confirmedAvatar, setConfirmedAvatar] = useState(undefined);
  const queryClient = useQueryClient();

  const toast = useToast();
  const { isOpen, onClose, onOpen } = useDisclosure();

  useEffect(() => {
    setStep(usersChildren?.members.length === 0 ? 1 : 2);
  }, [usersChildren]);

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('usersChildren');
        setStep(step + 1);
        return toast({
          title: 'Child Account Created',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    const birthDate = dayjs(values.birthDate).format('YYYY-MM-DD');
    const data = { type: 'Child', imageUrl: confirmedAvatar, ...values };

    delete data.confirmPassword;
    data.birthDate = birthDate;

    return mutation.mutate(data);
  };

  if (usersChildrenQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <>
      <Helmet>
        <title>Parent Dashboard - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Modal isOpen={isOpen} onClose={onClose} size="4xl">
          <ModalOverlay />
          <ModalContent mx={['10px', 0]}>
            <ModalHeader fontFamily="Changa">Avatar Selection</ModalHeader>
            <ModalCloseButton onClick={onClose} />
            <ModalBody>
              <AvatarSelector onChange={val => setSelectedAvatar(val)} />
            </ModalBody>
            <ModalFooter>
              <Button
                variant="ghost"
                onClick={() => {
                  setSelectedAvatar(undefined);
                  return onClose();
                }}
              >
                Cancel
              </Button>
              <Button
                ml={3}
                onClick={() => {
                  setConfirmedAvatar(selectedAvatar);
                  setSelectedAvatar(undefined);
                  return onClose();
                }}
                isDisabled={!selectedAvatar}
              >
                Select
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Welcome to the Agency of Curious Tasks</Heading>
          </Flex>
          <Divider />
          <Box my="6">
            <Accordion allowToggle={false} index={step - 1}>
              <AccordionItem borderTopWidth="0">
                <AccordionButton pl="0">
                  <Flex textAlign="left" flexDir="row" alignItems="center">
                    <Flex
                      mr={2}
                      borderWidth="1px"
                      borderRadius="40px"
                      borderColor={step === 1 ? 'gray.200' : 'green.500'}
                      w="40px"
                      h="40px"
                      alignItems="center"
                      justifyContent="center"
                    >
                      {step === 1 ? (
                        <Text>1</Text>
                      ) : (
                        <Icon as={AiOutlineCheck} color="green.500" />
                      )}
                    </Flex>
                    <Box>
                      <Heading size="md" mt="0">
                        Create a Child Account
                      </Heading>
                    </Box>
                  </Flex>
                </AccordionButton>
                <AccordionPanel ml="18px" pl={8} pb={4} borderLeftWidth="1px">
                  <Text>
                    First, let{"'"}s get your child into our database. Please
                    fill in the form below to create a new child account that
                    your child can then use to login to the Agency of Curious
                    Tasks.
                  </Text>
                  <AddForm
                    hideCancelBtn
                    onSubmit={onSubmit}
                    onOpen={onOpen}
                    selectedAvatar={confirmedAvatar}
                  />
                </AccordionPanel>
              </AccordionItem>
              <AccordionItem
                borderTopWidth="0"
                borderBottomWidth="0 !important"
              >
                <AccordionButton pl="0">
                  <Flex textAlign="left" flexDir="row" alignItems="center">
                    <Flex
                      mr={2}
                      borderWidth="1px"
                      borderRadius="40px"
                      w="40px"
                      h="40px"
                      alignItems="center"
                      justifyContent="center"
                    >
                      <Text>2</Text>
                    </Flex>
                    <Heading size="md">Enroll child into plan</Heading>
                  </Flex>
                </AccordionButton>
                <AccordionPanel ml="18px" pl={8} pb={4}>
                  <Text>
                    Great! Now that your child has an account at the Agency of
                    Curious Tasks, it is time to enroll them for a mission.
                    Click the button below to go to the store page to explore
                    the available missions and to select one.
                  </Text>
                  <Button as={Link} to="/store" my={6}>
                    Go to Store
                  </Button>
                  <Text>
                    Purchased a box from a physical store? You will need to
                    associate the box to your child{"'"}s account first. Click
                    on the button below to do that now.
                  </Text>
                  <Button as={Link} to="/store/assign" my={6}>
                    Assign Activation Code
                  </Button>
                </AccordionPanel>
              </AccordionItem>
            </Accordion>
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default Dashboard;
