import React from 'react';
import {
  Container,
  Box,
  Flex,
  Heading,
  Divider,
  Text,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import { Helmet } from 'react-helmet';

import BasicDetailsForm from '../../../components/Account/Profile/BasicDetailsForm';
import PasswordForm from '../../../components/Account/Profile/PasswordForm';
import AddressForm from '../../../components/Account/Profile/AddressForm';

const Profile = () => {
  const queryClient = useQueryClient();
  const toast = useToast();

  const user = queryClient.getQueryData('user');

  const userMutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users/current`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('authStatus');
        return queryClient.invalidateQueries('user');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const addressMutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/addresses/${user?.id}`,
        {
          method: user?.addresses.length > 0 ? 'PATCH' : 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('authStatus');
        return queryClient.invalidateQueries('user');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = (values, type = 'shipping') => {
    const data = { ...values };

    if (data.confirmPassword) {
      delete data.confirmPassword;
    }

    const keys = Object.keys(data);

    keys.forEach(key => {
      if (
        data[key] === '' ||
        data[key] === undefined ||
        data[key] === null ||
        data[key] === user[key]
      ) {
        delete data[key];
      }
    });

    if (Object.entries(data).length !== 0) {
      if (type === 'shipping') {
        return addressMutation.mutate(data);
      }
      return userMutation.mutate(data);
    }

    return toast({
      title: 'Nothing was changed',
      status: 'warning',
      duration: 5000,
      isClosable: true,
    });
  };

  return (
    <>
      <Helmet>
        <title>Edit Your Profile - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="6xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
      >
        <Box
          bg="white"
          borderColor="gray.300"
          borderWidth={1}
          borderRadius="md"
          px={6}
          w="full"
          mb={6}
        >
          <Flex justifyContent="space-between" alignItems="center" py={6}>
            <Heading>Profile Details</Heading>
          </Flex>
          <Divider />
          <Box mt="6">
            <Heading size="md">Basic Details</Heading>
            <BasicDetailsForm
              data={user}
              onSubmit={val => onSubmit(val, 'basic')}
            />
            <Divider />
            <Heading size="md" my={6}>
              Password and Security
            </Heading>
            <Text>
              If you change your password, you will be logged out of all logged
              in devices.
            </Text>
            <PasswordForm onSubmit={val => onSubmit(val, 'password')} />
            <Divider />
            <Heading size="md" my={6}>
              Shipping Address
            </Heading>
            <AddressForm
              data={user?.addresses[0]}
              onSubmit={val => onSubmit(val, 'shipping')}
            />
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default Profile;
