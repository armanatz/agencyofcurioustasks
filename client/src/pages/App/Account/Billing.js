import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {
  Box,
  Button,
  Container,
  Divider,
  Flex,
  Heading,
  Text,
  ButtonGroup,
  Modal,
  ModalOverlay,
  ModalHeader,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalCloseButton,
  VStack,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
  useToast,
  useDisclosure,
  SimpleGrid,
  FormControl,
  FormLabel,
  Input,
  Link as ChakraLink,
  Stack,
} from '@chakra-ui/react';
import { loadStripe } from '@stripe/stripe-js';
import {
  CardNumberElement,
  Elements,
  useElements,
  useStripe,
} from '@stripe/react-stripe-js';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import Payment from '../../../components/Account/Billing/Payment';
import ShareButtons from '../../../components/Account/Billing/ShareButtons';
import ReferralCode from '../../../components/Account/Billing/ReferralCode';
import CardInput from '../../../components/CardInput';

import {
  fetchReferralCode,
  fetchReferralCount,
  fetchCurrentUsersWalletBalance,
} from '../../../queries/Users';
import {
  fetchCustomersPaymentMethods,
  fetchAllSubscriptions,
} from '../../../queries/Stripe';
import { fetchSingleGlobalSetting } from '../../../queries/Settings';

import { useQueryParam } from '../../../helpers/CustomHooks';
import SubscriptionCard from '../../../components/Account/Billing/SubscriptionCard';

const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLIC_KEY);

const BillingPage = ({ paymentMethods }) => {
  const queryParams = useQueryParam();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [cardHolderName, setCardHolderName] = useState('');
  const [paymentErrors, setPaymentErrors] = useState([]);
  const [processing, setProcessing] = useState('');
  const [customerId, setCustomerId] = useState('');
  const [giftCardCodeInput, setGiftCardCodeInput] = useState('');
  const [giftCardCodeSubmitting, setGiftCardCodeSubmitting] = useState(false);
  const [paymentLoading, setPaymentLoading] = useState(false);

  const queryClient = useQueryClient();
  const toast = useToast();
  const elements = useElements();
  const stripe = useStripe();

  const { data: subscriptions, status: subscriptionsQueryStatus } = useQuery(
    'subscriptions',
    fetchAllSubscriptions,
  );

  const { data: referralDiscount, status: referralDiscountQueryStatus } =
    useQuery('referralDiscount', () =>
      fetchSingleGlobalSetting('name', 'referralDiscount'),
    );

  const { data: referralCode, status: referralCodeQueryStatus } = useQuery(
    'referralCode',
    fetchReferralCode,
  );

  const { data: referralCount, status: referralCountQueryStatus } = useQuery(
    ['referralCount', referralCode],
    () => fetchReferralCount(referralCode),
    {
      enabled: !!referralCode,
    },
  );

  const { data: walletBalance, status: walletBalanceQueryStatus } = useQuery(
    'walletBalance',
    fetchCurrentUsersWalletBalance,
  );

  const deletePaymentMethodMutation = useMutation(
    id => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/stripe/customer/payment_methods/${id}`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries('paymentMethods');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const giftCardMutation = useMutation(
    code => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/gift_cards/redeem`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify({ code }),
        },
      );
    },
    {
      onSuccess: async res => {
        setGiftCardCodeSubmitting(false);

        if (res.status !== 200) {
          const data = await res.json();
          return toast({
            description: data.message,
            duration: 5000,
            isClosable: true,
            status: 'error',
          });
        }

        toast({
          description: 'Gift Card Redeemed Successfully',
          duration: 5000,
          isClosable: true,
          status: 'success',
        });

        setGiftCardCodeInput('');

        return queryClient.invalidateQueries('walletBalance');
      },
      onError: () => {
        setGiftCardCodeSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  useEffect(() => {
    const getData = async () => {
      const req = await fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/stripe/customer`,
        {
          method: 'POST',
          credentials: 'include',
        },
      );

      let data = { id: '' };

      if (req.ok) {
        data = await req.json();
      }

      setCustomerId(data.id);
    };

    getData();
  }, []);

  useEffect(() => {
    const getData = async () => {
      const clientSecret = queryParams.get('setup_intent_client_secret');

      if (clientSecret) {
        const req = await stripe.retrieveSetupIntent(clientSecret);

        console.log(req);

        if (req.setupIntent.last_setup_error !== null) {
          return toast({
            title: 'Payment Method Was Not Added',
            description: req.setupIntent.last_setup_error.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }

        return toast({
          description: 'Payment Method Added',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      }
    };

    if (stripe) {
      getData();
    }
  }, [stripe]);

  if (
    referralDiscountQueryStatus === 'loading' ||
    referralCodeQueryStatus === 'loading' ||
    referralCountQueryStatus === 'loading' ||
    walletBalanceQueryStatus === 'loading' ||
    subscriptionsQueryStatus === 'loading'
  ) {
    return <Loading />;
  }

  const createPaymentMethod = async ({ card, name }) => {
    const result = await stripe.createPaymentMethod({
      type: 'card',
      card,
      billing_details: {
        name,
      },
    });

    console.log(result);

    if (result.error) {
      throw new Error(result.error.message);
    }

    return result.paymentMethod.id;
  };

  const createSetupIntent = async ({ customerId, paymentMethodId }) => {
    try {
      const request = await fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/stripe/setup_intent`,
        {
          method: 'POST',
          headers: {
            'Content-type': 'application/json',
          },
          body: JSON.stringify({
            customerId,
            paymentMethodId,
            fromPage: '/account/billing',
          }),
        },
      );

      const data = await request.json();

      return data;
    } catch (err) {
      return toast({
        description: err.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  };

  const handleSubmit = async e => {
    e.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    const cardNumber = elements.getElement(CardNumberElement);

    if (paymentErrors.length !== 0) {
      return toast({
        description: paymentErrors[0].message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    if (cardHolderName === '') {
      return toast({
        description: "The card holder's name is required.",
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    setProcessing(true);

    try {
      const paymentMethodId = await createPaymentMethod({
        card: cardNumber,
        name: cardHolderName,
      });

      const setupIntent = await createSetupIntent({
        customerId: customerId,
        paymentMethodId,
      });

      if (setupIntent.error) {
        throw new Error(setupIntent.error);
      }

      if (
        setupIntent.next_action &&
        setupIntent.next_action.type === 'redirect_to_url'
      ) {
        window.location = setupIntent.next_action.redirect_to_url.url;
        return;
      }

      setProcessing(false);
      setCardHolderName('');
      onClose();
      queryClient.invalidateQueries('paymentMethods');
      return toast({
        description: 'Payment Method Added',
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
    } catch (err) {
      setProcessing(false);
      return toast({
        title: 'Something went wrong',
        description: err.message,
        duration: 5000,
        isClosable: true,
      });
    }
  };

  const handleOnDelete = id => {
    return deletePaymentMethodMutation.mutate(id);
  };

  const handlePayment = async ({ subscription, paymentMethodId }) => {
    if (!paymentMethodId) {
      return toast({
        description: 'Please select a card in order to finish payment',
        status: 'error',
        duration: 5000,
      });
    }

    setPaymentLoading(true);

    const paymentIntent = subscription.latest_invoice.payment_intent;

    const payload = await stripe.confirmCardPayment(
      paymentIntent.client_secret,
      { payment_method: paymentMethodId },
    );

    if (payload.error) {
      setPaymentLoading(false);
      return toast({
        title: 'Something went wrong',
        description: payload.error.message,
        status: 'error',
        duration: 5000,
      });
    } else if (payload.paymentIntent.status === 'succeeded') {
      setPaymentLoading(false);
      return queryClient.invalidateQueries('subscriptions');
    }
  };

  return (
    <Container
      centerContent
      maxW="6xl"
      pt={['102px', '102px']}
      pb={['20px', '60px']}
    >
      <Modal isOpen={isOpen} onClose={onClose} size="3xl">
        <ModalOverlay />
        <ModalContent mx={['10px', 0]}>
          <form onSubmit={handleSubmit}>
            <ModalCloseButton onClick={onClose} />
            <ModalHeader>Add New Payment Method</ModalHeader>
            <ModalBody>
              <CardInput
                paymentErrors={paymentErrors}
                setPaymentErrors={setPaymentErrors}
                cardHolderName={cardHolderName}
                setCardHolderName={setCardHolderName}
              />
            </ModalBody>
            <ModalFooter>
              <ButtonGroup>
                <Button variant="ghost" onClick={onClose}>
                  Cancel
                </Button>
                <Button type="submit" isLoading={processing}>
                  Save
                </Button>
              </ButtonGroup>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
      <Box
        bg="white"
        borderColor="gray.300"
        borderWidth={1}
        borderRadius="md"
        px={6}
        w="full"
      >
        <Flex justifyContent="space-between" alignItems="center" py={6}>
          <Heading>Payment Methods</Heading>
          {customerId !== '' && <Button onClick={onOpen}>Add Card</Button>}
        </Flex>
        <Divider />
        <Box my="6">
          {paymentMethods.data.length === 0 ? (
            <Box textAlign="center" mb={6}>
              {customerId !== '' ? (
                <Text>
                  No payment methods saved. Please add one using the button
                  above.
                </Text>
              ) : (
                <Text>
                  To add a payment method, you have to first add your address to
                  your user profile.{' '}
                  <ChakraLink
                    as={Link}
                    to="/account/profile"
                    color="cerulean.500"
                  >
                    Click here to do that.
                  </ChakraLink>
                </Text>
              )}
            </Box>
          ) : (
            paymentMethods.data.map((pm, i, arr) => (
              <Box
                backgroundColor="linen.500"
                rounded="md"
                border="1px solid"
                borderColor="linen.600"
                px={6}
                mt={i === arr.length - 1 ? 6 : 0}
                mb={i === arr.length - 1 ? 0 : 6}
                key={pm.id}
              >
                <Payment card={pm} onDelete={() => handleOnDelete(pm.id)} />
              </Box>
            ))
          )}
        </Box>
      </Box>
      <Box
        bg="white"
        borderColor="gray.300"
        borderWidth={1}
        borderRadius="md"
        px={6}
        w="full"
        mt={6}
      >
        <Flex justifyContent="space-between" alignItems="center" py={6}>
          <Heading>Subscriptions</Heading>
        </Flex>
        <Divider />
        <Box my="6">
          <SimpleGrid columns={4} gap={6}>
            {subscriptions.map(el => {
              if (el.subscription.status !== 'incomplete_expired') {
                return (
                  <SubscriptionCard
                    key={el.subscription.id}
                    data={el}
                    paymentMethods={paymentMethods}
                    onPayClick={handlePayment}
                    paymentLoading={paymentLoading}
                  />
                );
              }
              return false;
            })}
          </SimpleGrid>
        </Box>
      </Box>
      <Box
        bg="white"
        borderColor="gray.300"
        borderWidth={1}
        borderRadius="md"
        px={6}
        w="full"
        mt={6}
      >
        <Flex justifyContent="space-between" alignItems="center" py={6}>
          <Heading>Referrals</Heading>
        </Flex>
        <Divider />
        <SimpleGrid
          py={6}
          gap={6}
          templateColumns={['1fr', '1fr', 'repeat(3, 1fr)']}
        >
          <VStack
            align="flex-start"
            backgroundColor="linen.500"
            rounded="md"
            p={4}
          >
            <ReferralCode referralCode={referralCode} />
            <ShareButtons
              referralCode={referralCode}
              referralDiscount={referralDiscount}
            />
          </VStack>
          <Flex
            backgroundColor="linen.500"
            rounded="md"
            p={4}
            minH="213px"
            align="center"
            mt={[4, 0]}
          >
            <Stat textAlign="center">
              <StatLabel>You have made</StatLabel>
              <StatNumber>{referralCount}</StatNumber>
              <StatHelpText>
                successful referral{parseInt(referralCount) !== 1 && 's'}
              </StatHelpText>
            </Stat>
          </Flex>
          <Flex
            backgroundColor="linen.500"
            rounded="md"
            p={4}
            minH="213px"
            align="center"
            mt={[4, 0]}
          >
            <Stat textAlign="center">
              <StatLabel>You have</StatLabel>
              <StatNumber>MYR {walletBalance.balance}</StatNumber>
              <StatHelpText>
                discount for your next purchase of a single mission
              </StatHelpText>
            </Stat>
          </Flex>
        </SimpleGrid>
        <VStack spacing={6} mb={6} align="flex-start">
          <Heading size="md">
            If you have a gift card, you may redeem it below:
          </Heading>
          <Box>
            <FormControl>
              <FormLabel>Gift Card Code</FormLabel>
              <Input
                onChange={e => setGiftCardCodeInput(e.target.value)}
                value={giftCardCodeInput}
              />
            </FormControl>
            <Button
              mt={6}
              onClick={() => {
                if (giftCardCodeInput !== '') {
                  setGiftCardCodeSubmitting(true);
                  return giftCardMutation.mutate(giftCardCodeInput);
                }
              }}
              isLoading={giftCardCodeSubmitting}
            >
              Redeem
            </Button>
          </Box>
        </VStack>
      </Box>
    </Container>
  );
};

const Billing = () => {
  const { data: paymentMethods, status: paymentMethodsQueryStatus } = useQuery(
    'paymentMethods',
    fetchCustomersPaymentMethods,
  );

  if (paymentMethodsQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <Elements
      stripe={stripePromise}
      options={{
        fonts: [
          {
            cssSrc: 'https://fonts.googleapis.com/css2?family=Comfortaa',
          },
        ],
      }}
    >
      <Helmet>
        <title>Payment {'&'} Billing - Agency of Curious Tasks</title>
      </Helmet>
      <BillingPage paymentMethods={paymentMethods} />
    </Elements>
  );
};

export default Billing;
