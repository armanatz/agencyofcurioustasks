import React from 'react';
import { Link } from 'react-router-dom';
import {
  Container,
  Box,
  Text,
  Stack,
  Heading,
  Link as ChakraLink,
  HStack,
  Code,
  Button,
} from '@chakra-ui/react';
import dayjs from 'dayjs';
import { useQueryParam } from '../../../helpers/CustomHooks';
import { useQuery } from 'react-query';
import { Helmet } from 'react-helmet';

import { fetchSingleGlobalSetting } from '../../../queries/Settings';

import Loading from '../../../components/Loading';

import ConveyorBelt from '../../../assets/common/conveyor_belt.gif';

const ThankYou = () => {
  const queryParam = useQueryParam();

  const { data: orderProcessingDays, status: orderProcessingDaysQueryStatus } =
    useQuery('orderProcessingDays', () =>
      fetchSingleGlobalSetting('name', 'orderProcessingDays'),
    );

  if (orderProcessingDaysQueryStatus === 'loading') {
    return <Loading />;
  }

  const orderInfo = JSON.parse(atob(queryParam.get('orderInfo')));

  return (
    <>
      <Helmet>
        <title>Order Placed - Agency of Curious Tasks</title>
      </Helmet>
      <Container
        centerContent
        maxW="3xl"
        pt={['102px', '102px']}
        pb={['20px', '60px']}
        px={{ base: 4, sm: 4 }}
        minH="100vh"
        justifyContent="center"
      >
        <Box
          rounded="md"
          backgroundColor="white"
          border="1px solid"
          borderColor="gray.300"
          px={[6, 10]}
        >
          <Stack spacing={10} mt={10} mb={12}>
            <Heading size="2xl" color="niagara.500">
              Thank you for your purchase!
            </Heading>
            <HStack spacing={10}>
              <Box>
                <Heading fontSize={['18px', '24px']}>Order ID:</Heading>
                <Code fontSize={['14px', '20px']}>{orderInfo.code}</Code>
              </Box>
              <Box>
                <Heading fontSize={['18px', '24px']}>Order Date:</Heading>
                <Code fontSize={['14px', '20px']}>
                  {dayjs(orderInfo.date).format('DD/MM/YYYY hh:mm a')}
                </Code>
              </Box>
            </HStack>
            <Box
              backgroundImage={`url(${ConveyorBelt})`}
              backgroundPosition="center"
              backgroundSize="cover"
              w="full"
              h={['200px', '300px']}
            />
            <Text fontSize="20px">
              We are getting ready to process your order.
            </Text>
            <Text>
              Usually processing will take approximately {orderProcessingDays}{' '}
              business days. When your package has been shipped, you will
              receive an email with the tracking details.
            </Text>
            <Text>
              If you don{"'"}t receive an email within the estimated time above,
              please drop us a message at{' '}
              <ChakraLink
                href="mailto:service@agencyofcurioustasks.com"
                color="cerulean.500"
              >
                service@agencyofcurioustasks.com
              </ChakraLink>{' '}
              and we will assist you.
            </Text>
            <Button as={Link} to="/store">
              Go Back to Store
            </Button>
          </Stack>
        </Box>
      </Container>
    </>
  );
};

export default ThankYou;
