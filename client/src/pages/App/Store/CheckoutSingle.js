import React, { useState, useEffect } from 'react';
import { Redirect, useHistory, useLocation } from 'react-router-dom';
import {
  Flex,
  Container,
  Box,
  Grid,
  GridItem,
  Divider,
  Heading,
  Text,
  Button,
  useToast,
} from '@chakra-ui/react';
import { loadStripe } from '@stripe/stripe-js';
import {
  CardNumberElement,
  Elements,
  useElements,
  useStripe,
} from '@stripe/react-stripe-js';
import { useQueryClient, useQuery, useMutation } from 'react-query';
import { Helmet } from 'react-helmet';

import Loading from '../../../components/Loading';
import ShippingAddress from '../../../components/Store/Checkout/ShippingAddress';
import ChildSelection from '../../../components/Store/Checkout/ChildSelection';
import CardForm from '../../../components/StripeFormComponents/CardForm';
import PaymentMethod from '../../../components/Store/Checkout/PaymentMethod';
import Summary from '../../../components/Store/Checkout/Summary';

import { useQueryParam } from '../../../helpers/CustomHooks';

import { fetchCustomersPaymentMethods } from '../../../queries/Stripe';
import { fetchCurrentUsersWalletBalance } from '../../../queries/Users';

const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLIC_KEY);

const CheckoutForm = ({ usersChildren, episode, paymentMethods, pricing }) => {
  const [selectedChild, setSelectedChild] = useState({
    id: usersChildren.members[0].id,
    fullName: usersChildren.members[0].fullName,
  });
  const [cardHolderName, setCardHolderName] = useState('');
  const [paymentErrors, setPaymentErrors] = useState([]);
  const [processing, setProcessing] = useState('');
  const [selectedCardId, setSelectedCardId] = useState(undefined);
  const [stripeCustomerId, setStripeCustomerId] = useState(undefined);
  const [discountCode, setDiscountCode] = useState('');
  const [discountAmount, setDiscountAmount] = useState(0);
  const [discountDetails, setDiscountDetails] = useState(undefined);
  const [cardNumberElement, setCardNumberElement] = useState(undefined);
  const [isNewCard, setIsNewCard] = useState(false);

  const queryClient = useQueryClient();
  const toast = useToast();
  const elements = useElements();
  const stripe = useStripe();
  const history = useHistory();

  const createOrderMutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/orders`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async data => {
        let order = await data.json();

        order = btoa(
          JSON.stringify({
            id: order.id,
            code: order.code,
            date: order.date,
            items: order.items,
          }),
        );

        queryClient.invalidateQueries('paymentMethods');

        setProcessing(false);

        return history.push(`/store/checkout/success?orderInfo=${order}`);
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const user = queryClient.getQueryData('user');

  useEffect(() => {
    const getData = async () => {
      const req = await fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/stripe/customer`,
        {
          method: 'POST',
          credentials: 'include',
        },
      );

      let data = { id: '' };

      if (req.ok) {
        data = await req.json();
      }

      setStripeCustomerId(data.id);
    };

    getData();
  }, [user]);

  useEffect(() => {
    if (stripe) {
      setCardNumberElement(elements.getElement(CardNumberElement));
    }
  }, [stripe]);

  const createPaymentIntent = async ({
    customerId,
    priceId,
    childId,
    childName,
    discountCode,
  }) => {
    try {
      const request = await fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/stripe/payment_intent`,
        {
          method: 'post',
          headers: {
            'Content-type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify({
            customerId,
            priceId,
            childId,
            childName,
            ...(discountCode && { discountCode }),
          }),
        },
      );

      const data = await request.json();

      return data;
    } catch (err) {
      return toast({
        description: err.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  };

  const createPaymentMethod = async ({ card, name }) => {
    const result = await stripe.createPaymentMethod({
      type: 'card',
      card,
      billing_details: {
        name,
      },
    });

    if (result.error) {
      setProcessing(false);
      toast({
        description: result.error.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
      return null;
    }

    queryClient.invalidateQueries('paymentMethods');

    return result.paymentMethod.id;
  };

  const handleCheckout = async e => {
    e.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    const cardNumber = elements.getElement(CardNumberElement);

    if (!selectedChild.id) {
      return toast({
        description: 'Please select a child.',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    if (user?.addresses.length === 0) {
      return toast({
        description: 'Please fill in your shipping address.',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    if (paymentErrors.length !== 0) {
      return toast({
        description: paymentErrors[0].message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    setProcessing(true);

    if (paymentMethods.data.length > 0 && !selectedCardId && !isNewCard) {
      setProcessing(false);
      return toast({
        description:
          'Please select at least one of your saved payment methods in order to checkout.',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    let paymentMethod = selectedCardId;

    if (paymentMethods.data.length === 0 || isNewCard) {
      if (cardHolderName === '') {
        setProcessing(false);
        return toast({
          description: "The card holder's name is required.",
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }

      try {
        const paymentMethodId = await createPaymentMethod({
          card: cardNumberElement,
          name: cardHolderName,
        });

        if (paymentMethodId === null) {
          return;
        }

        paymentMethod = paymentMethodId;
      } catch (err) {
        setProcessing(false);
        return toast({
          description: err.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    }

    const clientSecret = await createPaymentIntent({
      customerId: stripeCustomerId,
      priceId: pricing[0].id,
      childId: selectedChild.id,
      childName: selectedChild.fullName,
      ...(discountCode !== '' && { discountCode }),
    });

    if (clientSecret.message) {
      setProcessing(false);
      return toast({
        description: clientSecret.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    if (clientSecret.clientSecret === 'n/a') {
      setProcessing(false);
      return createOrderMutation.mutate({
        childId: selectedChild.id,
        seasonNumber: 0,
        episodeId: episode.id,
        ...(discountCode !== '' && { discountCode }),
      });
    }

    const payload = await stripe.confirmCardPayment(clientSecret.clientSecret, {
      payment_method: paymentMethod,
    });

    if (payload.error) {
      setProcessing(false);
      return toast({
        title: 'Payment Failed',
        description: payload.error.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    return createOrderMutation.mutate({
      childId: selectedChild.id,
      seasonNumber: 0,
      episodeId: episode.id,
      ...(discountCode !== '' && { discountCode }),
    });
  };

  if (cardNumberElement) {
    cardNumberElement.on('focus', () => {
      setSelectedCardId(undefined);
      setIsNewCard(true);
    });
  }

  return (
    <Container
      centerContent
      maxW="6xl"
      pt={['102px', '102px']}
      pb={['20px', '60px']}
      px={{ base: 1, sm: 4 }}
    >
      <Grid
        gap={6}
        templateColumns={{
          base: '1fr',
          sm: '1fr',
          lg: '645px 320px',
          xl: '730px 360px',
        }}
      >
        <GridItem>
          <Box
            bg="white"
            borderColor="gray.300"
            borderWidth={1}
            borderRadius="md"
            px={6}
            w="full"
          >
            <Flex justifyContent="space-between" alignItems="center" py={6}>
              <Heading>Let{"'"}s Enroll A Child</Heading>
            </Flex>
            <Divider />
            <ChildSelection
              usersChildren={usersChildren.members}
              setChild={setSelectedChild}
            />
            <ShippingAddress user={user} />
            <form onSubmit={handleCheckout}>
              <Box my="6">
                <Heading size="md" mt="0" mb={2}>
                  Payment Information
                </Heading>
                <Text mb={6}>
                  {paymentMethods.data.length === 0
                    ? 'This payment method will be used for all subsequent orders associated with your account.'
                    : 'Please select one of your saved payment methods.'}
                </Text>
                {paymentMethods.data.length !== 0 &&
                  paymentMethods.data.map((pm, i, arr) => (
                    <PaymentMethod
                      key={pm.id}
                      data={pm}
                      mt={i === arr.length - 1 ? 6 : 0}
                      mb={i === arr.length - 1 ? 0 : 6}
                      selectedCardId={selectedCardId}
                      setSelectedCardId={setSelectedCardId}
                      setIsNewCard={setIsNewCard}
                    />
                  ))}
                {paymentMethods.data.length !== 0 && (
                  <Text mt={6}>Or use another card:</Text>
                )}
                <CardForm
                  cardHolderName={cardHolderName}
                  setCardHolderName={setCardHolderName}
                  paymentErrors={paymentErrors}
                  setPaymentErrors={setPaymentErrors}
                />
              </Box>
              <Box my={6} textAlign="center">
                <Button
                  isDisabled={!stripe || !elements}
                  type="submit"
                  w={{ base: 'full', lg: 'auto' }}
                  isLoading={processing}
                  mb={6}
                >
                  Make Payment
                </Button>
              </Box>
            </form>
          </Box>
        </GridItem>
        <GridItem>
          <Summary
            product={episode}
            pricing={pricing}
            selectedChild={selectedChild}
            discountDetails={discountDetails}
            discountAmount={discountAmount}
            setDiscountDetails={setDiscountDetails}
            setDiscountAmount={setDiscountAmount}
            setDiscountCode={setDiscountCode}
          />
        </GridItem>
      </Grid>
    </Container>
  );
};

const Checkout = () => {
  const queryParam = useQueryParam();
  const location = useLocation();

  const { data: paymentMethods, status: paymentMethodsQueryStatus } = useQuery(
    'paymentMethods',
    fetchCustomersPaymentMethods,
  );

  const { data: walletBalance, status: walletBalanceQueryStatus } = useQuery(
    'walletBalance',
    fetchCurrentUsersWalletBalance,
  );

  if (queryParam.get('episode') === null || !location.state) {
    return <Redirect to="/store" />;
  }

  if (
    paymentMethodsQueryStatus === 'loading' ||
    walletBalanceQueryStatus === 'loading'
  ) {
    return <Loading />;
  }

  return (
    <Elements
      stripe={stripePromise}
      options={{
        fonts: [
          {
            cssSrc: 'https://fonts.googleapis.com/css2?family=Comfortaa',
          },
        ],
      }}
    >
      <Helmet>
        <title>Checkout - Agency of Curious Tasks</title>
      </Helmet>
      <CheckoutForm
        usersChildren={location.state.usersChildren}
        episode={location.state.episode}
        pricing={location.state.pricing}
        paymentMethods={paymentMethods}
        walletBalance={walletBalance?.balance}
      />
    </Elements>
  );
};

export default Checkout;
