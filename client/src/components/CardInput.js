import React from 'react';
import { Box, Image, Text, Input, Grid, GridItem } from '@chakra-ui/react';
import {
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
} from '@stripe/react-stripe-js';
import { isMobileOnly } from 'react-device-detect';

import CardChip from '../assets/common/card_chip.png';

const CardInput = ({
  paymentErrors,
  setPaymentErrors,
  cardHolderName,
  setCardHolderName,
}) => {
  return (
    <Grid
      gap={6}
      templateColumns={{ base: '1fr', sm: 'repeat(2, 1fr)' }}
      py={6}
      w="full"
    >
      <GridItem>
        <Box
          h={{ base: '190px', sm: '220px' }}
          border="2px solid"
          borderColor="black"
          borderRadius="lg"
          px={6}
          py={2}
          backgroundColor="gray.700"
          position="relative"
        >
          <Box textAlign="right">
            <Text fontSize="xs" color="white">
              Credit Card Front
            </Text>
          </Box>
          <Box pt={{ base: 4, sm: 8 }} mb="12px">
            <Image src={CardChip} w={{ base: '30px', sm: '50px' }} />
          </Box>
          <Box mb={{ base: 2, sm: 0 }}>
            <CardNumberElement
              onChange={e => {
                let arr = [...paymentErrors];
                const index = arr.map(el => el.type).indexOf('cardNumber');
                if (e.error) {
                  if (index > -1) {
                    arr[index].message = e.error.message;
                  } else {
                    arr.push({
                      type: 'cardNumber',
                      message: e.error.message,
                    });
                  }
                } else {
                  arr = arr.filter(el => el.type !== 'cardNumber');
                }
                setPaymentErrors(arr);
              }}
              options={{
                style: {
                  base: {
                    color: 'white',
                    fontSize: isMobileOnly ? '18px' : '22px',
                    fontFamily: 'Consolas',
                    '::placeholder': {
                      color: '#a0aec0',
                    },
                  },
                  invalid: {
                    color: '#f87375',
                  },
                },
              }}
            />
          </Box>
          <Box float="right" mb={{ base: 2, sm: 0 }}>
            <Box w="125px">
              <Grid gap={2} templateColumns="repeat(2, 1fr)">
                <GridItem textAlign="right">
                  <Text fontSize="8px" color="gray.500">
                    Valid
                    <br />
                    Thru
                  </Text>
                </GridItem>
                <GridItem pt="3px">
                  <CardExpiryElement
                    onChange={e => {
                      let arr = [...paymentErrors];
                      const index = arr
                        .map(el => el.type)
                        .indexOf('cardExpiry');
                      if (e.error) {
                        if (index > -1) {
                          arr[index].message = e.error.message;
                        } else {
                          arr.push({
                            type: 'cardExpiry',
                            message: e.error.message,
                          });
                        }
                      } else {
                        arr = arr.filter(el => el.type !== 'cardExpiry');
                      }
                      setPaymentErrors(arr);
                    }}
                    options={{
                      style: {
                        base: {
                          color: 'white',
                          fontSize: '14px',
                          fontFamily: 'Consolas',
                          '::placeholder': {
                            color: '#a0aec0',
                          },
                        },
                        invalid: {
                          color: '#f87375',
                        },
                      },
                    }}
                  />
                </GridItem>
              </Grid>
            </Box>
          </Box>
          <Input
            variant="unstyled"
            placeholder="Card Holder's Name"
            _placeholder={{ color: 'gray.400' }}
            color="gray.100"
            fontSize="14px"
            onChange={e => setCardHolderName(e.target.value)}
            value={cardHolderName}
          />
        </Box>
      </GridItem>
      <GridItem>
        <Box
          h="220px"
          border="2px solid"
          borderColor="black"
          borderRadius="lg"
          py={2}
          backgroundColor="gray.700"
          position="relative"
        >
          <Box textAlign="right" pr={6} mb={2}>
            <Text fontSize="xs" color="white">
              Credit Card Back
            </Text>
          </Box>
          <Box w="full" h="50px" backgroundColor="#51381d" />
          <Box mt={8} ml={6}>
            <Text color="gray.500" fontSize="14px" mb={1}>
              CVC / CVV
            </Text>
            <Box
              pl={2}
              pt="3.5px"
              h="32px"
              w={{ base: '30%', sm: '20%' }}
              backgroundColor="white"
            >
              <CardCvcElement
                onChange={e => {
                  let arr = [...paymentErrors];
                  const index = arr.map(el => el.type).indexOf('cardCvc');
                  if (e.error) {
                    if (index > -1) {
                      arr[index].message = e.error.message;
                    } else {
                      arr.push({
                        type: 'cardCvc',
                        message: e.error.message,
                      });
                    }
                  } else {
                    arr = arr.filter(el => el.type !== 'cardCvc');
                  }
                  setPaymentErrors(arr);
                }}
                options={{
                  placeholder: '123',
                  style: {
                    base: {
                      color: 'black',
                      fontSize: '22px',
                      fontFamily: 'Consolas',
                      '::placeholder': {
                        color: '#a0aec0',
                      },
                    },
                    invalid: {
                      color: '#f87375',
                    },
                  },
                }}
              />
            </Box>
          </Box>
        </Box>
      </GridItem>
    </Grid>
  );
};

export default CardInput;
