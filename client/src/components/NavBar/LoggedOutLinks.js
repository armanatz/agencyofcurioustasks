import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@chakra-ui/react';
import { isMobile } from 'react-device-detect';

const LoggedOutLinks = ({ location, onClose }) => {
  return (
    <>
      <Button
        as={Link}
        to="/"
        {...(isMobile && { onClick: onClose })}
        variant={
          location.pathname.split('/')[1] !== '' ? 'navLink' : 'navLinkActive'
        }
        size="sm"
      >
        Home
      </Button>
      <Button
        as={Link}
        to="/about"
        {...(isMobile && { onClick: onClose })}
        variant={
          location.pathname.split('/')[1] !== 'about'
            ? 'navLink'
            : 'navLinkActive'
        }
        size="sm"
        mx={{ base: '0', md: '0.75em!important' }}
        my={{ base: '0.75em!important', md: '0' }}
        w={{ base: 'full', md: 'auto' }}
      >
        About
      </Button>
      <Button
        as={Link}
        to="/store"
        {...(isMobile && { onClick: onClose })}
        variant={
          location.pathname.split('/')[1] !== 'store'
            ? 'navLink'
            : 'navLinkActive'
        }
        size="sm"
        mx={{ base: '0', md: '0.75em!important' }}
        my={{ base: '0.75em!important', md: '0' }}
        w={{ base: 'full', md: 'auto' }}
      >
        Store
      </Button>
      <Button
        as={Link}
        to="/faq"
        {...(isMobile && { onClick: onClose })}
        variant={
          location.pathname.split('/')[1] !== 'faq'
            ? 'navLink'
            : 'navLinkActive'
        }
        size="sm"
        mx={{ base: '0', md: '0.75em!important' }}
        my={{ base: '0.75em!important', md: '0' }}
        w={{ base: 'full', md: 'auto' }}
      >
        FAQ
      </Button>
      <Button
        as={Link}
        to="/login"
        {...(isMobile && { onClick: onClose })}
        variant={
          location.pathname.split('/')[1] !== 'login'
            ? 'navLink'
            : 'navLinkActive'
        }
        size="sm"
        mx={{ base: '0', md: '0.75em!important' }}
        my={{ base: '0.75em!important', md: '0' }}
        w={{ base: 'full', md: 'auto' }}
      >
        Log In
      </Button>
      <Button
        as={Link}
        to="/signup"
        {...(isMobile && { onClick: onClose })}
        variant="primarySolid"
        size="sm"
      >
        Sign Up
      </Button>
    </>
  );
};

export default LoggedOutLinks;
