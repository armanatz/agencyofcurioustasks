import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
  Button,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuGroup,
  MenuDivider,
} from '@chakra-ui/react';
import {
  AiOutlineDashboard,
  AiFillHome,
  AiOutlineShopping,
  AiOutlineUser,
  AiOutlineLogout,
  AiOutlineCaretDown,
} from 'react-icons/ai';
import { MdChildCare } from 'react-icons/md';
import { isMobile } from 'react-device-detect';

const LoggedInLinks = ({ roles, location, onClose, onLogout }) => {
  const internalACTRoles = [
    'Superuser',
    'Admin',
    'Editor',
    'Support',
    'Marketer',
    'Fulfillment',
  ];

  const adminRoles = ['Superuser', 'Admin'];
  const canAccessOrders = [...adminRoles, 'Support', 'Fulfillment'];
  const canAccessProducts = [
    ...adminRoles,
    'Marketer',
    'Editor',
    'Fulfillment',
  ];
  const canAccessPartners = [...adminRoles, 'Marketer'];
  const canAccessAccounts = [...adminRoles, 'Support'];
  const canAccessSettings = [...adminRoles, 'Support'];

  return (
    <>
      {internalACTRoles.some(role => roles.includes(role)) && (
        <Menu placement="bottom" autoSelect={false}>
          <MenuButton
            as={Button}
            variant={
              location.pathname.split('/')[1] !== 'admin'
                ? 'navLink'
                : 'navLinkActive'
            }
            size="sm"
            leftIcon={<AiOutlineDashboard />}
            rightIcon={<AiOutlineCaretDown />}
          >
            Admin
          </MenuButton>
          <MenuList color="cerulean.500">
            {canAccessOrders.some(role => roles.includes(role)) && (
              <MenuItem
                {...(isMobile && { onClick: onClose })}
                as={Link}
                to="/admin/orders"
                color="cerulean.500"
                _hover={{ backgroundColor: 'goldenrod.500' }}
                _focus={{
                  backgroundColor: 'goldenrod.500',
                }}
              >
                Orders
              </MenuItem>
            )}
            {canAccessProducts.some(role => roles.includes(role)) && (
              <MenuItem
                {...(isMobile && { onClick: onClose })}
                as={Link}
                to="/admin/products"
                color="cerulean.500"
                _hover={{ backgroundColor: 'goldenrod.500' }}
                _focus={{
                  backgroundColor: 'goldenrod.500',
                }}
              >
                Products
              </MenuItem>
            )}
            {canAccessPartners.some(role => roles.includes(role)) && (
              <MenuItem
                {...(isMobile && { onClick: onClose })}
                as={Link}
                to="/admin/partners"
                color="cerulean.500"
                _hover={{ backgroundColor: 'goldenrod.500' }}
                _focus={{
                  backgroundColor: 'goldenrod.500',
                }}
              >
                Partners
              </MenuItem>
            )}
            {canAccessAccounts.some(role => roles.includes(role)) && (
              <MenuItem
                {...(isMobile && { onClick: onClose })}
                as={Link}
                to="/admin/accounts"
                color="cerulean.500"
                _hover={{ backgroundColor: 'goldenrod.500' }}
                _focus={{
                  backgroundColor: 'goldenrod.500',
                }}
              >
                Accounts
              </MenuItem>
            )}
            {canAccessSettings.some(role => roles.includes(role)) && (
              <MenuItem
                {...(isMobile && { onClick: onClose })}
                as={Link}
                to="/admin/settings"
                color="cerulean.500"
                _hover={{ backgroundColor: 'goldenrod.500' }}
                _focus={{
                  backgroundColor: 'goldenrod.500',
                }}
              >
                Settings
              </MenuItem>
            )}
          </MenuList>
        </Menu>
      )}
      {roles?.includes('Adult') && (
        <>
          <Button
            {...(isMobile && { onClick: onClose })}
            as={Link}
            to="/dashboard"
            variant={
              location.pathname.split('/')[1] !== 'dashboard'
                ? 'navLink'
                : 'navLinkActive'
            }
            leftIcon={<AiFillHome />}
            size="sm"
            mx={{ base: '0', md: '0.75em!important' }}
            my={{ base: '0.75em!important', md: '0' }}
            w={{ base: 'full', md: 'auto' }}
          >
            Dashboard
          </Button>
          <Button
            {...(isMobile && { onClick: onClose })}
            as={Link}
            to="/store"
            variant={
              location.pathname.split('/')[1] !== 'store'
                ? 'navLink'
                : 'navLinkActive'
            }
            leftIcon={<AiOutlineShopping />}
            size="sm"
            mx={{ base: '0', md: '0.75em!important' }}
            my={{ base: '0.75em!important', md: '0' }}
            w={{ base: 'full', md: 'auto' }}
          >
            Store
          </Button>
          <Button
            {...(isMobile && { onClick: onClose })}
            as={Link}
            to="/children"
            variant={
              location.pathname.split('/')[1] !== 'children'
                ? 'navLink'
                : 'navLinkActive'
            }
            leftIcon={<MdChildCare />}
            size="sm"
            mx={{ base: '0', md: '0.75em!important' }}
            my={{ base: '0.75em!important', md: '0' }}
            w={{ base: 'full', md: 'auto' }}
          >
            Children
          </Button>
        </>
      )}

      {roles?.includes('Adult') ? (
        <Menu placement="bottom" autoSelect={false}>
          <MenuButton
            as={Button}
            variant={
              location.pathname.split('/')[1] !== 'account'
                ? 'navLink'
                : 'navLinkActive'
            }
            size="sm"
            leftIcon={<AiOutlineUser />}
            rightIcon={<AiOutlineCaretDown />}
          >
            Account
          </MenuButton>
          <MenuList color="cerulean.500">
            <MenuGroup
              title="Account Management"
              fontFamily="Changa"
              fontWeight="800"
            >
              <MenuItem
                {...(isMobile && { onClick: onClose })}
                as={Link}
                to="/account/profile"
                color="cerulean.500"
                _hover={{ backgroundColor: 'goldenrod.500' }}
                _focus={{
                  backgroundColor: 'goldenrod.500',
                }}
              >
                Profile
              </MenuItem>
              <MenuItem
                {...(isMobile && { onClick: onClose })}
                as={Link}
                to="/account/billing"
                color="cerulean.500"
                _hover={{ backgroundColor: 'goldenrod.500' }}
                _focus={{
                  backgroundColor: 'goldenrod.500',
                }}
              >
                Payment and Billing
              </MenuItem>
            </MenuGroup>
            <MenuDivider />
            <MenuItem
              onClick={() => {
                if (isMobile) {
                  onClose();
                }
                return onLogout();
              }}
              color="cerulean.500"
              _hover={{ backgroundColor: 'goldenrod.500' }}
              _focus={{
                backgroundColor: 'goldenrod.500',
              }}
            >
              Logout
            </MenuItem>
          </MenuList>
        </Menu>
      ) : (
        <Button
          variant="navLink"
          leftIcon={<AiOutlineLogout />}
          size="sm"
          onClick={() => {
            if (isMobile) {
              onClose();
            }
            return onLogout();
          }}
        >
          Logout
        </Button>
      )}
    </>
  );
};

export default LoggedInLinks;
