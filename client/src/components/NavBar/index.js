import React, { Fragment } from 'react';
import { useLocation } from 'react-router-dom';
import {
  Box,
  Flex,
  VisuallyHidden,
  HStack,
  useDisclosure,
  VStack,
  Image,
  IconButton,
} from '@chakra-ui/react';
import { AiOutlineClose, AiOutlineMenu } from 'react-icons/ai';
import { useQueryClient } from 'react-query';

import LoggedInLinks from './LoggedInLinks';
import LoggedOutLinks from './LoggedOutLinks';

import Logo from '../../assets/common/ACT-Logo.png';

import { useLogout } from '../../helpers/CustomHooks';

const NavBar = ({ showLinks }) => {
  const queryClient = useQueryClient();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const location = useLocation();
  const logoutMutation = useLogout();

  const user = queryClient.getQueryData('user');
  const authStatus = queryClient.getQueryData('authStatus');

  const onLogout = async () => {
    return logoutMutation.mutate();
  };

  const roles = user ? user?.roles : [];

  const navLinkOptions = {
    roles,
    location,
    onLogout,
  };

  return (
    <>
      <Box
        as="header"
        backgroundColor="cerulean.500"
        w="100%"
        px={{ base: 2, sm: 4 }}
        py={4}
        boxShadow="md"
        position={!roles?.includes('Child') ? 'fixed' : 'relative'}
        zIndex={1000}
      >
        <Flex
          alignItems="center"
          justifyContent="space-between"
          mx="auto"
          color="white"
        >
          {showLinks && (
            <VStack
              pos="absolute"
              top={82}
              left={0}
              right={0}
              display={isOpen ? 'flex' : 'none'}
              flexDirection="column"
              p={2}
              pb={4}
              backgroundColor="cerulean.500"
              spacing={0}
              boxShadow="md"
              zIndex={1}
            >
              {authStatus?.isAuthenticated ? (
                <LoggedInLinks {...navLinkOptions} onClose={onClose} />
              ) : (
                <LoggedOutLinks location={location} onClose={onClose} />
              )}
            </VStack>
          )}
          <Flex w="full" justifyContent="space-between">
            <Box as="a" href="/" alignItems="center">
              <Image
                alt="Agency of Curious Tasks Logo"
                src={Logo}
                style={{ height: '50px' }}
              />
              <VisuallyHidden>Agency of Curious Tasks</VisuallyHidden>
            </Box>
            <IconButton
              display={{ base: 'flex', md: 'none' }}
              aria-label="Open menu"
              fontSize="20px"
              variant="ghost"
              icon={isOpen ? <AiOutlineClose /> : <AiOutlineMenu />}
              onClick={isOpen ? onClose : onOpen}
              _hover={{
                backgroundColor: 'transparent',
              }}
              _active={{
                backgroundColor: 'transparent',
              }}
            />
          </Flex>

          {showLinks && (
            <HStack spacing={0} display={{ base: 'none', md: 'inline-flex' }}>
              {authStatus?.isAuthenticated ? (
                <LoggedInLinks {...navLinkOptions} />
              ) : (
                <LoggedOutLinks location={location} />
              )}
            </HStack>
          )}
        </Flex>
      </Box>
    </>
  );
};

export default NavBar;
