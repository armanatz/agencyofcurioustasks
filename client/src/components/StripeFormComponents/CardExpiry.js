import React, { forwardRef, useRef, useState } from 'react';
import {
  Box,
  FormControl,
  FormLabel,
  Text,
  useMergeRefs,
} from '@chakra-ui/react';
import { CardExpiryElement, useElements } from '@stripe/react-stripe-js';

const CardExpiry = forwardRef((props, ref) => {
  const [hasError, setHasError] = useState(false);
  const [errorMsg, setErrorMsg] = useState(undefined);
  const container = useRef(null);
  const element = useElements().getElement(CardExpiryElement);

  element?.on('focus', () => {
    if (hasError) {
      container.current.style.backgroundColor = 'white';
      container.current.style.borderColor = '#f87375';
    } else {
      container.current.style.backgroundColor = 'white';
      container.current.style.borderColor = '#047faa';
    }
  });

  element?.on('blur', e => {
    if (hasError) {
      container.current.style.backgroundColor = '#edf2f7';
      container.current.style.borderColor = '#f87375';
    } else {
      container.current.style.backgroundColor = '#fbf6ea';
      container.current.style.borderColor = '#e2ddd3';
    }
  });

  element?.on('change', e => {
    if (e.error) {
      setHasError(true);
      setErrorMsg(e.error.message);
      container.current.style.backgroundColor = 'white';
      container.current.style.borderColor = '#f87375';
    } else {
      setHasError(false);
      setErrorMsg(undefined);
      container.current.style.borderColor = '#047faa';
    }
  });

  return (
    <>
      <FormControl id="cardExpiry">
        <FormLabel>Card Expiration Date</FormLabel>
        <Box
          {...props}
          ref={useMergeRefs(container, ref)}
          border="2px solid"
          borderColor="linen.600"
          borderRadius="md"
          h={10}
          px={4}
          pt={2}
          backgroundColor="linen.500"
          _focus={{
            backgroundColor: 'white',
            borderColor: 'cerulean.500',
          }}
          onClick={() => {
            container.current.style.backgroundColor = 'white';
            container.current.style.borderColor = '#047faa';
            element.focus();
          }}
          cursor="text"
        >
          <CardExpiryElement
            options={{
              style: {
                base: {
                  color: '#047faa',
                  backgroundColor: '#fbf6ea',
                  fontSize: '16px',
                  ':focus': {
                    backgroundColor: 'white',
                  },
                  '::placeholder': {
                    color: '#bcb9b0',
                  },
                },
                invalid: {
                  backgroundColor: '#edf2f7',
                  color: '#f87375',
                  ':focus': {
                    backgroundColor: 'white',
                  },
                },
              },
            }}
          />
        </Box>
      </FormControl>
      {hasError && (
        <Text mt={4} color="froly.500">
          {errorMsg}
        </Text>
      )}
    </>
  );
});

export default CardExpiry;
