import React from 'react';
import {
  Box,
  Heading,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionIcon,
  AccordionPanel,
} from '@chakra-ui/react';

const FAQSection = ({
  id,
  title,
  colorScheme,
  hoverBgColor,
  hoverTextColor,
  data,
  containerProps,
}) => {
  return (
    <Box
      id={id}
      py={6}
      px={10}
      backgroundColor={`${colorScheme}.200`}
      rounded="md"
      border="1px solid"
      borderColor={`${colorScheme}.300`}
      boxShadow="lg"
      {...containerProps}
    >
      <Heading mb={2} color={`${colorScheme}.900`}>
        {title}
      </Heading>
      <Accordion defaultIndex={[0]}>
        {data.map((q, i) => (
          <AccordionItem key={i} mb={2} border={0}>
            <h2>
              <AccordionButton
                backgroundColor={`${colorScheme}.500`}
                color={`${colorScheme}.900`}
                _hover={{
                  backgroundColor: `${hoverBgColor}.500`,
                  color: hoverTextColor || 'white',
                }}
              >
                <Box flex="1" textAlign="left">
                  <Heading size="md">{q.title}</Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel
              pb={4}
              backgroundColor={`${colorScheme}.400`}
              color={`${colorScheme}.900`}
              whiteSpace="pre-wrap"
            >
              {q.description}
            </AccordionPanel>
          </AccordionItem>
        ))}
      </Accordion>
    </Box>
  );
};

export default FAQSection;
