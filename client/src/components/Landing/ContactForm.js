import React, { forwardRef } from 'react';
import {
  Stack,
  FormControl,
  FormLabel,
  FormHelperText,
  FormErrorMessage,
  Input,
  Textarea,
  Button,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import PhoneInput from 'react-phone-number-input/react-hook-form-input';
import { joiResolver } from '@hookform/resolvers/joi';
import * as JoiLibrary from 'joi';

import 'react-phone-number-input/style.css';

const Joi = JoiLibrary.extend(require('joi-phone-number'));

const ContactForm = ({ onSubmit, submitting }) => {
  const { handleSubmit, errors, register, control } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        name: Joi.string().required(),
        email: Joi.string().email({ tlds: false }),
        phone: Joi.string()
          .phoneNumber({
            defaultCountry: 'MY',
            format: 'e164',
            strict: true,
          })
          .required(),
        message: Joi.string().min(25).required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack spacing={4}>
        <FormControl id="name" isInvalid={errors.name}>
          <FormLabel>Name</FormLabel>
          <Input name="name" ref={register} placeholder="e.g. John" />
          <FormErrorMessage>
            {errors.name?.type === 'string.empty' && 'Name is required.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl id="email" isInvalid={errors.email}>
          <FormLabel>Email address</FormLabel>
          <Input
            name="email"
            type="email"
            autoComplete="email"
            placeholder="e.g. john@email.com"
            ref={register}
          />
          <FormErrorMessage>
            {errors.email && 'A valid email is required.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl id="phone" isInvalid={errors.phone}>
          <FormLabel>Phone Number</FormLabel>
          <PhoneInput
            name="phone"
            control={control}
            ref={register}
            rules={{}}
            defaultCountry="MY"
            placeholder="e.g. 012-345 6789"
            useNationalFormatForDefaultCountryValue
            inputComponent={forwardRef((props, ref) => {
              return <Input name="phone1" ref={ref} {...props} />;
            })}
          />
          <FormHelperText>
            If not a Malaysian number, please put in your country code.
          </FormHelperText>
          <FormErrorMessage>
            {errors.phone?.type === 'any.required' &&
              'Phone number is required.'}
          </FormErrorMessage>
          <FormErrorMessage>
            {errors.phone?.type === 'phoneNumber.invalid' &&
              'Phone number is not valid or not in the correct format.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl id="message" isInvalid={errors.message}>
          <FormLabel>Message</FormLabel>
          <Textarea name="message" ref={register} />
          <FormErrorMessage>
            {errors.message?.type === 'string.empty' &&
              'A message is required.'}
            {errors.message?.type === 'string.min' &&
              'Message should be a minimum of 25 characters.'}
          </FormErrorMessage>
        </FormControl>
        <Button type="submit" variant="secondarySolid" isLoading={submitting}>
          Submit
        </Button>
      </Stack>
    </form>
  );
};

export default ContactForm;
