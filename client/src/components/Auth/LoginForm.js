import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
  Button,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Checkbox,
  Stack,
  Link,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

import PasswordField from '../../components/PasswordField';

const LoginForm = ({ onSubmit, submitting }) => {
  const { handleSubmit, errors, register } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        loginId: Joi.string().required(),
        password: Joi.string().required(),
        remember: Joi.boolean(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack spacing="6">
        <FormControl id="loginId" isInvalid={errors.loginId}>
          <FormLabel>Login ID</FormLabel>
          <Input
            name="loginId"
            autoComplete="email"
            ref={register}
            placeholder="E-mail or Username"
          />
          <FormErrorMessage>
            {errors.loginId && 'E-mail / Username is required.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl id="password" isInvalid={errors.password}>
          <FormLabel>Password</FormLabel>
          <PasswordField
            name="password"
            autoComplete="current-password"
            ref={register}
          />
          <FormErrorMessage>
            {errors.password && 'Password is required.'}
          </FormErrorMessage>
        </FormControl>
        <Checkbox name="remember" ref={register}>
          Remember me?
        </Checkbox>
        <Link
          as={RouterLink}
          fontWeight="semibold"
          fontSize="sm"
          to="/forgot-password"
          color="cerulean.500"
        >
          Forgot Password?
        </Link>
        <Button type="submit" isLoading={submitting}>
          Log in
        </Button>
      </Stack>
    </form>
  );
};

export default LoginForm;
