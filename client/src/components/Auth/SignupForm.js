import React, { forwardRef } from 'react';
import { Link } from 'react-router-dom';
import {
  SimpleGrid,
  Center,
  Button,
  FormControl,
  FormLabel,
  FormHelperText,
  FormErrorMessage,
  Input,
  Checkbox,
  Link as ChakraLink,
  Box,
} from '@chakra-ui/react';
import PhoneInput from 'react-phone-number-input/react-hook-form-input';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import * as JoiLibrary from 'joi';

import 'react-phone-number-input/style.css';

import PasswordField from '../../components/PasswordField';

const Joi = JoiLibrary.extend(require('joi-phone-number'));

const SignupForm = ({ onSubmit, isLoading }) => {
  const { handleSubmit, errors, register, control } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.any().allow('').optional(),
        email: Joi.string().email({ tlds: false }).required(),
        mobilePhone: Joi.string()
          .phoneNumber({
            defaultCountry: 'MY',
            format: 'e164',
            strict: true,
          })
          .required(),
        password: Joi.string().min(8).required(),
        confirmPassword: Joi.any().equal(Joi.ref('password')).required(),
        referralCode: Joi.any().allow('').optional(),
        terms: Joi.boolean().invalid(false).required(),
        privacy: Joi.boolean().invalid(false).required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <SimpleGrid columns={{ base: 1, lg: 2 }} spacing={6}>
        <FormControl id="firstName" isInvalid={errors.firstName}>
          <FormLabel>First Name</FormLabel>
          <Input name="firstName" ref={register} placeholder="e.g. John" />
          <FormErrorMessage>
            {errors.firstName?.type === 'string.empty' &&
              'First name is required.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl id="lastName" isInvalid={errors.lastName}>
          <FormLabel>Last Name</FormLabel>
          <Input name="lastName" ref={register} placeholder="e.g. Doe" />
        </FormControl>
        <FormControl id="email" isInvalid={errors.email}>
          <FormLabel>Email address</FormLabel>
          <Input
            name="email"
            type="email"
            autoComplete="email"
            placeholder="e.g. john@email.com"
            ref={register}
          />
          <FormErrorMessage>
            {errors.email && 'A valid email is required.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl id="mobilePhone" isInvalid={errors.mobilePhone}>
          <FormLabel>Phone Number</FormLabel>
          <PhoneInput
            name="mobilePhone"
            control={control}
            ref={register}
            rules={{}}
            defaultCountry="MY"
            placeholder="e.g. 012-345 6789"
            international
            withCountryCallingCode
            useNationalFormatForDefaultCountryValue
            inputComponent={forwardRef((props, ref) => {
              return <Input ref={ref} {...props} />;
            })}
          />
          <FormHelperText>
            If not a Malaysian number, please put in your country code.
          </FormHelperText>
          <FormErrorMessage>
            {errors.mobilePhone?.type === 'string.empty' &&
              'Phone number is required.'}
          </FormErrorMessage>
          <FormErrorMessage>
            {errors.mobilePhone?.type === 'phoneNumber.invalid' &&
              'Phone number is not valid or not in the correct format.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl id="password" isInvalid={errors.password}>
          <FormLabel>Password</FormLabel>
          <PasswordField name="password" ref={register} />
          <FormHelperText>Needs to be a minimum of 8 characters</FormHelperText>
          <FormErrorMessage>Password is required.</FormErrorMessage>
        </FormControl>
        <FormControl id="confirmPassword" isInvalid={errors.confirmPassword}>
          <FormLabel>Confirm New Password</FormLabel>
          <PasswordField name="confirmPassword" ref={register} />
          <FormErrorMessage>Passwords do not match.</FormErrorMessage>
        </FormControl>
        <FormControl id="referralCode">
          <FormLabel>Referral code</FormLabel>
          <Input name="referralCode" placeholder="e.g. RE123" ref={register} />
          <FormHelperText>
            If you have a referral code, please input it here
          </FormHelperText>
        </FormControl>
      </SimpleGrid>
      <Box my={6}>
        <FormControl id="terms" isInvalid={errors.terms}>
          <Checkbox name="terms" size="sm" ref={register}>
            I have read and agree to the{' '}
            <ChakraLink
              as={Link}
              to="/terms"
              target="_blank"
              rel="noopener noreferrer"
              color="cerulean.500"
            >
              Terms {'&'} Conditions
            </ChakraLink>{' '}
            of ACT
          </Checkbox>
          <FormErrorMessage>
            Agreement to our Terms {'&'} Conditions is mandatory.
          </FormErrorMessage>
        </FormControl>
        <br />
        <FormControl id="privacy" isInvalid={errors.privacy}>
          <Checkbox name="privacy" size="sm" ref={register}>
            I have read and agree to having my personal {'&'} private
            information processed as per the{' '}
            <ChakraLink
              as={Link}
              to="/privacy"
              target="_blank"
              rel="noopener noreferrer"
              color="cerulean.500"
            >
              Privacy Policy
            </ChakraLink>{' '}
            of ACT
          </Checkbox>
          <FormErrorMessage>
            Agreement to our Privacy Policy is mandatory.
          </FormErrorMessage>
        </FormControl>
      </Box>
      <Center mt={6}>
        <Button w="full" type="submit" isLoading={isLoading}>
          Sign up
        </Button>
      </Center>
    </form>
  );
};

export default SignupForm;
