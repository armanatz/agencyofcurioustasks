import React from 'react';
import {
  Stack,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Button,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

import PasswordField from '../../components/PasswordField';

const ResetPasswordForm = ({ onSubmit, submitting }) => {
  const { handleSubmit, errors, register } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        password: Joi.string().min(8).required(),
        confirmPassword: Joi.any().equal(Joi.ref('password')).required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack spacing="6">
        <FormControl id="password" isInvalid={errors.password}>
          <FormLabel>Password</FormLabel>
          <PasswordField name="password" ref={register} />
          <FormHelperText>Needs to be a minimum of 8 characters</FormHelperText>
          <FormErrorMessage>Password is required.</FormErrorMessage>
        </FormControl>
        <FormControl id="confirmPassword" isInvalid={errors.confirmPassword}>
          <FormLabel>Confirm New Password</FormLabel>
          <PasswordField name="confirmPassword" ref={register} />
          <FormErrorMessage>Passwords do not match.</FormErrorMessage>
        </FormControl>
        <Button type="submit" isLoading={submitting}>
          Reset Password
        </Button>
      </Stack>
    </form>
  );
};

export default ResetPasswordForm;
