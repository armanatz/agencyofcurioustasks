import React from 'react';
import {
  Stack,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const ForgotPasswordForm = ({ onSubmit, submitting }) => {
  const { handleSubmit, errors, register } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        email: Joi.string().email({ tlds: false }).required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack spacing="6">
        <FormControl id="email" isInvalid={errors.email}>
          <FormLabel>Email address</FormLabel>
          <Input
            name="email"
            type="email"
            autoComplete="email"
            ref={register}
          />
          <FormErrorMessage>
            {errors.email && 'A valid email is required.'}
          </FormErrorMessage>
        </FormControl>
        <Button type="submit" isLoading={submitting}>
          Send reset link
        </Button>
      </Stack>
    </form>
  );
};

export default ForgotPasswordForm;
