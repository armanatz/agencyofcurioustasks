import React, { Fragment } from 'react';
import {
  Table as ChakraTable,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Box,
  Icon,
  Flex,
  Heading,
} from '@chakra-ui/react';
import { useBlockLayout, useSortBy, useTable } from 'react-table';
import { AiFillCaretDown, AiFillCaretUp } from 'react-icons/ai';

const Table = ({ columns, data, fixedWidth = true }) => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({ columns, data }, useSortBy, fixedWidth ? useBlockLayout : '');

  return (
    <ChakraTable {...getTableProps()} variant="unstyled" fontSize="12px">
      <Thead>
        {headerGroups.map((headerGroup, i) => (
          <Tr
            key={i}
            {...headerGroup.getHeaderGroupProps()}
            backgroundColor="linen.500"
          >
            {headerGroup.headers.map((column, i) => (
              <Th
                key={i}
                {...column.getHeaderProps(column.getSortByToggleProps())}
                px="10px"
                borderRight={
                  i !== headerGroup.headers.length - 1
                    ? '1px solid #CBD5E0'
                    : ''
                }
                borderColor="gray.300"
                userSelect="none"
                textAlign={column.headerAlign ? column.headerAlign : 'center'}
              >
                <Flex
                  justifyContent={
                    column.headerAlign ? column.headerAlign : 'center'
                  }
                  alignItems="center"
                  h="100%"
                >
                  <>
                    <Heading fontSize={12}>{column.render('Header')}</Heading>
                    {column.isSorted ? (
                      <Box pl={1}>
                        {column.isSortedDesc ? (
                          <Icon
                            as={AiFillCaretDown}
                            aria-label="sorted descending"
                          />
                        ) : (
                          <Icon
                            as={AiFillCaretUp}
                            aria-label="sorted ascending"
                          />
                        )}
                      </Box>
                    ) : null}
                  </>
                </Flex>
              </Th>
            ))}
          </Tr>
        ))}
      </Thead>
      <Tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <Tr key={i} {...row.getRowProps()}>
              {row.cells.map((cell, i) => (
                <Td
                  key={i}
                  {...cell.getCellProps()}
                  borderRight={
                    i !== row.cells.length - 1 ? '1px solid #CBD5E0' : ''
                  }
                  textAlign={
                    cell.column.cellAlign ? cell.column.cellAlign : 'left'
                  }
                  p="10px"
                >
                  {cell.column.customRender
                    ? cell.render(cell.column.customRender)
                    : cell.render('Cell')}
                </Td>
              ))}
            </Tr>
          );
        })}
      </Tbody>
    </ChakraTable>
  );
};

export default Table;
