import React, { useState, useEffect, useCallback, useRef } from 'react';
import { Box, Fade, Text } from '@chakra-ui/react';

const LoadingMessages = () => {
  const intervalId = useRef();

  const [messages, setMessages] = useState([
    'Rendering game world...',
    'Adjusting flux capacitor...',
    'Reticulating splines...',
    'Putting mushrooms in place...',
    'Placing exclamation marks above enemy heads...',
    'Booting gladOS...',
    'Getting gravity gun ready...',
    'Reconfoobling energymotron...',
    'Embiggening prototypes...',
    "Making sure all the i's have dots...",
    'Spinning the hamster...',
    'There is no spoon. We are not done loading it...',
    'Searching for plot device...',
    'Entangling superstrings...',
    'Winter is coming...',
    'Ordering 1s and 0s...',
    'Consulting the manual...',
    'Searching for Llamas...',
    'Removing Master Sword from Stone...',
    'Adding icing to the cake. The cake is not a lie...',
    'Sending data packets via carrier pidgeon...',
    'Finishing placements of gigapixels...',
    'Finding the droids we are looking for...',
    'Loving you 3000...',
    "I'll be back...",
    'Just keep swimming...',
    'Twiddling thumbs...',
    'Turning it on and off again...',
    'Gathering more dilithium crystals...',
    'Choosing starter Pokemon...',
  ]);

  const [index, setIndex] = useState(0);

  const randomLoadingMessage = useCallback(messages => {
    const messagesCopy = [...messages];
    messagesCopy.splice(index, 1);

    const messageIndex = Math.floor(Math.random() * messagesCopy.length);

    setMessages(messagesCopy);
    return setIndex(messageIndex);
  });

  useEffect(() => {
    intervalId.current = setInterval(
      () => randomLoadingMessage(messages),
      2000,
    );
    return () => clearInterval(intervalId.current);
  }, [messages]);

  return (
    <Box my={4}>
      {messages.map((el, i) => (
        <Fade key={i} in={index === i}>
          <Text display={index === i ? 'block' : 'none'}>{el}</Text>
        </Fade>
      ))}
    </Box>
  );
};

export default LoadingMessages;
