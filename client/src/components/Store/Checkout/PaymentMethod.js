import React from 'react';
import { Box, Grid, GridItem, Image, Text } from '@chakra-ui/react';

import MasterCardLogo from '../../../assets/common/MasterCard.svg';
import VisaLogo from '../../../assets/common/Visa.svg';

const PaymentMethod = ({
  data,
  selectedCardId,
  setSelectedCardId,
  setIsNewCard,
  ...rest
}) => {
  const handleOnClick = () => {
    if (selectedCardId === data.id) {
      if (setIsNewCard) {
        setIsNewCard(true);
      }
      return setSelectedCardId(undefined);
    }

    if (setIsNewCard) {
      setIsNewCard(false);
    }
    return setSelectedCardId(data.id);
  };

  return (
    <Box
      backgroundColor="linen.500"
      rounded="md"
      border="1px solid"
      borderColor="linen.600"
      px={6}
      onClick={handleOnClick}
      _hover={{
        cursor: 'pointer',
      }}
      {...rest}
    >
      <Grid
        gap={6}
        templateColumns="50px auto 50px"
        templateRows="auto"
        py={4}
        alignItems="center"
      >
        <GridItem colSpan={1} textAlign={{ base: 'center', sm: 'left' }}>
          {data.brand === 'visa' ? (
            <Image src={VisaLogo} boxSizing="50px" />
          ) : (
            <Image src={MasterCardLogo} w="50px" />
          )}
        </GridItem>
        <GridItem textAlign={{ base: 'center', sm: 'left' }}>
          <Text>**** {data.last4}</Text>
          <Text>
            {data.expMonth} / {data.expYear}
          </Text>
        </GridItem>
        <GridItem>
          <Box
            w="16px"
            h="16px"
            rounded="full"
            border="1px solid"
            borderColor={
              selectedCardId === data.id ? 'niagara.500' : 'gray.400'
            }
            position="relative"
          >
            <Box
              w="8px"
              h="8px"
              rounded="full"
              position="absolute"
              top="2.8px"
              left="2.8px"
              backgroundColor={
                selectedCardId === data.id ? 'niagara.500' : 'transparent'
              }
            />
          </Box>
        </GridItem>
      </Grid>
    </Box>
  );
};

export default PaymentMethod;
