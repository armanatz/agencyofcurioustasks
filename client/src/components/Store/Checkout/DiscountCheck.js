import React from 'react';
import {
  Box,
  VStack,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
  useToast,
} from '@chakra-ui/react';
import { useMutation } from 'react-query';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const DiscountCheck = ({
  product,
  pricing,
  setDiscountDetails,
  setDiscountAmount,
  setDiscountCode,
}) => {
  const toast = useToast();

  const { handleSubmit, errors, register } = useForm({
    resolver: joiResolver(
      Joi.object({
        code: Joi.string().required(),
      }),
    ),
  });

  const mutation = useMutation(
    code => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/stripe/discount/check`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify({
            code,
            productId: product.stripeProductId,
            unitAmount: pricing[0].unitAmount,
          }),
        },
      );
    },
    {
      onSuccess: async req => {
        const res = await req.json();

        if (req.ok) {
          setDiscountDetails(res.coupon);

          if (res.coupon.amount_off !== null) {
            const amount = (
              Math.round(
                parseFloat(((res.coupon.amount_off / 100) * 100).toFixed(11)),
              ) / 100
            ).toFixed(2);

            setDiscountAmount(parseFloat(amount, 10));
            return setDiscountCode(res.code);
          }

          const amount = (
            Math.round(
              parseFloat(
                (
                  (pricing[0].unitAmount / 100) *
                  (res.coupon.percent_off / 100) *
                  100
                ).toFixed(11),
              ),
            ) / 100
          ).toFixed(2);

          setDiscountAmount(parseFloat(amount, 10));
          return setDiscountCode(res.code);
        }

        return toast({
          description: res.message,
          duration: 5000,
          isClosable: true,
          status: 'error',
        });
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = async ({ code }) => {
    return mutation.mutate(code);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Box mt={6}>
        <VStack spacing={6}>
          <FormControl id="code" isInvalid={errors.code}>
            <FormLabel>Discount Code</FormLabel>
            <Input name="code" ref={register} />
            <FormErrorMessage>
              {errors.code?.type === 'string.empty' &&
                'Discount code cannot be empty.'}
            </FormErrorMessage>
          </FormControl>
          <Button type="submit" w="full">
            Apply Discount
          </Button>
        </VStack>
      </Box>
    </form>
  );
};

export default DiscountCheck;
