import React, { Fragment } from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalHeader,
  ModalBody,
  Heading,
  Box,
  Text,
  Button,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';
import { useMutation, useQueryClient } from 'react-query';
import { AiFillEdit } from 'react-icons/ai';

import ShippingAddressForm from '../../Account/Profile/AddressForm';

const ShippingAddress = ({ user }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const queryClient = useQueryClient();
  const toast = useToast();

  const shippingAddressMutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/addresses/${user?.id}`,
        {
          method: user?.addresses.length > 0 ? 'PATCH' : 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('user');
        toast({
          title: 'Shipping Address Updated',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
        return onClose();
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onShippingAddressSubmit = values => {
    const data = { ...values };

    const keys = Object.keys(data);

    if (user.addresses.length > 0) {
      keys.forEach(key => {
        if (
          data[key] === '' ||
          data[key] === undefined ||
          data[key] === null ||
          data[key] === user.addresses[0][key]
        ) {
          delete data[key];
        }
      });
    }

    if (Object.entries(data).length !== 0) {
      return shippingAddressMutation.mutate(data);
    }

    return toast({
      title: 'Nothing was changed',
      status: 'warning',
      duration: 5000,
      isClosable: true,
    });
  };

  return (
    <Box my={6}>
      <Heading size="md" mt="0" mb={2}>
        Shipping Details
      </Heading>
      <Text>
        This address will be used for all subsequent orders associated with your
        account.
      </Text>
      <Modal isOpen={isOpen} onClose={onClose} size="4xl">
        <ModalOverlay />
        <ModalContent mx={['10px', 0]}>
          <ModalCloseButton onClick={onClose} />
          <ModalHeader>
            <Heading size="lg">Shipping Address</Heading>
          </ModalHeader>
          <ModalBody>
            <ShippingAddressForm
              data={user?.addresses.length !== 0 ? user?.addresses[0] : null}
              onSubmit={onShippingAddressSubmit}
              onCancel={onClose}
              isCancelable
              inModal
            />
          </ModalBody>
        </ModalContent>
      </Modal>
      <Box
        bg="linen.300"
        border="1px solid"
        borderColor="linen.700"
        borderRadius="md"
        my={6}
        p={4}
        w={{ base: 'full', sm: '55%', lg: '55%' }}
        fontSize={['xs', 'sm']}
      >
        <Heading as="h3" size="md" mb={2}>
          Primary Shipping Address
        </Heading>
        {user?.addresses.length === 0 ? (
          <Text>You have no shipping address defined. Please add one now.</Text>
        ) : (
          <>
            <Text>{user?.addresses[0].line1}</Text>
            <Text>{user?.addresses[0]?.line2}</Text>
            <Text>
              {user?.addresses[0].postCode}, {user?.addresses[0].state}
            </Text>
            <Text>{user?.addresses[0].country}</Text>
          </>
        )}
        <Button
          leftIcon={<AiFillEdit />}
          variant="secondarySolid"
          size="sm"
          mt={2}
          onClick={onOpen}
        >
          {user?.addresses.length === 0 ? 'Add' : 'Edit'} Address
        </Button>
      </Box>
    </Box>
  );
};

export default ShippingAddress;
