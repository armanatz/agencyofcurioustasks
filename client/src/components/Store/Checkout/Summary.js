import React from 'react';
import { Box, Flex, Heading, Text, Divider } from '@chakra-ui/react';
import { useQueryClient } from 'react-query';

import DiscountCheck from './DiscountCheck';

const Summary = ({
  product,
  pricing,
  selectedChild,
  discountDetails,
  discountAmount,
  setDiscountDetails,
  setDiscountAmount,
  setDiscountCode,
  isSeason,
}) => {
  // console.log({ discountAmount, discountDetails, product });
  const queryClient = useQueryClient();

  let walletBalance = queryClient.getQueryData('walletBalance');
  walletBalance = parseFloat(walletBalance.balance, 10);

  const basePrice = parseInt(pricing[0].unitAmount, 10) / 100;

  const to2DecimalPlaces = value => {
    return (Math.round(parseFloat((value * 100).toFixed(11))) / 100).toFixed(2);
  };

  let total = basePrice;

  if (!isSeason) {
    if (walletBalance >= 0 && walletBalance < basePrice) {
      total -= walletBalance;
    } else {
      total -= basePrice;
    }
  }

  total -= discountAmount;

  if (total < 0) {
    total = 0;
  }

  total = to2DecimalPlaces(total);

  const calculateTotalForSeason = ({
    basePrice,
    totalMonths = 1,
    discountAmount = 0,
    discountDetails,
  }) => {
    if (discountDetails) {
      let total = basePrice - discountAmount;

      if (discountDetails.duration === 'forever') {
        total *= totalMonths;
      } else if (discountDetails.duration === 'repeating') {
        total *= discountDetails.duration_in_months;
        total += basePrice * (totalMonths - discountDetails.duration_in_months);
      } else {
        total += basePrice * (totalMonths - 1);
      }

      return to2DecimalPlaces(total);
    }

    return to2DecimalPlaces(basePrice * totalMonths);
  };

  return (
    <Box
      position={{ base: 'relative', sm: 'relative', lg: 'fixed' }}
      bg="white"
      borderColor="gray.300"
      borderWidth={1}
      borderRadius="md"
      p={6}
      w={{ base: 'full', sm: 'full', lg: '320px', xl: '360px' }}
    >
      <Heading as="h2" fontSize="24px">
        Summary
      </Heading>
      <Box my={6}>
        <Flex
          justifyContent="space-between"
          alignItems="center"
          fontSize="12px"
        >
          <Text>{product.title}</Text>
          <Text>
            {pricing[0].currency.toUpperCase()} {to2DecimalPlaces(basePrice)}
            {isSeason && '/mo'}
          </Text>
        </Flex>
        <Text fontSize="12px" fontStyle="italic" mt={2}>
          {isSeason
            ? `${product.totalPlayableEpisodes} Monthly Payments`
            : '- '}{' '}
          for {selectedChild.fullName}
        </Text>
        {!isSeason && (
          <Flex
            justifyContent="space-between"
            alignItems="center"
            mt={6}
            fontSize="12px"
          >
            <Text>Discount from referrals</Text>
            <Text>
              - {pricing[0].currency.toUpperCase()}{' '}
              {walletBalance > basePrice
                ? to2DecimalPlaces(basePrice)
                : to2DecimalPlaces(walletBalance)}
            </Text>
          </Flex>
        )}
        {discountDetails && (
          <Box>
            <Flex
              justifyContent="space-between"
              alignItems="center"
              mt={6}
              fontSize="12px"
            >
              <Text>{discountDetails.name}</Text>
              <Text>
                - {pricing[0].currency.toUpperCase()}{' '}
                {to2DecimalPlaces(discountAmount)}
              </Text>
            </Flex>
          </Box>
        )}
        <Flex justifyContent="space-between" alignItems="center" mt={6}>
          <Text>{isSeason ? 'Episode 1' : 'Total'}</Text>
          <Text fontWeight="bold" textDecoration="underline">
            {pricing[0].currency.toUpperCase()} {total}
          </Text>
        </Flex>
        {isSeason && (
          <Flex
            justifyContent="space-between"
            alignItems="center"
            mt={6}
            fontSize="12px"
          >
            <Text>
              Total for {product.totalPlayableEpisodes} episodes{' '}
              {discountDetails && '(with discount)'}
            </Text>
            <Text>
              {pricing[0].currency.toUpperCase()}{' '}
              {calculateTotalForSeason({
                basePrice,
                totalMonths: product.totalPlayableEpisodes,
                discountAmount,
                discountDetails,
              })}
            </Text>
          </Flex>
        )}
      </Box>
      <Divider />
      <DiscountCheck
        product={product}
        pricing={pricing}
        setDiscountDetails={setDiscountDetails}
        setDiscountAmount={setDiscountAmount}
        setDiscountCode={setDiscountCode}
      />
    </Box>
  );
};

export default Summary;
