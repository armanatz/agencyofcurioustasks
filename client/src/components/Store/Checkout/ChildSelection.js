import React from 'react';
import { Box, Heading, Text, Select } from '@chakra-ui/react';

const ChildSelection = ({ usersChildren, setChild }) => {
  return (
    <Box mt={6}>
      <Heading size="md" mt="0" mb={2}>
        Select Child
      </Heading>
      <Text mb={4}>
        Since each child is unique and benefits from learning at their own pace,
        you will need to select a child to apply one of our plans below to.
      </Text>
      <Select
        name="children"
        defaultValue={usersChildren[0].fullName}
        onChange={e => {
          usersChildren.forEach(child => {
            if (child.fullName === e.target.value) {
              setChild({ id: child.id, fullName: child.fullName });
            }
          });
        }}
        w={{ base: 'full', sm: '35%', xl: '50%' }}
      >
        {usersChildren.map(child => (
          <option key={child.id} value={child.fullName}>
            {child.fullName}
          </option>
        ))}
      </Select>
    </Box>
  );
};

export default ChildSelection;
