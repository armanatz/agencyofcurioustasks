import React, { useState } from 'react';
import {
  Avatar,
  Box,
  FormControl,
  FormHelperText,
  FormLabel,
  Grid,
  GridItem,
  HStack,
  IconButton,
  Input,
  ListItem,
  Text,
  UnorderedList,
  useToast,
} from '@chakra-ui/react';
import { AiOutlinePlus } from 'react-icons/ai';
import { useMutation, useQueryClient } from 'react-query';

const Child = ({ data }) => {
  const [activationCode, setActivationCode] = useState('');

  const queryClient = useQueryClient();
  const toast = useToast();

  const user = queryClient.getQueryData('user');

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${
          process.env.REACT_APP_API_VERSION
        }/products/assign${
          user.roles.find(el => el === 'Superuser' || el === 'Admin')
            ? '?admin=true'
            : ''
        }`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async data => {
        if (data.status !== 200) {
          const error = await data.json();
          return toast({
            title: error.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }

        setActivationCode('');

        queryClient.invalidateQueries('usersChildren');

        return toast({
          title: 'Successfully assigned product.',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = () => {
    if (activationCode === '') {
      return toast({
        title: 'Activation Code Required',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
    return mutation.mutate({
      userId: data.id,
      activationCode: activationCode.toUpperCase(),
    });
  };

  return (
    <Box>
      <Grid
        gap={6}
        templateColumns={['auto', '84px auto 300px']}
        templateRows="auto"
        py={4}
        alignItems="center"
        mb={6}
      >
        <GridItem colSpan={1} textAlign={{ base: 'center', sm: 'left' }}>
          <Avatar size="lg" src={data.imageUrl} />
        </GridItem>
        <GridItem textAlign={{ base: 'center', sm: 'left' }}>
          <Box>
            <Text>
              <strong>{data.fullName}</strong> ({data.username})
            </Text>
          </Box>
          <Box mt={2} fontSize="14px">
            {data.ownedProducts.length > 0 ? (
              <>
                <Text>Products Owned:</Text>
                <UnorderedList>
                  {data.ownedProducts.map(product => (
                    <ListItem key={product.title} mr={2}>
                      {product.title}
                    </ListItem>
                  ))}
                </UnorderedList>
              </>
            ) : (
              <Text>No products currently assigned.</Text>
            )}
          </Box>
        </GridItem>
        <GridItem>
          <HStack>
            <FormControl>
              <FormLabel>Activation Code</FormLabel>
              <Input
                onChange={e => setActivationCode(e.target.value)}
                value={activationCode}
              />
              <FormHelperText>
                This should be in the box you bought.
              </FormHelperText>
            </FormControl>
            <IconButton
              variant="secondarySolid"
              aria-label="Edit Child"
              icon={<AiOutlinePlus />}
              mt="-7px!important"
              onClick={onSubmit}
            />
          </HStack>
        </GridItem>
      </Grid>
    </Box>
  );
};

export default Child;
