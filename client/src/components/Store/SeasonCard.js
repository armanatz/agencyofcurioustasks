import React from 'react';
import {
  Box,
  Grid,
  GridItem,
  Heading,
  Text,
  Button,
  Flex,
} from '@chakra-ui/react';

import { Link } from 'react-router-dom';

const SeasonCard = props => {
  return (
    <Box
      bg="white"
      border="1px solid"
      borderColor="gray.300"
      borderRadius="md"
      minH="200px"
      boxShadow="lg"
    >
      <Grid
        gap={6}
        templateColumns={{ base: '1fr', sm: '1fr 4fr 1fr' }}
        minH="200px"
      >
        <GridItem h="200px">
          <Box
            backgroundPosition="center"
            backgroundSize="cover"
            backgroundRepeat="no-repeat"
            backgroundImage={`url(${props.coverImageUrl})`}
            h="full"
            w="full"
            borderLeftRadius={['0', 'md']}
            borderTopLeftRadius={['md', '0']}
            borderTopRightRadius={['md', '0']}
          />
        </GridItem>
        <GridItem px={{ base: 4, sm: 0 }}>
          <Flex flexDir="column" justifyContent="center" h="full">
            <Heading
              as="h3"
              fontSize="24px"
              fontWeight="bold"
              mb={2}
              noOfLines={{ base: 3, sm: 2 }}
            >
              {props.seasonNumber !== 0 && `Season ${props.seasonNumber}:`}
              {props.seasonNumber !== 0 && <br />}
              {props.title}
            </Heading>
            <Text noOfLines={3}>{props.description}</Text>
          </Flex>
        </GridItem>
        <GridItem p={{ base: '0 1rem 1rem 1rem', sm: '0 1.25rem 0 0' }}>
          <Flex h="full" alignItems="center">
            <Button
              w="full"
              variant="ghost"
              color="niagara.500"
              isDisabled={props.comingSoon}
              {...(!props.comingSoon && {
                as: Link,
                to: {
                  pathname: `/store/season/${props.seasonNumber}`,
                  state: {
                    ...props,
                  },
                },
              })}
            >
              {props.comingSoon ? 'Coming Soon' : 'Explore >'}
            </Button>
          </Flex>
        </GridItem>
      </Grid>
    </Box>
  );
};

export default SeasonCard;
