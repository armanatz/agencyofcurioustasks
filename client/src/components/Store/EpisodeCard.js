import React, { useState } from 'react';
import {
  Box,
  Grid,
  GridItem,
  Heading,
  Text,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  UnorderedList,
  ListItem,
  Image,
  Center,
  Stack,
  useDisclosure,
  Wrap,
  WrapItem,
  ButtonGroup,
  Skeleton,
} from '@chakra-ui/react';

import { fetchPricesByProductId } from '../../queries/Stripe';

const EpisodeCard = ({ episodeData, onSubscribeBtnClick, canSubscribe }) => {
  const [pricing, setPricing] = useState([]);
  const [selectedImg, setSelectedImg] = useState(undefined);

  const { isOpen, onClose, onOpen } = useDisclosure();

  let coverImg = episodeData.images.filter(image => image.coverPhoto);
  coverImg = coverImg[0];

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size="6xl">
        <ModalOverlay />
        <ModalContent mx={['10px', 0]}>
          <ModalCloseButton onClick={onClose} />
          <ModalBody>
            <Grid
              templateColumns={{ base: '1fr', sm: 'repeat(2, 1fr)' }}
              gap={6}
              my={6}
            >
              <GridItem>
                <Box
                  backgroundImage={
                    selectedImg
                      ? `url(${selectedImg.imageUrl})`
                      : `url(${coverImg.imageUrl})`
                  }
                  w="full"
                  h="300px"
                  backgroundPosition="center"
                  backgroundRepeat="no-repeat"
                  backgroundSize="cover"
                  mb={6}
                />
                <Wrap>
                  {episodeData.images.map(image => (
                    <WrapItem key={image.id}>
                      <Box>
                        <Image
                          boxSize="80px"
                          objectFit="cover"
                          src={image.imageUrl}
                          onClick={() => {
                            if (selectedImg) {
                              if (selectedImg.id !== image.id) {
                                setSelectedImg(image);
                              }
                            } else {
                              setSelectedImg(image);
                            }
                          }}
                        />
                      </Box>
                    </WrapItem>
                  ))}
                </Wrap>
              </GridItem>
              <GridItem>
                <Heading as="h1" mb={2}>
                  {episodeData.title}
                </Heading>
                <Text mb={6} whiteSpace="pre-wrap">
                  {episodeData.description}
                </Text>
                <Grid
                  templateColumns={{ base: '1fr', sm: 'repeat(2, 1fr)' }}
                  gap={6}
                  mt={6}
                >
                  {episodeData.boxContents && (
                    <GridItem>
                      <Heading as="h3" fontSize="2xl">
                        Box Contents
                      </Heading>
                      <UnorderedList>
                        {episodeData.boxContents.split('\n').map((el, i) => (
                          <ListItem key={i}>{el}</ListItem>
                        ))}
                      </UnorderedList>
                    </GridItem>
                  )}
                  {episodeData.onlineGames && (
                    <GridItem>
                      <Heading as="h3" fontSize="2xl">
                        Online Games
                      </Heading>
                      <UnorderedList>
                        {episodeData.onlineGames.split('\n').map((el, i) => (
                          <ListItem key={i}>{el}</ListItem>
                        ))}
                      </UnorderedList>
                    </GridItem>
                  )}
                </Grid>
                {episodeData.seasonNumber === 0 && (
                  <Box mt={6}>
                    <Heading size="lg">Pricing</Heading>
                    <Skeleton isLoaded={pricing.length > 0}>
                      {pricing.map(price => (
                        <Text key={price.id}>
                          {price.currency.toUpperCase()}{' '}
                          {price.unitAmount / 100}{' '}
                          {price.type !== 'one_time' &&
                            `every ${price.interval}`}
                        </Text>
                      ))}
                    </Skeleton>
                  </Box>
                )}
              </GridItem>
            </Grid>
          </ModalBody>
          <ModalFooter>
            <ButtonGroup>
              <Button variant="ghost" onClick={onClose}>
                Go Back
              </Button>
              {episodeData.comingSoon ? (
                <Button isDisabled={true}>Coming Soon</Button>
              ) : (
                <Button
                  onClick={() => {
                    if (episodeData.seasonNumber === 0) {
                      return onSubscribeBtnClick(pricing);
                    }
                    return onSubscribeBtnClick();
                  }}
                >
                  {canSubscribe
                    ? episodeData.seasonNumber === 0
                      ? 'Purchase Episode'
                      : 'Subscribe to this Season'
                    : 'Add Child to Subscribe'}
                </Button>
              )}
            </ButtonGroup>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Center py={12}>
        <Box
          role="group"
          p={6}
          maxW="430px"
          w="full"
          bg="white"
          boxShadow="lg"
          rounded="lg"
          pos="relative"
          zIndex={1}
          border="1px solid"
          borderColor="gray.300"
          borderRadius="md"
        >
          <Box
            rounded="lg"
            mt={-12}
            pos="relative"
            height="230px"
            _after={{
              transition: 'all .3s ease',
              content: '""',
              w: 'full',
              h: 'full',
              pos: 'absolute',
              top: 5,
              left: 0,
              backgroundImage: `url(${coverImg.imageUrl})`,
              backgroundPosition: 'center',
              backgroundSize: 'cover',
              filter: 'blur(10px)',
              zIndex: -1,
            }}
            _groupHover={{
              _after: {
                filter: 'blur(20px)',
              },
            }}
            border="1px solid"
            borderColor="gray.300"
            borderRadius="md"
          >
            <Image
              rounded="lg"
              height={230}
              width={382}
              objectFit="cover"
              src={`${coverImg.imageUrl}`}
            />
          </Box>
          <Stack spacing={6} pt={10} align="center">
            <Heading fontSize="2xl" fontFamily="body" fontWeight={500}>
              {episodeData.title}
            </Heading>
            <Text color="gray.500" fontSize="sm" noOfLines={4}>
              {episodeData.description}
            </Text>
            <Button
              w="full"
              variant="secondarySolid"
              onClick={async () => {
                if (episodeData.seasonNumber === 0) {
                  onOpen();
                  const req = await fetchPricesByProductId(
                    episodeData.stripeProductId,
                  );
                  return setPricing(req);
                }
                return onOpen();
              }}
            >
              Read More
            </Button>
          </Stack>
        </Box>
      </Center>
    </>
  );
};

export default EpisodeCard;
