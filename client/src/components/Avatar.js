import React from 'react';
import { Box, Icon, Image } from '@chakra-ui/react';
import { FaUser } from 'react-icons/fa';

const Avatar = ({ size, src, ...rest }) => {
  let whSize;

  switch (size) {
    case '2xs':
      whSize = '1rem';
      break;
    case 'xs':
      whSize = '1.5rem';
      break;
    case 'sm':
      whSize = '2rem';
      break;
    case 'md':
      whSize = '3rem';
      break;
    case 'lg':
      whSize = '4rem';
      break;
    case 'xl':
      whSize = '6rem';
      break;
    case '2xl':
      whSize = '8rem';
      break;
    default:
      whSize = '3rem';
      break;
  }

  return (
    <Box
      as="span"
      borderRadius="full"
      display="inline-flex"
      alignItems="center"
      justifyContent="center"
      textAlign="center"
      textTransform="uppercase"
      fontWeight="medium"
      position="relative"
      flexShrink="0"
      backgroundColor="cerulean.200"
      color="white"
      verticalAlign="top"
      w={whSize}
      h={whSize}
      fontSize={`calc(${whSize} / 2.5)`}
      {...rest}
    >
      {src ? (
        <Image
          src={src}
          w="full"
          h="full"
          borderRadius="full"
          objectFit="contain"
        />
      ) : (
        <Icon as={FaUser} />
      )}
    </Box>
  );
};

export default Avatar;
