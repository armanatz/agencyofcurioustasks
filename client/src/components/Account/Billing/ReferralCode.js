import React from 'react';
import {
  Box,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
  Code,
  Button,
  useClipboard,
} from '@chakra-ui/react';

const ReferralCode = ({ referralCode }) => {
  const { hasCopied, onCopy } = useClipboard(referralCode);

  return (
    <Box>
      <Stat w="16em">
        <StatLabel>Your referral code</StatLabel>
        <StatNumber>
          <Code fontSize={24} my={2} onClick={onCopy} cursor="pointer">
            {referralCode}
          </Code>
          <Button size="xs" ml={2} variant="secondarySolid" onClick={onCopy}>
            {hasCopied ? 'Copied' : 'Copy'}
          </Button>
        </StatNumber>
        <StatHelpText>
          Share your code with friends to enjoy discounts on your next purchase
          of a single mission.
        </StatHelpText>
      </Stat>
    </Box>
  );
};

export default ReferralCode;
