import React from 'react';
import { Grid, GridItem, Text, Image } from '@chakra-ui/react';

import DeleteDialog from '../../DeleteDialog';

import MasterCardLogo from '../../../assets/common/MasterCard.svg';
import VisaLogo from '../../../assets/common/Visa.svg';

const Payment = ({ card, onDelete }) => {
  return (
    <Grid
      gap={6}
      templateColumns="50px auto 50px"
      templateRows="auto"
      py={4}
      alignItems="center"
    >
      <GridItem colSpan={1} textAlign={{ base: 'center', sm: 'left' }}>
        {card.brand === 'visa' ? (
          <Image src={VisaLogo} boxSizing="50px" />
        ) : (
          <Image src={MasterCardLogo} w="50px" />
        )}
      </GridItem>
      <GridItem textAlign={{ base: 'center', sm: 'left' }}>
        <Text>**** {card.last4}</Text>
        <Text>
          {card.expMonth} / {card.expYear}
        </Text>
      </GridItem>
      <GridItem>
        <DeleteDialog subject="payment method" onDelete={onDelete} />
      </GridItem>
    </Grid>
  );
};

export default Payment;
