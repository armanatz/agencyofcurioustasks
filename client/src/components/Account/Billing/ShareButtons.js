import React from 'react';
import { HStack } from '@chakra-ui/react';
import {
  EmailShareButton,
  FacebookShareButton,
  TelegramShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  EmailIcon,
  FacebookIcon,
  TelegramIcon,
  TwitterIcon,
  WhatsappIcon,
} from 'react-share';

const ShareButtons = ({ referralCode, referralDiscount }) => {
  const shareUrl = 'https://agencyofcurioustasks.com';
  const title = `Here’s an exclusive code to save MYR${referralDiscount} off your first non-subscription STEM box from Agency of Curious Tasks. Our children absolutely enjoy the story based learning style that combines both offline hands-on projects and e-learning!${'\n\n'}Sign up using my code ${referralCode} and save MYR${referralDiscount} on your next order of a single mission.`;

  const titleTwitter = `Sign up using my code ${referralCode} and save MYR${referralDiscount} on your first single mission purchase at the Agency of Curious Tasks, a story-based STEM box for kids!`;

  return (
    <HStack mb={6}>
      <EmailShareButton
        url={shareUrl}
        subject="STEM-based Subscription Box for Kids"
        body={title}
      >
        <EmailIcon round size="30px" />
      </EmailShareButton>
      <FacebookShareButton url={shareUrl} quote={title}>
        <FacebookIcon round size="30px" />
      </FacebookShareButton>
      <TelegramShareButton url={shareUrl} title={title}>
        <TelegramIcon round size="30px" />
      </TelegramShareButton>
      <TwitterShareButton url={shareUrl} title={titleTwitter}>
        <TwitterIcon round size="30px" />
      </TwitterShareButton>
      <WhatsappShareButton url={shareUrl} title={title} separator=" - ">
        <WhatsappIcon round size="30px" />
      </WhatsappShareButton>
    </HStack>
  );
};

export default ShareButtons;
