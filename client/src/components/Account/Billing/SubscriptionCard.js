import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Box,
  Heading,
  Text,
  Button,
  VStack,
  HStack,
  Modal,
  ModalOverlay,
  ModalHeader,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalCloseButton,
  ButtonGroup,
  useToast,
  useDisclosure,
} from '@chakra-ui/react';
import dayjs from 'dayjs';
import { useMutation, useQueryClient } from 'react-query';

import PaymentMethod from '../../Store/Checkout/PaymentMethod';

const SubscriptionCard = ({
  data,
  paymentMethods,
  onPayClick,
  paymentLoading,
}) => {
  const queryClient = useQueryClient();
  const toast = useToast();
  const history = useHistory();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const [cancelLoading, setCancelLoading] = useState(false);
  const [selectedCardId, setSelectedCardId] = useState(undefined);

  useEffect(() => {
    if (data.subscription.status === 'active') {
      onClose();
    }
  }, [data]);

  const cancelMutation = useMutation(
    id => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/stripe/subscriptions/${id}`,
        {
          method: 'DELETE',
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: async req => {
        setCancelLoading(false);

        const res = await req.json();

        if (req.ok) {
          queryClient.invalidateQueries('subscriptions');
          return toast({
            title: 'Subscription Cancelled',
            description: `${data.child.fullName} will no longer receive any further episodes from this mission`,
            status: 'success',
            duration: 5000,
            isClosable: true,
          });
        }

        return toast({
          title: 'Something went wrong',
          description: res.message,
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const handleOnClick = (type, options) => {
    setCancelLoading(true);
    if (type === 'redirect') {
      return history.push(options);
    }
    return cancelMutation.mutate(data.subscription.id);
  };

  return (
    <Box
      key={data.subscription.id}
      fontSize="sm"
      bgColor="linen.500"
      p={4}
      rounded="md"
      borderColor="linen.600"
      borderWidth={1}
    >
      <Modal isOpen={isOpen} onClose={onClose} size="3xl">
        <ModalOverlay />
        <ModalContent mx={['10px', 0]}>
          <ModalCloseButton onClick={onClose} />
          <ModalHeader>Select Payment Method To Use</ModalHeader>
          <ModalBody>
            {paymentMethods.data.length !== 0 &&
              paymentMethods.data.map((pm, i, arr) => (
                <PaymentMethod
                  key={pm.id}
                  data={pm}
                  mt={i === arr.length - 1 ? 6 : 0}
                  mb={i === arr.length - 1 ? 0 : 6}
                  selectedCardId={selectedCardId}
                  setSelectedCardId={setSelectedCardId}
                />
              ))}
          </ModalBody>
          <ModalFooter>
            <ButtonGroup>
              <Button variant="ghost" onClick={onClose}>
                Cancel
              </Button>
              <Button
                onClick={() =>
                  onPayClick({
                    subscription: data.subscription,
                    paymentMethodId: selectedCardId,
                  })
                }
                isLoading={paymentLoading}
              >
                Pay
              </Button>
            </ButtonGroup>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <VStack spacing={2} align="flex-start">
        <Heading size="md">{data.subscription.plan.product.name}</Heading>
        <Text fontWeight="bold">{data.child.fullName}</Text>
        <Text>
          <Text as="span" fontWeight="bold">
            Start:
          </Text>{' '}
          {dayjs.unix(data.subscription.start_date).format('DD/MM/YYYY')}
        </Text>
        <Text>
          <Text as="span" fontWeight="bold">
            {data.subscription.cancel_at === null ? 'Cancelled:' : 'End:'}
          </Text>{' '}
          {data.subscription.cancel_at === null
            ? dayjs.unix(data.subscription.canceled_at).format('DD/MM/YYYY')
            : dayjs.unix(data.subscription.cancel_at).format('DD/MM/YYYY')}
        </Text>
        <Text>
          <Text as="span" fontWeight="bold">
            Status:
          </Text>{' '}
          <Text
            as="span"
            color={
              data.subscription.status !== 'active'
                ? 'froly.500'
                : 'niagara.500'
            }
          >
            {data.subscription.status.charAt(0).toUpperCase() +
              data.subscription.status.slice(1)}
          </Text>
        </Text>
        <HStack pt={2}>
          {data.subscription.status !== 'active' &&
            data.subscription.status !== 'canceled' && (
              <Button size="sm" onClick={onOpen}>
                Pay Now
              </Button>
            )}
          {data.subscription.status !== 'canceled' && (
            <Button
              size="sm"
              variant="dangerSolid"
              onClick={handleOnClick}
              isLoading={cancelLoading}
            >
              {data.subscription.status === 'incomplete' ? 'Delete' : 'Cancel'}
            </Button>
          )}
        </HStack>
      </VStack>
    </Box>
  );
};

export default SubscriptionCard;
