import React from 'react';
import {
  Grid,
  GridItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Button,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

import PasswordField from '../../PasswordField';

const PasswordForm = ({ data, onSubmit }) => {
  const { handleSubmit, errors, register, formState } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        password: Joi.string().min(8).required(),
        confirmPassword: Joi.any().equal(Joi.ref('password')).required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid
        gap={6}
        templateColumns={['1fr', 'repeat(2, 1fr)']}
        templateRows="auto"
        py={6}
      >
        <GridItem>
          <FormControl id="password" isInvalid={errors.password}>
            <FormLabel>New Password</FormLabel>
            <PasswordField name="password" ref={register} />
            <FormHelperText>
              Needs to be a minimum of 8 characters
            </FormHelperText>
            <FormErrorMessage>Password is required.</FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="confirmPassword" isInvalid={errors.confirmPassword}>
            <FormLabel>Confirm New Password</FormLabel>
            <PasswordField name="confirmPassword" ref={register} />
            <FormErrorMessage>Passwords do not match.</FormErrorMessage>
          </FormControl>
        </GridItem>
      </Grid>
      <Button type="submit" mb={6} isLoading={formState.isSubmitting}>
        Save
      </Button>
    </form>
  );
};

export default PasswordForm;
