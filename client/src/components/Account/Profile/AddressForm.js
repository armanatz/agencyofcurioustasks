import React from 'react';
import {
  Grid,
  GridItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  Select,
  Button,
  ButtonGroup,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const AddressForm = ({ data, onSubmit, isCancelable, onCancel, inModal }) => {
  const { handleSubmit, errors, register, formState } = useForm({
    reValidateMode: 'onBlur',
    defaultValues: {
      line1: data?.line1,
      line2: data?.line2,
      postCode: data?.postCode,
      city: data?.city,
      state: data?.state,
      country: data?.country,
    },
    resolver: joiResolver(
      Joi.object({
        line1: Joi.string().required(),
        line2: Joi.string().allow('').optional(),
        postCode: Joi.string().required(),
        city: Joi.string().required(),
        state: Joi.string().required(),
        country: Joi.string().required(),
      }),
    ),
  });

  const msianStates = [
    'Selangor',
    'Kuala Lumpur',
    'Johor',
    'Kedah',
    'Kelantan',
    'Labuan',
    'Malacca',
    'Negeri Sembilan',
    'Pahang',
    'Penang',
    'Perak',
    'Perlis',
    'Putrajaya',
    'Sabah',
    'Sarawak',
    'Terrengganu',
  ];

  const msianStateOptions = msianStates.map(state => (
    <option key={state} value={state}>
      {state}
    </option>
  ));

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid gap={6} templateColumns={['1fr', 'repeat(2, 1fr)']} py={6}>
        <GridItem>
          <FormControl id="line1" isInvalid={errors.line1}>
            <FormLabel>Address Line 1</FormLabel>
            <Input name="line1" ref={register} />
          </FormControl>
          <FormErrorMessage>
            {errors.line1 && 'Address Line 1 is required.'}
          </FormErrorMessage>
        </GridItem>
        <GridItem>
          <FormControl id="line2" isInvalid={errors.line2}>
            <FormLabel>Address Line 2</FormLabel>
            <Input name="line2" ref={register} />
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="postcode" isInvalid={errors.postCode}>
            <FormLabel>Post Code</FormLabel>
            <Input name="postCode" ref={register} />
            <FormErrorMessage>
              {errors.postCode && 'Post code is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="city" isInvalid={errors.city}>
            <FormLabel>City</FormLabel>
            <Input name="city" ref={register} />
            <FormErrorMessage>
              {errors.city && 'City is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="state" isInvalid={errors.state}>
            <FormLabel>State</FormLabel>
            <Select name="state" ref={register}>
              {msianStateOptions}
            </Select>
            <FormErrorMessage>
              {errors.state && 'State is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="country" isInvalid={errors.country}>
            <FormLabel>Country</FormLabel>
            <Select name="country" ref={register}>
              <option value="Malaysia">Malaysia</option>
            </Select>
            <FormHelperText>
              We currently only ship within Malaysia and are expanding to more
              countries soon.
            </FormHelperText>
            <FormErrorMessage>
              {errors.country && 'Country is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
      </Grid>
      <ButtonGroup
        w={isCancelable ? ['full', 'auto'] : 'auto'}
        {...(inModal && { float: 'right' })}
      >
        {isCancelable && (
          <Button variant="ghost" mb={6} mr={2} onClick={onCancel} w="50%">
            Cancel
          </Button>
        )}
        <Button
          type="submit"
          mb={6}
          isLoading={formState.isSubmitting}
          w={isCancelable ? '50%' : 'auto'}
        >
          Save
        </Button>
      </ButtonGroup>
    </form>
  );
};

export default AddressForm;
