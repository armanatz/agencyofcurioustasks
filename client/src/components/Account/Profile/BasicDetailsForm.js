import React, { forwardRef } from 'react';
import {
  Grid,
  GridItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
  FormHelperText,
} from '@chakra-ui/react';
import PhoneInput from 'react-phone-number-input/react-hook-form-input';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import * as JoiLibrary from 'joi';

import 'react-phone-number-input/style.css';

const Joi = JoiLibrary.extend(require('joi-phone-number'));

const BasicDetailsForm = ({ data, onSubmit }) => {
  const { handleSubmit, errors, register, formState, control } = useForm({
    reValidateMode: 'onBlur',
    defaultValues: {
      firstName: data?.firstName,
      lastName: data?.lastName,
    },
    resolver: joiResolver(
      Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().allow('').optional(),
        mobilePhone: Joi.string()
          .phoneNumber({
            defaultCountry: 'MY',
            format: 'e164',
            strict: true,
          })
          .required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid
        gap={6}
        templateColumns={['1fr', 'repeat(2, 1fr)']}
        templateRows="auto"
        py={6}
      >
        <GridItem>
          <FormControl id="firstName" isInvalid={errors.firstName}>
            <FormLabel>First Name</FormLabel>
            <Input name="firstName" ref={register} placeholder="e.g. John" />
            <FormErrorMessage>
              {errors.firstName?.type === 'string.empty' &&
                'First name is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="lastName" isInvalid={errors.lastName}>
            <FormLabel>Last Name</FormLabel>
            <Input name="lastName" ref={register} placeholder="e.g. Doe" />
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="email" h={['auto', '100px']} isReadOnly>
            <FormLabel>E-mail</FormLabel>
            <Input
              type="email"
              _hover={{ bg: 'linen.500' }}
              defaultValue={data?.email}
            />
            <FormHelperText>
              Your e-mail is tied to your account and cannot be changed.
            </FormHelperText>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="mobilePhone" isInvalid={errors.mobilePhone}>
            <FormLabel>Phone Number</FormLabel>
            <PhoneInput
              name="mobilePhone"
              control={control}
              ref={register}
              rules={{}}
              defaultCountry="MY"
              defaultValue={data?.mobilePhone}
              international
              withCountryCallingCode
              useNationalFormatForDefaultCountryValue={false}
              inputComponent={forwardRef((props, ref) => {
                return <Input ref={ref} {...props} />;
              })}
            />
            <FormHelperText>
              If not a Malaysian number, please put in your country code.
            </FormHelperText>
            <FormErrorMessage>
              {errors.mobilePhone?.type === 'any.required' &&
                'Phone number is required.'}
            </FormErrorMessage>
            <FormErrorMessage>
              {errors.mobilePhone?.type === 'phoneNumber.invalid' &&
                'Phone number is not valid or not in the correct format.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
      </Grid>
      <Button type="submit" mb={6} isLoading={formState.isSubmitting}>
        Save
      </Button>
    </form>
  );
};

export default BasicDetailsForm;
