import React from 'react';
import { Box, Center, Spinner } from '@chakra-ui/react';

const Loading = () => {
  return (
    <Box>
      <Center h="100vh" bg="gray.50">
        <Spinner color="niagara.500" size="xl" />
      </Center>
    </Box>
  );
};

export default Loading;
