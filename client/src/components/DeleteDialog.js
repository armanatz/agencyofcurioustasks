import React, { useState, useRef, Fragment } from 'react';
import {
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
  Button,
  IconButton,
} from '@chakra-ui/react';
import { AiFillDelete } from 'react-icons/ai';

const DeleteDialog = ({ subject, onDelete, buttonProps, loading }) => {
  const [isOpen, setIsOpen] = useState(false);
  const onClose = () => setIsOpen(false);
  const cancelRef = useRef();

  return (
    <>
      <IconButton
        variant="dangerSolid"
        aria-label={`Delete ${subject}`}
        icon={<AiFillDelete />}
        onClick={() => setIsOpen(true)}
        {...buttonProps}
      />
      <AlertDialog
        isOpen={isOpen}
        leastDestructiveRef={cancelRef}
        onClose={onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader>Delete {subject}?</AlertDialogHeader>
            <AlertDialogBody>
              Are you sure? You cannot undo this action.
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button variant="secondaryGhost" onClick={onClose}>
                Cancel
              </Button>
              <Button
                variant="dangerSolid"
                onClick={onDelete}
                isLoading={loading}
              >
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
};

export default DeleteDialog;
