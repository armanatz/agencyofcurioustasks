import React from 'react';
import { Link } from 'react-router-dom';
import {
  Box,
  Button,
  Flex,
  Grid,
  GridItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  RadioGroup,
  Radio,
  HStack,
  ButtonGroup,
} from '@chakra-ui/react';
import { useForm, Controller } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';
import DatePicker from 'react-datepicker';
import dayjs from 'dayjs';

import PasswordField from '../PasswordField';
import Avatar from '../Avatar';

import 'react-datepicker/dist/react-datepicker.css';

const EditForm = ({ data, onSubmit, onOpenModal, selectedAvatar }) => {
  const { handleSubmit, errors, register, formState, control } = useForm({
    reValidateMode: 'onBlur',
    defaultValues: {
      firstName: data?.firstName,
      lastName: data?.lastName,
      gender: data?.data.gender,
      birthDate: dayjs(data.birthDate).toDate(),
    },
    resolver: joiResolver(
      Joi.object({
        firstName: Joi.string()
          .pattern(/^[a-z ,.'-]+$/i)
          .required(),
        lastName: Joi.string()
          .pattern(/^[a-z ,.'-]+$/i)
          .allow('')
          .optional(),
        gender: Joi.string().required(),
        birthDate: Joi.any().required(),
        password: Joi.string().min(8).allow('').optional(),
        confirmPassword: Joi.any().equal(Joi.ref('password')).required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Box my={6} display={['flex', 'block']} justifyContent="center">
        <Flex w="max-content" flexDir="column" alignItems="center">
          <Avatar size="2xl" mb={6} src={selectedAvatar} />
          <Button variant="secondarySolid" onClick={onOpenModal}>
            Select Avatar
          </Button>
        </Flex>
      </Box>
      <Grid
        gap={6}
        templateColumns={{ base: '1fr', sm: 'repeat(2, 2fr)' }}
        templateRows="auto"
        py={6}
      >
        <GridItem>
          <FormControl id="firstName" isInvalid={errors.firstName}>
            <FormLabel>First Name</FormLabel>
            <Input name="firstName" ref={register} />
            <FormErrorMessage>
              {errors.firstName && 'First name is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="lastName" isInvalid={errors.lastName}>
            <FormLabel>Last Name</FormLabel>
            <Input name="lastName" ref={register} />
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="birthDate" isInvalid={errors.birthDate}>
            <FormLabel>Date of Birth</FormLabel>
            <Controller
              name="birthDate"
              control={control}
              render={({ onChange, value }) => {
                return (
                  <DatePicker
                    dateFormat="dd MMM yyyy"
                    yearDropdownItemNumber={10}
                    popperPlacement="top"
                    customInput={<Input />}
                    selected={value}
                    minDate={dayjs()
                      .subtract(10, 'year')
                      .startOf('year')
                      .toDate()}
                    maxDate={dayjs().subtract(1, 'year').toDate()}
                    onChange={onChange}
                    disabledKeyboardNavigation
                    showYearDropdown
                    scrollableYearDropdown
                  />
                );
              }}
            />
            <FormErrorMessage>
              {errors.birthDate && 'Date of Birth is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem>
          <Box>
            <Box as="label">Gender</Box>
            <RadioGroup name="gender" defaultValue={data?.data.gender}>
              <HStack spacing="24px" mt={15}>
                <Radio value="Male" ref={register} isInvalid={errors.gender}>
                  Male
                </Radio>
                <Radio value="Female" ref={register} isInvalid={errors.gender}>
                  Female
                </Radio>
              </HStack>
            </RadioGroup>
          </Box>
        </GridItem>
        <GridItem>
          <FormControl id="password" isInvalid={errors.password}>
            <FormLabel>Password</FormLabel>
            <PasswordField
              name="password"
              autoComplete="current-password"
              ref={register}
            />
            <FormHelperText>
              Needs to be a minimum of 8 characters
            </FormHelperText>
            <FormErrorMessage>Password is required.</FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="confirmPassword" isInvalid={errors.confirmPassword}>
            <FormLabel>Confirm Password</FormLabel>
            <PasswordField name="confirmPassword" ref={register} />
            <FormErrorMessage>Passwords do not match.</FormErrorMessage>
          </FormControl>
        </GridItem>
      </Grid>
      <ButtonGroup my={6}>
        <Button
          variant="ghost"
          aria-label="Go back to Children"
          as={Link}
          to="/children"
        >
          Cancel
        </Button>
        <Button type="submit" isLoading={formState.isSubmitting}>
          Save
        </Button>
      </ButtonGroup>
    </form>
  );
};

export default EditForm;
