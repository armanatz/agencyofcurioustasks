import React from 'react';
import {
  Box,
  ButtonGroup,
  Grid,
  GridItem,
  IconButton,
  Text,
  useToast,
} from '@chakra-ui/react';
import { AiFillEdit } from 'react-icons/ai';
import { Link } from 'react-router-dom';
import { useMutation, useQueryClient } from 'react-query';

import DeleteDialog from '../DeleteDialog';
import Avatar from '../Avatar';

const Child = ({ data }) => {
  const queryClient = useQueryClient();
  const toast = useToast();

  const mutation = useMutation(
    id => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users/${data.id}`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries('usersChildren');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const handleOnDelete = id => {
    return mutation.mutate(id);
  };

  return (
    <Box>
      <Grid
        gap={6}
        templateColumns={['auto', '84px auto 100px']}
        templateRows="auto"
        py={4}
        alignItems="center"
        mb={6}
      >
        <GridItem colSpan={1} textAlign={{ base: 'center', sm: 'left' }}>
          <Avatar size="lg" src={data.imageUrl} />
        </GridItem>
        <GridItem textAlign={{ base: 'center', sm: 'left' }}>
          <Text>
            {data.fullName} ({data.username})
          </Text>
        </GridItem>
        <GridItem>
          <ButtonGroup w={{ base: 'full' }}>
            <IconButton
              variant="secondarySolid"
              aria-label="Edit Child"
              icon={<AiFillEdit />}
              w="full"
              as={Link}
              to={`/children/edit/${data.id}`}
            />
            <DeleteDialog
              subject="child"
              onDelete={() => handleOnDelete(data.id)}
            />
          </ButtonGroup>
        </GridItem>
      </Grid>
    </Box>
  );
};

export default Child;
