import React, { useState } from 'react';
import {
  Box,
  Flex,
  Heading,
  Spinner,
  Stack,
  Icon,
  Image,
  useRadioGroup,
} from '@chakra-ui/react';
import { BiError } from 'react-icons/bi';

import RadioCard from './RadioCard';
import Loading from '../../Loading';

const AvatarRadio = ({ src, radio }) => {
  const [status, setStatus] = useState('loading');
  return (
    <RadioCard {...radio}>
      {status === 'loading' && <Spinner color="niagara.500" />}
      {status === 'error' && <Icon as={BiError} color="froly.500" />}
      <Image
        src={src}
        w="70px"
        onLoad={() => setStatus('loaded')}
        onError={() => setStatus('error')}
        display={status === 'loaded' ? 'block' : 'none'}
      />
    </RadioCard>
  );
};

const AvatarRadios = props => {
  const { getRootProps, getRadioProps } = useRadioGroup({
    name: 'avatars',
    onChange: props.onChange,
  });

  const group = getRootProps();

  return (
    <Flex {...group} flexDir="row" flexWrap="wrap" justifyContent="flex-start">
      {props.data.map(el => {
        const radio = getRadioProps({ value: el });
        return <AvatarRadio key={el} src={el} radio={radio} />;
      })}
    </Flex>
  );
};

const AvatarSelector = ({ onChange }) => {
  const maleAvatars = [];
  const femaleAvatars = [];

  for (let i = 0; i < 12; i += 1) {
    maleAvatars.push(
      `https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/avatars/GuyAvatar_${
        i + 1
      }.png`,
    );
  }

  for (let i = 0; i < 12; i += 1) {
    femaleAvatars.push(
      `https://agencyofcurioustasks.sgp1.digitaloceanspaces.com/avatars/GirlAvatar_${
        i + 1
      }.png`,
    );
  }

  if (maleAvatars.length === 0 || femaleAvatars.length === 0) {
    return <Loading />;
  }

  return (
    <Box>
      <Stack spacing={4}>
        <Heading>Female</Heading>
        <AvatarRadios data={femaleAvatars} onChange={onChange} />
        <Heading>Male</Heading>
        <AvatarRadios data={maleAvatars} onChange={onChange} />
      </Stack>
    </Box>
  );
};

export default AvatarSelector;
