import React from 'react';
import { Box, Flex, useRadio } from '@chakra-ui/react';

const RadioCard = props => {
  const { getInputProps, getCheckboxProps } = useRadio(props);

  const input = getInputProps();
  const checkbox = getCheckboxProps();

  return (
    <Box as="label" mr={2} mb={2}>
      <input {...input} />
      <Flex
        flexDir="column"
        justifyContent="center"
        alignItems="center"
        {...checkbox}
        cursor="pointer"
        borderWidth="2px"
        borderRadius="md"
        _checked={{
          color: 'cerulean.500',
          borderColor: 'cerulean.500',
        }}
        px={1}
        py={1}
        minH="140px"
        minW="82px"
        textAlign="center"
      >
        {props.children}
      </Flex>
    </Box>
  );
};

export default RadioCard;
