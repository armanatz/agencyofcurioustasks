import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
  Box,
  Button,
  Flex,
  Grid,
  GridItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  RadioGroup,
  Radio,
  HStack,
  ButtonGroup,
} from '@chakra-ui/react';
import { useForm, Controller } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';
import DatePicker from 'react-datepicker';
import dayjs from 'dayjs';

import PasswordField from '../PasswordField';
import Avatar from '../Avatar';

import 'react-datepicker/dist/react-datepicker.css';

const AddForm = ({ hideCancelBtn, onSubmit, onOpen, selectedAvatar }) => {
  const { handleSubmit, errors, register, formState, control } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().allow('').optional(),
        username: Joi.string()
          .pattern(/^[a-z0-9]+$/i)
          .required(),
        gender: Joi.string().required(),
        birthDate: Joi.date().required(),
        password: Joi.string().min(8).required(),
        confirmPassword: Joi.any().equal(Joi.ref('password')).required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Box my={6} display={['flex', 'block']} justifyContent="center">
        <Flex w="max-content" flexDir="column" alignItems="center">
          <Avatar size="2xl" mb={6} src={selectedAvatar} />
          <Button variant="secondarySolid" onClick={onOpen}>
            Select Avatar
          </Button>
        </Flex>
      </Box>
      <Grid
        gap={6}
        templateColumns={{ base: '1fr', sm: 'repeat(4, 2fr)' }}
        templateRows="auto"
        py={6}
      >
        <GridItem colSpan={[2, 1]}>
          <FormControl id="firstName" isInvalid={errors.firstName}>
            <FormLabel>First Name</FormLabel>
            <Input name="firstName" placeholder="e.g. John" ref={register} />
            <FormErrorMessage>
              {errors.firstName && 'First name is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[2, 1]}>
          <FormControl id="lastName" isInvalid={errors.lastName}>
            <FormLabel>Last Name</FormLabel>
            <Input name="lastName" placeholder="e.g. Doe" ref={register} />
          </FormControl>
        </GridItem>
        <GridItem colSpan={2}>
          <FormControl id="username" isInvalid={errors.username}>
            <FormLabel>Username</FormLabel>
            <Input
              name="username"
              placeholder="e.g. JohnDoe123"
              _hover={{ bg: 'linen.500' }}
              ref={register}
            />
            {errors.username ? (
              <>
                <FormErrorMessage>
                  {errors.username?.type === 'string.empty' &&
                    'Username is required.'}
                </FormErrorMessage>
                <FormErrorMessage>
                  {errors.username?.type === 'string.pattern.base' &&
                    'Username can only be alphanumerical.'}
                </FormErrorMessage>
              </>
            ) : (
              <FormHelperText>Pick something fun and unique.</FormHelperText>
            )}
          </FormControl>
        </GridItem>
        <GridItem colSpan={2}>
          <FormControl id="birthDate" isInvalid={errors.birthDate}>
            <FormLabel>Date of Birth</FormLabel>
            <Controller
              name="birthDate"
              control={control}
              defaultValue=""
              render={({ onChange, value }) => {
                return (
                  <DatePicker
                    dateFormat="dd MMM yyyy"
                    yearDropdownItemNumber={21}
                    popperPlacement="top"
                    customInput={<Input />}
                    selected={value}
                    openToDate={dayjs().subtract(4, 'year').toDate()}
                    minDate={dayjs()
                      .subtract(21, 'year')
                      .startOf('year')
                      .toDate()}
                    maxDate={dayjs().subtract(4, 'year').toDate()}
                    onChange={onChange}
                    disabledKeyboardNavigation
                    showYearDropdown
                    scrollableYearDropdown
                    placeholderText="Use the calendar to pick a date"
                  />
                );
              }}
            />
            <FormErrorMessage>
              {errors.birthDate && 'Date of Birth is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem colSpan={2}>
          <FormControl id="gender">
            <FormLabel>Gender</FormLabel>
            <RadioGroup name="gender" defaultValue="Male">
              <HStack spacing="24px" mt={15}>
                <Radio value="Male" ref={register}>
                  Male
                </Radio>
                <Radio value="Female" ref={register}>
                  Female
                </Radio>
              </HStack>
            </RadioGroup>
          </FormControl>
        </GridItem>
        <GridItem colSpan={2}>
          <FormControl id="password" isInvalid={errors.password}>
            <FormLabel>Password</FormLabel>
            <PasswordField
              name="password"
              autoComplete="current-password"
              ref={register}
            />
            {errors.password ? (
              <FormErrorMessage>
                {errors.password &&
                  'Password is required and needs to be a minimum of 8 characters.'}
              </FormErrorMessage>
            ) : (
              <FormHelperText>
                Needs to be a minimum of 8 characters
              </FormHelperText>
            )}
          </FormControl>
        </GridItem>
        <GridItem colSpan={2}>
          <FormControl id="confirmPassword" isInvalid={errors.confirmPassword}>
            <FormLabel>Confirm Password</FormLabel>
            <PasswordField name="confirmPassword" ref={register} />
            <FormErrorMessage>
              {errors.confirmPassword && 'Passwords do not match.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
      </Grid>
      <ButtonGroup my={6}>
        {!hideCancelBtn && (
          <Button
            variant="ghost"
            aria-label="Go back to Children"
            as={Link}
            to="/children"
          >
            Cancel
          </Button>
        )}
        <Button type="submit" isLoading={formState.isSubmitting}>
          Create Child
        </Button>
      </ButtonGroup>
    </form>
  );
};

export default AddForm;
