import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

const ScrollToTop = ({ behavior = 'auto' }) => {
  const { pathname, search } = useLocation();
  useEffect(() => {
    try {
      window.scroll({
        top: 0,
        left: 0,
        behavior,
      });
    } catch (error) {
      window.scrollTo(0, 0);
    }
  }, [behavior, pathname, search]);
  return null;
};

export default ScrollToTop;
