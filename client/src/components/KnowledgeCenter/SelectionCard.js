import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
  Box,
  Heading,
  GridItem,
  Icon,
  LinkBox,
  LinkOverlay,
} from '@chakra-ui/react';
import { BsArrowRightShort, BsLock } from 'react-icons/bs';

const SelectionCard = ({ imageUrl, locked, name, pathname, ...rest }) => {
  return (
    <GridItem>
      <LinkBox>
        <Box
          rounded="xl"
          w="full"
          h={['225px', '185px', '325px']}
          p={6}
          color="white"
          backgroundColor="gray.800"
          backgroundImage={`url(${imageUrl})`}
          backgroundPosition="center"
          backgroundRepeat="no-repeat"
          backgroundSize="cover"
          boxShadow={
            locked
              ? 'inset 0 0 0 1000px rgba(0, 0, 0, 0.8)'
              : 'inset 0 0 0 1000px rgba(0, 0, 0, 0.3)'
          }
          _hover={{
            boxShadow: !locked
              ? 'inset 0 0 0 1000px rgba(6, 180, 150, 0.8)'
              : 'inset 0 0 0 1000px rgba(0, 0, 0, 0.8)',
          }}
          {...rest}
        >
          {locked ? (
            <>
              <Icon
                as={locked ? BsLock : BsArrowRightShort}
                fontSize="4xl"
                w="36px"
                h="36px"
                p={locked ? '8px' : 0}
              />
              <Heading fontSize="24px">{name}</Heading>
            </>
          ) : (
            <LinkOverlay as={Link} to={pathname}>
              <Icon
                as={locked ? BsLock : BsArrowRightShort}
                fontSize="4xl"
                w="36px"
                h="36px"
                p={locked ? '8px' : 0}
              />
              <Heading fontSize="24px">{name}</Heading>
            </LinkOverlay>
          )}
        </Box>
      </LinkBox>
    </GridItem>
  );
};

export default SelectionCard;
