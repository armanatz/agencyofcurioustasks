import React, { useState } from 'react';
import { HashLink } from 'react-router-hash-link';
import {
  Box,
  chakra,
  Container,
  Link as ChakraLink,
  SimpleGrid,
  Stack,
  Text,
  VisuallyHidden,
  Input,
  IconButton,
  Image,
  Heading,
  FormControl,
  FormErrorMessage,
  useToast,
} from '@chakra-ui/react';
import { FaFacebookF, FaInstagram, FaYoutube } from 'react-icons/fa';
import { BiMailSend } from 'react-icons/bi';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';
import dayjs from 'dayjs';
import { isMobile } from 'react-device-detect';

import Logo from '../assets/common/ACT-Logo.png';
import { useQueryClient } from 'react-query';

const SocialButton = ({ children, label, ...rest }) => {
  return (
    <chakra.button
      bg="blackAlpha.100"
      rounded="full"
      w={8}
      h={8}
      cursor="pointer"
      as="a"
      display="inline-flex"
      alignItems="center"
      justifyContent="center"
      transition="background 0.3s ease"
      _hover={{
        bg: 'blackAlpha.200',
      }}
      {...rest}
    >
      <VisuallyHidden>{label}</VisuallyHidden>
      {children}
    </chakra.button>
  );
};

const Footer = () => {
  const [submitting, setSubmitting] = useState(false);
  const queryClient = useQueryClient();
  const toast = useToast();

  const { handleSubmit, errors, register } = useForm({
    resolver: joiResolver(
      Joi.object({
        email: Joi.string().email({ tlds: false }).required(),
      }),
    ),
  });

  const authStatus = queryClient.getQueryData('authStatus');

  const scrollOffset = (el, offsetAmount = 0) => {
    window.scrollTo({
      top: el.getBoundingClientRect().top + window.pageYOffset + offsetAmount,
      behavior: 'smooth',
    });
  };

  const footerHeight = authStatus?.isAuthenticated
    ? { base: '104px', md: '24px' }
    : { base: '520px', md: '230px' };

  if (authStatus?.isAuthenticated) {
    return (
      <Box
        bg="niagara.500"
        color="white"
        position="absolute"
        bottom="0"
        w="full"
      >
        <Container
          as={Stack}
          maxW={'6xl'}
          py={4}
          direction={{ base: 'column', md: 'row' }}
          spacing={4}
          justify={{ base: 'center', md: 'space-between' }}
          align={{ base: 'center', md: 'center' }}
        >
          <Text>© {dayjs().year()} Agency of Curious Tasks</Text>
          <Stack direction={'row'} spacing={6}>
            <SocialButton
              label="Twitter"
              href="https://facebook.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaFacebookF />
            </SocialButton>
            <SocialButton
              label="YouTube"
              href="https://youtube.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaYoutube />
            </SocialButton>
            <SocialButton
              label="Instagram"
              href="https://instagram.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaInstagram />
            </SocialButton>
          </Stack>
        </Container>
      </Box>
    );
  }

  const subscribeToNewsletter = async ({ email }) => {
    setSubmitting(true);
    try {
      const req = await fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users/mailchimp`,
        {
          method: 'POST',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email,
            tags: ['Newsletter'],
          }),
        },
      );

      if (req.ok) {
        const res = await req.json();
        setSubmitting(false);
        return toast({
          description: res.message,
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      }
    } catch (err) {
      setSubmitting(false);
      return toast({
        description: err.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  };

  return (
    <Box
      bg="niagara.500"
      color="white"
      position="absolute"
      bottom="0"
      w="full"
      h={footerHeight}
    >
      <Container as={Stack} maxW="6xl" py={10}>
        <SimpleGrid
          templateColumns={{
            md: '2fr 1fr 2fr',
            lg: '2fr 1fr 1fr 2fr',
          }}
          spacing={8}
        >
          <Stack spacing={6}>
            <Box>
              <Image src={Logo} w="100px" />
            </Box>
            <Text fontSize="sm">
              © {dayjs().year()} Agency of Curious Tasks
            </Text>
            <Stack direction="row" spacing={6}>
              <SocialButton
                label="Facebook"
                href="https://www.facebook.com/agencyofcurioustasks"
                target="_blank"
                rel="noopener noreferrer"
              >
                <FaFacebookF />
              </SocialButton>
              <SocialButton
                label="YouTube"
                href="https://www.youtube.com/channel/UCHV0iX53WvTlHOmKi1qU6Sg"
                target="_blank"
                rel="noopener noreferrer"
              >
                <FaYoutube />
              </SocialButton>
              <SocialButton
                label="Instagram"
                href="https://www.instagram.com/agencyofcurioustasks/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <FaInstagram />
              </SocialButton>
            </Stack>
          </Stack>
          <Stack align="flex-start">
            <Heading fontWeight="500" fontSize="lg" mb={2}>
              Company
            </Heading>
            <ChakraLink
              as={HashLink}
              fontSize="14px"
              to="/about#"
              smooth
              scroll={el => scrollOffset(el)}
            >
              About
            </ChakraLink>
            <ChakraLink
              as={HashLink}
              fontSize="14px"
              to="/store#"
              smooth
              scroll={el => scrollOffset(el)}
            >
              Store
            </ChakraLink>
            <ChakraLink
              as={HashLink}
              fontSize="14px"
              to="/faq#"
              smooth
              scroll={el => scrollOffset(el)}
            >
              FAQ
            </ChakraLink>
            <ChakraLink
              as={HashLink}
              fontSize="14px"
              to="/about#contact"
              smooth
              scroll={el => scrollOffset(el, -150)}
            >
              Contact
            </ChakraLink>
          </Stack>
          {!isMobile && (
            <Stack align="flex-start">
              <Heading fontWeight="500" fontSize="lg" mb={2}>
                Useful Links
              </Heading>
              <ChakraLink
                as={HashLink}
                fontSize="14px"
                to="/terms#"
                smooth
                scroll={el => scrollOffset(el)}
              >
                Terms {'&'} Conditions
              </ChakraLink>
              <ChakraLink
                as={HashLink}
                fontSize="14px"
                to="/privacy#"
                smooth
                scroll={el => scrollOffset(el)}
              >
                Privacy Policy
              </ChakraLink>
              <ChakraLink
                as={HashLink}
                fontSize="14px"
                to="/faq#shipping"
                smooth
                scroll={el => scrollOffset(el, -100)}
              >
                Shipping {'&'} Delivery
              </ChakraLink>
              <ChakraLink
                as={HashLink}
                fontSize="14px"
                to="/faq#returns"
                smooth
                scroll={el => scrollOffset(el, -120)}
              >
                Returns {'&'} Exchange
              </ChakraLink>
            </Stack>
          )}
          <Stack align="flex-start">
            <Heading fontWeight="500" fontSize="lg" mb={2}>
              Stay Up to Date
            </Heading>
            <form onSubmit={handleSubmit(subscribeToNewsletter)}>
              <Stack direction="row">
                <FormControl id="email" isInvalid={errors.email}>
                  <Input
                    placeholder="Your email address"
                    name="email"
                    ref={register}
                  />
                  <FormErrorMessage>
                    {errors.email && 'Email needs to be valid.'}
                  </FormErrorMessage>
                </FormControl>
                <IconButton
                  aria-label="Subscribe"
                  icon={<BiMailSend />}
                  rounded="md"
                  type="submit"
                  isLoading={submitting}
                />
              </Stack>
            </form>
          </Stack>
        </SimpleGrid>
      </Container>
    </Box>
  );
};

export default Footer;
