import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@chakra-ui/react';
import { isMobile } from 'react-device-detect';

const LoggedInLinks = ({ location, onClose }) => {
  return (
    <>
      <Button
        {...(isMobile && { onClick: onClose })}
        as={Link}
        to="/knowledge-center"
        variant="navLinkKC"
        size="sm"
      >
        Home
      </Button>
      <Button
        {...(isMobile && { onClick: onClose })}
        as={Link}
        to="/games"
        variant="navLinkKC"
        size="sm"
      >
        Games
      </Button>
    </>
  );
};

export default LoggedInLinks;
