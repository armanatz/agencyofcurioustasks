import React, { forwardRef } from 'react';
import {
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
  useDisclosure,
} from '@chakra-ui/react';
import { HiEye, HiEyeOff } from 'react-icons/hi';

const PasswordField = forwardRef((props, ref) => {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <InputGroup>
      <Input ref={ref} type={isOpen ? 'text' : 'password'} {...props} />
      <InputRightElement>
        <IconButton
          bg="transparent !important"
          variant="ghost"
          aria-label={isOpen ? 'Mask password' : 'Reveal password'}
          icon={isOpen ? <HiEyeOff /> : <HiEye />}
          onClick={onToggle}
        />
      </InputRightElement>
    </InputGroup>
  );
});

export default PasswordField;

PasswordField.displayName = 'PasswordField';
