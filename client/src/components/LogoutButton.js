import React from 'react';
import { Box, IconButton, Tooltip } from '@chakra-ui/react';
import { AiOutlinePoweroff } from 'react-icons/ai';

import { useLogout } from '../helpers/CustomHooks';

const LogoutButton = () => {
  const logoutMutation = useLogout();

  const onLogout = async () => {
    return logoutMutation.mutate();
  };

  return (
    <Tooltip label="Logout" placement="left">
      <Box position="fixed" right="10px" top="10px" zIndex="1000">
        <IconButton
          icon={<AiOutlinePoweroff />}
          onClick={onLogout}
          rounded="full"
          size="sm"
        />
      </Box>
    </Tooltip>
  );
};

export default LogoutButton;
