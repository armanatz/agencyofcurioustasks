import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Grid,
  GridItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
  Checkbox,
  FormHelperText,
  ButtonGroup,
  Tooltip,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const PartnerAddForm = ({ data, onSubmit, submitting }) => {
  const [hideCodeField, setHideCodeField] = useState(data?.useActivationCode);

  const history = useHistory();

  const { handleSubmit, errors, register } = useForm({
    reValidateMode: 'onBlur',
    defaultValues: {
      name: data?.name,
      email: data?.email,
      category: data?.category,
      promoId: data?.promoId,
      useActivationCode: data?.useActivationCode,
      blockRegistration: data?.blockRegistration,
      enabled: data?.enabled,
    },
    resolver: joiResolver(
      Joi.object({
        name: Joi.string().required(),
        email: Joi.string().email({ tlds: false }),
        category: Joi.string().required(),
        promoId: Joi.string().optional().allow(''),
        useActivationCode: Joi.boolean().required(),
        blockRegistration: Joi.boolean().required(),
        enabled: Joi.boolean().required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid
        gap={6}
        templateColumns={['1fr', 'repeat(4, 1fr)']}
        templateRows="auto"
        py={6}
      >
        <GridItem colSpan={[1, 4]}>
          <FormControl id="enabled">
            <Checkbox name="enabled" ref={register} h="40px">
              Enabled?
            </Checkbox>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[1, 2]}>
          <FormControl id="name" isInvalid={errors.name}>
            <FormLabel>Name</FormLabel>
            <Input name="name" ref={register} />
            <FormErrorMessage>
              {errors.name?.type === 'string.empty' && 'Name is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[1, 2]}>
          <FormControl id="email" isInvalid={errors.email}>
            <FormLabel>Email</FormLabel>
            <Input name="email" ref={register} />
            <FormErrorMessage>
              {errors.email?.type === 'string.empty' && 'Email is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem colStart={1} colEnd={3}>
          <FormControl id="category" isInvalid={errors.category}>
            <FormLabel>Category</FormLabel>
            <Input name="category" ref={register} />
            <FormErrorMessage>
              {errors.category?.type === 'string.empty' &&
                'Category is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="blockRegistration">
            <Checkbox name="blockRegistration" ref={register} h="40px">
              Block Registration?
            </Checkbox>
            <FormHelperText>
              If enabled, the email address of this partner cannot be used to
              register as a parent in the ACT system.
            </FormHelperText>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="useActivationCode">
            <Checkbox
              name="useActivationCode"
              ref={register}
              h="40px"
              onChange={e => setHideCodeField(e.target.checked)}
            >
              Use Activation Code?
            </Checkbox>
            <FormHelperText>
              The system will track successful signups based on activation code
              instead of referral code when this option is enabled.
            </FormHelperText>
          </FormControl>
        </GridItem>
        {!hideCodeField && (
          <GridItem colSpan={[4, 2]}>
            <FormControl id="promoId" isInvalid={errors.promoId}>
              <FormLabel>Promotion Code API ID</FormLabel>
              <Tooltip
                label="Go to your Stripe Dashboard > Products > Coupons
                > Select your coupon > Copy the API ID of the relevant
                Promotion Code"
                placement="top"
                closeOnClick={false}
                backgroundColor="niagara.500"
              >
                <Input name="promoId" ref={register} />
              </Tooltip>
              <FormErrorMessage>
                {errors.promoId?.type === 'string.empty' &&
                  'Promotion Code API ID is required.'}
              </FormErrorMessage>
            </FormControl>
          </GridItem>
        )}
      </Grid>
      <ButtonGroup mb={6}>
        <Button onClick={() => history.push('/admin/partners')} variant="ghost">
          Cancel
        </Button>
        <Button type="submit" isLoading={submitting}>
          Save
        </Button>
      </ButtonGroup>
    </form>
  );
};

export default PartnerAddForm;
