import React from 'react';
import {
  Box,
  SimpleGrid,
  HStack,
  Text,
  FormControl,
  FormLabel,
  Input,
  ButtonGroup,
  Button,
} from '@chakra-ui/react';
import { useForm, Controller } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';
import DatePicker from 'react-datepicker';
import dayjs from 'dayjs';

import 'react-datepicker/dist/react-datepicker.css';

const PartnersFilter = ({ submitting, onSubmit, onReset }) => {
  const { handleSubmit, errors, register, control, reset } = useForm({
    reValidateMode: 'onBlur',
    defaultValues: {
      dateStart: dayjs().startOf('month').toDate(),
      dateEnd: dayjs().endOf('month').toDate(),
    },
    resolver: joiResolver(
      Joi.object({
        dateStart: Joi.date().optional().allow(''),
        dateEnd: Joi.date().optional().allow(''),
        name: Joi.string().optional().allow(''),
        email: Joi.string().email({ tlds: false }).optional().allow(''),
        category: Joi.string().optional().allow(''),
      }),
    ),
  });

  return (
    <Box my={6}>
      <form
        onSubmit={handleSubmit(values => {
          const data = { ...values };

          delete data.dateStart;
          delete data.dateEnd;

          Object.keys(values).forEach(key => {
            if (values[key] === '') {
              delete data[key];
            }
          });

          return onSubmit({
            ...data,
            dateRange: [
              dayjs(values.dateStart).format('YYYY-MM-DD'),
              dayjs(values.dateEnd).format('YYYY-MM-DD'),
            ],
          });
        })}
      >
        <SimpleGrid
          gap={6}
          templateColumns={{ base: '1fr', lg: 'repeat(3, 1fr)' }}
        >
          <HStack align="flex-end">
            <FormControl id="dateStart" isInvalid={errors.dateStart}>
              <FormLabel fontSize="sm">Sign ups between</FormLabel>
              <Controller
                name="dateStart"
                control={control}
                render={({ onChange, value }) => {
                  return (
                    <DatePicker
                      dateFormat="dd/MM/yyyy"
                      popperPlacement="bottom"
                      customInput={<Input size="sm" />}
                      selected={value}
                      openToDate={dayjs().toDate()}
                      onChange={onChange}
                      disabledKeyboardNavigation
                      placeholderText="dd/mm/yyyy"
                    />
                  );
                }}
              />
            </FormControl>
            <Text>to</Text>
            <FormControl id="dateEnd" isInvalid={errors.dateEnd}>
              <Controller
                name="dateEnd"
                control={control}
                render={({ onChange, value }) => {
                  return (
                    <DatePicker
                      dateFormat="dd/MM/yyyy"
                      popperPlacement="bottom"
                      customInput={<Input size="sm" />}
                      selected={value}
                      openToDate={dayjs().toDate()}
                      onChange={onChange}
                      disabledKeyboardNavigation
                      placeholderText="dd/mm/yyyy"
                    />
                  );
                }}
              />
            </FormControl>
          </HStack>
          <FormControl id="name" isInvalid={errors.name}>
            <FormLabel fontSize="sm">Partner Name</FormLabel>
            <Input name="name" size="sm" ref={register} />
          </FormControl>
          <FormControl id="email" isInvalid={errors.email}>
            <FormLabel fontSize="sm">Partner Email</FormLabel>
            <Input name="email" size="sm" ref={register} />
          </FormControl>
          <FormControl id="category" isInvalid={errors.category}>
            <FormLabel fontSize="sm">Category</FormLabel>
            <Input name="category" size="sm" ref={register} />
          </FormControl>
        </SimpleGrid>
        <ButtonGroup mt={6}>
          <Button
            type="reset"
            variant="ghost"
            size="sm"
            onClick={() => {
              onReset();
              return reset({
                dateStart: dayjs().startOf('month').toDate(),
                dateEnd: dayjs().endOf('month').toDate(),
              });
            }}
          >
            Reset
          </Button>
          <Button type="submit" size="sm" isLoading={submitting}>
            Apply
          </Button>
        </ButtonGroup>
      </form>
    </Box>
  );
};

export default PartnersFilter;
