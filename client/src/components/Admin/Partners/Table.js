import React, { useMemo } from 'react';
import { Link } from 'react-router-dom';
import {
  Badge,
  Link as ChakraLink,
  ButtonGroup,
  IconButton,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import { AiFillEdit } from 'react-icons/ai';

import Table from '../../Table';
import DeleteDialog from '../../DeleteDialog';

const PartnersTable = ({ data }) => {
  const queryClient = useQueryClient();
  const toast = useToast();

  const mutation = useMutation(
    id => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/partners/${id}`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries('partners');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const handleOnDelete = id => {
    return mutation.mutate(id);
  };

  const columns = useMemo(
    () => [
      {
        key: 'name',
        Header: 'Name',
        accessor: 'name',
        width: 230,
      },
      {
        key: 'email',
        Header: 'Email',
        accessor: 'email',
        width: 350,
        disableSortBy: true,
        customRender: table => (
          <ChakraLink color="cerulean.500" href={`mailto:${table.cell.value}`}>
            {table.cell.value}
          </ChakraLink>
        ),
      },
      {
        key: 'category',
        Header: 'Category',
        accessor: 'category',
        width: 200,
      },
      {
        key: 'registrationsCount',
        Header: 'No. of Sign Ups',
        accessor: 'registrationsCount',
        width: 100,
        cellAlign: 'center',
      },
      {
        key: 'useActivationCode',
        Header: 'Use Activation Code?',
        accessor: 'useActivationCode',
        width: 115,
        cellAlign: 'center',
        customRender: table => (
          <Badge colorScheme={table.cell.value ? 'niagara' : 'froly'}>
            {table.cell.value ? 'Yes' : 'No'}
          </Badge>
        ),
      },
      {
        key: 'blockRegistration',
        Header: 'Block Registration?',
        accessor: 'blockRegistration',
        width: 115,
        cellAlign: 'center',
        customRender: table => (
          <Badge colorScheme={table.cell.value ? 'niagara' : 'froly'}>
            {table.cell.value ? 'Yes' : 'No'}
          </Badge>
        ),
      },
      {
        key: 'enabled',
        Header: 'Enabled?',
        accessor: 'enabled',
        width: 85,
        cellAlign: 'center',
        customRender: table => (
          <Badge colorScheme={table.cell.value ? 'niagara' : 'froly'}>
            {table.cell.value ? 'Yes' : 'No'}
          </Badge>
        ),
      },
      {
        key: 'actions',
        Header: 'Actions',
        accessor: 'actions',
        width: 100,
        cellAlign: 'center',
        disableSortBy: true,
        customRender: table => (
          <ButtonGroup spacing={0}>
            <IconButton
              as={Link}
              icon={<AiFillEdit />}
              borderTopRightRadius={0}
              borderBottomRightRadius={0}
              size="sm"
              to={`/admin/partners/edit/${table.row.original.id}`}
            />
            <DeleteDialog
              subject="Partner"
              onDelete={() => handleOnDelete(table.row.original.id)}
              buttonProps={{
                borderTopLeftRadius: 0,
                borderBottomLeftRadius: 0,
                size: 'sm',
              }}
            />
          </ButtonGroup>
        ),
      },
    ],
    [],
  );

  return <Table columns={columns} data={data} />;
};

export default PartnersTable;
