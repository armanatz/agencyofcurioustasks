import React, { useMemo } from 'react';
import { Box, Text, Badge, Code, Tooltip, useToast } from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import dayjs from 'dayjs';

import Table from '../../Table';
import DeleteDialog from '../../DeleteDialog';

const GiftCardsTable = ({ data }) => {
  const queryClient = useQueryClient();
  const toast = useToast();

  const mutation = useMutation(
    id => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/gift_cards/${id}`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries('giftCards');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const handleOnDelete = id => {
    return mutation.mutate(id);
  };

  const columns = useMemo(
    () => [
      {
        key: 'code',
        Header: 'Gift Card Code',
        accessor: 'code',
        disableSortBy: true,
        customRender: table => <Code>{table.cell.value}</Code>,
      },
      {
        key: 'value',
        Header: 'Amount',
        accessor: 'value',
        cellAlign: 'center',
        customRender: table => (
          <Text>
            {table.row.original.currency.toUpperCase()} {table.cell.value}
          </Text>
        ),
      },
      {
        key: 'redeemed',
        Header: 'Redeemed?',
        accessor: 'redeemed',
        cellAlign: 'center',
        customRender: table => (
          <>
            <Badge colorScheme={table.cell.value ? 'niagara' : 'froly'}>
              {table.cell.value ? 'Yes' : 'No'}
            </Badge>
            {table.cell.value && (
              <Tooltip label={table.row.original.redeemedBy.email}>
                <Text mt={2}>
                  By:{' '}
                  <Box
                    as="span"
                    color="cerulean.500"
                    fontWeight="bold"
                    _hover={{ cursor: 'pointer' }}
                  >
                    {table.row.original.redeemedBy.fullName}
                  </Box>
                </Text>
              </Tooltip>
            )}
          </>
        ),
      },
      {
        key: 'redeemedAt',
        Header: 'Date of Redemption',
        accessor: 'redeemedAt',
        cellAlign: 'center',
        customRender: table =>
          table.cell.value !== null
            ? dayjs(table.cell.value).format('DD/MM/YYYY hh:mm A')
            : '-',
      },
      {
        key: 'createdAt',
        Header: 'Date of Creation',
        accessor: 'createdAt',
        cellAlign: 'center',
        customRender: table =>
          dayjs(table.cell.value).format('DD/MM/YYYY hh:mm A'),
      },
      {
        key: 'actions',
        Header: 'Actions',
        accessor: 'actions',
        cellAlign: 'center',
        disableSortBy: true,
        customRender: table => (
          <DeleteDialog
            subject="gift card"
            onDelete={() => handleOnDelete(table.row.original.id)}
            buttonProps={{
              size: 'sm',
            }}
          />
        ),
      },
    ],
    [],
  );

  return <Table columns={columns} data={data} fixedWidth={false} />;
};

export default GiftCardsTable;
