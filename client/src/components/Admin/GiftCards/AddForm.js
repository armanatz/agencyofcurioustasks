import React from 'react';
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Button,
  VStack,
  NumberInput,
  NumberInputField,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const GiftCardAddForm = ({ onSubmit, submitting }) => {
  const { handleSubmit, errors, register } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        value: Joi.number().required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <VStack spacing={6} align="flex-start" mb={6}>
        <FormControl id="value" isInvalid={errors.value}>
          <FormLabel>Gift Card Value (MYR)</FormLabel>
          <NumberInput min={1} w="30%">
            <NumberInputField name="value" ref={register} />
          </NumberInput>
          <FormErrorMessage>
            {errors.value?.type === 'number.base' &&
              'Gift Card Value is required and has to be a number.'}
          </FormErrorMessage>
        </FormControl>
        <Button type="submit" isLoading={submitting}>
          Save
        </Button>
      </VStack>
    </form>
  );
};

export default GiftCardAddForm;
