import React from 'react';
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Button,
  FormHelperText,
  VStack,
  NumberInput,
  NumberInputField,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const SettingsForm = ({ data, onSubmit, submitting }) => {
  const defaultValues = {};

  data.forEach(obj => {
    defaultValues[obj.name] = obj.value;
  });

  const { handleSubmit, errors, register } = useForm({
    reValidateMode: 'onBlur',
    defaultValues,
    resolver: joiResolver(
      Joi.object({
        referralDiscount: Joi.number().required(),
        orderProcessingDays: Joi.number().required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <VStack spacing={6} align="flex-start">
        <FormControl id="referralDiscount" isInvalid={errors.referralDiscount}>
          <FormLabel>Referral Discount Amount (MYR)</FormLabel>
          <NumberInput min={1} w="20%">
            <NumberInputField name="referralDiscount" ref={register} />
          </NumberInput>
          <FormErrorMessage>
            {errors.referralDiscount?.type === 'number.base' &&
              'Referral Discount Amount is required and has to be a number.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl
          id="orderProcessingDays"
          isInvalid={errors.orderProcessingDays}
        >
          <FormLabel>Order Processing Time</FormLabel>
          <NumberInput min={1} w="20%">
            <NumberInputField name="orderProcessingDays" ref={register} />
          </NumberInput>
          <FormHelperText>This value should be in days</FormHelperText>
          <FormErrorMessage>
            {errors.orderProcessingDays?.type === 'number.base' &&
              'Order Processing Duration is required and has to be a number.'}
          </FormErrorMessage>
        </FormControl>
        <Button type="submit" isLoading={submitting}>
          Save
        </Button>
      </VStack>
    </form>
  );
};

export default SettingsForm;
