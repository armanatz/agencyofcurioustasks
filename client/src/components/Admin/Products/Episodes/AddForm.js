import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Box,
  Flex,
  Grid,
  GridItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
  Textarea,
  Checkbox,
  FormHelperText,
  Image,
  Text,
  Select,
  Wrap,
  WrapItem,
  ButtonGroup,
  IconButton,
  NumberInput,
  NumberInputField,
} from '@chakra-ui/react';
import { AiOutlineClose } from 'react-icons/ai';
import { useForm } from 'react-hook-form';
import { useDropzone } from 'react-dropzone';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const EpisodeAddForm = ({ seasons, onSubmit, submitting }) => {
  const [otherImages, setOtherImages] = useState([]);
  const [coverImage, setCoverImage] = useState([]);
  const [selectedSeason, setSelectedSeason] = useState('1');

  const history = useHistory();

  const { handleSubmit, errors, register, formState } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        seasonId: Joi.any().required(),
        title: Joi.string().required(),
        description: Joi.string().min(100).required(),
        episodeNumber: Joi.string().required(),
        active: Joi.boolean().required(),
        comingSoon: Joi.boolean().required(),
        boxContents: Joi.string().optional().allow(''),
        onlineGames: Joi.string().optional().allow(''),
        ...(selectedSeason === '1' && {
          price: Joi.number().required(),
        }),
      }),
    ),
  });

  const {
    getRootProps: coverImageRootProps,
    getInputProps: coverImageInputProps,
  } = useDropzone({
    accept: 'image/jpeg, image/png',
    onDrop: useCallback(acceptedFiles => {
      setCoverImage(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    }, []),
    multiple: false,
    maxFiles: 1,
  });

  const {
    getRootProps: otherImagesRootProps,
    getInputProps: otherImagesInputProps,
  } = useDropzone({
    accept: 'image/jpeg, image/png',
    onDrop: useCallback(
      acceptedFiles => {
        const images = [...otherImages, ...acceptedFiles];
        setOtherImages(
          images.map(file => {
            if (!file.preview) {
              return Object.assign(file, {
                preview: URL.createObjectURL(file),
              });
            }
            return file;
          }),
        );
      },
      [otherImages],
    ),
    validator: useCallback(
      file => {
        if (otherImages.every(image => image.name !== file.name)) {
          return null;
        }
        return {
          code: 'duplicate-file',
          message:
            'There is another file with the same name in list. Please either rename the file or select another one.',
        };
      },
      [otherImages],
    ),
  });

  useEffect(
    () => () => {
      coverImage.forEach(file => URL.revokeObjectURL(file.preview));
      otherImages.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [coverImage, otherImages],
  );

  return (
    <form
      onSubmit={handleSubmit(values =>
        onSubmit({
          ...values,
          seasonId: parseInt(values.seasonId, 10),
          coverImage: coverImage[0],
          otherImages,
        }),
      )}
    >
      <Grid
        gap={6}
        templateColumns={['1fr', 'repeat(4, 1fr)']}
        templateRows="auto"
        py={6}
      >
        <GridItem colSpan={[1, 2]}>
          <FormControl id="seasonId">
            <FormLabel>Season</FormLabel>
            <Select
              name="seasonId"
              ref={register}
              onChange={e => setSelectedSeason(e.target.value)}
            >
              {seasons?.map(season => (
                <option key={season.id} value={season.id}>
                  {season.title} (S{season.seasonNumber})
                </option>
              ))}
            </Select>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[1, 2]}>
          <FormControl id="title" isInvalid={errors.title}>
            <FormLabel>Title</FormLabel>
            <Input name="title" ref={register} />
            <FormErrorMessage>
              {errors.title?.type === 'string.empty' && 'Title is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[1, 4]}>
          <FormControl id="description" isInvalid={errors.description}>
            <FormLabel>Description</FormLabel>
            <Textarea rows="5" name="description" ref={register} />
            <FormErrorMessage>
              {errors.description?.type === 'string.empty' &&
                'Description is required.'}
              {errors.description?.type === 'string.min' &&
                'Description should be a minimum of 100 characters.'}
            </FormErrorMessage>
            <FormHelperText>Minimum of 100 characters required.</FormHelperText>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="episodeNumber" isInvalid={errors.episodeNumber}>
            <FormLabel>Episode Number</FormLabel>
            <Input name="episodeNumber" ref={register} />
            <FormErrorMessage>
              {errors.episodeNumber?.type === 'string.empty' &&
                'Episode Number is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        {selectedSeason === '1' && (
          <GridItem>
            <FormControl id="price" isInvalid={errors.price}>
              <FormLabel>Price (MYR)</FormLabel>
              <NumberInput min={0}>
                <NumberInputField name="price" ref={register} />
              </NumberInput>
              <FormErrorMessage>
                {errors.price?.type === 'number.base' &&
                  'Price is required and must be a number.'}
              </FormErrorMessage>
            </FormControl>
          </GridItem>
        )}
        <GridItem>
          <FormControl id="active">
            <Checkbox name="active" ref={register} h="40px">
              Active?
            </Checkbox>
            <FormHelperText>
              If active, then the episode will be visible to the public.
            </FormHelperText>
          </FormControl>
        </GridItem>
        <GridItem>
          <FormControl id="comingSoon">
            <Checkbox name="comingSoon" ref={register} h="40px">
              Coming Soon?
            </Checkbox>
            <FormHelperText>
              If coming soon, then the episode will be visible to the public;
              just not accessible.
            </FormHelperText>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[1, 2]}>
          <FormControl id="boxContents">
            <FormLabel>Box Contents</FormLabel>
            <Textarea rows="5" name="boxContents" ref={register} />
            <FormHelperText>
              Each item should be separated by a new line.
            </FormHelperText>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[1, 2]}>
          <FormControl id="onlineGames">
            <FormLabel>Online Games</FormLabel>
            <Textarea rows="5" name="onlineGames" ref={register} />
            <FormHelperText>
              Each item should be separated by a new line.
            </FormHelperText>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[1, 2]}>
          <FormControl
            isInvalid={coverImage.length === 0 && formState.isSubmitted}
          >
            <FormLabel>Cover Image</FormLabel>
            <Box
              bg={
                coverImage.length === 0 && formState.isSubmitted
                  ? 'gray.100'
                  : 'linen.500'
              }
              borderRadius="md"
              border="2px solid"
              borderColor={
                coverImage.length === 0 && formState.isSubmitted
                  ? 'froly.500'
                  : 'linen.600'
              }
              my={4}
              cursor="pointer"
            >
              <Flex
                {...coverImageRootProps()}
                minH="100px"
                flexDir="column"
                justifyContent="center"
                textAlign="center"
                _focus={{ outline: 'none' }}
              >
                <input {...coverImageInputProps()} />
                <Box textAlign="center" p={2}>
                  {coverImage.length > 0 ? (
                    <>
                      <Image
                        src={coverImage[0].preview}
                        mx="auto"
                        maxW="200px"
                      />
                      <Text fontSize="12px" mt={2}>
                        Click to change image.
                      </Text>
                    </>
                  ) : (
                    <Text color="linen.700">
                      Drag and Drop or click to browse your device.
                    </Text>
                  )}
                </Box>
              </Flex>
            </Box>
            <FormHelperText>Max file size is 10MB.</FormHelperText>
            <FormErrorMessage>
              {coverImage.length === 0 &&
                formState.isSubmitted &&
                'Cover image is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[1, 2]}>
          <FormControl
            isInvalid={otherImages.length === 0 && formState.isSubmitted}
          >
            <FormLabel>Other Images</FormLabel>
            <Box
              bg={
                otherImages.length === 0 && formState.isSubmitted
                  ? 'gray.100'
                  : 'linen.500'
              }
              borderRadius="md"
              border="2px solid"
              borderColor={
                otherImages.length === 0 && formState.isSubmitted
                  ? 'froly.500'
                  : 'linen.600'
              }
              my={4}
              cursor="pointer"
            >
              <Flex
                {...otherImagesRootProps()}
                minH="100px"
                flexDir="column"
                justifyContent="center"
                _focus={{ outline: 'none' }}
              >
                <input {...otherImagesInputProps()} />
                <Box textAlign="center" p={2}>
                  <Text color="linen.700">
                    Drag & Drop files here or click to select files.
                  </Text>
                </Box>
              </Flex>
            </Box>
            <>
              <Wrap spacing={6} mb={4}>
                {otherImages.map((file, i) => (
                  <WrapItem key={file.name}>
                    <Box position="relative">
                      <Image
                        boxSize="100px"
                        objectFit="cover"
                        src={file.preview}
                      />
                      <IconButton
                        size="xs"
                        variant="dangerSolid"
                        icon={<AiOutlineClose />}
                        isRound
                        position="absolute"
                        right="-10px"
                        top="-10px"
                        onClick={() => {
                          const arr = otherImages.filter(
                            image => image.name !== otherImages[i].name,
                          );
                          setOtherImages(arr);
                        }}
                      />
                    </Box>
                  </WrapItem>
                ))}
              </Wrap>
            </>
            <FormHelperText>Max size per file is 10MB</FormHelperText>
            <FormErrorMessage>
              {otherImages.length === 0 &&
                formState.isSubmitted &&
                'At least one image is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
      </Grid>
      <ButtonGroup mb={6}>
        <Button onClick={() => history.push('/admin/products')} variant="ghost">
          Cancel
        </Button>
        <Button type="submit" isLoading={submitting}>
          Save
        </Button>
      </ButtonGroup>
    </form>
  );
};

export default EpisodeAddForm;
