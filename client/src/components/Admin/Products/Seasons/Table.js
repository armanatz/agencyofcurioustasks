import React, { useMemo, useState } from 'react';
import { Link } from 'react-router-dom';
import {
  Text,
  Badge,
  Link as ChakraLink,
  ButtonGroup,
  IconButton,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import { AiFillEdit } from 'react-icons/ai';

import Table from '../../../Table';
import DeleteDialog from '../../../DeleteDialog';

const SeasonsTable = ({ data }) => {
  const [loading, setLoading] = useState(false);
  const queryClient = useQueryClient();
  const toast = useToast();

  const mutation = useMutation(
    id => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/seasons/${id}`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('episodes');
        queryClient.invalidateQueries('seasons');
        return setLoading(false);
      },
      onError: () => {
        setLoading(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const user = queryClient.getQueryData('user');
  const canDelete = user.roles.some(role => {
    switch (role) {
      case 'Superuser':
      case 'Admin':
      case 'Editor':
        return true;
      default:
        return false;
    }
  });

  const handleOnDelete = id => {
    setLoading(true);
    return mutation.mutate(id);
  };

  const columns = useMemo(
    () => [
      {
        key: 'seasonNumber',
        Header: 'Season Number',
        accessor: 'seasonNumber',
        width: 105,
        cellAlign: 'center',
      },
      {
        key: 'title',
        Header: 'Title',
        accessor: 'title',
        width: 230,
        customRender: table => (
          <Text isTruncated noOfLines={2}>
            {table.cell.value}
          </Text>
        ),
      },
      {
        key: 'description',
        Header: 'Description',
        accessor: 'description',
        width: 330,
        disableSortBy: true,
        customRender: table => (
          <Text isTruncated noOfLines={2}>
            {table.cell.value}
          </Text>
        ),
      },
      {
        key: 'totalPlayableEpisodes',
        Header: 'Playable Episodes',
        accessor: 'totalPlayableEpisodes',
        width: 105,
        cellAlign: 'center',
      },
      {
        key: 'coverImageUrl',
        Header: 'Cover Image',
        accessor: 'coverImageUrl',
        width: 85,
        cellAlign: 'center',
        disableSortBy: true,
        customRender: table => (
          <ChakraLink
            href={table.cell.value}
            target="_blank"
            rel="noopener noreferrer"
            color="cerulean.500"
            _hover={{ color: 'cerulean.800', textDecoration: 'underline' }}
          >
            View
          </ChakraLink>
        ),
      },
      {
        key: 'active',
        Header: 'Active?',
        accessor: 'active',
        width: 85,
        cellAlign: 'center',
        customRender: table => (
          <Badge colorScheme={table.cell.value ? 'niagara' : 'froly'}>
            {table.cell.value ? 'Yes' : 'No'}
          </Badge>
        ),
      },
      {
        key: 'comingSoon',
        Header: 'Coming Soon?',
        accessor: 'comingSoon',
        width: 85,
        cellAlign: 'center',
        customRender: table => (
          <Badge colorScheme={table.cell.value ? 'niagara' : 'froly'}>
            {table.cell.value ? 'Yes' : 'No'}
          </Badge>
        ),
      },
      {
        key: 'actions',
        Header: 'Actions',
        accessor: 'actions',
        width: 100,
        cellAlign: 'center',
        disableSortBy: true,
        customRender: table => {
          if (canDelete) {
            return (
              <ButtonGroup spacing={0}>
                <IconButton
                  as={Link}
                  icon={<AiFillEdit />}
                  borderTopRightRadius={
                    table.row.original.seasonNumber !== 0 ? 0 : 'md'
                  }
                  borderBottomRightRadius={
                    table.row.original.seasonNumber !== 0 ? 0 : 'md'
                  }
                  size="sm"
                  to={`/admin/products/season/edit/${table.row.original.id}`}
                />
                {table.row.original.seasonNumber !== 0 && (
                  <DeleteDialog
                    subject="season"
                    onDelete={() => handleOnDelete(table.row.original.id)}
                    buttonProps={{
                      borderTopLeftRadius: 0,
                      borderBottomLeftRadius: 0,
                      size: 'sm',
                    }}
                    loading={loading}
                  />
                )}
              </ButtonGroup>
            );
          }
          return (
            <IconButton
              as={Link}
              icon={<AiFillEdit />}
              size="sm"
              to={`/admin/products/season/edit/${table.row.original.id}`}
            />
          );
        },
      },
    ],
    [],
  );

  return <Table columns={columns} data={data} />;
};

export default SeasonsTable;
