import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Box,
  Flex,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
  Textarea,
  Checkbox,
  NumberInput,
  NumberInputField,
  FormHelperText,
  Tooltip,
  Image,
  Text,
  ButtonGroup,
  HStack,
  VStack,
} from '@chakra-ui/react';
import { useDropzone } from 'react-dropzone';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const SeasonAddForm = ({ onSubmit, submitting }) => {
  const [image, setImage] = useState([]);

  const history = useHistory();

  const { handleSubmit, errors, register, formState } = useForm({
    reValidateMode: 'onBlur',
    resolver: joiResolver(
      Joi.object({
        title: Joi.string().required(),
        description: Joi.string().min(100).required(),
        seasonNumber: Joi.number().min(0).integer().required(),
        totalPlayableEpisodes: Joi.number().min(0).integer().required(),
        active: Joi.boolean().required(),
        comingSoon: Joi.boolean().required(),
        price: Joi.number().required(),
      }),
    ),
  });

  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/jpeg, image/png',
    onDrop: acceptedFiles => {
      setImage(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    },
    maxFiles: 1,
  });

  useEffect(
    () => () => {
      image.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [image],
  );

  return (
    <form
      onSubmit={handleSubmit(values =>
        onSubmit({
          ...values,
          price: values.price * 100,
          coverImage: image[0],
        }),
      )}
    >
      <VStack spacing={6} py={6} align="flex-start">
        <FormControl id="title" isInvalid={errors.title} w={['full', '50%']}>
          <FormLabel>Title</FormLabel>
          <Input name="title" ref={register} />
          <FormErrorMessage>
            {errors.title?.type === 'string.empty' && 'Title is required.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl id="description" isInvalid={errors.description}>
          <FormLabel>Description</FormLabel>
          <Textarea rows="5" name="description" ref={register} />
          <FormErrorMessage>
            {errors.description?.type === 'string.empty' &&
              'Description is required.'}
            {errors.description?.type === 'string.min' &&
              'Description should be a minimum of 100 characters.'}
          </FormErrorMessage>
          <FormHelperText>Minimum of 100 characters required</FormHelperText>
        </FormControl>
        <HStack align="flex-start" w="full" spacing={6}>
          <FormControl
            id="seasonNumber"
            isInvalid={errors.seasonNumber}
            w={['full', '25%']}
          >
            <FormLabel>Season Number</FormLabel>
            <NumberInput min={0}>
              <NumberInputField name="seasonNumber" ref={register} />
            </NumberInput>
            <FormErrorMessage>
              {errors.seasonNumber?.type === 'number.base' &&
                'Season Number is required and must be a number.'}
            </FormErrorMessage>
          </FormControl>
          <FormControl
            id="totalPlayableEpisodes"
            isInvalid={errors.totalPlayableEpisodes}
            w={['full', '25%']}
          >
            <FormLabel>Total Playable Episodes</FormLabel>
            <Tooltip
              closeOnClick={false}
              placement="top"
              backgroundColor="niagara.500"
              label="Only input the number of episodes that will be sent to the
                  customer and NOT the total number of episodes in this season
                  as this is used for billing cycle calculation."
            >
              <NumberInput min={0}>
                <NumberInputField name="totalPlayableEpisodes" ref={register} />
              </NumberInput>
            </Tooltip>
            <FormErrorMessage>
              {errors.totalPlayableEpisodes?.type === 'number.base' &&
                'Total Playable Episodes is required and must be a number.'}
            </FormErrorMessage>
          </FormControl>
        </HStack>
        <HStack align="flex-start" w="full" spacing={6}>
          <FormControl id="active" w={['full', '25%']}>
            <Checkbox name="active" ref={register} h="40px">
              Active?
            </Checkbox>
            <FormHelperText>
              If active, then the season will be visible to the public.
            </FormHelperText>
          </FormControl>
          <FormControl id="comingSoon" w={['full', '25%']}>
            <Checkbox name="comingSoon" ref={register} h="40px">
              Coming Soon?
            </Checkbox>
            <FormHelperText>
              If coming soon, then the season will be visible to the public;
              just not accessible.
            </FormHelperText>
          </FormControl>
        </HStack>
        <FormControl id="price" isInvalid={errors.price} w={['full', '25%']}>
          <FormLabel>Monthly Price (MYR)</FormLabel>
          <NumberInput min={0}>
            <NumberInputField name="price" ref={register} />
          </NumberInput>
          <FormErrorMessage>
            {errors.price?.type === 'number.base' &&
              'Price is required and must be a number.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl
          w={['full', '50%']}
          isInvalid={image.length === 0 && formState.isSubmitted}
        >
          <FormLabel>Cover Image</FormLabel>
          <Box
            bg={
              image.length === 0 && formState.isSubmitted
                ? 'gray.100'
                : 'linen.500'
            }
            borderRadius="md"
            border="2px solid"
            borderColor={
              image.length === 0 && formState.isSubmitted
                ? 'froly.500'
                : 'linen.600'
            }
            my={4}
            cursor="pointer"
          >
            <Flex
              {...getRootProps()}
              minH="100px"
              flexDir="column"
              justifyContent="center"
              _focus={{ outline: 'none' }}
            >
              <input {...getInputProps()} />
              <Box textAlign="center" p={2}>
                {image.length > 0 ? (
                  <>
                    <Image src={image[0].preview} mx="auto" />
                    <Text fontSize="12px" mt={2}>
                      Click to change image.
                    </Text>
                  </>
                ) : (
                  <Text color="linen.700">
                    Drag and Drop or click to browse your device.
                  </Text>
                )}
              </Box>
            </Flex>
          </Box>
          <FormHelperText>Max file size is 10MB</FormHelperText>
          <FormErrorMessage>
            {image.length === 0 &&
              formState.isSubmitted &&
              'Cover image is required.'}
          </FormErrorMessage>
        </FormControl>
        <ButtonGroup>
          <Button
            onClick={() => history.push('/admin/products')}
            variant="ghost"
          >
            Cancel
          </Button>
          <Button type="submit" isLoading={submitting}>
            Save
          </Button>
        </ButtonGroup>
      </VStack>
    </form>
  );
};

export default SeasonAddForm;
