import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Box,
  Flex,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
  Textarea,
  Checkbox,
  NumberInput,
  NumberInputField,
  FormHelperText,
  Image,
  Text,
  ButtonGroup,
  VStack,
  HStack,
} from '@chakra-ui/react';
import { useQueryClient } from 'react-query';
import { useDropzone } from 'react-dropzone';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const SeasonEditForm = ({ data, onSubmit, submitting }) => {
  const [image, setImage] = useState([{ preview: data?.coverImageUrl }]);

  const queryClient = useQueryClient();
  const history = useHistory();

  const { handleSubmit, errors, register } = useForm({
    reValidateMode: 'onBlur',
    defaultValues: {
      title: data?.title,
      description: data?.description,
      active: data?.active,
      comingSoon: data?.comingSoon,
      ...(data.stripeProductId !== null && {
        price: data?.prices[0].unitAmount / 100,
      }),
    },
    resolver: joiResolver(
      Joi.object({
        title: Joi.string().required(),
        description: Joi.string().min(100).required(),
        active: Joi.boolean().required(),
        comingSoon: Joi.boolean().required(),
        ...(data.stripeProductId !== null && {
          price: Joi.number().required(),
        }),
      }),
    ),
  });

  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/jpeg, image/png',
    onDrop: acceptedFiles => {
      setImage(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    },
    maxFiles: 1,
  });

  useEffect(
    () => () => {
      image.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [image],
  );

  const user = queryClient.getQueryData('user');

  const canEdit = user.roles.some(role => {
    switch (role) {
      case 'Superuser':
      case 'Admin':
      case 'Editor':
        return true;
      default:
        return false;
    }
  });

  return (
    <form
      onSubmit={handleSubmit(values =>
        onSubmit({
          ...values,
          ...(data.prices.length > 0 && { price: values.price * 100 }),
          ...(image[0].preview !== data.coverImageUrl && {
            coverImage: image[0],
          }),
        }),
      )}
    >
      <VStack spacing={6} py={6} align="flex-start">
        <FormControl id="title" isInvalid={errors.title} w={['full', '50%']}>
          <FormLabel>Title</FormLabel>
          <Input name="title" ref={register} />
          <FormErrorMessage>
            {errors.title?.type === 'string.empty' && 'Title is required.'}
          </FormErrorMessage>
        </FormControl>
        <FormControl id="description" isInvalid={errors.description}>
          <FormLabel>Description</FormLabel>
          <Textarea rows="5" name="description" ref={register} />
          <FormErrorMessage>
            {errors.description?.type === 'string.empty' &&
              'Description is required.'}
            {errors.description?.type === 'string.min' &&
              'Description should be a minimum of 100 characters.'}
          </FormErrorMessage>
          <FormHelperText>Minimum of 100 characters required</FormHelperText>
        </FormControl>
        <HStack align="flex-start" w="full" spacing={6}>
          <FormControl id="active" w={['full', '25%']}>
            <Checkbox name="active" ref={register} h="40px">
              Active?
            </Checkbox>
            <FormHelperText>
              If active, then the season will be visible to the public.
            </FormHelperText>
          </FormControl>
          <FormControl id="comingSoon" w={['full', '25%']}>
            <Checkbox name="comingSoon" ref={register} h="40px">
              Coming Soon?
            </Checkbox>
            <FormHelperText>
              If coming soon, then the season will be visible to the public;
              just not accessible.
            </FormHelperText>
          </FormControl>
        </HStack>
        {data.seasonNumber !== 0 && (
          <FormControl id="price" isInvalid={errors.price} w={['full', '25%']}>
            <FormLabel>Monthly Price (MYR)</FormLabel>
            <NumberInput min={0}>
              <NumberInputField name="price" ref={register} />
            </NumberInput>
            <FormErrorMessage>
              {errors.price?.type === 'number.base' &&
                'Price is required and must be a number.'}
            </FormErrorMessage>
          </FormControl>
        )}
        <FormControl w={['full', '50%']}>
          <FormLabel>Cover Image</FormLabel>
          <Box
            bg="linen.500"
            borderRadius="md"
            border="2px solid"
            borderColor="linen.600"
            my={4}
            cursor="pointer"
          >
            <Flex
              {...getRootProps()}
              minH="100px"
              flexDir="column"
              justifyContent="center"
              _focus={{ outline: 'none' }}
            >
              <input {...getInputProps()} />
              <Box textAlign="center" p={2}>
                {image.length > 0 ? (
                  <>
                    <Image src={image[0].preview} mx="auto" />
                    <Text fontSize="12px" mt={2}>
                      Click to change image.
                    </Text>
                  </>
                ) : (
                  <Text color="linen.700">
                    Drag and Drop or click to browse your device.
                  </Text>
                )}
              </Box>
            </Flex>
          </Box>
          <FormHelperText>Max file size is 10MB</FormHelperText>
        </FormControl>
        <ButtonGroup>
          <Button
            onClick={() => history.push('/admin/products')}
            variant="ghost"
          >
            Cancel
          </Button>
          {canEdit && (
            <Button type="submit" isLoading={submitting}>
              Save
            </Button>
          )}
        </ButtonGroup>
      </VStack>
    </form>
  );
};

export default SeasonEditForm;
