import React, { useMemo } from 'react';
import { Badge, Code, useToast } from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';

import Table from '../../../Table';
import DeleteDialog from '../../../DeleteDialog';

const ActivationCodesTable = ({ data, partners }) => {
  const queryClient = useQueryClient();
  const toast = useToast();

  const episodes = queryClient.getQueryData('episodes');
  const user = queryClient.getQueryData('user');
  const canDelete = user.roles.some(role => {
    switch (role) {
      case 'Superuser':
      case 'Admin':
      case 'Editor':
        return true;
      default:
        return false;
    }
  });

  const mutation = useMutation(
    id => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/products/activation_codes/${id}`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries('activationCodes');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const handleOnDelete = id => {
    return mutation.mutate(id);
  };

  const columns = useMemo(
    () => [
      {
        key: 'episodeId',
        Header: 'Episode',
        accessor: 'episodeId',
        width: 350,
        customRender: table => {
          const ep = episodes.filter(
            episode => episode.id === table.cell.value,
          );
          if (ep.length > 0) {
            return ep[0].title;
          }
          // return 'N/A';
        },
      },
      {
        key: 'code',
        Header: 'Code',
        accessor: 'code',
        cellAlign: 'center',
        customRender: table => <Code>{table.cell.value}</Code>,
      },
      {
        key: 'fromPartner',
        Header: 'Partner Code?',
        accessor: 'fromPartner',
        cellAlign: 'center',
        customRender: table => (
          <Badge colorScheme={table.cell.value ? 'niagara' : 'froly'}>
            {table.cell.value ? 'Yes' : 'No'}
          </Badge>
        ),
      },
      {
        key: 'partnerId',
        Header: 'Partner',
        accessor: 'partnerId',
        width: 350,
        customRender: table => {
          const arr = partners.filter(
            partner => partner.id === table.cell.value,
          );
          if (arr.length > 0) {
            return arr[0].name;
          }
          return 'N/A';
        },
      },
      {
        key: 'actions',
        Header: 'Actions',
        accessor: 'actions',
        cellAlign: 'center',
        disableSortBy: true,
        customRender: table =>
          canDelete && (
            <DeleteDialog
              subject="activation code"
              onDelete={() => handleOnDelete(table.row.original.id)}
              buttonProps={{
                size: 'sm',
                w: '50%',
              }}
            />
          ),
      },
    ],
    [],
  );

  return <Table columns={columns} data={data} />;
};

export default ActivationCodesTable;
