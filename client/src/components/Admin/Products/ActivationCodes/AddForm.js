import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Grid,
  GridItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
  Checkbox,
  FormHelperText,
  ButtonGroup,
  Select,
} from '@chakra-ui/react';
import { useQuery } from 'react-query';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

import { fetchAllEpisodes } from '../../../../queries/Episodes';
import { fetchPartners } from '../../../../queries/Partners';

import Loading from '../../../Loading';

const ActivationCodeAddForm = ({ onSubmit, submitting }) => {
  const history = useHistory();
  const [showPartnerField, setShowPartnerField] = useState(false);

  const { data: episodes, status: episodesQueryStatus } = useQuery(
    'episodes',
    fetchAllEpisodes,
  );

  const { data: partners, status: partnersQueryStatus } = useQuery(
    'partners',
    fetchPartners,
  );

  const { handleSubmit, errors, register } = useForm({
    resolver: joiResolver(
      Joi.object({
        episodeId: Joi.string().required(),
        code: Joi.string().required(),
        fromPartner: Joi.boolean().optional().allow(''),
        partnerId: Joi.any().optional().allow(''),
      }),
    ),
  });

  if (episodesQueryStatus === 'loading' || partnersQueryStatus === 'loading') {
    return <Loading />;
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid
        gap={6}
        templateColumns={['1fr', 'repeat(4, 1fr)']}
        templateRows="auto"
        py={6}
      >
        <GridItem colSpan={[1, 2]}>
          <FormControl id="episodeId" isInvalid={errors.episodeId}>
            <FormLabel>Episode</FormLabel>
            <Select name="episodeId" ref={register}>
              {episodes.map(episode => (
                <option key={episode.id} value={episode.id}>
                  {episode.title}
                </option>
              ))}
            </Select>
          </FormControl>
        </GridItem>
        <GridItem colSpan={[1, 2]}>
          <FormControl id="code" isInvalid={errors.code}>
            <FormLabel>Code</FormLabel>
            <Input name="code" ref={register} />
            <FormErrorMessage>
              {errors.code?.type === 'string.empty' && 'Code is required.'}
            </FormErrorMessage>
          </FormControl>
        </GridItem>
        {partners.length > 0 && (
          <>
            <GridItem>
              <FormControl id="fromPartner">
                <Checkbox
                  name="fromPartner"
                  ref={register}
                  h="40px"
                  onChange={e => setShowPartnerField(e.target.checked)}
                >
                  Partner Code?
                </Checkbox>
                <FormHelperText>
                  If enabled, this activation code will be used to associate
                  sign ups for the selected partner.
                </FormHelperText>
              </FormControl>
            </GridItem>
            {showPartnerField && (
              <GridItem>
                <FormControl id="partnerId" isInvalid={errors.partnerId}>
                  <FormLabel>Partner</FormLabel>
                  <Select name="partnerId" ref={register}>
                    {partners.map(partner => (
                      <option key={partner.id} value={partner.id}>
                        {partner.name}
                      </option>
                    ))}
                  </Select>
                  <FormErrorMessage>
                    {errors.partnerId?.type === 'string.empty' &&
                      'Code is required.'}
                  </FormErrorMessage>
                </FormControl>
              </GridItem>
            )}
          </>
        )}
      </Grid>
      <ButtonGroup mb={6}>
        <Button onClick={() => history.push('/admin/products')} variant="ghost">
          Cancel
        </Button>
        <Button type="submit" isLoading={submitting}>
          Save
        </Button>
      </ButtonGroup>
    </form>
  );
};

export default ActivationCodeAddForm;
