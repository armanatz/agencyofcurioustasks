import React, { useState } from 'react';
import { Box, Text, HStack, Input, Button, useToast } from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';

const ProductAssignmentForm = ({ data }) => {
  const [submitting, setSubmitting] = useState(false);
  const [username, setUsername] = useState('');
  const [activationCode, setActivationCode] = useState('');
  const toast = useToast();
  const queryClient = useQueryClient();

  const mutation = useMutation(
    data => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/products/assign?admin=true`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(data),
        },
      );
    },
    {
      onSuccess: async data => {
        setSubmitting(false);

        if (data.status !== 200) {
          const error = await data.json();
          return toast({
            title: error.message,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
        }

        setUsername('');
        setActivationCode('');

        queryClient.invalidateQueries('usersChildren');

        return toast({
          title: 'Successfully assigned product.',
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = () => {
    if (!username || username === '') {
      return toast({
        title: 'Username Required',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    if (!activationCode || activationCode === '') {
      return toast({
        title: 'Activation Code Required',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    setSubmitting(true);

    return mutation.mutate({
      username,
      activationCode: activationCode.toUpperCase(),
    });
  };

  return (
    <Box>
      <Text mb={6}>
        Assign an episode to a child using their username and the relevant
        episode{"'"}s activation code
      </Text>
      <HStack spacing={6}>
        <Input
          value={username}
          onChange={e => setUsername(e.target.value)}
          placeholder="Username of child to assign..."
          w="30%"
        />
        <Input
          value={activationCode}
          onChange={e => setActivationCode(e.target.value)}
          placeholder="Activation code of episode to be assigned..."
          w="30%"
        />
        <Button
          onClick={onSubmit}
          isLoading={submitting}
          isDisabled={username === '' || activationCode === ''}
        >
          Assign
        </Button>
      </HStack>
    </Box>
  );
};

export default ProductAssignmentForm;
