import React, { useState } from 'react';
import {
  Box,
  Text,
  HStack,
  Input,
  Select,
  Button,
  useToast,
} from '@chakra-ui/react';
import { useMutation } from 'react-query';

const ChildResetForm = ({ data }) => {
  const [submitting, setSubmitting] = useState(false);
  const [selectedChild, setSelectedChild] = useState(undefined);
  const [username, setUsername] = useState('');
  const toast = useToast();

  const mutation = useMutation(
    userId => {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');

      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/games/reset/${userId}`,
        {
          headers,
          method: 'POST',
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: async res => {
        setSubmitting(false);

        const data = await res.json();
        return toast({
          description: data.message,
          status: res.status !== 200 ? 'error' : 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onResetClick = () => {
    if (selectedChild) {
      return mutation.mutate(selectedChild);
    }
    return mutation.mutate(username);
  };

  return (
    <Box>
      <Text mb={6}>
        Type the username of the child you would like to reset the progress for
        or select one of the child accounts associated to this one from the
        dropdown
      </Text>
      <HStack spacing={6}>
        <Input
          value={username}
          onChange={e => setUsername(e.target.value)}
          placeholder="Username of child to reset..."
          w="30%"
        />
        <Select
          w="30%"
          onChange={e => setSelectedChild(e.target.value)}
          placeholder="Select child..."
        >
          {data?.members.map(child => (
            <option key={child.id} value={child.username}>
              {child.fullName}
            </option>
          ))}
        </Select>
        <Button
          onClick={onResetClick}
          isLoading={submitting}
          isDisabled={!selectedChild && username === ''}
        >
          Reset Game Progress
        </Button>
      </HStack>
    </Box>
  );
};

export default ChildResetForm;
