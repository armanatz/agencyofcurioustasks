import React from 'react';
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Button,
  VStack,
  Input,
  Select,
  FormHelperText,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const AdminAddForm = ({ onSubmit, submitting, roles }) => {
  const { handleSubmit, errors, register } = useForm({
    reValidateMode: 'onBlur',
    defaultValues: {
      role: roles[0].name,
    },
    resolver: joiResolver(
      Joi.object({
        email: Joi.string().email({ tlds: false }).required(),
        role: Joi.string().required(),
      }),
    ),
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <VStack spacing={6} align="flex-start" mb={6}>
        <FormControl id="email" isInvalid={errors.email}>
          <FormLabel>Email address of user</FormLabel>
          <Input name="email" type="email" ref={register} />
          <FormHelperText>
            This user needs to have signed up first as a parent before being
            added as an admin.
          </FormHelperText>
          <FormErrorMessage>
            A valid email address is required.
          </FormErrorMessage>
        </FormControl>
        <FormControl id="role" isInvalid={errors.role}>
          <FormLabel>Role / Permissions</FormLabel>
          <Select name="role" ref={register}>
            {roles.map(role => (
              <option key={role.id} value={role.name}>
                {role.name}
              </option>
            ))}
          </Select>
          <FormErrorMessage>
            Please select a role from the dropdown menu.
          </FormErrorMessage>
        </FormControl>
        <Button type="submit" isLoading={submitting}>
          Save
        </Button>
      </VStack>
    </form>
  );
};

export default AdminAddForm;
