import React from 'react';
import {
  Box,
  Heading,
  Text,
  Badge,
  SimpleGrid,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';

import DeleteDialog from '../../DeleteDialog';

const Item = ({ fullName, email, roles, onDelete }) => {
  const roleTags = roles.map((role, i) => (
    <Badge key={role} ml={i === 0 ? 0 : 2}>
      {role}
    </Badge>
  ));

  return (
    <SimpleGrid templateColumns="1fr 1fr 50px">
      <Text>{fullName}</Text>
      <Box>{roleTags}</Box>
      {email !== 'act@fliped.my' && (
        <DeleteDialog
          subject="admin"
          onDelete={onDelete}
          buttonProps={{
            size: 'sm',
          }}
        />
      )}
    </SimpleGrid>
  );
};

const AdminList = ({ data }) => {
  const queryClient = useQueryClient();
  const toast = useToast();

  const deleteAdminMutation = useMutation(
    id => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/users/admins/${id}`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
        },
      );
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries('admins');
      },
      onError: () => {
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const handleOnDelete = id => {
    return deleteAdminMutation.mutate(id);
  };

  return (
    <Box>
      <Box
        bgColor="linen.500"
        py={4}
        px={6}
        w="full"
        borderBottom="1px solid"
        borderBottomColor="gray.300"
      >
        <SimpleGrid templateColumns="1fr 1fr 50px">
          <Heading fontSize="12px">Name</Heading>
          <Heading fontSize="12px">Roles</Heading>
          <Heading fontSize="12px">Action</Heading>
        </SimpleGrid>
      </Box>
      {data.map((el, i) => (
        <Box key={el.id} py={4} px={6} w="full">
          <Item
            fullName={el.fullName}
            email={el.email}
            roles={el.roles.filter(role => role !== 'Adult')}
            onDelete={() => handleOnDelete(el.id)}
          />
        </Box>
      ))}
    </Box>
  );
};

export default AdminList;
