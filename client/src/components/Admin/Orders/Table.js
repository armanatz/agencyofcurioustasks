import React, { useMemo } from 'react';
import {
  Text,
  Link as ChakraLink,
  IconButton,
  Code,
  Tooltip,
  useDisclosure,
} from '@chakra-ui/react';
import { AiFillEdit } from 'react-icons/ai';
import dayjs from 'dayjs';

import Table from '../../Table';
import OrderEditModal from './EditModal';

const EditButton = ({ uniqueKey, data, statusCodes }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <OrderEditModal
        data={data}
        statusCodes={statusCodes}
        isOpen={isOpen}
        onClose={onClose}
        key={uniqueKey}
      />
      <IconButton icon={<AiFillEdit />} size="sm" onClick={onOpen} />
    </>
  );
};

const OrdersTable = ({ data, statusCodes }) => {
  const columns = useMemo(
    () => [
      {
        key: 'orderCode',
        Header: 'Order ID',
        accessor: 'orderCode',
        width: 105,
        cellAlign: 'center',
        customRender: table => <Code>{table.cell.value}</Code>,
      },
      {
        key: 'orderDate',
        Header: 'Order Date',
        accessor: 'orderDate',
        width: 150,
        customRender: table => (
          <Text>{dayjs(table.cell.value).format('DD/MM/YYYY hh:mm a')}</Text>
        ),
      },
      {
        key: 'episodeTitle',
        Header: 'Episode Title',
        accessor: 'episodeTitle',
        width: 150,
      },
      {
        key: 'statusName',
        Header: 'Status',
        accessor: 'statusName',
        width: 100,
      },
      {
        key: 'maxShipByDate',
        Header: 'Max Ship By Date',
        accessor: 'maxShipByDate',
        width: 120,
        customRender: table => (
          <Text
            color={
              dayjs(table.cell.value, 'YYYY-MM-DD')
                .endOf('day')
                .isSame(dayjs().endOf('day'))
                ? 'froly.500'
                : 'black'
            }
          >
            {dayjs(table.cell.value, 'YYYY-MM-DD').format('DD/MM/YYYY')}
          </Text>
        ),
      },
      {
        key: 'parentName',
        Header: 'Parent Name',
        accessor: 'parent.fullName',
        width: 200,
        customRender: table => (
          <Tooltip label={table.row.original.parent.email}>
            <ChakraLink
              href={`mailto:${table.row.original.parent.email}`}
              color="cerulean.500"
            >
              {table.cell.value}
            </ChakraLink>
          </Tooltip>
        ),
      },
      {
        key: 'address',
        Header: 'Address',
        width: 250,
        customRender: table => (
          <>
            <Text>{table.row.original.address[0].line1}</Text>
            {table.row.original.address[0].line1 !== null && (
              <Text>{table.row.original.address[0].line2}</Text>
            )}
            <Text>
              {table.row.original.address[0].postCode}{' '}
              {table.row.original.address[0].city}
            </Text>
            <Text>
              {table.row.original.address[0].state},{' '}
              {table.row.original.address[0].country}
            </Text>
            <Text mt={2}>
              <strong>Phone:</strong> {table.row.original.parent.mobilePhone}
            </Text>
          </>
        ),
      },
      {
        key: 'childName',
        Header: 'Child Name',
        accessor: 'child.fullName',
        width: 200,
        customRender: table => (
          <Text>
            {table.cell.value}
            <br />
            <strong>Username: </strong>
            {table.row.original.child.username}
          </Text>
        ),
      },
      {
        key: 'notes',
        Header: 'Order Notes',
        accessor: 'notes',
        width: 200,
        customRender: table => {
          if (table.cell.value === null) {
            return '-';
          }
          return (
            <Tooltip label={table.cell.value}>
              <Text isTruncated noOfLines={3}>
                {table.cell.value}
              </Text>
            </Tooltip>
          );
        },
      },
      {
        key: 'courier',
        Header: 'Courier',
        accessor: 'courier',
        width: 200,
        cellAlign: 'center',
        customRender: table =>
          table.cell.value && table.cell.value !== null
            ? table.cell.value
            : 'No Courier Assigned',
      },
      {
        key: 'trackingCode',
        Header: 'Tracking Code',
        accessor: 'trackingCode',
        width: 150,
        cellAlign: 'center',
        customRender: table =>
          table.cell.value !== null ? (
            <ChakraLink
              href={table.row.original.trackingUrl}
              target="_blank"
              rel="noopener noreferrer"
            >
              <Code
                color="cerulean.500"
                _hover={{ textDecoration: 'underline' }}
              >
                {table.cell.value}
              </Code>
            </ChakraLink>
          ) : (
            'N/A'
          ),
      },
      {
        key: 'lastUpdateDate',
        Header: 'Last Update',
        accessor: 'lastUpdateDate',
        width: 150,
        customRender: table => (
          <Text>{dayjs(table.cell.value).format('DD/MM/YYYY hh:mm a')}</Text>
        ),
      },
      {
        key: 'lastUpdateBy',
        Header: 'Update By',
        accessor: 'admin.fullName',
        width: 150,
        customRender: table => {
          if (!table.cell.value) {
            return 'System';
          }
          return <Text>{table.cell.value}</Text>;
        },
      },
      {
        key: 'actions',
        Header: 'Actions',
        accessor: 'actions',
        width: 100,
        cellAlign: 'center',
        disableSortBy: true,
        customRender: table => {
          return (
            <EditButton
              uniqueKey={`${Date.now()}-${table.row.original.id}`}
              data={table.row.original}
              statusCodes={statusCodes}
            />
          );
        },
      },
    ],
    [],
  );

  return (
    <>
      <Table columns={columns} data={data} />
    </>
  );
};

export default OrdersTable;
