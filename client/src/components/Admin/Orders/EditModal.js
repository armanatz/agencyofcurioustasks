import React, { useState } from 'react';
import {
  ButtonGroup,
  Button,
  Modal,
  ModalOverlay,
  ModalCloseButton,
  ModalHeader,
  ModalContent,
  ModalFooter,
  ModalBody,
  FormControl,
  FormLabel,
  FormHelperText,
  FormErrorMessage,
  Select,
  Textarea,
  Stack,
  Input,
  HStack,
  Text,
  Divider,
  useToast,
} from '@chakra-ui/react';
import { useQueryClient, useMutation } from 'react-query';
import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';

const OrderEditModal = ({ data, statusCodes, isOpen, onClose }) => {
  const [submitting, setSubmitting] = useState(false);
  const [statusCode, setStatusCode] = useState(
    data.statusCode.toString() || '1',
  );
  const queryClient = useQueryClient();
  const toast = useToast();

  const { handleSubmit, register, errors } = useForm({
    defaultValues: {
      statusCode: data.statusCode,
      courier: data.courier || 'Best Express',
      trackingUrl: data.trackingUrl || 'https://www.best-inc.my/track',
      trackingCode: data.trackingCode,
      notes: data.notes,
    },
    ...(statusCode === '3' && {
      reValidateMode: 'onBlur',
      resolver: joiResolver(
        Joi.object({
          statusCode: Joi.string().optional().allow(''),
          courier: Joi.string().required(),
          trackingUrl: Joi.string().required(),
          trackingCode: Joi.string().required(),
          notes: Joi.string().optional().allow(''),
        }),
      ),
    }),
  });

  const mutation = useMutation(
    values => {
      return fetch(
        `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}/orders/${data.id}`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(values),
        },
      );
    },
    {
      onSuccess: async res => {
        setSubmitting(false);

        if (res.ok) {
          queryClient.invalidateQueries('orders');
          onClose();
          return toast({
            title: 'Order Updated',
            status: 'success',
            duration: 5000,
            isClosable: true,
          });
        }

        const data = await res.json();

        return toast({
          description: data.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        setSubmitting(false);
        return toast({
          title: 'Woops, an error occurred :(',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      },
    },
  );

  const onSubmit = values => {
    const obj = { ...values };

    const keys = Object.keys(values);

    obj.statusCode = parseInt(obj.statusCode, 10);

    if (obj.statusCode !== 2 && obj.statusCode !== 3) {
      delete obj.courier;
      delete obj.trackingUrl;
      delete obj.trackingCode;
    }

    keys.forEach(key => {
      if (
        obj[key] === '' ||
        obj[key] === undefined ||
        obj[key] === null ||
        obj[key] === data[key]
      ) {
        delete obj[key];
      }
    });

    if (Object.entries(obj).length !== 0) {
      setSubmitting(true);
      return mutation.mutate(obj);
    }

    return toast({
      title: 'Nothing was changed',
      status: 'warning',
      duration: 5000,
      isClosable: true,
    });
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose} size="3xl">
      <ModalOverlay />
      <ModalContent mx={['10px', 0]}>
        <form onSubmit={handleSubmit(values => onSubmit(values))}>
          <ModalCloseButton onClick={onClose} />
          <ModalHeader>Edit Order {data.orderCode}</ModalHeader>
          <ModalBody>
            <Stack spacing={6}>
              <FormControl id="statusCode">
                <FormLabel>Order Status</FormLabel>
                <Select
                  name="statusCode"
                  w="40%"
                  ref={register}
                  onChange={e => setStatusCode(e.target.value)}
                >
                  {statusCodes.map(status => (
                    <option key={status.code} value={status.code}>
                      {status.name}
                    </option>
                  ))}
                </Select>
              </FormControl>
              <Divider />
              <Text my={6} fontSize="12px">
                Please note that the shipping related fields below will only be
                updatable if the order status is set to {'"'}Awaiting Shipment
                {'"'} <strong>OR</strong> to {'"'}Shipped{'"'}.
                <br />
                <br />
                Also be aware that if the order status is set to {'"'}Shipped
                {'"'}, an email notification will be sent to the customer with
                the details input below.
              </Text>
              <HStack spacing={6}>
                <FormControl id="courier" isInvalid={errors.courier}>
                  <FormLabel>Courier</FormLabel>
                  <Input name="courier" ref={register} />
                  <FormErrorMessage>
                    Courier cannot be empty if status is set to Shipped
                  </FormErrorMessage>
                </FormControl>
                <FormControl id="trackingCode" isInvalid={errors.trackingCode}>
                  <FormLabel>Tracking Code</FormLabel>
                  <Input name="trackingCode" ref={register} />
                  <FormErrorMessage>
                    Tracking Code cannot be empty if status is set to Shipped
                  </FormErrorMessage>
                </FormControl>
              </HStack>
              <FormControl id="trackingUrl" isInvalid={errors.trackingUrl}>
                <FormLabel>Tracking Link</FormLabel>
                <Input name="trackingUrl" ref={register} />
                <FormErrorMessage>
                  Tracking Link cannot be empty if status is set to Shipped
                </FormErrorMessage>
              </FormControl>
              <Divider />
              <FormControl id="notes">
                <FormLabel>Notes</FormLabel>
                <Textarea rows="4" name="notes" ref={register} />
                <FormHelperText>
                  Notes are for internal use only and will not be shown to
                  customers at all
                </FormHelperText>
              </FormControl>
            </Stack>
          </ModalBody>
          <ModalFooter>
            <ButtonGroup>
              <Button variant="ghost" onClick={onClose}>
                Cancel
              </Button>
              <Button type="submit" isLoading={submitting}>
                Update
              </Button>
            </ButtonGroup>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};

export default OrderEditModal;
