import React from 'react';
import {
  Box,
  SimpleGrid,
  HStack,
  Text,
  FormControl,
  FormLabel,
  Input,
  Select,
  ButtonGroup,
  Button,
} from '@chakra-ui/react';
import { useQueryClient } from 'react-query';
import { useForm, Controller } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import Joi from 'joi';
import DatePicker from 'react-datepicker';
import dayjs from 'dayjs';

import 'react-datepicker/dist/react-datepicker.css';

const OrdersFilter = ({ submitting, onSubmit, onReset }) => {
  const queryClient = useQueryClient();

  const { handleSubmit, errors, register, control, reset } = useForm({
    reValidateMode: 'onBlur',
    defaultValues: {
      status: 'all',
      orderDateStart: dayjs().subtract(2, 'weeks').toDate(),
      orderDateEnd: dayjs().toDate(),
      maxShipByDateStart: dayjs().subtract(4, 'days').toDate(),
      maxShipByDateEnd: dayjs().add(2, 'weeks').toDate(),
    },
    resolver: joiResolver(
      Joi.object({
        orderId: Joi.string().optional().allow(''),
        email: Joi.string().email({ tlds: false }).optional().allow(''),
        username: Joi.string().optional().allow(''),
        status: Joi.any().optional().allow(''),
        orderDateStart: Joi.date().optional().allow(''),
        orderDateEnd: Joi.date().optional().allow(''),
        maxShipByDateStart: Joi.date().optional().allow(''),
        maxShipByDateEnd: Joi.date().optional().allow(''),
      }),
    ),
  });

  const statusCodes = queryClient.getQueryData('statusCodes');

  return (
    <Box my={6}>
      <form
        onSubmit={handleSubmit(values => {
          const data = { ...values };

          delete data.orderDateStart;
          delete data.orderDateEnd;
          delete data.maxShipByDateStart;
          delete data.maxShipByDateEnd;

          Object.keys(values).forEach(key => {
            if (values[key] === '') {
              delete data[key];
            }

            if (key === 'status' && values[key] === 'all') {
              delete data.status;
            }
          });

          return onSubmit({
            ...data,
            orderDate: [
              dayjs(values.orderDateStart).format('YYYY-MM-DD'),
              dayjs(values.orderDateEnd).format('YYYY-MM-DD'),
            ],
            maxShipByDate: [
              dayjs(values.maxShipByDateStart).format('YYYY-MM-DD'),
              dayjs(values.maxShipByDateEnd).format('YYYY-MM-DD'),
            ],
          });
        })}
      >
        <SimpleGrid
          gap={6}
          templateColumns={{ base: '1fr', lg: 'repeat(3, 1fr)' }}
        >
          <FormControl id="orderId" isInvalid={errors.orderId}>
            <FormLabel fontSize="sm">Order ID</FormLabel>
            <Input name="orderId" size="sm" ref={register} />
          </FormControl>
          <HStack align="flex-end">
            <FormControl id="orderDateStart" isInvalid={errors.orderDateStart}>
              <FormLabel fontSize="sm">Order Date</FormLabel>
              <Controller
                name="orderDateStart"
                control={control}
                render={({ onChange, value }) => {
                  return (
                    <DatePicker
                      dateFormat="dd/MM/yyyy"
                      popperPlacement="bottom"
                      customInput={<Input size="sm" />}
                      selected={value}
                      openToDate={dayjs().toDate()}
                      maxDate={dayjs().toDate()}
                      onChange={onChange}
                      disabledKeyboardNavigation
                      placeholderText="dd/mm/yyyy"
                    />
                  );
                }}
              />
            </FormControl>
            <Text>to</Text>
            <FormControl id="orderDateEnd" isInvalid={errors.orderDateEnd}>
              <Controller
                name="orderDateEnd"
                control={control}
                render={({ onChange, value }) => {
                  return (
                    <DatePicker
                      dateFormat="dd/MM/yyyy"
                      popperPlacement="bottom"
                      customInput={<Input size="sm" />}
                      selected={value}
                      openToDate={dayjs().toDate()}
                      maxDate={dayjs().toDate()}
                      onChange={onChange}
                      disabledKeyboardNavigation
                      placeholderText="dd/mm/yyyy"
                    />
                  );
                }}
              />
            </FormControl>
          </HStack>
          <FormControl id="status" isInvalid={errors.status}>
            <FormLabel fontSize="sm">Status</FormLabel>
            <Select name="status" size="sm" ref={register}>
              <option value="all">All</option>
              {statusCodes.map(el => (
                <option key={el.code} value={el.code}>
                  {el.name}
                </option>
              ))}
            </Select>
          </FormControl>
          <HStack align="flex-end">
            <FormControl
              id="maxShipByDateStart"
              isInvalid={errors.maxShipByDateStart}
            >
              <FormLabel fontSize="sm">Max Ship By Date</FormLabel>
              <Controller
                name="maxShipByDateStart"
                control={control}
                render={({ onChange, value }) => {
                  return (
                    <DatePicker
                      dateFormat="dd/MM/yyyy"
                      popperPlacement="bottom"
                      customInput={<Input size="sm" />}
                      selected={value}
                      openToDate={dayjs().toDate()}
                      onChange={onChange}
                      disabledKeyboardNavigation
                      placeholderText="dd/mm/yyyy"
                    />
                  );
                }}
              />
            </FormControl>
            <Text>to</Text>
            <FormControl
              id="maxShipByDateEnd"
              isInvalid={errors.maxShipByDateEnd}
            >
              <Controller
                name="maxShipByDateEnd"
                control={control}
                render={({ onChange, value }) => {
                  return (
                    <DatePicker
                      dateFormat="dd/MM/yyyy"
                      popperPlacement="bottom"
                      customInput={<Input size="sm" />}
                      selected={value}
                      openToDate={dayjs().toDate()}
                      onChange={onChange}
                      disabledKeyboardNavigation
                      placeholderText="dd/mm/yyyy"
                    />
                  );
                }}
              />
            </FormControl>
          </HStack>
          <FormControl id="email" isInvalid={errors.email}>
            <FormLabel fontSize="sm">Parent Email</FormLabel>
            <Input name="email" size="sm" ref={register} />
          </FormControl>
          <FormControl id="username" isInvalid={errors.username}>
            <FormLabel fontSize="sm">Child Username</FormLabel>
            <Input name="username" size="sm" ref={register} />
          </FormControl>
        </SimpleGrid>
        <ButtonGroup mt={6}>
          <Button
            type="reset"
            variant="ghost"
            size="sm"
            onClick={() => {
              onReset();
              return reset({
                status: 'all',
                orderDateStart: dayjs().subtract(2, 'weeks').toDate(),
                orderDateEnd: dayjs().toDate(),
                maxShipByDateStart: dayjs().subtract(4, 'days').toDate(),
                maxShipByDateEnd: dayjs().add(2, 'weeks').toDate(),
              });
            }}
          >
            Reset
          </Button>
          <Button type="submit" size="sm" isLoading={submitting}>
            Apply
          </Button>
        </ButtonGroup>
      </form>
    </Box>
  );
};

export default OrdersFilter;
